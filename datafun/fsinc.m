function s=fsinc(x)
% s=FSINC(x)
%   Returns an array, y, whose elements are the sinc of the elements 
%   of the input, x. y=sin(x)/x is the same size as x.
%   FSINC(pi*x) is the same as SINC(x)
%
% Example: y=-pi:0.1:pi; all(fsinc(y*pi)-sinc(y) < 1e-3)

index    = find(x ~= 0 & isfinite(x));
s        = zeros(size(x));
s(index) = sin(x(index)) ./ x(index);

% special case for x=0
s(x == 0) = 1;
