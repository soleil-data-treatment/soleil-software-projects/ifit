function y=constant(varargin)
% CONSTANT Constant model.
%   M = CONSTANT is a constant/background/single value model
%     y=p(1);
%   Parameters are: 
%     p = [ Constant ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = CONSTANT(cte)     creates a model with specified constant value.
%
%   M = CONSTANT('name')  is a constant model with given parameter name.
%
%   Y = CONSTANT(p, X)    creates the model and returns its value on axis X.
%
% Example: m=constant('Temperature'); isnumeric(feval(m))
% Example: m=constant(5); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, quadline, plane2d, strline

if length(varargin)==1 && ischar(varargin{1}) ...
        && ~any(strcmp(varargin{1}, {'guess','identify'})) && ~isempty(varargin{1})
  y.Name      = varargin{1};
  y.Parameters= { y.Name };
  varargin(1) = [];
else
  y.Name      = [ 'Constant (0D) [' mfilename ']' ];
  y.Parameters= {'Constant'};
end

y.Guess     = [0];
y.Description='Constant/User value';
y.Expression= 'p(1)';
y.Dimension  = 1;  

y = iFunc(y);

if length(varargin) && isnumeric(varargin{1})
  y.ParameterValues = varargin{1};
end

