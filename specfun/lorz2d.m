function signal=lorz2d(varargin)
% LORZ2D 2D Lorenztian function.
%   M = LORZ2D creates a 2D Lorenztian function (fit 2D function/model)
%     x0=p(2); y0=p(3); sx=p(4); sy=p(5); theta=p(6) [given in deg]
%     a = cos(theta)^2/2/sx/sx + sin(theta)^2/2/sy/sy;
%     b =-sin(2*theta)/4/sx/sx + sin(2*theta)/4/sy/sy;
%     c = sin(theta)^2/2/sx/sx + cos(theta)^2/2/sy/sy;
%     signal = p(1) ./ (1+ (a*(x-x0).^2+2*b*(x-x0).*(y-y0)+c*(y-y0).^2) ) + p(7);
%   This expression assumes that the Amplitudes are independent from the Widths.
%   Parameters are: 
%     p(1)=Amplitude
%     p(2)=Centre_X
%     p(3)=Center_Y
%     p(4)=HalfWidth_X
%     p(5)=HalfWidth_Y
%     p(6)=Angle       (in deg)
%     p(7)=Background
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = LORZ2D([w1 w2])        creates a model with specified widths.
%
%   M = LORZ2D([ parameters ]) creates a model with specified model parameters.
%
%   Z = LORZ2D(p, X,Y)         creates the model and returns its value on axes X,Y.
%
% Reference: http://en.wikipedia.org/wiki/Lorentzian_function
%
% Example: m=lorz2d([1 2 .5 .2 .3 30 .2]); ndims(feval(m)) == 2
% Example: m=lorz2d([1 2 .5 .2 .3 30 .2]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, lorz, gauss2d, gauss

signal.Name           = [ 'Lorenztian-2D function with tilt angle (2D) [' mfilename ']' ];
signal.Description    = '2D Lorenztian function with tilt angle. http://en.wikipedia.org/wiki/Gaussian_function';
signal.Parameters     = {  'Amplitude' 'Centre_X' 'Center_Y' 'HalfWidth_X' 'HalfWidth_Y' 'Angle tilt [deg]' 'Background' };
signal.Dimension      = 2;         % dimensionality of input space (axes) and result

% moments of distributions
m1 = @(x,s) sum(s(:).*x(:))/sum(s(:));
m2 = @(x,s) sqrt(abs( sum(x(:).*x(:).*s(:))/sum(s(:)) - m1(x,s).^2 ));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
signal.Guess     = @(x,y,signal) iif(...
  ~isempty(signal)&&numel(signal)==numel(x), @() [ max(signal(:))-min(signal(:)) m1(x,signal) m1(y, signal) m2(x,signal) m2(y,signal) 20*randn min(signal(:)) ], ...
  true            , @() [1 2 .5 .2 .3 30 .2]);
  
signal.Expression     = {'x0=p(2); y0=p(3); sx=p(4); sy=p(5);', ...
  'theta = p(6)*pi/180;', ...
  'aa = cos(theta)^2/2/sx/sx + sin(theta)^2/2/sy/sy;', ...
  'bb =-sin(2*theta)/4/sx/sx + sin(2*theta)/4/sy/sy;', ...
  'cc = sin(theta)^2/2/sx/sx + cos(theta)^2/2/sy/sy;', ...
  'signal = (aa*(x-x0).^2+2*bb*(x-x0).*(y-y0)+cc*(y-y0).^2);', ...
  'signal = p(1) ./ (1+ signal ) + p(7);' };

signal=iFunc(signal);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 0 varargin{1} varargin{1} 20*randn 0]};
  elseif length(varargin{1}) == 2
    varargin = {[ 1 0 0 varargin{:} 20*randn 0]};
  end
  signal.ParameterValues = varargin{1};
elseif nargin > 1
  signal = signal(varargin{:});
end
