function y=pseudovoigt(varargin)
% PSEUDOVOIGT Pseudo Voigt model.
%   M = PSEUDOVOIGT creates a Pseudo Voigt fitting function which is an
%   approximation of the convolution of a Gaussian and a Lorentzian.
%       y = a * (d * (1/(1+((x-b)/c)^2)) + (1-d) * exp(-0.5 * ((x-b)/c)^2))
%   Parameters are: 
%     p = [ Amplitude Centre HalfWidth BackGround LorentzianRatio ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = PSEUDOVOIGT(width)          creates a model with a specified width.
%
%   M = PSEUDOVOIGT([ parameters ]) creates a model with specified model parameters.
%
%   Y = PSEUDOVOIGT(p, X)           creates the model and returns its value on axis X.
%
% Reference: http://en.wikipedia.org/wiki/Voigt_profile
%            P. Thompson, D.E. Cox, J.B. Hastings, J. Appl. Cryst. 1987, 20, 79.
%
% Example: m=pseudovoigt([1 0 1 1 0.5]); isnumeric(feval(m))
% Example: m=pseudovoigt(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, pseudovoigt2d, lorz, gauss, voigt

y.Name      = [ 'Pseudo-Voigt (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Centre','HalfWidth','Background','LorentzianRatio'};
y.Description='Single 1D Pseudo Voigt model (convolution of gauss and lorz approx.). Ref: P. Thompson, D.E. Cox, J.B. Hastings, J. Appl. Cryst. 1987, 20, 79.';
y.Expression= @(p,x) p(1) * (p(5) * (1./(1+ ((x-p(2))/p(3)).^2 )) + (1-p(5)) * exp(-0.5 * (2.355*(x-p(2))/p(3)).^2 ));

m1 = @(x,s) sum(s(:).*x(:))/sum(s(:));
m2 = @(x,s) sqrt(abs( sum(x(:).*x(:).*s(:))/sum(s(:)) - m1(x,s).^2 ));

y.Guess     = @(x,s) [ NaN m1(x, s-min(s(:))) m2(x, s-min(s(:))) NaN 0.5 ];

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,s) iif(...
  ~isempty(s)&&numel(s)==numel(x), @() [ NaN m1(x, s-min(s(:))) m2(x, s-min(s(:))) NaN 0.5 ], ...
  true            , @() [1 1 1 0 0.5]);
  
y.Dimension =1;

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 varargin{1} 0 0.5]};
  end
  y.ParameterValues = varargin{1};
elseif length(varargin) > 1
  y = y(varargin{:});
end

