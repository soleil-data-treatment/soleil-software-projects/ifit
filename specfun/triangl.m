function y=triangl(varargin)
% TRIANGL Triangular model.
%   M = TRIANGL creates a Triangular fitting function
%     y=(p(3)-sign(x-p(2)).*(x-p(2)))/p(3)^2;
%   and y is the background outside the full width.
%   Parameters are: 
%     p = [ Amplitude Centre HalfWidth BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = TRIANGL(width)          creates a model with a specified width.
%
%   M = TRIANGL([ parameters ]) creates a model with specified model parameters.
%
%   Y = TRIANGL(p, X)           creates the model and returns its value on axis X.
%
% Example: m=triangl([1 0 1 1]); isnumeric(feval(m))
% Example: m=triangl(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, heavisde, tophat

y.Name      = [ 'Triangular function (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Centre','HalfWidth','Background'};
y.Description='Triangular function';
% MZ <mzinkin@sghms.ac.uk>
y.Expression= @(p,x) ((p(3)-sign(x-p(2)).*(x-p(2)))/p(3)^2)*p(1).*(abs(x-p(2)) < p(3))+p(4);
y.Dimension      = 1;

m1 = @(x,s) sum(s(:).*x(:))/sum(s(:));
m2 = @(x,s) sqrt(abs( sum(x(:).*x(:).*s(:))/sum(s(:)) - m1(x,s).^2 ));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,s) iif(...
  ~isempty(s)&&numel(s)==numel(x), @() [ NaN m1(x, s-min(s(:))) m2(x, s-min(s(:))) NaN ], ...
  true            , @() [1 0 1 0]);
                            
y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

