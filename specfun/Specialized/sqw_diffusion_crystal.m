function signal=sqw_diffusion_crystal(varargin)
% SQW_DIFFUSION_CRYSTAL Chudley-Elliot Jump Diffusion on a crystal lattice Sqw2D
%   SQW = SQW_DIFFUSION_CRYSTAL creates a 2D S(q,w) with a jump diffusion dispersion
%     based on the Chudley-Elliot model. 
%   The liquid is assumed to have appreciable short range order in a quasi-
%     crystalline form. Diffusive motion takes place in large discrete jumps, 
%     between which the atoms oscillate as in a solid. 
%   It is suited for quasi-elastic neutron scattering data (QENS).
%   This is a classical pure incoherent Lorentzian scattering law (no structure).
%
%   Model Parameters:
%             p(1)= Amplitude 
%             p(2)= l0        Jump distance, e.g. 0.1-5 Angs [Angs]
%             p(3)= t0        Residence time step between jumps, e.g. 1-4 ps [s]
%   The 1st axis Q is the wavevector/momentum [Angs-1] and the 2nd is energy E [meV].
%   The model SQW is returned, and can be evaluated with e.g. s=SQW(p, Q,E)
%
%   SQW = SQW_DIFFUSION_CRYSTAL([ l0 t0 ]) builds model for a given distance and
%   residence time. You can of course tune other parameters once the model 
%   object has been created.
%
%   SQW = SQW_DIFFUSION_CRYSTAL(p) builds model with given parameters.
%
%   S = SQW_DIFFUSION_CRYSTAL(p, Q,E) creates the model and returns its value for Q,E.
%
%   Theory
%   ------
%   The dispersion has the form: 
%      S(q,w) = f(q)/(w^2+f(q)^2)
%   where
%      f(q)   = 1/t0*(1-sinc(q l0))     width of the Lorentzian
%
%   where we commonly define:
%     l0   = Jump distance, e.g. 0.1-5 Angs [Angs]
%     t0   = Residence time step between jumps, e.g. 1-4 ps [s]
%
%   The jump distance l0 can be related to the diffusion constant D as 
%   l0^2 = 6*D*t0, where D is usually around D=1-10 E-9 [m^2/s] in liquids. Its 
%   value in [meV.Angs^2] is D*4.1356e+08. The residence time t0 is usually in 0-4 ps.
%
%   Additional remarks
%   ------------------
%   The model is classical, e.g. symmetric in energy, and does NOT satisfy the
%   detailed balance.
%
%   To get the 'true' quantum S(q,w) definition, use e.g.
%     sqw = Bosify(sqw_diffusion_crystal);
%
%   To add a Debye-Waller factor (thermal motions around the equilibrium), use e.g.
%     sqw = DebyeWaller(sqw);
%
%   Energy conventions:
%   w = omega = Ei-Ef = energy lost by the neutron [meV]
%       omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
%       omega < 0, neutron gains energy, anti-Stokes
%
%   This model is equivalent to the QENS_ChudleyElliot model in LAMP.
%
% Reference: 
%   C T Chudley and R J Elliott 1961 Proc. Phys. Soc. 77 353
% Example: s=sqw_diffusion_crystal; f=iData(s, [], 0:.1:20, -50:50); plot(log10(f)); ...
%   close(gcf); isa(f, 'iData')
% See also iData iFunc sqw_diffusion_bound sqw_diffusion_rotational 
%   sqw_diffusion_jump sqw_diffusion sqw_recoil

signal.Name           = [ 'sqw_diffusion_crystal jump diffusion in a crystal [' mfilename ']' ];
signal.Description    = 'A 2D S(q,w) Chudley-Elliot jump diffusion in a semi-ordered material.';

signal.Parameters     = {  ...
  'Amplitude' ...
  'l0             Jump distance, e.g. 0.1-5 Angs [Angs]' ...
  't0             Residence time step between jumps, e.g. 1-4 ps [s]' ...
   };
  
signal.Dimension      = 2;         % dimensionality of input space (axes) and result
signal.Guess          = [ 1 3 1e-12 ];
signal.UserData.classical     = true;
signal.UserData.DebyeWaller   = false;

% Expression of S(q,w) is found in Egelstaff, Intro to Liquids, Eq (11.13)+(11.16), p 227.
signal.Expression     = { ...
 'q = x; w = y; l0 = p(2); t0 = p(3)*241.8E9; % in meV-1' ...
 'fq=0;' ...
 'sq=sin(q*l0)./(q*l0); sq(q==0) = 1;' ...
 'if t0>0, fq = abs(1-sq)/t0; end' ...
 'signal = p(1)/pi*fq./(w.^2+fq.^2);' ...
 };

signal= iFunc(signal);
signal= iFunc_Sqw2D(signal); % overload Sqw flavour

if nargin == 1 && isnumeric(varargin{1})
  p = varargin{1};
  if numel(p) == 2
    signal.ParameterValues(2) = p(1);
    signal.ParameterValues(3) = p(2);
  else
    signal.ParameterValues = p;
  end
elseif nargin > 1
  signal = signal(varargin{:});
end
