function signal=sqw_diffusion_bound(varargin)
% SQW_DIFFUSION_BOUND Hall-Ross Bound Jump Diffusion Sqw2D
%   SQW = SQW_DIFFUSION_BOUND builds a 2D S(q,w) with a bound jump diffusion dispersion
%     based on the Hall-Ross model (within a restricted volume/cage).
%   It is suited for quasi-elastic neutron scattering data (QENS).
%   This is a classical pure incoherent Lorentzian scattering law (no structure).
%
%   Model parameters
%             p(1)= Amplitude 
%             p(2)= l0        Jump distance, e.g. 0.1-5 Angs [Angs]
%             p(3)= t0        Residence time step between jumps, e.g. 1-4 ps [s]
%   The 1st axis Q is the wavevector/momentum [Angs-1] and the 2nd is energy [meV].
%   The model SQW is returned, and can be evaluated with e.g. s=SQW(p, Q,E)
%
%   SQW = SQW_DIFFUSION_BOUND([ l0 t0 ]) builds a bound jump diffusion model 
%   for a given distance and residence time.
%
%   You may of course tune other parameters once the model object has been created.
%
%   S = SQW_DIFFUSION_BOUND(p, Q,E) creates the model and returns its value for Q,E.
%
%   Theory
%   ------
%   The dispersion has the form:
%      S(q,w) = f(q)/(w^2+f(q)^2)
%   where
%      f(q)   = 1/t0*(1-exp(Q^2 l0))     width of the Lorentzian
%
%   where we commonly define:
%     l0   = Jump distance, e.g. 0.1-5 Angs [Angs]
%     t0   = Residence time step between jumps, e.g. 1-4 ps [s]
%
%   The jump distance l0 can be related to the diffusion constant D as 
%   l0^2 = 6*D*t0, where D is usually around D=1-10 E-9 [m^2/s] in liquids. Its 
%   value in [meV.Angs^2] is D*4.1356e+08. The residence time t0 is usually in 0-4 ps.
%
%   Additional remarks
%   ------------------
%   The model is classical, e.g. symmetric in energy, and does NOT satisfy the
%   detailed balance.
%
%   To get the 'true' quantum S(q,w) definition, use e.g.
%     sqw = Bosify(sqw_diffusion_bound);
%
%   To add a Debye-Waller factor (thermal motions around the equilibrium), use e.g.
%     sqw = DebyeWaller(sqw);
%
%   Energy conventions:
%   w = omega = Ei-Ef = energy lost by the neutron [meV]
%       omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
%       omega < 0, neutron gains energy, anti-Stokes
%
%   This model is equivalent to the QENS_ChudleyElliot model in LAMP.
%
% Reference: P L Hall & D K Ross Mol Phys 36 1549 (1978)
% Example: s=sqw_diffusion_bound; f=iData(s, [], 0:.1:20, -50:50); h=plot(log10(f)); ...
%   close(gcf); isa(f, 'iData')
% See also iData, iFunc, sqw_recoil, sqw_diffusion

signal.Name           = [ 'sqw_diffusion_bound jump diffusion in a crystal [' mfilename ']' ];
signal.Description    = 'A 2D S(q,w) Hall-Ross bound jump diffusion.';

signal.Parameters     = {  ...
  'Amplitude' ...
  'l0             Jump distance, e.g. 0.1-5 Angs [Angs]' ...
  't0             Residence time step between jumps, e.g. 1-4 ps [s]' ...
   };
  
signal.Dimension      = 2;         % dimensionality of input space (axes) and result
signal.Guess          = [ 1 3 1e-12 ];
signal.UserData.classical     = true;
signal.UserData.DebyeWaller   = false;

% Expression of S(q,w) is found in Egelstaff, Intro to Liquids, Eq (11.13)+(11.16), p 227.
signal.Expression     = { ...
 'q = x; w = y; l0 = p(2); t0 = p(3)*241.8E9; % in meV-1' ...
 'fq=0;' ...
 'if t0>0, fq = (1-exp(l0*q.^2))/t0; fq(fq<0)=0; end' ...
 'signal = p(1)/pi*fq./(w.^2+fq.^2);' ...
 };

signal= iFunc(signal);
signal= iFunc_Sqw2D(signal); % overload Sqw flavour

if nargin == 1 && isnumeric(varargin{1})
  p = varargin{1};
  if numel(p) >= 1, signal.ParameterValues(2) = p(1); end
  if numel(p) >= 2, signal.ParameterValues(3) = p(2); end
elseif nargin > 1
  signal = signal(varargin{:});
end
