function y=ff_core_shell(varargin)
% FF_CORE_SHELL Spherical/core shell form-factor P(q) [Guinier] 
%   F = FF_CORE_SHELL creates a concentric spherical form-factor scattering P(q)
%   model, with 2 shells. The Q wave-vector/momentum axis is usually in nm-1 
%   or Angs-1. The parameters R1,R2 are given in inverse unit of the axis 
%   (i.e nm or Angs). Typical values for parameters are R1,R2=10-100 Angs, 
%   eta1,eta2=1e-6. When eta2=0, the model is equivalent to FF_SPHERE.
%
%   Model parameters:
%     p(1)=R1 shell (outer) sphere radius [1/x]', ...
%     p(2)=R2 core  (inner) sphere radius [1/x]', ...
%     p(3)=eta1 scattering length density difference between shell and matrix [x^2]
%     p(4)=eta2 scattering length density difference between shell and core [x^2]
%   The model F is returned, and can be evaluated with e.g. Fq=F(p, q)
%
%   F = FF_CORE_SHELL([ parameters ]) creates a model with specified model parameters.
%
%   Fq = FF_CORE_SHELL(p, Q) creates the model and returns its value on axis Q.
%
% Ref: Guinier, A. and G. Fournet, "Small-Angle Scattering of X-Rays", 
%            John Wiley and Sons, New York, (1955).
%   Extracted from sasfit/sasfit_ff/sasfit_ff_spherical_shell.c
%   I. Bressler, et al, Journal of Applied Crystallography, 2015, 48 (5), 1587-1598
% Example: m=ff_core_shell([14 11 0.1 0.1]); isvector(feval(m,'', 0:0.01:1))
% Example: m=ff_core_shell([14 11 0.1 0.1]); h=plot(m,'', 0:0.01:1); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc, iFunc/fits, iFunc/plot, ff_sphere

y.Name      = [ 'Spherical/core shell P(q) (1D) [' mfilename ']' ];
y.Description='Concentric spherical geometry, with 2 shells form factor [Guinier]';
y.Parameters={'R1 shell (outer) sphere radius [1/x]', ...
              'R2 core (inner) sphere radius [1/x]', ...
              'eta1 scattering length density difference between shell and matrix [x^2]', ...
              'eta2 scattering length density difference between shell and core [x^2]'};
y.Expression= @(p, x) ( sqrt(ff_sphere(p([1 3]), x )) - sqrt(ff_sphere(p([2 4]), x )) ).^2;
y.Dimension = 1;
y.Guess     = @(x,signal) [ pi/std(x(:)) 1e-6 pi/std(x(:))/2 1e-6 ];
y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

