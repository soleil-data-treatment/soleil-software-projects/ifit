function signal=sab_recoil(varargin)
% SAB_RECOIL Free-gas/recoil dispersion(Q) Sab
%
%   SAB = SAB_RECOIL builds a 2D S(alpha,beta) with a free-gas/recoil dispersion.
%   This is a 2D free-gas model appropriate to model the scattering law of a gas
%     molecule, as well as the translational component of a liquid. 
%     This is a pure incoherent scattering law (no structure).
%     This model is equivalent to the NJOY/LEAPR "free-gas translation".
%
%   Model parameters:
%       p(1)=wt             Translational weight; wt=Mdiff/M [e.g. 1]
%       p(2)=Amplitude
%   The axes are 'alpha' and 'beta', unit-less momentum and energy resp.
%   The model SAB is returned, and can be evaluated with e.g. s=SAB(p, alpha,beta)
%
%   The input parameter 'wt' is the translational weight, which quantifies the 
%   fraction of the scattering originating from recoil/diffusion, e.g. wt=Mdiff/M
%   A pure recoil is obtained with wt=1. The model satisfies the detailed balance.
%
%   SAB = SAB_RECOIL(wt) builds a free-gas model for a given translational weight.
%
%   S = SAB_RECOIL(p, alpha, beta) creates the model and returns its value on axis alpha,beta.
%
%   Theory
%   ------
%   The dispersion has the form (Egelstaff book Eq 10.14):
%      S(alpha,beta) = 1/sqrt(4*pi*wt*alpha)*exp( (wt*alpha + beta)^2/4/wt/a)
%
%   where we commonly define (h stands for hbar):
%     alpha=  h2q2/2MkT = (Ei+Ef-2*mu*sqrt(Ei*Ef))/AkT   unit-less moment
%     beta = -hw/kT     = (Ef-Ei)/kT                     unit-less energy
%     A    = M/m                
%     mu   = cos(theta) = (Ki.^2 + Kf.^2 - q.^2) ./ (2*Ki.*Kf)
%     m    = mass of the neutron
%     M    = mass of the scattering target [g/mol]
%     wt   = translational weight, wt=Mdiff/M e.g. in [0-1]
%
%   Conventions:
%   w = omega = Ei-Ef = energy lost by the neutron [meV]
%       omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
%       omega < 0, neutron gains energy, anti-Stokes
%
% Reference: 
%   P.A.Egelstaff, An introduction to the liquid state, 2nd ed., Oxford (2002)
%   M.Mattes and J.Keinert, IAEA INDC (NDS)-0470 (2005) https://www-nds.iaea.org/publications/indc/indc-nds-0470/
%   R.E.McFarlane, LA-12639-MS (ENDF 356) (March 1994) https://t2.lanl.gov/nis/publications/thermal.pdf
% Example: s=sab_recoil; z=s('', 0:.1:20, -50:50); ndims(z) ==2
% Example: s=sab_recoil; h=plot(log10(iData(s, [], 0:.1:20, -50:50))); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc, sab_diffusion, sqw_recoil, sqw_diffusion

signal.Name           = [ 'sab_recoil free-gas/recoil dispersion [' mfilename ']' ];
signal.Description    = 'A 2D S(alpha,beta) recoil/free-gas/translational dispersion.';

signal.Parameters     = {  ...
  'wt             Translational weight; wt=Mdiff/M [1]' ...
  'Amplitude' };
  
signal.Dimension      = 2;         % dimensionality of input space (axes) and result
signal.Guess          = [ 1 1 ];
signal.UserData.classical     = false;
signal.UserData.DebyeWaller   = false;

% Egelstaff Eq 10.14
signal.Expression     = { ...
 'a = x; b = y; wt = p(1);' ...
 'signal  = p(2)./sqrt(4*pi*wt*a).*exp(-(wt*a + b).^2/4/wt./a);' ...
 };

signal= iFunc(signal);
signal= iFunc_Sab(signal); % overload Sab flavour

if nargin == 1 && isnumeric(varargin{1})
  p = varargin{1};
  if numel(p) >= 1, signal.ParameterValues(1) = p(1); end
elseif nargin > 1
  signal = signal(varargin{:});
end
end
