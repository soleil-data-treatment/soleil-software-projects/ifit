function y=ff_sphere(varargin)
% FF_SPHERE Sphere form-factor [Guinier]
%   F = FF_SPHERE creates a monodisperse spherical particle form factor P(q) 
%   with uniform scattering length density. 
%   The Q wave-vector/momentum axis is usually in nm-1 or Angs-1.
%   The parameter R is given in inverse unit of the axis (that is nm or Angs).
%   Typical values for parameters are R=10-100 Angs, eta=1e-6.
%   The value at q=0 is (4/3*pi*eta*R^3)^2
%
%   Model parameters:
%     p(1)=R sphere radius [1/x]
%     p(2)=eta scattering length density difference between particle and matrix [x^2]
%   The model F is returned, and can be evaluated with e.g. Fq=F(p, q)
%
%   F = FF_SPHERE([ parameters ]) creates a model with specified model parameters.
%
%   Fq = FF_SPHERE(p, Q) creates the model and returns its value on axis Q.
%
% Ref: Guinier, A. and G. Fournet, "Small-Angle Scattering of X-Rays", 
%          John Wiley and Sons, New York, (1955).
%   Extracted from sasfit/sasfit_ff/sasfit_ff_sphere.c
%   I. Bressler, et al, Journal of Applied Crystallography, 2015, 48 (5), 1587-1598
% Example: m=ff_sphere([10 2e-6]); isvector(feval(m,'',0:0.01:1))
% Example: m=ff_sphere([14 11 0.1 0.1]); h=plot(m,'', 0:0.01:1); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc, iFunc/plot, ff_core_shell

y.Name      = [ 'Sphere P(q) (1D) [' mfilename ']' ];
y.Description='Sphere form factor [Guinier]';
y.Parameters={'R sphere radius [1/x]', ...
              'eta scattering length density difference between particle and matrix [x^2]'};
y.Expression= @(p,x) ( (p(2)*4.0*pi*(sin(p(1)*x) - p(1)*x.*cos(p(1)*x))./(x+(x == 0)).^3).*(x ~= 0) ...
  + (x == 0).*(p(2)*4/3*pi*p(1)^3) ).^2;
  
y.Guess     = @(x,signal) [ pi/std(x(:)) 1e-6 ];
y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

