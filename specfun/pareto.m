function y=pareto(varargin)
% PARETO Pareto distribution function. 
%   M = PARETO creates a Pareto distribution function. 
%     y  = p(4)+ p(1)*(p(3)./x).^p(2);
%   Parameters are: 
%     p = [ Amplitude Exponent Width BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = PARETO(width)          creates a model with a specified width.
%
%   M = PARETO([ parameters ]) creates a model with specified model parameters.
%
%   Y = PARETO(p, X)           creates the model and returns its value on axis X.
%
% Reference: http://en.wikipedia.org/wiki/Pareto_distribution
%
% Example: m=pareto([1 0.2 1 1]); isnumeric(feval(m))
% Example: m=pareto(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, powerlaw

y.Name      = [ 'Pareto distribution distribution function (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Exponent','Width','Background'};
y.Description='Pareto distribution distribution function. http://en.wikipedia.org/wiki/Pareto_distribution';
y.Expression= @(p,x) p(4)+ p(1)*(p(3)./abs(x)).^p(2);

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ (max(y(:))-min(y(:)))/2 mean(abs(x(:))) std(x(:)) min(y(:)) ], ...
  true            , @() [1 0.2 1 1]);
y.Dimension =1;

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 1 varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif length(varargin) > 1
  y = y(varargin{:});
end


