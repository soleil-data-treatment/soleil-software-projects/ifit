function c=least_absolute(Signal, Error, Model)
% LEAST_ABSOLUTE Weighted least absolute criteria.
%   X = LEAST_ABSOLUTE(Signal, Error, Model) returns the weighted least absolute 
%   criteria X as a vector. Most optimizers use its sum (except LM).
%     X = |Signal-Model|/Error
%   A good fit corresponds with a criteria lower or equal to numel(Signal).
%
% Ref: <http://en.wikipedia.org/wiki/Least_absolute_deviation>
% Example: s=-10:10; e=sqrt(abs(s)); m=1; sum(least_absolute(s,e,m))

  if ~isnumeric(Signal) || ~isnumeric(Model), return; end
  if isempty(Error) || isscalar(Error) || all(Error == Error(end))
    index = find(isfinite(Model) & isfinite(Signal));
    c = abs(Signal(index)-Model(index)); % raw least absolute
  else
    index = find(isfinite(Error) & isfinite(Model) & isfinite(Signal));
    residuals  = Signal - Model;
    % make sure weight=1/sigma does not reach unrealistic values
    %   initially, most weights will be equal, but when fit impproves, 
    %   stdE will get lower, allowing better matching of initial weight.
    normE = sum(Error(index));
    stdE  = std(residuals(index));
    Error( Error < stdE/4 ) = stdE/4; 
    Error = Error *(normE/sum(Error(index)));
    
    if isempty(index), c=Inf;
    else               c=abs((residuals(index))./Error(index));
    end
  end
end % least_absolute
