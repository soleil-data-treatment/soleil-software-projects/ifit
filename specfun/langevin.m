function y=langevin(varargin)
% LANGEVIN Langevin function for magnetic polarization.
%   M = LANGEVIN creates a Langevin function for magnetic polarization
%     x = (x-p(2))/p(3); y  = p(4)+ p(1) .* ( coth(x) - 1./x );
%   Parameters are: 
%     p = [ Amplitude Center Width BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = LANGEVIN(centre)         creates a model with a specified centre/threshold.
%
%   M = LANGEVIN([ parameters ]) creates a model with specified model parameters.
%
%   Y = LANGEVIN(p, X)           creates the model and returns its value on axis X. 
%
% Ref: https://en.wikipedia.org/wiki/Brillouin_and_Langevin_functions
% Example: m=langevin([1 0 1 0]); isnumeric(feval(m))
% Example: m=langevin(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, doseresp, sigmoid

y.Name      = [ 'Langevin function for magnetic polarization (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Centre','Width','Background'};
y.Description='Langevin function for magnetic polarization';
y.Expression= @(p,x) p(4)+ p(1) .* ( coth((x-p(2))/p(3)) - p(3)./(x-p(2)) );
y.Dimension  = 1;

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @()  [ (max(y(:))-min(y(:)))/2 mean(x(:)) std(x(:)) min(y(:)) ], ...
  true            , @() [1 0 1 0]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} varargin{1}/4 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

