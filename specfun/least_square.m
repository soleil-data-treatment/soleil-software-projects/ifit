function c=least_square(Signal, Error, Model)
% LEAST_SQUARE Weighted least square criteria
%   X = LEAST_SQUARE(Signal, Error, Model) returns the weighted least square 
%   criteria, which is also the Chi square. The return value is a vector, 
%   and most optimizers use its sum (except LM).
%     X = (|Signal-Model|/Error).^2
%   A good fit corresponds with a criteria lower or equal to numel(Signal).
%
% Ref: <http://en.wikipedia.org/wiki/Least_squares>
% Example: s=-10:10; e=sqrt(abs(s)); m=1; sum(least_square(s,e,m))

  c = least_absolute(Signal, Error, Model);
  c = c.*c;
end % least_square
