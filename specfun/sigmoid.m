function y=sigmoid(varargin)
% SIGMOID Sigmoidal model.
%  M = SIGMOID creates a Sigmoidal S-shaped model
%     y  = A0+(A1-A0)./(1+exp(-(x-x0)/w))
%   This is a sigmoid S-shaped curve, aka logistic.
%   Parameters are: 
%     p = [ Amplitude Center Width BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = SIGMOID(threshold)      creates a model with specified threshold.
%
%   M = SIGMOID([ parameters ]) creates a model with specified model parameters.
%
%   Y = SIGMOID(p, X)           creates the model and returns its value on axis X.
%
% Ref: http://en.wikipedia.org/wiki/Sigmoid_function
%
% Example: m=sigmoid([1 0 1 1]); isnumeric(feval(m))
% Example: m=sigmoid(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, doseresp

y.Name           = [ 'Sigmoidal curve (1D) [' mfilename ']' ];
y.Parameters     = {'Amplitude','Center','Slope','Background'};
y.Description    = 'Sigmoidal curve. Ref: http://en.wikipedia.org/wiki/Sigmoid_function';
y.Expression     = @(p,x) p(4)+p(1)./(1+exp(-(x-p(2))/p(3)));
y.Dimension      = 1;

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ max(y(:))-min(y(:)) mean(x(:)) std(x(:))/3 min(y(:)) ], ...
  true            , @() [1 0 1 0]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} varargin{1}/4 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

