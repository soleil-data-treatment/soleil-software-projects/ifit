function signal=pseudovoigt2d(varargin)
% PSEUDOVOIGT2D 2D Pseudo Voigt function.
%   M = PSEUDOVOIGT2D creates a 2D Pseudo Voigt function (fit 2D function/model)
%     x0=p(2); y0=p(3); sx=p(4); sy=p(5); theta=p(6) [given in deg]
%     a = cos(theta)^2/2/sx/sx + sin(theta)^2/2/sy/sy;
%     b =-sin(2*theta)/4/sx/sx + sin(2*theta)/4/sy/sy;
%     c = sin(theta)^2/2/sx/sx + cos(theta)^2/2/sy/sy;
%     signal = (a*(x-x0).^2+2*b*(x-x0).*(y-y0)+c*(y-y0).^2);
%     signal = p(1) * (p(8) * (1./(1+signal)) + (1-p(8)) * exp(-0.5 * 2.355* signal)) + p(7);
%   Parameters are: 
%     p(1)=Amplitude
%     p(2)=Centre_X
%     p(3)=Center_Y
%     p(4)=HalfWidth_X
%     p(5)=HalfWidth_Y
%     p(6)=Angle        (in deg)
%     p(7)=Background
%     p(8)=LorentzianRatio
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = PSEUDOVOIGT2D([w1 w2])        creates a model with specified widths.
%
%   M = PSEUDOVOIGT2D([ parameters ]) creates a model with specified model parameters.
%
%   Z = PSEUDOVOIGT2D(p, X,Y)         creates the model and returns its value on axes X,Y.
%
% Reference: http://en.wikipedia.org/wiki/Voigt_profile
%            P. Thompson, D.E. Cox, J.B. Hastings, J. Appl. Cryst. 1987, 20, 79.
%
% Example: m=pseudovoigt2d([1 2 .5 .2 .3 30 .2 .5]); ndims(feval(m))==2
% Example: m=pseudovoigt2d([1 2 .5 .2 .3 30 .2 .5]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, lorz, pseudovoigt, gauss, voigt, gauss2d, lorz2d

signal.Name           = [ 'Pseudo-Voigt-2D function with tilt angle (2D) [' mfilename ']' ];
signal.Parameters     = {  'Amplitude' 'Centre_X' 'Center_Y' 'HalfWidth_X' 'HalfWidth_Y' 'Angle tilt in [deg]' 'Background' 'LorentzianRatio' };
signal.Description    = '2D Pseudo Voigt function with tilt angle (convolution of gauss and lorz approx.). Ref: P. Thompson, D.E. Cox, J.B. Hastings, J. Appl. Cryst. 1987, 20, 79.';
signal.Dimension      = 2;         % dimensionality of input space (axes) and result

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
signal.Guess     = @(x,y,signal) iif(...
  ~isempty(signal)&&numel(signal)==numel(x), @() [ max(signal(:))-min(signal(:)) mean(x(:)) mean(y(:)) std(x(:)) std(y(:)) 20*randn min(signal(:)) 0.5 ], ...
  true            , @() [1 2 .5 .2 .3 30 .2 .5]);
  
signal.Expression     = {'x0=p(2); y0=p(3); sx=p(4); sy=p(5);', ...
  'theta = p(6)*pi/180;', ...
  'aa = cos(theta)^2/2/sx/sx + sin(theta)^2/2/sy/sy;', ...
  'bb =-sin(2*theta)/4/sx/sx + sin(2*theta)/4/sy/sy;', ...
  'cc = sin(theta)^2/2/sx/sx + cos(theta)^2/2/sy/sy;', ...
  'signal = (aa*(x-x0).^2+2*bb*(x-x0).*(y-y0)+cc*(y-y0).^2);', ...
  'signal = p(1) * (p(8) * (1./(1+signal)) + (1-p(8)) * exp(-0.5 * 2.355 * signal)) + p(7);' };

signal=iFunc(signal);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 0 varargin{1} varargin{1} 20*randn 0 0.5]};
  elseif length(varargin{1}) == 2
    varargin = {[ 1 0 0 varargin{:} 20*randn 0 0.5]};
  end
  signal.ParameterValues = varargin{1};
elseif nargin > 1
  signal = signal(varargin{:});
end

