function signal=sqw_spinw(varargin)
% SQW_SPINW S(q,w) dispersion(HKL) spin-wave Sqw4D
%   SQW = SQW_SPINW creates a 4D S(q,w) dispersion obtained from the
%   SpinW package from S. Toth. A SpinW object must first be created, and is then
%   converted into an iFunc model for HKL evaluation. The intensity is computed
%   by default for a neutron scattering experiment.
%   The model does not includes the Debye-Waller structure factor, but satisfies  
%     the detailed balance. Pure inelastic coherent 4D model.
%   Without argument, SQW_SPINW produces a square lattice Heisenberg 
%   Antiferromagnet model with S = 1 and J = 1.
%
%   Model parameters:
%             p(1)    = Gamma       energy broadening [meV]
%             p(2)    = Temperature of the material [K]
%             p(3)    = Amplitude
%             p(4...) = coupling parameters of the Hamiltonian
%   The axes are H,K,L momentum in rlu and energy E in meV.
%   The model SQW is returned, and can be evaluated with e.g. s=SQW(p, H,K,L,E)
%
%   SQW = SQW_SPINW(SW) creates a model from the given SpinW object SW.
%
%   SQW = SQW_SPINW('CIF_filename') creates a model from the given CIF file,
%   which defines the structure. A spin-orbit coupling is assumed, and the spins
%   are set from an energy minimization starting from a random configuration.
%
%   SQW = SQW_SPINW(..., OPTIONS) sets additional OPTIONS for the calculation.
%   The OPTIONS should be a structure with:
%     OPTIONS.component: the component to use as intensity, as documented in 
%              <a href="matlab:doc sw_egrid">sw_egrid</a>.
%              Default is 'Sperp'. Suggested is also 'Sxx+Syy+Szz'.
%     OPTIONS.ki: the initial neutron energy in Angs^-1. Default is 1e4 to allow
%              full dynamic range.
%     OPTIONS.neutron: when set (default), the neutron intensity is calculated.
%   All options are stored in SQW.UserData, and can be tuned afterwards.
%
%   SQW = SQW_SPINW(..., 'no_neutron') sets OPTIONS.neutron to false at creation.
%
%   SQW = SQW_SPINW('defaults') creates a default model (squareAF).
%
% Reference: https://en.wikipedia.org/wiki/Spin_wave
% SpinW <https://github.com/SpinW/spinw> and <https://spinw.org>.
%       S. Toth and B. Lake, J. Phys.: Condens. Matter 27, 166002 (2015).
% Example: sq = sw_model('squareAF',2,0); s  = sqw_spinw(sq); ...
%   qh = linspace(0.01,1.5,30);qk=qh; ql=qh'; w=linspace(0.01,10,50); ...
%   f  = iData(s,s.p,qh,qk,ql,w); plot(f(:,:,1,:)); ...
%   close(gcf); isa(s, 'iFunc')
% See also iData, iFunc/fits, iFunc/plot, gauss, sqw_phonons, sqw_cubic_monoatomic, sqw_vaks, sqw_sine3d

if ~exist('sw') && ~exist('spinw')
  disp([ mfilename ': ERROR: requires SpinW to be installed.' ])
  disp('  Get it at <https://github.com/SpinW/spinw>.');
  signal=[]; return
end
sq      = [];
options = [];

for index=1:numel(varargin)
  this = varargin{index};
  if isa(this, 'sw') || isa(this, 'spinw')    % a SpinW object
    sq = this;
  elseif ischar(this) && ~isempty(dir(this))  % a CIF file
    sq = sw(this);
    % the following auto-setting for spin-couplig usually fails. Should be improved.
    disp([ mfilename ': defining a spin-coupling set (may be wrong)' ])
    sq.gencoupling;
    J =ones(1, ceil(numel(sq.unit_cell.label)/2));
    J(2:2:end) = -1;
    sq = quickham(sq, J); % from SpinW/Git on Aug 25th 2016. Private below.
    sq.genmagstr('mode','random');
    % optimize mag structure
    disp([ mfilename ': optimizing the magnetic structure.' ])
    sq.optmagstr;
  elseif isstruct(this)                       % some options
    options = this;
  elseif ischar(this) && strcmp(this, 'identify')
    signal  = sqw_spinw('defaults');
    return
  elseif ischar(this) && strcmp(this, 'no_neutron')
    option.neutron = false;
  elseif ischar(this) && strcmp(this, 'defaults')
    sq = [];
  end
end
if isempty(sq)
  sq = sw_model('squareAF',2,0);
end

% We could use a more general way to enter SpinW options and default values.
if ~isfield(options, 'component')
  options.component ='Sperp';
end
if ~isfield(options, 'ki')
  options.ki        = 1e4;
end
if ~isfield(options, 'neutron')
  options.neutron   = true;
end

signal.Name           = [ 'Sqw_spinw spin-wave dispersion(HKL) [' mfilename ']' ];
signal.Description    = 'A HKL spin-wave dispersion from SpinW package with Gaussian energy width';

signal.Parameters     = {  ...
  'Gamma energy broadening around spin-wave modes [meV]' ...
  'Temperature [K]' 'Amplitude' };
  
% we add more parameters from the SpinW object.matrix.mat
J     = sq.matrix.mat;
iJ    = size(J, 3);
pJ    = sq.matrix.label;
label = sprintf(' %s', sq.unit_cell.label{:}, pJ{:});

signal.Parameters  = [ signal.Parameters pJ ];
signal.Description = [ signal.Description ': ' label ];
  
signal.Dimension      = 4;         % dimensionality of input space (axes) and result

% get the norm of J which can be rescaled as Parameters
nJ = zeros(1, iJ);
for index=1:iJ
  nJ(index) = norm(J(:,:,index));
end
signal.Guess          = [ .3 0 1 nJ ];        % default parameters
  
signal.UserData.component = options.component;
signal.UserData.ki        = options.ki;
signal.UserData.neutron   = options.neutron;
signal.UserData.spinw     = sq;
signal.UserData.cell      = [ sq.lattice.lat_const sq.lattice.angle*180/pi ];
% compute reciprocal basis 'B'
alpha =signal.UserData.cell(4);
beta  =signal.UserData.cell(5);
gamma =signal.UserData.cell(6);
a_vec =signal.UserData.cell(1)*[1; 0; 0];
b_vec =signal.UserData.cell(2)*[cosd(gamma); sind(gamma); 0];
c1    =cosd(beta);
c2    =(cosd(alpha)-cosd(gamma)*cosd(beta))/sind(gamma);
c3    =sqrt(1-c1^2-c2^2);
c_vec =signal.UserData.cell(3)*[c1; c2; c3;];
V     =dot(a_vec,cross(b_vec,c_vec));
signal.UserData.reciprocal_cell=2*pi*[cross(b_vec,c_vec) cross(c_vec,a_vec) cross(a_vec,b_vec)]/V; % reciprocal basis, as columns
signal.UserData.volume      = V;
signal.UserData.classical   = false;
signal.UserData.DebyeWaller = false;


% get code to read xyzt and build HKL list and convolve DHO line shapes
[script_hkl, script_dho] = sqw_phonons_templates;

signal.Expression     = { ...
[ '% spinw(' label sprintf(') p(1:%i)', iJ+3) ], ...
'% x=qh; y=qk; z=ql; t=w', ....
script_hkl{:}, ...
'for index=1:size(this.UserData.spinw.matrix.mat,3)', ...
'  this.UserData.spinw.matrix.mat(:,:,index) = this.UserData.spinw.matrix.mat(:,:,index)./norm(this.UserData.spinw.matrix.mat(:,:,index)).*p(index+3);', ...
'end', ...
'spec = spinwave(this.UserData.spinw, HKL'',''optmem'',1);', ...
'this.UserData.HKL =HKL;', ...
'this.UserData.FREQ=spec.omega'';', ...
'if this.UserData., spec = sw_neutron(spec); end', ...
'this.UserData.maxFreq=max(spec.omega(:));', ...
'spec   = sw_egrid(spec,''component'',this.UserData.component,''Evect'',t(:)'', ''T'', p(2));', ...
'spec   = sw_instrument(spec,''dE'',p(1),''ki'',this.UserData.ki);', ...
'signal = reshape(spec.swConv,resize_me([end 1:(end-1)])); signal=p(3)*permute(signal,[2:ndims(signal) 1]);' };

signal = iFunc(signal);
signal = iFunc_Sqw4D(signal); % overload Sqw4D flavour

if ~isdeployed && usejava('jvm') && usejava('desktop')
  disp([ '<a href="matlab:doc(''' mfilename ''')">' mfilename '</a>: Model ' sprintf(' %s', sq.unit_cell.label{:}) ' built using SpinW.' ])
else
  disp([ mfilename ': Model ' label ' built using SpinW.' ])
end
disp([ 'Ground state energy: ' num2str(sq.energy) ' [mev/spin]' ]);
if nargin == 0
  plot(sq); % in GUI mode
end
% display SpinW version
try
  sw_version;
end
disp(' * S. Toth and B. Lake, J. Phys.: Condens. Matter 27, 166002 (2015).' );



% ------------------------------------------------------------------------------
% Extracted from from SpinW/Git on Aug 25th 2016

function obj=quickham(obj,J)
% creates magnetic Hamiltonian with a single command
%
% QUICKHAM(obj, J)
%
% The function generates the bonds from the predefined crystal structure
% and assigns exchange values to bonds such as J(1) to first neighbor, J(2)
% for second neighbor etc. The command will erase all previous bond,
% anisotropy, g-tensor and matrix definitions. Even if J(idx) == 0, the
% corresponding bond and matrix will be created.
%
% Input:
%
% obj       Spinw object.
% J         Vector containing the Heisenberg exchange values. J(1) for
%           first neighbor bonds, etc.
%

fid = obj.fileid;

obj.fileid(0);

dMax = 8;
nMax = 0;
nJ   = numel(J);

idx = 1;
% generate the necessary bonds and avoid infinite loop
while nMax < nJ && idx < 12
    obj.gencoupling('maxDistance',dMax);
    dMax = dMax+8;
    % maximum bond index
    nMax = obj.coupling.idx(end);
    idx  = idx+1;
end

obj.fileid(fid);

if nMax < nJ
    warning('The necessary bond length is too long (d>100 A), not all Js will be assigned!');
    J = J(1:nMax);
end

% clear matrix definitions
obj.matrix.mat   = zeros(3,3,0);
obj.matrix.color = int32(zeros(3,0));
obj.matrix.label = cell(1,0);

nDigit = floor(log10(nJ))+1;

for ii = 1:numel(J)
    % assign non-zero matrices to bonds
    matLabel = num2str(ii,num2str(nDigit,'J%%0%dd'));
    obj.addmatrix('label',matLabel,'value',J(ii));
    obj.addcoupling(matLabel,ii);
end


