function y=heaviside(varargin)
% HEAVISIDE Heaviside step function
%   M = HEAVISIDE creates a Heaviside step fitting function
%     y = 0*x+p(4); y(find(x >= p(2))) = p(1);
%   The Width parameter sign indicates if this is a raising (positive) or 
%   falling (negative) Heaviside.
%   Parameters are: 
%     p = [ Amplitude Centre FullWidth BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = HEAVISIDE(threshold)      creates a model with a specified threshold.
%
%   M = HEAVISIDE([ parameters ]) creates a model with specified model parameters.
%
%   Y = HEAVISIDE(p, X)           creates the model and returns its value on axis X.
%
% Example: m=heaviside([1 0 1 0]); isnumeric(feval(m))
% Example: m=heaviside(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% Version: $Date$ $Version$ $Author$
% See also iFunc, iFunc/fits, iFunc/plot, tophat

y.Name      = [ 'Heaviside (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Centre','FullWidth','Background'};
y.Description='Heaviside model. The Width parameter sign indicates if this is a raising (positive) or falling (negative) Heaviside.';
y.Expression= {'signal = zeros(size(x))+p(4);', ...
  'if p(3) >= 0, signal(find(x >= p(2))) = p(1);', ...
  'else signal(find(x <= p(2))) = p(1); end' };
y.Dimension  = 1;

% moments of distributions
m1 = @(x,s) sum(s(:).*x(:))/sum(s(:));
m2 = @(x,s) sqrt(abs( sum(x(:).*x(:).*s(:))/sum(s(:)) - m1(x,s).^2 ));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,s) iif(...
  ~isempty(s)&&numel(s)==numel(x), @()  [ NaN m1(x, s-min(s(:))) m2(x, s-min(s(:))) NaN ], ...
  true            , @() [1 0 1 0]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} varargin{1}/4 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

