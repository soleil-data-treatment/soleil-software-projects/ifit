function y=bilorz(varargin)
% BILORZ Bi-Lorentzian model.
%   M = BILORZ creates a Bi-Lorentzian fitting function with expression
%     y = p(1)*exp(-0.5*((x-p(2))/s).^2) + p(5);
%   where s = p(3) for x < p(2) and s = p(4) for x > p(2).
%   Parameters are: 
%     p = [ Amplitude Centre HalfWidth1 HalfWidth2 BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = BILORZ([ w1 w2])       creates a model with specified widths.
%
%   M = BILORZ([ parameters ]) creates a model with specified model parameters.
%
%   Y = BILORZ(p, X)           creates the model and returns its value on axis X.
%
% Example: m=bilorz([1 0 1 1 0]); isnumeric(feval(m))
% Example: m=bilorz([1 0 1 1 0]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, lorz, bigauss

y.Name      = [ 'Bi-Lorentzian (1D) [' mfilename ']' ];
y.Description='Bi-Lorentzian/asymmetric fitting function';
y.Parameters= { 'Amplitude', 'Centre', 'HalfWidth1', 'HalfWidth2', 'BackGround' };
y.Expression = @(p,x) p(1)*exp(-0.5*((x-p(2))./ (p(3)*(x < p(2)) + p(4) * (x >= p(2))) ).^2) + p(5);
y.Dimension  = 1;  
% moments of distributions
m1 = @(x,s) sum(s(:).*x(:))/sum(s(:));
m2 = @(x,s) sqrt(abs( sum(x(:).*x(:).*s(:))/sum(s(:)) - m1(x,s).^2 ));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,s) iif(...
  ~isempty(s), @() [ NaN m1(x, s-min(s(:))) m2(x, s-min(s(:)))/1.5 m2(x, s-min(s(:)))*1.5 NaN ], ...
  true            , @() [1 0 1 1 0]);

y=iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 varargin{1}*.75 varargin{1}*1.25 0]};
  elseif length(varargin{1}) == 2
    varargin = {[ 1 0 varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end
