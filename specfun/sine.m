function y=sine(varargin)
% SINE Sine function. 
%   M = SINE creates a Sine function. 
%     y  = p(4) + p(1)*sin((x - p(2))/p(3))
%   Parameters are: 
%     p = [ Amplitude Phase_Shift Period BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = SINE(period)         creates a model with specified period.
%
%   M = SINE([ parameters ]) creates a model with specified model parameters.
%
%   Y = SINE(p, X)           creates the model and returns its value on axis X.
%
% Example: m=sine([1 0 1 1]); isnumeric(feval(m))
% Example: m=sine(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, sinedamp

y.Name           = [ 'Sine function (1D) [' mfilename ']' ];
y.Parameters     = {'Amplitude','Phase_Shift','Period','Background'};
y.Dimension      = 1;
y.Description    = 'Sine function';
y.Expression     = @(p,x) p(4) + p(1)*sin((x - p(2))/p(3));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ max(y(:))-min(y(:)) mean(x(:)) std(x(:))/4 min(y(:)) ], ...
  true            , @() [1 0 1 0]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

