function y=doseresp(varargin)
% DOSERESP Dose-response curve with variable Hill slope.
%   M = DOSERESP creates a Dose-response curve with variable Hill slope (sigmoid)
%     y  = p(4)+ p(1) ./ (1+10.^((p(2)-x).*p(3)));
%   This is a sigmoid S-shaped curve, aka logistic.
%   Parameters are: 
%     p = [ Amplitude Center Slope BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = DOSERESP(threshold)      creates a model with a specified threshold.
%
%   M = DOSERESP([ parameters ]) creates a model with specified model parameters.
%
%   Y = DOSERESP(p, X)           creates the model and returns its value on axis X.
%
% Example: m=doseresp([1 0 1 1]); isnumeric(feval(m))
% Example: m=doseresp([1 0 1 1]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, sigmoid, langevin

y.Name           = [ 'Dose-response (sigmoid) (1D) [' mfilename ']' ];
y.Description    = 'sigmoid S-shaped curve, aka logistic, aka dose response';
y.Parameters     = {'Amplitude','Center threshold','Slope','Background'};
y.Expression = @(p,x) p(4)+ p(1) ./ (1+10.^((p(2)-x).*p(3)));
y.Dimension      = 1;
% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ max(y(:))-min(y(:)) mean(x(:)) (max(y(:))-min(y(:)))/std(x(:)) min(y(:)) ], ...
  true            , @() [1 0 1 1]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} 1 0 ]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end


