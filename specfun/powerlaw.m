function y=powerlaw(varargin)
% POWERLAW Power law model.
%   M = POWERLAW creates a power law fitting function
%     y = p(1)*(x - p(2))^p(3) + p(4);
%   Parameters are: 
%     p = [ Amplitude Centre Exponent BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = POWERLAW(centre)         creates a model with a specified centre.
%
%   M = POWERLAW([ parameters ]) creates a model with specified model parameters.
%
%   Y = POWERLAW(p, X)           creates the model and returns its value on axis X.
%
% Example: m=powerlaw([1 0 2 0]); isnumeric(feval(m))
% Example: m=powerlaw(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, quadline, quad2d, strline, allometric

y.Name      = [ 'Power law (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Centre','Exponent','Background'};
y.Description='power law';
y.Expression= @(p,x) real(p(1)*(x - p(2)).^p(3) + p(4));
% fill guessed information
  % ln y = ln a + c*ln (x-b)
  
% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ ...
  exp(subsref(strline('guess', log(x(:)),log(y(:))), struct('type','()', 'subs',{{1}}))) ...
  mean(x(:)) ...
  subsref(strline('guess', log(x(:)),log(y(:))), struct('type','()', 'subs',{{2}})) ...
  min(y(:)) ], ...
  true            , @() [1 0 2 0]);
  
y.Dimension =1;

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} 2 0]};
  end
  y.ParameterValues = varargin{1};
elseif length(varargin) > 1
  y = y(varargin{:});
end

