function y=bose(varargin)
% BOSE   Bose model.
%   M = BOSE creates a Bose fitting function with expression
%     n(x) = 1 ./ (exp(x/p(1)) - 1);
%   Parameters are: 
%     p = [ Kb.T/hbar   in 'x' units ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%     y = n(x)+1 for x > 0
%     y = n(x)   for x < 0
%   When 'x' is an energy in meV, the 'Temperature' parameter is T/11.6045 with T in [K].
%
%   M = BOSE(kT)       creates a model with specified 'Temperature' (kT).
%
%   Y = BOSE(p, X)     creates the model and returns its value on axis X.
%
%   The Bose factor has the sign of x.
%     (n(x)+1) converges to 0 for x -> -Inf, and to 1 for x-> +Inf. 
%     It diverges at x=0. n(x)+n(-x) = -1
%
%   Energy conventions:
%     x = omega = Ei-Ef = energy lost by the neutron [meV]
%         omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
%         omega < 0, neutron gains energy, anti-Stokes
%
% Reference: http://en.wikipedia.org/wiki/Bose-Einstein_statistics
%
% Example: m=bose(300/11.605); isnumeric(feval(m))
% Example: m=bose(300/11.605); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, dho, expon

y.Name       = [ 'Bose (1D) [' mfilename ']' ];
y.Description='Bose-Einstein distribution fitting function. Ref: http://en.wikipedia.org/wiki/Bose%E2%80%93Einstein_statistics';
y.Parameters = {'Temperature [x unit, 2pi*kT/h]'};
% the Bose factor is negative for w<0, positive for w>0
% (n+1) converges to 0 for w -> -Inf, and to 1 for w-> +Inf. It diverges at w=0
y.Expression = { ...
  'signal = 1 ./ (exp(x/p(1)) - 1);' ...
  'signal(find(x==0)) = 1;' ...
};
y.Dimension  = 1;   
% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,signal) iif(...
  ~isempty(signal), @() abs(log(1./mean(signal(:))+1)/mean(abs(x(:)))), ...
  true            , @() 25);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end
