function y=dho(varargin)
% DHO    Damped harmonic oscillator
%   M = DHO is a Damped harmonic oscillator fitting function, including Bose factor.
%     y=p(1)*p(3) *p(2)^2.*(1+1./(exp(abs(x)/p(5))-1))./((x.^2-p(2)^2).^2+(p(3)*x).^2)
%   Parameters are: 
%     p = [ Amplitude Centre HalfWidth BackGround Temperature(in x units)]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = DHO(energy)         creates a model with a line at specified energy (centre).
%
%   M = DHO([ parameters ]) creates a model with specified model parameters.
%
%   Y = DHO(p, X)           creates the model and returns its value on axis X.
%
% Reference: B. Fak, B. Dorner / Physica B 234-236 (1997) 1107-1108
%
% Example: m=dho([1 0 1 1 1]); isnumeric(feval(m))
% Example: m=dho([1 0 1 1 1]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, gauss, lorz, bose, voigt, pseudovoigt

y.Name       = [ 'Damped-harmonic-oscillator (1D) [' mfilename ']' ];
y.Description='Damped harmonic oscillator S(q,w) fitting function. Ref: B. Fak, B. Dorner / Physica B 234-236 (1997) 1107-1108';
y.Parameters = {'Amplitude one phonon structure factor Zq=exp(-2W)|Q.e|²/2M',...
  'Centre renormalized frequency Omega_q',...
  'HalfWidth phonon linewidth Gamma_q',...
  'Background',...
  'Temperature kT in "x" unit'};
y.Expression = @(p,x) (1./(exp(x/p(5))-1)+1)*p(1)*4.*x*p(3)/pi./((x.^2-p(2)^2).^2 + 4*x.^2*p(3)^2) + p(4);
y.Dimension  = 1;
% moments of distributions
m1 = @(x,s) sum(s(:).*x(:))/sum(s(:));
m2 = @(x,s) sqrt(abs( sum(x(:).*x(:).*s(:))/sum(s(:)) - m1(x,s).^2 ));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,s) iif(...
  ~isempty(s)&&numel(s)==numel(x), @() [ NaN m1(x, s-min(s(:))) m2(x, s-min(s(:))) NaN 1 ], ...
  true            , @() [1 0 1 1 1]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} varargin{1}/4 0 1]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end
