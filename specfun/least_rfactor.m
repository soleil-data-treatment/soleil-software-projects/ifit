function c=least_rfactor(Signal, Error, Model)
% LEAST_RFACTOR Normalized R-factor weighted least square criteria.
%   R = LEAST_RFACTOR(Signal, Error, Model) returns the minimal R-factor 
%   criteria R = (|Signal-Model|/Error).^2/(Signal/Error).^2
%   (weighted least square criteria) normalised to the Signal.
%   The return value X is a vector, and most optimizers use its sum (except LM).
%   A good fit corresponds with an R-factor lower than .2
%
% Ref: <http://en.wikipedia.org/wiki/R-factor_%28crystallography%29>
% Example: s=-10:10; e=sqrt(abs(s)); m=1; sum(least_rfactor(s,e,m))

  if ~isnumeric(Signal) || ~isnumeric(Model), return; end
  if isempty(Error) || isscalar(Error) || all(Error == Error(end))
    index = find(isfinite(Model) & isfinite(Signal) & Signal);
    c = sqrt(((Signal(index)-Model(index)).^2)./(Signal(index).^2)); % raw normalised least absolute
  else
    index = find(isfinite(Error) & isfinite(Model) & isfinite(Signal) & Signal);
    residuals  = Signal - Model;
    % make sure weight=1/sigma does not reach unrealistic values
    %   initially, most weights will be equal, but when fit impproves, 
    %   stdE will get lower, allowing better matching of initial weight.
    normE = sum(Error(index));
    stdE  = std(residuals(index));
    Error( Error < stdE/4 ) = stdE/4; 
    Error = Error *(normE/sum(Error(index)));
    
    if isempty(index), c=Inf;
    else               c=sqrt(((residuals(index)./Error(index)).^2)./((Signal(index)./Error(index)).^2));
    end
  end
end % least_rfactor
