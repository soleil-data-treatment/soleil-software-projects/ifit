function y=lognormal(varargin)
% LOGNORMAL Log-Normal distribution function. 
%   M = LOGNORMAL creates a Log-Normal distribution function. 
%     y  = p(4)+ p(1)/sqrt(2)/p(3)./x .* exp( -log(x/p(2)).^2 /2/p(3)/p(3) )
%   Parameters are: 
%     p = [ Amplitude Center Width BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = LOGNORMAL(centre)         creates a model with a specified centre.
%
%   M = LOGNORMAL([ parameters ]) creates a model with specified model parameters.
%
%   Y = LOGNORMAL(p, X)           creates the model and returns its value on axis X.
%
% Reference: http://en.wikipedia.org/wiki/Log_normal
%
% Example: m=lognormal([1 0 1 0]); isnumeric(feval(m))
% Example: m=lognormal(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, expon, expstretched

y.Name      = [ 'Log-Normal distribution function (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Centre','Width','Background'};
y.Description='Log-Normal distribution function. Ref: http://en.wikipedia.org/wiki/Log_normal';
y.Expression= @(p,x) real(p(4)+ p(1)/sqrt(2)/p(3)./x .* exp( -log(x/p(2)).^2 /2/p(3)/p(3) ));
y.Dimension  = 1;

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @()  [ (max(y(:))-min(y(:)))/2 mean(abs(x(:))) std(x(:))/2 min(y(:)) ], ...
  true            , @() [1 0 1 0]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} varargin{1}/4 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

