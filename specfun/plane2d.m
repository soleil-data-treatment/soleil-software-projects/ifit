function signal=plane2d(varargin)
% PLANE2D Planar 2D model.
%   M = PLANE2D creates a Planar function (fit 2D function/model)
%       signal = p(1)*x+p(2)*y+p(3)
%   Parameters are: 
%     p = [  Slope_X Slope_Y Background ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = PLANE2D([s1 s2])        creates a model with specified slopes.
%
%   M = PLANE2D([ parameters ]) creates a model with specified model parameters.
%
%   Z = PLANE2D(p, X,Y)         creates the model and returns its value on axes X,Y.
%
% Example: m=plane2d([1 2 0]); ndims(feval(m))==2
% Example: m=plane2d([1 2 0]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, strline, quadline, quad2d

signal.Name           = [ 'Planar function (2D) [' mfilename ']' ];
signal.Parameters     = { 'Slope_X' 'Slope_Y' 'Background' };
signal.Description    = '2D Planar function';
signal.Dimension      = 2;         % dimensionality of input space (axes) and result
signal.Guess          = [1 2 0];        % default parameters
signal.Expression     = @(p,x,y) p(1)*x+p(2)*y+p(3);

signal=iFunc(signal);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ varargin{1} varargin{1} 0]};
  elseif length(varargin{1}) == 2
    varargin = {[ varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif length(varargin) > 1
  y = y(varargin{:});
end

