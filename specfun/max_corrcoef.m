function c=max_corrcoef(Signal, Error, Model)
% MAX_CORRCOEF maximum correlation coefficient criteria.
%   R = MAX_CORRCOEF(Signal, Error, Model) returns the maximum correlation 
%   coefficient R = 1-corrcoef(Signal, Model) as a scalar
%   with non diagonal term of the correlation matrix. This value lies within 
%   [0 2]. R=0 is a perfect match. This criteria does not use the Error.
%
% Ref: <http://en.wikipedia.org/wiki/Correlation_coefficient>
% Example: s=0:20; m=sqrt(abs(s)); max_corrcoef(s,[],m)

  index = find(isfinite(Model) & isfinite(Signal));
  c = corrcoef(Signal(index),Model(index));
  c = 1 - c(1,2); % non diag is in [-1 1]

end % max_corrcoef
