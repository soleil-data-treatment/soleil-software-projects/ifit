function c=least_mean(Signal, Error, Model)
% LEAST_MEAN Weighted mean absolute criteria
%   X = LEAST_MEAN(Signal, Error, Model) returns the weighted mean absolute 
%   criteria X = mean(|Signal-Model|/Error)
%   A good fit corresponds with a criteria lower or equal to 1.
%
% Ref: <https://en.wikipedia.org/wiki/Mean_absolute_difference>
% Example: s=-10:10; e=sqrt(abs(s)); m=1; least_mean(s,e,m)

  c = mean(least_absolute(Signal, Error, Model));
end % least_mean
