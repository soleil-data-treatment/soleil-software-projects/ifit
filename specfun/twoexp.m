function y=twoexp(varargin)
% TWOEXP Two exponential decay functions.
%   M = TWOEXP creates a Two exponential decay functions (fit 1D function/model)
%     y=p(1)*exp(-x/p(2))+p(3)*exp(-x/p(4)) + p(5);
%   Parameters are: 
%     p = [ Amplitude1 Tau1 Amplitude2 Tau2 Background ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = TWOEXP([tau1 tau2])    creates a model with specified decay constants.
%
%   M = TWOEXP([ parameters ]) creates a model with specified model parameters.
%
%   Y = TWOEXP(p, X)           creates the model and returns its value on axis X.
%
% Example: m=twoexp([2 0.1 1 0.5 0]); isnumeric(feval(m))
% Example: m=twoexp([0.1 0.5]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, expon, sinedamp, expstretched

y.Name           = [ 'Bi-Exponential decay (1D) [' mfilename ']' ];
y.Parameters     = {'Amplitude1' 'Tau1 decay in inverse "x" unit' 'Amplitude2' 'Tau2 decay in inverse "x" unit' 'Background'};
y.Dimension      = 1;         % dimensionality of input space (axes) and result
y.Description    = '2 Exponential decay';
y.Expression     = @(p,x) p(1)*exp(-x/p(2))+p(3)*exp(-x/p(4)) + p(5);
    
% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ ...
   exp(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{2}})))*0.66 ...
   -1/(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{1}}))-(abs(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{1}}))) < 1e-2)*.1) ...
   exp(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{2}})))/3 ...
   -2/(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{1}}))-(abs(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{1}}))) < 1e-2)*.2) ...
    min(y(:)) ], ...
  true            , @() [2 0.1 1 0.5 0]);
  
y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    tau = varargin{1};
    varargin = {[ 1 tau 1 tau 0]};
  elseif length(varargin{1}) == 2
    tau = varargin{1};
    varargin = {[ 1 tau(1) 1 tau(2) 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

