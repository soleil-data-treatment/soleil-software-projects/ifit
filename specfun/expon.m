function y=expon(varargin)
% EXPON Exponential decay model.
%   M = EXPON creates an Exponential decay fitting function
%     p(2)=Tau is the exponential decay parameter, in inverse 'x' units.
%     y=p(3)+p(1)*exp(-x/p(2));
%   Parameters are: 
%     p = [ Amplitude Tau BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = EXPON(decay)          creates a model with specified decay constant.
%
%   M = EXPON([ parameters ]) creates a model with specified model parameters.
%
%   Y = EXPON(p, X)           creates the model and returns its value on axis X.
%
% Example: m=expon([1 0.5 0]); isnumeric(feval(m))
% Example: m=expon([1 0.5 0]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iData, iFunc/fits, iFunc/plot, twoexp, sinedamp

y.Name           = [ 'Exponential decay (1D) [' mfilename ']' ];
y.Description    = 'Exponential decay';
y.Parameters     = {'Amplitude','Tau decay in inverse "x" unit', 'Background'};
y.Expression     = @(p,x) p(3)+p(1)*exp(-x/p(2));
y.Dimension      = 1;         % dimensionality of input space (axes) and result

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ ...
   exp(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{2}}))) ...
    -1/(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{1}}))-(abs(subsref(polyfit(x(:),log(y(:)-min(y(:))+0.01*abs(min(y(:)))),1), struct('type','()', 'subs',{{1}}))) < 1e-2)*.1) ...
    min(y(:)) ], ...
  true            , @() [1 0.5 0]);
y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} 0 ]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end


