function c=least_median(Signal, Error, Model)
% LEAST_MEDIAN Weighted median absolute criteria
%   X = LEAST_MEDIAN(Signal, Error, Model) returns the weighted median absolute 
%   criteria X = median(|Signal-Model|/Error)
%   A good fit corresponds with a criteria lower or equal to 1.
%
% Ref: <http://en.wikipedia.org/wiki/Median_absolute_deviation>
% Example: s=-10:10; e=sqrt(abs(s)); m=1; least_median(s,e,m)

  c = median(least_absolute(Signal, Error, Model));
end % least_median
