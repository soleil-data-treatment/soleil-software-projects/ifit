function y=strline(varargin)
% STRLINE Straight line.
%   M = STRLINE creates a Straight line fitting function
%     y=p(2)+p(1)*x;
%   The p(1)=Gradient parameter is the slope of the straight line.
%   Parameters are: 
%     p = [ Gradient BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = STRLINE(slope)          creates a model with specified slope/gradient.
%
%   M = STRLINE([ parameters ]) creates a model with specified model parameters.
%
%   Y = STRLINE(p, X)           creates the model and returns its value on axis X.
%
% Example: m=strline([1 1]); isnumeric(feval(m))
% Example: m=strline([1 1]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, quadline, plane2d
  
y.Name      = [ 'Straight-line (1D) [' mfilename ']' ];
y.Parameters={'Gradient slope','Constant'};
y.Description='Straight line';
y.Expression= @(p,x) p(2)+p(1)*x;
y.Dimension      = 1;

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ polyfit(x(:), y(:), 1) ], ...
  true            , @() [1 1]);

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ varargin{1} 0 ]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

