function y=poisson(varargin)
% POISSON Poisson distribution function. 
%   M = POISSON creates a Poisson distribution function. 
%     y  = p(3)+ p(1)*(exp(-p(2)).*(p(2).^x))./factorial(x);
%   which is valid for integer 'x' axis values (counts).
%   Parameters are: 
%     p = [ Amplitude Center BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = POISSON(centre)         creates a model with a specified centre.
%
%   M = POISSON([ parameters ]) creates a model with specified model parameters.
%
%   Y = POISSON(p, X)           creates the model and returns its value on axis X.
%
% Example: m=poisson([1 1 0]); isnumeric(feval(m))
% Example: m=poisson(1); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, gauss

y.Name      = [ 'Poisson distribution function (1D) [' mfilename ']' ];
y.Parameters={'Amplitude','Center','Background'};
y.Description='Poisson distribution function. Valid with integer axis values';
y.Expression= @(p,x) p(3)+ p(1)*(exp(-p(2)).*(p(2).^floor(abs(x))))./factorial(floor(abs(x)));

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ (max(y(:))-min(y(:)))/2 mean(abs(x(:))) min(x(:)) ], ...
  true            , @() [1 1 0]);
  
y.Dimension =1;

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif length(varargin) > 1
  y = y(varargin{:});
end

