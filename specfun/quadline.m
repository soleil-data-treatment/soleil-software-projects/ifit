function y=quadline(varargin)
% QUADLINE Quadratic line model.
%   M = QUADLINE creates a Quadratic fitting function
%     y=p(3)+p(2)*x+p(1)*x.*x;
%   Parameters are: 
%     p = [ Quadratic Linear Constant ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = QUADLINE([ parameters ]) creates a model with specified model parameters.
%
%   Y = QUADLINE(p, X)           creates the model and returns its value on axis X.
%
% Reference: http://en.wikipedia.org/wiki/Quadratic_function
%
% Example: m=quadline([1 1 0]); isnumeric(feval(m))
% Example: m=quadline([1 1 0]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, strline, quad2d

y.Name      = [ 'Quadratic equation (1D) [' mfilename ']' ];
y.Parameters={'Quadratic' 'Linear','Constant'};
y.Description='Quadratic equation. Ref: http://en.wikipedia.org/wiki/Quadratic_function';
y.Expression= @(p,x) p(3)+p(2)*x+p(1)*x.*x;

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,y) iif(...
  ~isempty(y), @() [ polyfit(x(:), y(:), 2) ], ...
  true            , @() [1 1 0]);
  
y.Dimension =1;

y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end

