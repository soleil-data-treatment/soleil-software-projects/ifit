function c=least_max(Signal, Error, Model)
% LEAST_MAX Weighted max absolute criteria
%   X = LEAST_MAX(Signal, Error, Model) returns the weighted max absolute 
%   criteria X = max(|Signal-Model|/Error)
%   A good fit corresponds with a criteria lower or equal to 1.
%
% Ref: <http://en.wikipedia.org/wiki/Absolute_deviation>
% Example: s=-10:10; e=sqrt(abs(s)); m=1; least_max(s,e,m)

  c = max(least_absolute(Signal, Error, Model));
end % least_max
