function y=allometric(varargin)
% ALLOMETRIC Power/Freundlich/Belehradek model.
%   M = ALLOMETRIC creates a Power/Freundlich/Belehradek fitting function
%   to describe power and asymptotic laws. y = p(1)*(x-p(2)).^p(3) + p(4);
%   Parameters are: 
%     p = [ Amplitude Offset Exponent BackGround ]
%   The model M is returned, and can be evaluated with e.g. y=M(p, x)
%
%   M = ALLOMETRIC(decay)          creates a model with specified decay constant.
%
%   M = ALLOMETRIC([ parameters ]) creates a model with specified model parameters.
%
%   Y = ALLOMETRIC(p, X)           creates the model and returns its value on axis X.
%
% Reference: Ratkowksy, David A. 1990. Handbook of Nonlinear Regression Models. Marcel Dekker, Inc. 4.3.1 
%
% Example: m=allometric([1 0 1 1]); isnumeric(feval(m,[], -10:10))
% Example: m=allometric([1 0 1 1]); h=plot(m); ...
%   tf=ishandle(h); close(gcf); tf
% See also iFunc, iFunc/fits, iFunc/plot, powerlaw

y.Name      = [ 'Allometric/Freundlich (1D) [' mfilename ']' ];
y.Description='Power/Freundlich/Belehradek fitting function to describe power and asymptotic laws. Ref: Ratkowksy, David A. 1990. Handbook of Nonlinear Regression Models. Marcel Dekker, Inc. 4.3.1 ';
y.Parameters= {'Amplitude','Offset','Exponent','Background'};
y.Expression= @(p,x) p(1)*(x-p(2)).^p(3) + p(4); 

% use ifthenelse anonymous function
% <https://blogs.mathworks.com/loren/2013/01/10/introduction-to-functional-programming-with-anonymous-functions-part-1/>
% iif( cond1, exec1, cond2, exec2, ...)
iif = @(varargin) varargin{2 * find([varargin{1:2:end}], 1, 'first')}();
y.Guess     = @(x,signal) iif(...
  ~isempty(signal), @() [ max(signal(:))-min(signal(:)) min(x(:))-mean(x(:))/10 .1 min(signal(:)) ], ...
  true            , @() [1 0 1 1]);
y.Dimension = 1;
 
y = iFunc(y);

if nargin == 1 && isnumeric(varargin{1})
  if length(varargin{1}) == 1
    varargin = {[ 1 0 varargin{1} 0]};
  end
  y.ParameterValues = varargin{1};
elseif nargin > 1
  y = y(varargin{:});
end
