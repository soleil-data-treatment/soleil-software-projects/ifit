classdef iData_Sab < iData
  % IDATA_SAB Create a 2D S(alpha,beta) data set (iData flavour)
  %
  % The iData_Sab class is a 2D data set holding a S(alpha,beta) dynamic 
  %   structure factor aka scattering function/law.
  %   The data set axes are alpha as 1st axis (rows), beta as 2nd axis (columns).
  %
  % The S(alpha,beta) is a representation of the dynamic structure factor 
  % using unitless momentum and energy variables defined as:
  %     alpha= h2q2/2MkT  = (Ei+Ef-2*mu*sqrt(Ei*Ef))/AkT
  %     beta = -hw/kT     = (Ef-Ei)/kT                  energy gained by neutron
  %     A    = M/m
  %     mu   = cos(theta) = (Ki.^2 + Kf.^2 - q.^2) ./ (2*Ki.*Kf)
  % This representation is common in nuclear data, neutron sections (e.g. ENDF MF7).
  %
  % Conventions:
  % beta = (Ef-Ei)/kT = -hw/kT  energy gained by the neutron, unitless
  %    beta < 0, neutron looses energy, down-scattering (Stokes)
  %    beta > 0, neutron gains energy,  up-scattering (anti-Stokes)
  %
  % Example: sab=iData_Sab('SQW_coh_lGe.nc', 72.6,1235)
  %
  % Useful methods for this iData flavour:
  %
  % methods(iData_Sab)
  %   all iData methods can be used.
  % iData_Sab(sab)
  %   convert input [e.g. a 2D iData object] into an iData_Sab to give access to
  %   the methods below.
  %
  % d   = dos(sab)
  %   Compute the generalized vibrational density of states (gDOS).
  %
  % t   = thermochemistry(sab,T)
  %   Compute and display thermochemistry quantities from the gDOS.
  %
  % m   = moments(sab, M, T)
  %   Compute the S(alpha,beta) moments/sum rules (harmonic frequencies).
  %
  % sym = symmetrize(sab)
  %   Extend the S(alpha,beta) in both 'beta' sides. The resulting S(a,b) is classical/symmetric in energy.
  %
  % sb  = Bosify(sab,T)
  %   Apply the 'Bose' factor (detailed balance) to a classical data set.
  %
  % s   = deBosify(sab,T)
  %   Remove Bose factor (detailed balance) from an 'experimental/quantum' data set.
  %
  % p   = parseparams(sab)
  %   Search for physical quantities in S(alpha,beta) data set.
  %
  % sqw = Sqw(sab)
  %   Convert an S(alpha,beta) to an S(q,w), which is roughly independent of the temperature.
  %
  % xs  = scattering_cross_section(sab, Ei)
  %   Compute the total integrated scattering cross section
  %
  % dr  = dynamic_range(sab, Ei, angles)
  %   Compute the dynamic range restricted to given incident energy and detector angles
  %
  % sq  = structure_factor(sab)
  %   Compute the structure factor S(q)
  %
  % [inc,multi] = incoherent(sab)
  %   Compute the incoherent neutron scattering law estimate in the incoherent 
  %     gaussian approximation, using its density of states.
  %
  % coh = coherent(Sqw(Sab), SQ)
  %   Estimate the coherent scattering law from the incoherent and a structure factor.
  %
  % [gDOS,M]    = multi_phonons(sab)
  %   Compute the integrated multi-phonon DOS terms from an initial density of states
  %
  % See also: iData, iData_Sqw2D, iData_vDOS
  
  properties
  end
  
  methods
    % main instantiation method
    function obj = iData_Sab(s, varargin)
      % IDATA_SAB Create the iData_Sab subclass for S(alpha,beta)
      %   S = IDATA_SAB(D) Convert D into an iData_Sab object so that 
      %   specific neutron S(alpha,beta) methods can be used. 
      %   The data D can be a 2D iData object holding a S(q,w), S(phi,t), 
      %   S(alpha,beta) or S(phi,w) or file name with such data.
      %
      %   S = IDATA_SAB(D, M) specifies the molar mass [g/mol]. When not given,
      %   or empty, it is searched in the input data set.
      %
      %   S = IDATA_SAB(D, M, T) specifies the temperature [K]. When not given,
      %   it is searched in the input data set.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc', 72.6,1235);
      % See also: iData_Sqw2D
      
      obj = obj@iData;
      
      if ~nargin, return; end  % empty object
      
      % convert/test
      if     isa(s, mfilename)   m = s; 
      elseif isa(s, 'iData_Sqw2D') m = Sqw2Sab(s, varargin{:});
      elseif ischar(s)
        m = iData_Sqw2D(s);
        m = Sqw2Sab(m, varargin{:});
      else
        m = Sqw_check(s, 'ab'); 
        if ~isa(m, 'iData') || any(isempty(m)) || any(ndims(m) ~= 2)
          error([ mfilename ': the given input ' class(s) ' does not seem to be convertible to iData_Sab.' ])
        end
      end
      
      % copy all properties
      obj = copy_prop(obj, m);
 
    end % iData_Sab constructor
    
    % parameters (search for parameters in iData)
    function parameters = parseparams(s)
      % PARSEPARAMS Search for physical quantities in object.
      %   P = PARSEPARAMS(Sab) searches in object Sab for physical parameters.  
      %   The initial object is updated with a 'parameters' property, and the 
      %   parameters are returned as a structure P.
      %
      %   PARSEPARAMS(Sab, 'Bose') compute the Temperature from the Bose factor 
      %   (detailed balance). The data set should extend on both positive and 
      %   negative beta sides.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc', 72.6,1235); parseparams(sab)
      % See also: iData_Sqw2D/parseparams
      
      [s,parameters,fields] = Sqw_parameters(s);
      if nargin > 1 && strcmpi(option, 'bose')
        T = Sqw_getT(s, 'T', 'Bose');
        if ~isempty(T)
          parameters.Temperature = T;
        end
      end
    end % parseparams
    
    function s = Sqw(self)
      % SQW    Convert a 2D S(alpha,beta) into an S(q,w).
      %   S = SQW(Sab) returns S(q,w) from S(alpha,beta).
      %   The S(alpha,beta) is a representation of the dynamic structure factor 
      %   using unitless momentum and energy variables defined as:
      %     alpha= h2q2/2MkT  = (Ei+Ef-2*mu*sqrt(Ei*Ef))/AkT
      %     beta = -hw/kT     = (Ef-Ei)/kT
      %     A    = M/m
      %     mu   = cos(theta) = (Ki.^2 + Kf.^2 - q.^2) ./ (2*Ki.*Kf)
      %   The S(q,w) is a representation of the scattering law that does not 
      %   depend on the temperature, and can thus be used on a wide temperature
      %   range far from phase transitions.
      %
      %   Conventions:
      %   w = omega = Ei-Ef = energy lost by the neutron [meV]
      %     omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
      %     omega < 0, neutron gains energy, anti-Stokes
      %
      % References: M. Mattes and J. Keinert, IAEA INDC(NDS)-0470, 2005.
      %             R. E. MacFarlane, LA-12639-MS (ENDF-356), 1994.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); ...
      %          sqw = Sqw(sab); sab2= Sab(sqw); ...
      %          mean(abs(sab - sab2),0) < 1e-10

      s = iData_Sqw2D(Sab2Sqw(self));  % private
    end % Sqw
    
    function f = iData(self)
      % IDATA  Convert a iData_Sab back to iData.
      %   S = IDATA(Sab) returns a pure iData object. The iData_Sab methods are 
      %   then not accessible anymore.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc', 72.6,1235);  d=iData(sab); isa(d,'iData')
      % See also: iData, iData_Sqw2D
      
      f = [];
      for index=1:numel(self)
        this = self(index);
        f1   = iData;
        this = struct(this);
        w = warning('query','iData:subsasgn');
        warning('off','iData:subsasgn');
        for p = fieldnames(this)'
          f1.(p{1}) = this.(p{1}); % generates a warning when setting Alias
        end
        warning(w);
        f = [ f f1 ];
      end
    end % iData
    
    function h = plot(self, varargin)
      % PLOT   Display a S(alpha,beta) 2D data set as a plot.
      
      h=plot@iData(transpose(self), varargin{:});
    end
    
    function DOS = dos(self, varargin)
      % DOS    Density of states (DOS) from S(alpha,beta).
      %   D = DOS(S) computes the density of states from S(alpha,beta).
      %   The gDOS is an approximation of the vibrational spectra (DOS).
      %   This routine should better be applied on an incoherent S(a,b) data set.
      %   The incoherent approximation states that the gDOS from an incoherent 
      %   S(a,b) is roughly equal to that obtained from a coherent S(a,b).
      %
      %       gDOS(q,w) = S(q,w) w^2/q^2                    Bellissent
      %       gDOS(q,w) = S(q,w) w  /q^2/[1 + n(hw)]        Carpenter/Price
      %   and:
      %       gDOS(w)   = lim(q->0) [ gDOS(q,w) ]
      %
      %       gDOS(q,w) = w*S(q,w)/(1+n(w))/(Q^4max-Q^4min) Bredov/Oskotskii
      %       gDOS(w)   = trapz(g, 2)                       (integral on all q values)
      %
      %   The Bredov/Oskotskii methodology provides the best gDOS estimate, using the
      %   whole data set, especially when restricted on a dynamic range (default method).
      %
      %   The applicability to a coherent dynamic structure factor S(q,w) should be
      %   taken with great care, as this formalism then does not fully hold.
      %
      %   D = DOS(S, 'method') comptes the dos using given method.
      %   The method to use in the gDOS computation can be given as 2nd argument
      %       gDOS = dos(S, 'Bredov')         more accurate as it uses 100% of data
      %       gDOS = dos(S, 'Carpenter')      Temperature must be a property
      %       gDOS = dos(S, 'Bellissent')     simple yet efficient
      %
      %   D = DOS(S, 'method', N) uses N low-angle values for the gGOS estimate
      %   with Bellisent and Carpenter methods. Default is N=10.
      %
      %   D = DOS(S, 'method', 'n', n, 'T', T, 'DW', dw) specifies additional arguments 
      %   in order or as name-value pairs:
      %     n:      number of low-angle values to integrate (integer). Default is 10 when omitted.
      %             when 0 or 'force', the gDOS is re-computed. Only for Carpenter/Bellissent.
      %     T:      optional temperature to use for computation (leave undefined for automatic).
      %     DW:     optional Debye-Waller coefficient gamma=<u^2> [Angs^2] e.g. 0.005
      %             The Debye-Waller function is      2W(q)=gamma*q^2
      %             The Debye-Waller factor   is exp(-2W(q))
      %
      %   The gDOS is stored in the 'gDOS' property, and is retrieved without recomputation
      %   when available. To force a recomputation of the gDOS, use:
      %       DOS(Sab, 'method', 0) or DOS(Sab, 'method', 'force')
      %
      %   When no output is used, a plot is shown.
      %
      % References: Price J. et al, Non Cryst Sol 92 (1987) 153
      %         Bellissent-Funel et al, J. Mol. Struct. 250 (1991) 213
      %         Carpenter and Pelizarri, Phys. Rev. B 12, 2391 (1975)
      %         Suck et al, Journal of Alloys and Compounds 342 (2002) 314
      %         Bredov et al., Sov. Phys. Solid State 9, 214 (1967)
      %         V.S. Oskotskii, Sov. Phys. Solid State 9 (1967), 420.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); d=dos(sab); size(d,2)==2000
      % See also: iData_Sqw2D/dos, iData_vDOS
      
      DOS = dos(iData_Sqw2D(self), varargin{:});
      set(self, 'gDOS', DOS, DOS.Title);
      
      if nargout == 0 && ~isempty(DOS)
        fig=figure; 
        % plot total DOS
        h=plot(DOS); set(h,'LineWidth',2);
        set(fig, 'NextPlot','new');
      end
    end % dos
    
    function t = thermochemistry(s, T, options)
      % THERMOCHEMISTRY Thermodynamic quantities for 2D S(alpha,beta) data sets.
      %   T = THERMOCHEMISTRY(Sab) computes thermodynamic quantities.
      %   The function returns T as an array of iData objects:
      %     entropy                           S [eV/K/cell]
      %     internal_energy                   U [eV/cell]
      %     Helmholtz_energy                  F [eV/cell]
      %     heat_capacity at constant volume Cv [eV/K/cell]
      %   When missing, the density of states (gDOS) is estimated.
      %   When no output is used, a plot is shown.
      %
      %   T = THERMOCHEMISTRY(sab, T)
      %   T = THERMOCHEMISTRY(sab, Tmin:Tmax) computes using given temperature 
      %   range in K. Default is 0:500 [K].
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); t=thermochemistry(sab); ...
      %   numel(t) == 4
      % See also: iData_Sab/dos, iData_vDOS/thermochemistry
      
      if nargin < 2, T=[]; end
      if nargin < 3, options=[]; end
      if isempty(T)
        T = 1:500;  % default
      end
      
      if nargout == 0 || ~isempty(strfind(options, 'plot'))
        options = [ options ' newplot' ];
      end
      s = iData_Sqw2D(s);
      t = thermochemistry(s, T, options);
    end % thermochemistry
    
    function m = moments(data, varargin)
      % MOMENTS S(q,w) moments/sum rules (harmonic frequencies).
      %   M = MOMENTS(Sab) computes the structure factor (moment 0), recoil 
      %   energy (moment 1) and the collective, harmonic and mean energy transfer
      %   dispersions.
      %
      %   The result is given as an iData array with data sets:
      %   S(q) = \int S(q,w) dw = <S(q,w)>             structure factor [moment 0]
      %   Er   = \int w*S(q,w) dw = <wS(q,w)> = h2q2/2M   recoil energy [moment 1]
      %   Wc   = sqrt(2kT*Er/S(q))                collective/isothermal dispersion
      %   Wl                                      harmonic/longitudinal excitation
      %   Wq   = 2q*sqrt(kT/S(q)/M)                           mean energy transfer
      %   M2   = <w2S(q,w)>                                             [moment 2]
      %   M3   = <w3S(q,w)>                                             [moment 3]
      %   M4   = <w4S(q,w)>                                             [moment 4]
      %   When no output is used, a plot is shown.
      %
      %   M = MOMENTS(Sab, m, T, classical) computes the moments using given
      %   molar weight m [g/mol], temperature T [K] and 'classical' flag
      %   0 for non symmetric S(a,b) [with Bose, from exp.], 1 for symmetric (from MD).
      %   M = MOMENTS(Sab, 'M', m, 'T', T, 'classical', classical)
      %   Input arguments can be given in order, or with name-value pairs, or as a 
      %   structure with named fields.
      %
      % References: 
      %   Helmut Schober, Journal of Neutron Research 17 (2014) pp. 109
      %   Lovesey, Theory of Neutron Scattering from Condensed Matter, Vol 1, p180 eq. 5.38 (w0)
      %   J-P.Hansen and I.R.McDonald, Theory of simple liquids Academic Press New York 2006.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); m=moments(sab); ...
      %   numel(m)==9 && all(arrayfun(@size, m, 1)==95)
      % See also: iData_Sqw2D/moments
      
      m = moments(iData_Sqw2D(data), varargin{:});
      if nargout == 0
        fig=figure; h  =plot(m(1:6),'legend'); set(fig, 'NextPlot','new');
      end
    end % moments
    
    function s = symmetrize(s0)
      % SYMMETRIZE Extend the S(alpha,beta) in both beta/energy sides.
      %   Ssym = SYMMETRIZE(Sab) combines S(alpha,beta) and S(alpha,-beta), which
      %   is thus symmetric in beta(energy): Ssym(alpha,beta) = Ssym(alpha,-beta)
      %
      %   The incoming data set should NOT contain the Bose factor, that is it
      %   should be 'classical'. To obtain a 'classical' S(alpha,beta) from an 
      %   experiment, use first:
      %     deBosify(Sab,T)
      %
      %   The positive energy values in the S(alpha,-beta) map correspond to Stokes 
      %   processes i.e. material gains energy, and neutrons loose energy when 
      %   down-scattered. Neutron up-scattering corresponds to anti-Stokes processes.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); s=symmetrize(sab); ...
      %   all(size(s) == [95 3999])
      % See also: iData_Sab/Bosify, iData_Sab/deBosify, 
      %           iData_Sab/dynamic_range, iData_Sab/scattering_cross_section
      
      s = Sab(symmetrize(Sqw(s0)));
      title(s, [  'symmetrize(' title(s0) ')' ]);
      if nargout == 0
        fig=figure; h  =plot(log10(s)); set(fig, 'NextPlot','new');
      end
    end % symmetrize
    
    function s = Bosify(s0, varargin)
      % BOSIFY Apply the 'Bose' factor (detailed balance) to a classical data set.
      %   SabT = BOSIFY(Sab, T) applies the Bose factor to a symmetric S(alpha,beta) 
      %   The initial data set should obey S*=S(a,b) = S(a,-b) ie be 'classical'. 
      %   The resulting data set is 'quantum/experimental' and satisfies the 
      %   detailed balance. It contains the temperature T effects (modes population).
      %   When not given or empty, the temperature T [K] is searched in the object.
      %
      %   Conventions:
      %   beta = (Ef-Ei)/kT = -hw/kT  energy gained by the neutron, unitless
      %     beta < 0, neutron looses energy, down-scattering (Stokes)
      %     beta > 0, neutron gains energy,  up-scattering (anti-Stokes)
      %
      %     S(q,-w) = exp(-hw/kT) S(q,w)
      %     S(q,w)  = exp( hw/kT) S(q,-w)
      %     S(q,w)  = Q(w) S*(q,w) with S*=classical limit and Q(w) defined below.
      %   For beta > 0, S(q,-beta) > S(q,beta)
      %
      %   SabT = BOSIFY(Sab, T, 'method') specifies the semi-classical correction, 
      %   Q, aka 'quantum' correction factor, as:
      %     Q = exp(hw_kT/2)                 'Schofield' or 'Boltzmann'
      %     Q = hw_kT./(1-exp(-hw_kT))       'harmonic'  or 'Bader'
      %     Q = 2./(1+exp(-hw_kT))           'standard'  or 'Frommhold' (default)
      %   The 'Boltzmann' correction leads to a divergence of the S(q,w) for e.g. 
      %   w above few 100 meV. The 'harmonic' correction provides a reasonable 
      %   correction but does not fully avoid the divergence at large energies.
      %
      %   Bose factor: n(w) = 1./(exp(w*11.605/T) -1) ~ exp(-w*11.605/T)
      %                w in [meV], T in [K]
      %
      % References:
      %  B. Hehr, http://www.lib.ncsu.edu/resolver/1840.16/7422 PhD manuscript (2010).
      %  S. A. Egorov, K. F. Everitt and J. L. Skinner. J. Phys. Chem., 103, 9494 (1999).
      %  P. Schofield. Phys. Rev. Lett., 4, 239 (1960).
      %  J. S. Bader and B. J. Berne. J. Chem. Phys., 100, 8359 (1994).
      %  T. D. Hone and G. A. Voth. J. Chem. Phys., 121, 6412 (2004).
      %  L. Frommhold. Collision-induced absorption in gases, 1 st ed., Cambridge
      %    Monographs on Atomic, Molecular, and Chemical Physics, Vol. 2,
      %    Cambridge Univ. Press: London (1993).
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); sb=Bosify(symmetrize(sab)); ...
      %   all(size(sb) == [95 3999])
      % See also: iData_Sab/deBosify, iData_Sab/symmetrize, iData_Sqw2D
      
      s = iData_Sab(Bosify(iData_Sqw2D(s0), varargin{:}));
      s.Title = [ 'Bosify(' s0.Title  ')' ];
      set(s,'classical',   0); 
      label(s,'classical','[0=from measurement, with Bose factor included, 1=from MD, symmetric]');
      if nargout == 0
        fig=figure; h  =plot(log10(s)); set(fig, 'NextPlot','new');
      end
    end % Bosify
    
    function s = deBosify(s0, varargin)
      % DEBOSIFY Remove the 'Bose' factor from an experimental/quantum data set.
      %   Sab=DEBOSIFY(SabT, T, method) The initial data set is 'quantum/experimental'
      %   and satisfies the detailed balance. The resulting data set obeys 
      %   S*=S(a,b) = S(a,-b), i.e. is 'classical'. It suppresses the temperature 
      %   effect (population).
      %   When not given or empty, the temperature T [K] is searched in the object.
      %
      %   Conventions:
      %   beta = (Ef-Ei)/kT = -hw/kT  energy gained by the neutron, unitless
      %     beta < 0, neutron looses energy, down-scattering (Stokes)
      %     beta > 0, neutron gains energy,  up-scattering (anti-Stokes)
      %
      %     S(q,-w) = exp(-hw/kT) S(q,w)
      %     S(q,w)  = exp( hw/kT) S(q,-w)
      %     S(q,w)  = Q(w) S*(q,w) with S*=classical limit and Q(w) defined below.
      %   For beta > 0, S(q,-beta) > S(q,beta)
      %               
      %   Sab = DEBOSIFY(SabT, T, 'method') specifies the semi-classical correction, 
      %   Q, aka 'quantum' correction factor, as:
      %     Q = exp(hw_kT/2)                 'Schofield' or 'Boltzmann'
      %     Q = hw_kT./(1-exp(-hw_kT))       'harmonic'  or 'Bader'
      %     Q = 2./(1+exp(-hw_kT))           'standard'  or 'Frommhold' (default)
      %   The 'Boltzmann' correction leads to a divergence of the S(q,w) for e.g. 
      %   w above few 100 meV. The 'harmonic' correction provides a reasonable 
      %   correction but does not fully avoid the divergence at large energies.
      %
      %   Bose factor: n(w) = 1./(exp(w*11.605/T) -1) ~ exp(-w*11.605/T)
      %                w in [meV], T in [K]
      %
      % References:
      %  B. Hehr, http://www.lib.ncsu.edu/resolver/1840.16/7422 PhD manuscript (2010).
      %  S. A. Egorov, K. F. Everitt and J. L. Skinner. J. Phys. Chem., 103, 9494 (1999).
      %  P. Schofield. Phys. Rev. Lett., 4, 239 (1960).
      %  J. S. Bader and B. J. Berne. J. Chem. Phys., 100, 8359 (1994).
      %  T. D. Hone and G. A. Voth. J. Chem. Phys., 121, 6412 (2004).
      %  L. Frommhold. Collision-induced absorption in gases, 1 st ed., Cambridge
      %    Monographs on Atomic, Molecular, and Chemical Physics, Vol. 2,
      %    Cambridge Univ. Press: London (1993).
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); sb=Bosify(symmetrize(sab));
      %          sab0=deBosify(sb); mean(sab-sab0,0) < 1e-15
      % See also: iData_Sab/Bosify, iData_Sab/symmetrize, iData_Sqw2D
      
      s = iData_Sab(deBosify(iData_Sqw2D(s0), varargin{:}));
      s.Title = [ 'deBosify(' s0.Title  ')' ];
      set(s,'classical',   1,'[0=from measurement, with Bose factor included, 1=from MD, symmetric]');
      if nargout == 0
        fig=figure; h  =plot(log10(s)); set(fig, 'NextPlot','new');
      end
    end % deBosify
    
    function [s2] = dynamic_range(s, varargin)
      % DYNAMIC_RANGE Crop the S(alpha,beta) to the available dynamic range.
      %   Sab_Ei = DYNAMIC_RANGE(Sab, Ei) crops the S(alpha,beta) for given 
      %   incident neutron energy Ei [meV].
      %   The dynamic range is defined from the momentum and energy conservation laws:
      %     Ef         = Ei - w                                is positive
      %     cos(theta) = (Ki.^2 + Kf.^2 - q.^2) ./ (2*Ki.*Kf)  is within [-1:1]
      %   The dataset is set to zero outside the dynamic range.
      %
      %   The incident neutron energy can be computed using:
      %   Ei = 2.0721*Ki^2 = 81.8042/lambda^2 with Ki in [Angs-1] and lambda in [Angs]
      %
      %   Conventions:
      %   beta = (Ef-Ei)/kT = -hw/kT  energy gained by the neutron, unitless
      %     beta < 0, neutron looses energy, down-scattering (Stokes)
      %     beta > 0, neutron gains energy,  up-scattering (anti-Stokes)
      %
      %   Sab_Ei = DYNAMIC_RANGE(Sab, Ei, ANGLES) also restricts the
      %   detection area 'phi' from min(ANGLES) to max(ANGLES) [in deg].
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); d=dynamic_range(symmetrize(sab), 14.8, [-20 135]); ...
      %   all(size(d)==[95 3999]) && numel(find(double(d)))==96641
      % See also: iData_Sab/Bosify, iData_Sab/deBosify, iData_Sab/symmetrize, iData_Sab/scattering_cross_section

      s2 = dynamic_range(iData_Sqw2D(s), varargin{:});
      s2 = iData_Sab(s2);
      if nargout == 0
        fig=figure; h  =subplot(log10([ s2 ]), 'view2 tight'); set(fig, 'NextPlot','new');
      end
    end % dynamic_range
    
    function sigma = scattering_cross_section(s, varargin)
      % SCATTERING_CROSS_SECTION Total neutron scattering cross section.
      %   SIGMA = SCATTERING_CROSS_SECTION(Sab, Ei) computes the total neutron 
      %   scattering cross section from S(alpha,beta) for given incoming neutron 
      %   energy Ei [meV]. The S(alpha,beta) should be the non-classical
      %   dynamic structure factor (i.e. quantum, with Bose fator). 
      %   The cross section SIGMA is returned as a scalar or vector.
      %
      %   Such data sets are obtained from e.g. Xray and neutron scattering 
      %   experiments on isotropic density materials (liquids, powders, amorphous
      %   systems). 
      %
      %   Data sets from analytical models and molecular dynamics simulations must 
      %   be symmetrised in energy, and the detailed balance must be applied to 
      %   take into account the material temperature on the inelastic part.
      %
      %   The incident neutron energy is given in [meV], and may be computed:
      %     Ei = 2.0721*Ki^2 = 81.8042/lambda^2 with Ki in [Angs-1] and lambda in [Angs]
      %     
      %   The S(alpha,beta) is first restricted to the achievable dynamic range:
      %     Ef         = Ei - w                                is positive
      %     cos(theta) = (Ki.^2 + Kf.^2 - q.^2) ./ (2*Ki.*Kf)  is within [-1:1]
      %   and then integrated as XS = 1/2Ki^2 \int q S(q,w) dq dw
      %
      %   The computed values must then be multiplied by the tabulated bound 
      %   cross-sections [barn] from e.g. Sears, Neut. News 3 (1992) 26.
      %
      %   SIGMA = SCATTERING_CROSS_SECTION(Sab, Ei, M) uses the given weight M for 
      %   the scattering unit in [g/mol], used to multiply the cross section
      %   by the estimated Debye-Waller-like factor so that it equals
      %   A/(A+1)]^2 at Ei=1eV to gradually go from the bound (thermal) to the free 
      %   cross section at 1 eV. The threshold of 1eV is used by e.g. OpenMC.
      %     W  = 2e-3*(log(M)-log(M+1));
      %     DW = exp(W*Ei) = exp(2e-3*(log(M)-log(M+1))*Ei)
      %   Above the epithermal energy threshold, the DW factor is kept fixed. 
      %   For a poly-atomic scatterer, the effective mass is computed by weighting
      %   with the bound cross sections for elements A, e.g.
      %     r = sqrt(sum((A/(A+1))^2 * sigma_bound(A)) / sum(sigma_bound(A)));
      %     M = r/(1-r)
      %   For instance, for H2O (twice A=1 sigma=80.2 and A=18 sigma=4.2):
      %     r = sqrt((2*(1/(1+1))^2*80.2+(18/(18+1))^2*4.2)/(2*80.2+4.2)) = 0.52
      %     M = r/(1-r) = 1.06 i.e. scattering is mostly the hydrogen one.
      %   WARNING: this factor is NOT the Debye-Waller factor exp(-<u2>Q2) !
      %   When M is given empty, it is searched as 'weight' or 'mass' is the object.
      %   Default M is 0, i.e. the Debye-Waller-like factor is not taken into account.
      %
      %   A classical S(a,b) obeys S(a,b) = S(a,-b).
      %   The non classical S(a,b), needed by this function, can be obtained from a 
      %   classical S(a,b) (which is symmetric in beta) with e.g.:
      %     extend to +/- energy range
      %       s = symmetrize(s); 
      %     apply detailed balance (Bose factor). Omit T if you do not know it.
      %       s = Bosify(s, T);
      %
      %   The positive energy values in the S(q,w) map correspond to Stokes processes, 
      %   i.e. material gains energy, and neutrons loose energy when down-scattered.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); ...
      %          sigma = scattering_cross_section(Bosify(symmetrize(sab)), 14.6); ...
      %          abs(sigma*15.4 - 7.1256) < 1e-2
      % See also: iData_Sab/Bosify, iData_Sab/deBosify, iData_Sab/symmetrize, iData_Sab/dynamic_range
      
      sigma = scattering_cross_section(iData_Sqw2D(s), varargin{:});
      if nargout == 0
        fig=figure; h  =plot(sigma); set(fig, 'NextPlot','new');
      end
    end % scattering_cross_section
    
    function sq = structure_factor(s)
      % STRUCTURE_FACTOR Structure factor.
      %   Sq = STRUCTURE_FACTOR(Sab) computes the structure factor, i.e. the integral
      %   of the dynamic structure factor along the energy axis. It is representative
      %   of the material structure.
      %   Its Fourier transform is correlated with the pair distribution function g(r).
      %
      %   This function is basically a call to trapz(s,1)
      %
      % References: Fischer, Barnes and Salmon, Rev Prog Phys 69 (2006) 233
      %
      % Example: sq=structure_factor(iData_Sab('SQW_coh_lGe.nc',72.6,1235)); ...
      %   size(sq,1)==95
      % See also: trapz, iData/trapz, iData_Sqw2D/structure_factor
      
      sq = structure_factor(iData_Sqw2D(s));
      if nargout == 0
        fig=figure; h  =plot(sq); set(fig, 'NextPlot','new');
      end
    end % structure_factor
    
    function [inc, inc1, multi] = incoherent(s, varargin)
      % INCOHERENT Multi-phonon incoherent approximation.
      %   Sinc = INCOHERENT(Sab) computes the multi-phonon incoherent S(a,b) 
      %   from the density of states in the incoherent gaussian approximation.
      %
      %   This implementation is in principle exact for an isotropic monoatomic
      %   material e.g. a liquid or powder.
      %   This methodology is equivalent to the LEAPR module of NJOY ("phonon expansion")
      %   to compute S(alpha,beta) from a vibrational density of states.
      %   It must be noted that an incoherent scattering-law does not by essence
      %   contain any "phonon", so this labeling may be erroneous.
      %
      %   [Sinc, Sinc1, Smulti] = INCOHERENT(Sab, Q, T, M, N, DW)
      %   [Sinc, Sinc1, Smulti] = INCOHERENT(Sab, 'q', Q, 'T', T, 'm', M, 'n', N, 'DW', DW)
      %   specifies additional arguments as:
      %     Q:  the momentum axis [Angs-1, vector]
      %     T:  temperature [K]
      %     M:  mass of the scattering unit [g/mol]
      %     N:  number of iterations in the series expansion, e.g. 5
      %     DW: Debye-Waller coefficient gamma=<u^2> [Angs^2] e.g. 0.005
      %         The Debye-Waller function is      2W(q)=gamma*q^2
      %         The Debye-Waller factor   is exp(-2W(q))
      %   Missing arguments (or given as [] empty), are searched in the object.
      %   The total 'Sinc', so-called single one-phonon 'Sinc1', and multi-phonon
      %   'Smulti' (orders >=2) are returned.
      %
      %   For a poly-atomic material with a set of non-equivalent atoms with relative 
      %   concentration Ci, mass Mi and bound scattering cross section sigma_i, 
      %   one should use:
      %     sigma = sum_i{Ci sigma_i}                       weighted cross section
      %     m     = sum_i{Ci sigma_i}/sum_i{Ci sigma_i/Mi}  weighted mass
      %
      %   If you wish to obtain the intermediate scattering function I(q,t), use
      %   [Sinc, Iqt] = incoherent(dos(Sab), ...). These should be e.g. multiplied
      %   by the neutron scattering bound cross section 'sigma_inc' [barns].
      %
      %   You may estimate the coherent scattering law by e.g.
      %     coherent(Sqw(Sab), SQ) 
      %   where SQ is a 1D structure factor (vector/iData/iFunc) or a single 
      %   d-spacing [Angs] for Percus-Yevick.
      %
      % References:
      %   H. Schober, Journal of Neutron Research 17 (2014) 109–357
      %     DOI 10.3233/JNR-140016 (see esp. pages 328-331)
      %   V.S. Oskotskii, Sov. Phys. Solid State 9 (1967), 420.
      %   A. Sjolander, Arkiv for Fysik 14 (1958), 315.
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); inc=incoherent(sab); ...
      %   all(size(inc)==[100 4000])
      % See also: iData_Sab/dos, iData_Sab/multi_phonons
      
      [inc,inc1,multi] = incoherent(Sqw(s), varargin{:});
      inc   = Sab(inc);
      inc1  = Sab(inc1);
      multi = Sab(multi);
      if nargout == 0
        fig=figure; 
        h  =subplot(log10([inc inc1 multi]), 'view2'); 
        set(fig, 'NextPlot','new');
      end
    end % incoherent
    
    function [G1, G2, g] = multi_phonons(s, varargin)
      % MULTI_PHONONS Multi-phonon density-of-states.
      %   G = MULTI_PHONONS(Sab) computes the 'generalized' neutron weighted 
      %   density of states (gDOS) from given S(a,b).
      %   This implementation is in principle exact for an isotropic monoatomic material,
      %   e.g. a liquid or powder. The total gDOS [p>=1] G is returned.
      %   The gDOS is the 'experimental' density of states, which includes the
      %   dos(Sab) added with multi-phonon estimates.
      %   This methodology is a complete rewrite of the MUPHOCOR code.
      %
      %   [G,MULTI,dos] = MULTI_PHONONS(Sab, Ki, T, m, n, ...) 
      %   [G,MULTI,dos] = MULTI_PHONONS(Sab, 'Ki',Ki, 'T',T, ...)
      %   specifies additional arguments as:
      %     Ki:   incident wavevector [Angs-1]. Ki=2*pi/lambda=0.695*sqrt(Ei)
      %     T:    temperature [K]
      %     m:    mass [g/mol]
      %     phi:  detector angles, min and max [deg]
      %     n:    number of iterations in the series expansion, e.g. 5
      %     DW: Debye-Waller coefficient gamma=<u^2> [Angs^2] e.g. 0.005
      %         The Debye-Waller function is      2W(q)=gamma*q^2
      %         The Debye-Waller factor   is exp(-2W(q))
      %   Missing arguments (or given as [] empty), are searched in the object.
      %   The additional returned argument 'MULTI' holds the pure multi-phonon 
      %   terms gDOS [p>=2], and D is the DOS (all 1D with energy axis in [meV]).
      %
      % Reference:
      %   H. Schober, Journal of Neutron Research 17 (2014) 109–357
      %     DOI 10.3233/JNR-140016 (see esp. pages 328-331)
      %   V.S. Oskotskii, Sov. Phys. Solid State 9 (1967), 420.
      %   A. Sjolander, Arkiv for Fysik 14 (1958), 315.
      %   W. Reichardt, MUPHOCOR Karlsruhe Report 13.03.01p06L (1984)
      %
      % Example: sab=iData_Sab('SQW_coh_lGe.nc',72.6,1235); m=multi_phonons(sab); ...
      %   size(m, 1)==4000
      % See also: iData_Sab/dos, iData_Sab/incoherent
      
      g  = dos(s);
      G1 = multi_phonons(g, varargin{:});
      G2 = plus(G1(2:end)); title(G2, 'gDOS [multi]'); 
      G1 = plus(G1);        title(G1, 'gDOS [single+multi]'); 
      if nargout == 0
        fig=figure; 
        h  =plot([ G1 G2 ]); 
        set(fig, 'NextPlot','new');
      end
    end % multi_phonons
    
  end % methods
  
end
