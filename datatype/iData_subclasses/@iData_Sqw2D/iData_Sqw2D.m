classdef iData_Sqw2D < iData
  % IDATA_SQW2D Create a 2D S(q,w) data set (iData flavour).
  %   S = IDATA_SQW2D(FILENAME) imports the given file as a 2D S(q,w). The
  %   initial FILENAME axes may be any set of q, w, alpha, beta, time-of-flight,
  %   and angle. The proper conversion to (q,w) space is performed.
  %
  %   S = IDATA_SQW2D(iData_object) converts the given iData object or flavour
  %   to an S(q,w) dataset.
  %
  %   The IDATA_SQW2D class is a 2D data set holding a S(q,w) dynamic structure factor
  %   aka scattering function/law.
  %   The first  axis (rows)    is the Momentum transfer [Angs-1] (wavevector). 
  %   The second axis (columns) is the Energy   transfer [meV].
  %
  %   This quantity usually derives from the double differential neutron cross section
  %
  %      d2 sigma 
  %     ---------   = N sigma /4pi kf/ki S(q,w)
  %     dOMEGA dEf
  %
  %   and it is specific to materials.
  %
  % Conventions:
  % w = omega = Ei-Ef = energy lost by the neutron [meV]
  %     omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
  %     omega < 0, neutron gains energy, anti-Stokes
  %
  % Useful methods for this iData flavour:
  %
  % methods(iData_Sqw2D)
  %   all iData methods can be used.
  %
  % iData_Sqw2D(s)
  %   convert input [e.g. a 2D iData object] into an iData_Sqw2D to give access to
  %   the methods below.
  %
  % SQW = ddcs2Sqw(s)
  %   convert a double differential neutron scattering cross section [Kf/Ki] to a S(q,w) 
  %
  % ddcs = Sqw2ddcs(s)
  %   convert a S(q,w) to a double differential neutron scattering cross section [Kf/Ki]
  %
  % spw = qw2phiw(s, lambda)
  %   Convert a S(q,w) into a S(phi,w) iData (scattering angle)
  %
  % sqt = qw2qt(s, lambda)
  %   Compute S(q,tof) from S(q,w) for given wavelength [Angs]
  %
  % [spt,spc] = qw2phit(s, lambda)
  %   Compute S(phi,tof) from S(q,w) for given wavelength [Angs]. 
  %   Also return the S(phi,channel) as 2nd arg.
  %
  % sab = Sab(s, M, T)
  %   Compute S(alpha,beta) from S(q,w) for given mass and temperature,
  %   suitable for nuclear data bases (ENDF, etc).
  %
  % d   = dos(s)
  %   Compute the vibrational density of states.
  %
  % t   = thermochemistry(s)
  %   Compute and display thermochemistry quantities from the density of states.
  %
  % m   = moments(s)
  %   Compute the S(q,w) moments/sum rules (harmonic frequencies).
  %
  % sym = symmetrize(s)
  %   Extend the S(|q|,w) in both energy sides.
  %
  % sb  = Bosify(s)
  %   Apply the 'Bose' factor (detailed balance) to a classical data set.
  %
  % s   = deBosify(sb)
  %   Remove Bose factor (detailed balance) from an 'experimental/quantum' data set.
  %
  % p   = parseparams(s)
  %   Search for physical quantities in S(q,w) data set..
  %
  % xs  = scattering_cross_section(s)
  %   Compute the total integrated scattering cross section
  %
  % dr  = dynamic_range(s, Ei, angles)
  %   Compute the dynamic range restricted to given incident energy and detector angles
  %
  % sq  = structure_factor(s)
  %   Compute the structure factor S(q)
  %
  % [inc, multi] = incoherent(s, q, T, m, n)
  %   Compute an estimate of the incoherent neutron scattering law in the 
  %   gaussian approximation (Sjolander)
  %
  % [coh] = coherent(inc, sq)
  %   Compute an estimate of the coherent S(q,w) from an incoherent S(q,w) and 
  %   a structure factor (Skold)
  %
  % [gDOS_total,gDOS_multi, gDOS_1]     = multi_phonons(s)
  %   Compute the integrated multi-phonon DOS terms from an initial density of 
  %   states (Sjolander)
  %
  % g = muphocor(s)
  %   Compute the gDOS, vDOS and multi-phonon terms using MUPHOCOR (same as 
  %   multi_phonons method)
  %
  % saveas(s, filename, 'McStas'|'Sqw'|'inx'|'spe')
  %   Save the S(q,w) as a McStas Sqw, INX or ISIS SPE file format
  %
  % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); isa(s, 'iData_Sqw2D')
  % See also: iData, iData_Sab, iData_vDOS, iFunc_Sqw2D

  properties
  end
  
  methods
    % main instantiation method
    function obj = iData_Sqw2D(s)
      % IDATA_SQW2D Create the iData_Sqw2D subclass for 2D S(q,w).
      %   S = IDATA_SQW2D(D) Convert D into an iData_Sqw2D object so that 
      %   specific neutron S(q,w) methods can be used. 
      %   The data D can be a 2D iData object holding a S(q,w), S(phi,t), 
      %   S(alpha,beta) or S(phi,w) or file name with such data.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); isa(s,'iData_Sqw2D')
      
      obj = obj@iData;
      
      if ~nargin, return; end  % empty object
      
      % convert/test
      if     isa(s, mfilename)   m = s;
      elseif isa(s, 'iData_Sab') m = Sab2Sqw(s);
      else
        m = Sqw_check(s, 'qw'); % check and possibly convert to Sqw
        if ~isa(m, 'iData') || any(isempty(m)) || any(ndims(m) ~= 2)
          error([ mfilename ': the given input ' class(s) ' ndims=' num2str(ndims(s)) ' does not seem to be convertible to iData_Sqw2D.' ])
        end
      end
      
      % copy all properties
      obj = copy_prop(obj, m);
      obj.Title = [  mfilename '(' obj.Title ')' ];
 
    end % iData_Sqw2D constructor
    
    % parameters (search for parameters in iData)
    function parameters = parseparams(s, option)
      % PARSEPARAMS Search for physical quantities in object.
      %   P = PARSEPARAMS(Sqw) searches in object Sqw for physical parameters.  
      %   The initial object is updated with a 'parameters' property, and the 
      %   parameters are returned as a structure P.
      %
      %   PARSEPARAMS(Sqw, 'Bose') compute the Temperature from the Bose factor 
      %   (detailed balance). The data set should extend on both positive and 
      %   negative beta sides.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); p=parseparams(s); isstruct(p)
      [s,parameters,fields] = Sqw_parameters(s);
      if nargin > 1 && strcmpi(option, 'bose')
        T = Sqw_getT(s, 'T', 'Bose');
        if ~isempty(T)
          parameters.Temperature = T;
        end
      end
    end
    
    function f = iData(self)
      % IDATA  Convert a iData_Sqw2D to iData.
      %   S = IDATA(Sqw) returns a pure iData object. The iData_Sqw2D methods are 
      %   then not accessible anymore.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); d=iData(s); isa(d,'iData')
      f = [];
      for index=1:numel(self)
        f1   = copy_prop(iData, self(index));
        label(f1, 0, [  'iData' '(' label(self(index), 0) ')' ]);
        f = [ f f1 ];
      end
    end
    
    % sound_velocity ?
    % MSD ?
    % diffusion constant ?
    % compressibility ?
    % g(r) pdf
    
    function [inc, inc1, multi] = incoherent(s, varargin)
      % INCOHERENT Multi-phonon incoherent approximation.
      %   Sinc = INCOHERENT(Sqw) computes the multi-phonon incoherent S(q,w) 
      %   from the density of states in the incoherent gaussian approximation.
      %
      %   This implementation is in principle exact for an isotropic monoatomic
      %   material e.g. a liquid or powder.
      %   This methodology is equivalent to the LEAPR module of NJOY ("phonon expansion")
      %   to compute S(alpha,beta) from a vibrational density of states.
      %   It must be noted that an incoherent scattering-law does not by essence
      %   contain any "phonon", so this labeling may be erroneous.
      %
      %   [Sinc, Sinc1, Smulti] = INCOHERENT(Sqw, Q, T, M, N, DW)
      %   [Sinc, Sinc1, Smulti] = INCOHERENT(Sqw, 'q', Q, 'T', T, 'm', M, 'n', N, 'DW', DW)
      %   specifies additional arguments as:
      %     Q:  the momentum axis [Angs-1, vector]
      %     T:  temperature [K]
      %     M:  mass of the scattering unit [g/mol]
      %     N:  number of iterations in the series expansion, e.g. 5
      %     DW: Debye-Waller coefficient gamma=<u^2> [Angs^2] e.g. 0.005
      %         The Debye-Waller function is      2W(q)=gamma*q^2
      %         The Debye-Waller factor   is exp(-2W(q))
      %   Missing arguments (or given as [] empty), are searched in the object.
      %   The total 'Sinc', so-called single one-phonon 'Sinc1', and multi-phonon
      %   'Smulti' (orders >=2) are returned.
      %
      %   For a poly-atomic material with a set of non-equivalent atoms with relative 
      %   concentration Ci, mass Mi and bound scattering cross section sigma_i, 
      %   one should use:
      %     sigma = sum_i{Ci sigma_i}                       weighted cross section
      %     m     = sum_i{Ci sigma_i}/sum_i{Ci sigma_i/Mi}  weighted mass
      %
      %   If you wish to obtain the intermediate scattering function I(q,t), use
      %   [Sinc, Iqt] = incoherent(dos(Sqw), ...). These should be e.g. multiplied
      %   by the neutron scattering bound cross section 'sigma_inc' [barns].
      %
      %   You may estimate the coherent scattering law by e.g.
      %     coherent(Sqw, SQ) 
      %   where SQ is a 1D structure factor (vertor/iData/iFunc) or a single d-spacing.
      %
      % Example: s=iData_Sqw2D('D2O_liq_290_coh.sqw.zip'); ...
      %   inc = incoherent(s,'m',20,'T',290); h=plot(log10(inc)); ...
      %   tf=ishandle(h) && isa(inc, 'iData_Sqw2D') && max(inc) > 0
      %
      % See also: iData_Sqw2D/multi_phonons
      
      g     = dos(s, varargin{:});
      inc   = incoherent(g, varargin{:});
      if nargout > 1
        inc1= plus(inc(1:2));   % elastic+1-phonon
        label(inc1, 0, [  'single-phonon' '(' label(s, 0) ')' ]);
      end
      if nargout > 2
        multi = plus(inc(3:end)); % multi-phonon
        label(multi,  0, [  'multi-phonons' '(' label(s, 0) ')' ]);
      end
      inc   = plus(inc);        % total
      label(inc,    0, [  'incoherent' '(' label(s, 0) ')' ]);

      if nargout == 0
        fig=figure; 
        h  =subplot(log10([inc inc1 multi]),'view2'); 
        set(fig, 'NextPlot','new');
      end
    end % incoherent
    
    function [coh] = coherent(self, sq)
      % COHERENT Coherent scattering cross section from incoherent and structure factor.
      %   coh = COHERENT(inc, sq) computes the coherent scattering cross section
      %   from the incoherent 'inc' and structure factor 'sq', in the Skold approximation.
      %   The structure factor can be given as a 1D iData, a vector, an iFunc model
      %   of a single d-spacing [Angs] for Percus-Yevick.
      %   The Skold approximation is:
      %
      %     Scoh(q,w) = Sinc(q/sqrt(S(q)), w) S(q)
      %
      %   The result should be e.g. multiplied by the neutron scattering bound cross 
      %   section 'sigma_coh' [barns].
      %
      %   The structure factor can be obtained from the 'structure_factor' method 
      %   for iFunc_Sqw2D and iData_Sqw2D classes. A crude estimate can be obtained
      %   from the S(q,w) moments as:
      %     M2   = \int w^2 S(q,w) dw
      %     wl   = characteristic harmonic/longitudinal excitation dispersion
      %     S(q) = M2./(wl-min(wl))^2/sqrt(q)
      %   This estimate does NOT require the elastic line, and is thus not sensitive to
      %   experimental data reduction (container contribution, etc) around w=0.
      %
      % Reference: K. Skold, Phys. Rev. Lett. 19, 1023 (1967).
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); sq=structure_factor(s); ...
      %   inc=incoherent(s,'m',72.6,'T',290); coh=coherent(inc, sq); ...
      %   isa(coh, 'iData_Sqw2D') && max(inc) > 0
      
      if nargin < 2, sq = []; end
      
      % make sure we use a regular grid
      inc = meshgrid(self,'vector');
      q   = axis(inc, 1);
      
      if isempty(sq)
        sq = [ 3 .2]; % will use Percus-Yevick
      end
      if prod(size(sq)) <= 2
        % assume sq is a mean interatomic distance -> Percus-Yevick model
        if isscalar(sq), p = [ double(sq) .2 ];
        else p =double(sq); end
        sq = sf_hard_spheres(p);
      end
      if isa(sq, 'iFunc')
        sq = feval(sq, q);
      end
      if isa(sq, 'iData') && ndims(sq)==1
        sq = interpn(sq, q); % make sure we use same q values in S(q,w) and S(q)
      end
      
      sq = double(sq); sq = sq(:);
      
      % K. Skold, Phys. Rev. Lett. 19, 1023 (1967).
      % Scoh(q,w) = Sinc(q/sqrt(S(q)), w) S(q)
      
      % index method: we directly use index values to avoid interpolations
      % index of new Skold q value in initial Sinc data set
      
      % number of q values in initial and final data sets
      nq = size(self, 1);

      index_q_skold = round( (1:nq)./ sqrt(sq') );
      index_q_skold(index_q_skold < 1)  = 1;
      index_q_skold(index_q_skold > nq) = nq;
      index_q_skold(~isfinite(index_q_skold)) = [];
      
      coh        = copyobj(inc);
      signal_inc = axis(inc, 0);
      signal_coh = zeros(size(signal_inc));
      signal_coh(1:numel(index_q_skold),:) = signal_inc(index_q_skold,:);  % Sinc(q/sqrt(sq),w)
      coh.Signal = signal_coh;
      coh.Name   = 'Coherent, Skold approximation';
      coh.Title  = coh.Name;
      
      % interpolation method: compute the new Sinc with modified q axis
      % qinc = axis(inc, 1)./sqrt(sq);
      % sinc = interpn(s, qinc, w).*sq;
      % interpolate on the initial grid
      % coh  = interpn(coh, q,w);
      
      % get the coherent estimate
      if size(coh,2) == numel(sq)
        sq = sq';
      end
      coh  = coh.*sq;

    end % coherent
    
    function [G,multi,g] = multi_phonons(s, varargin)
      % MULTI_PHONONS Multi-phonon density-of-states.
      %   G = MULTI_PHONONS(Sqw) computes the 'generalized' neutron weighted 
      %   density of states (gDOS) from given S(q,w).
      %   This implementation is in principle exact for an isotropic monoatomic material,
      %   e.g. a liquid or powder. The total gDOS [p>=1] G is returned.
      %   The gDOS is the 'experimental' density of states, which includes the
      %   dos added with multi-phonon estimates.
      %   This methodology is a complete rewrite of the MUPHOCOR code.
      %
      %   [G,MULTI,D] = MULTI_PHONONS(Sqw, Ki, T, m, n, ...) 
      %   [G,MULTI,D] = MULTI_PHONONS(Sqw, 'Ki',Ki, 'T',T, ...)
      %   specifies additional arguments as:
      %     Ki:   incident wavevector [Angs-1]. Ki=2*pi/lambda=0.695*sqrt(Ei)
      %     T:    temperature [K]
      %     m:    mass [g/mol]
      %     phi:  detector angles, min and max [deg]
      %     n:    number of iterations in the series expansion, e.g. 5
      %     DW: Debye-Waller coefficient gamma=<u^2> [Angs^2] e.g. 0.005
      %         The Debye-Waller function is      2W(q)=gamma*q^2
      %         The Debye-Waller factor   is exp(-2W(q))
      %   Missing arguments (or given as [] empty), are searched in the object.
      %   The additional returned argument 'MULTI' holds the pure multi-phonon 
      %   terms gDOS [p>=2], and D is the DOS (all 1D with energy axis in [meV]).
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); G=multi_phonons(s); isa(G, 'iData')
      
      g     = dos(s);
      G     = multi_phonons(g, varargin{:});
      multi = plus(G(2:end)); 
      G     = plus(G); 
      label(G,     0, [  'total gDOS' '(' label(s, 0) ')' ]);
      label(multi, 0, [  'multi_phonons gDOS' '(' label(s, 0) ')' ]);
      if nargout == 0
        fig=figure; 
        h  =plot([ G multi ]); 
        set(fig, 'NextPlot','new');
      end
    end
    
    function g = gdos(self, varargin)
      % GDOS Generalised density of states (gDOS) from a S(q,w).
      %   G = GDOS(Sqw) computes the 'generalized' neutron weighted 
      %   density of states (gDOS) from given S(q,w).
      %   This implementation is in principle exact for an isotropic monoatomic material,
      %   e.g. a liquid or powder. The total gDOS [p>=1] G is returned.
      %   The gDOS is the 'experimental' density of states, which includes the
      %   dos(Sqw) added with multi-phonon estimates.
      %   This call is equivalent to multi_phonons(Sqw).
      %
      %   G = GDOS(Sqw, Ki, T, m, n, ...) specifies additional argments for 
      %   multi_phonons.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); g=gdos(s); isa(g, 'iData')
      % See: iData_Sqw2D/dos, iData_Sqw2D/multi_phonons
      
      g = multi_phonons(self, varargin{:});
    end
    
    function spw = qw2phiw(self, varargin)
      % QW2PHIW Convert a S(q,w) into a S(phi,w) data set (scattering angle).
      %   SPW = QW2PHIW(Sqw) converts a 2D S(q,w) into S(phi,w) where phi is the 
      %   scattering angle [deg].
      %   This function is equivalent to angle(Sqw)
      %
      %   SPW = QW2PHIW(Sqw, lambda) also specifies the neutron wavelength [Angs].
      %   When not given , it is searched in the S(q,w) data set.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); spw=qw2phiw(s); isa(spw, 'iData')
      
      spw = Sqw_q2phi(self, varargin{:});
      label(spw, 0, [  'S(phi, w) qw2phiw (' label(self, 0) ')' ]);
      if nargout == 0
        fig=figure; 
        h  =plot(log10(spw)); 
        set(fig, 'NextPlot','new');
      end
    end
    
    function spw = angle(self, varargin)
      % ANGLE Convert a S(q,w) into a S(phi,w) data set (scattering angle).
      %   SPW = ANGLE(Sqw) converts a 2D S(q,w) into S(phi,w) where phi is the 
      %   scattering angle [deg].
      %   This function is equivalent to qw2phiw(Sqw)
      %
      %   SPW = ANGLE(Sqw, lambda) also specifies the neutron wavelength [Angs].
      %   When not given , it is searched in the S(q,w) data set.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); spw=angle(s); isa(spw, 'iData')
      
      spw = qw2phiw(self, varargin{:});
      if nargout == 0
        fig=figure; 
        h  =plot(log10(spw)); 
        set(fig, 'NextPlot','new');
      end
    end
    
    function sqt = qw2qt(self, varargin)
      % QW2QT Convert a S(q,w) into a S(q,tof) iData (time-of-flight from sample).
      %   SQT = QW2QT(Sqw) converts a S(q,w) into a time-of-flight S(q,tof) where
      %   'tof' is in [s].
      %
      %   SQT = QW2QT(Sqw, lambda) also specifies the neutron wavelength [Angs].
      %   When not given , it is searched in the S(q,w) data set.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); sqt=qw2qt(s); isa(sqt,'iData')
      
      sqt = Sqw_e2t(self, varargin{:});
      label(sqt, 0, [  'S(q, tof) qw2qt [e2t] (' label(self, 0) ')' ]);
      if nargout == 0
        fig=figure; 
        h  =plot(log10(sqt)); 
        set(fig, 'NextPlot','new');
      end
    end
    
    function [spt, spc, spw] = qw2phit(self, varargin)
      % QW2PHIT Convert a S(q,w) into S(phi,tof) iData (scattering angle,ToF).
      %   SPT = QW2PHIT(Sqw) returns S(phi,tof) to reproduce an experimental neutron
      %   time-of-flight measurement using a large detector. The angle 'phi' is
      %   in [deg], and the tof (from sample) is in [s].
      %
      %   SPT = QW2PHIT(Sqw, lambda) also specifies the neutron wavelength [Angs].
      %   When not given , it is searched in the S(q,w) data set.
      %
      %   [SPT, SPC, SPW] = QW2PHIT(sqw) returns in addition the S(phi,tof_channels)
      %   and the S(phi,w) where 'w' is the energy in [meV].
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); spt=qw2phit(s); isa(spt,'iData')
      spw        = Sqw_q2phi(self, varargin{:});
      [spt, spc] = Sqw_e2t(spw, varargin{:});
      if nargout == 0
        fig=figure; 
        h  =plot(log10(spt)); 
        set(fig, 'NextPlot','new');
      end
    end
    
    function ddcs = Sqw2ddcs(s, lambda, inverse)
      % SQW2DDCS Convert a S(q,w) into a double differential cross-section (q,w).
      %   S = SQW2DDCS(Sqw) multiplies S(q,w) by Kf/Ki. In addition, if the initial
      %   dataset S(q,w) is classical, it is converted to a Quantum one by applying
      %   the Bosify method. The resulting dataset should be further multiplied
      %   by the scattering cross-section.
      %
      %   The DDCS is related to S(q,w) by:
      %     d2(sigma)/dOmega/dE = N.sigma Kf/Ki S(q,w)
      %
      %   S = SQW2DDCS(Sqw, lambda) also specifies the neutron wavelength [Angs].
      %   When not given , it is searched in the S(q,w) data set.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); ddcs=Sqw2ddcs(s); isa(ddcs,'iData')
      
      ddcs = [];
      if isempty(s), return; end
      if nargin < 2, lambda = []; end
      if nargin < 3, inverse = false; else inverse = true; end
      if isempty(lambda)
        [s,lambda,distance,chwidth] = Sqw_search_lambda(s);
      else
        [s,~,distance,chwidth] = Sqw_search_lambda(s);
      end
      if isempty(lambda)
        lambda   = 2.36;
        warning([ mfilename ': ' s.Tag ' ' s.Title ' using <wavelength>               =' num2str(lambda) ' [Angs]']);
      end

      SE2V = 437.393377;        % Convert sqrt(E)[meV] to v[m/s]
      V2K  = 1.58825361e-3;     % Convert v[m/s] to k[1/AA]
      K2V  = 1/V2K;
      VS2E = 5.22703725e-6;     % Convert (v[m/s])**2 to E[meV]
      
      Ki   = 2*pi/lambda;
      Vi   = K2V*Ki;
      Ei   = VS2E*Vi.^2;
      % compute final energy
      hw   = axis(s, 2);
      Ef   = Ei - hw;
      kfki = sqrt(Ef./Ei);
      if inverse
        kfki = 1./kfki; % DDCS -> Sqw=ki/kf DDCS
      end
      
      if ~inverse
        % check if initial data set is classical: apply Bose factor
        if (isfield(s,'classical') || ~isempty(findfield(s, 'classical')))
          classical = get(s,'classical');
        else classical = false;
        end
        if ~isempty(classical) && classical(1)
          s = Bosify(s);
        end
      end
      
      ddcs = copyobj(s) .* kfki;
      set(ddcs, 'IncidentWavelength', lambda, 'Wavelength [Angs]');
      
      if inverse
        label(ddcs, 0, [  'S(q, w) ddcs2Sqw' '(' label(s, 0) ')' ]);
      else
        label(ddcs, 0, [  'Kf/Ki*S(q, w) Kf/Ki*(' label(s, 0) ')' ]);
      end
    
    end % Sqw2ddcs
    
    function self = ddcs2Sqw(ddcs, varargin)
      % DDCS2SQW Convert a double differential cross-section (q,w) into S(q,w).
      %   S = DDCS2SQW(Sqw) divides the DDCS by Kf/Ki. The resulting dataset should
      %   be further divided by the scattering cross-section.
      %
      %   The DDCS is related to S(q,w) by:
      %     d2(sigma)/dOmega/dE = N.sigma Kf/Ki S(q,w)
      %
      %   S = DDCS2SQW(Sqw, lambda) also specifies the neutron wavelength [Angs].
      %   When not given , it is searched in the S(q,w) data set.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); ddcs=Sqw2ddcs(s); s2=ddcs2Sqw(ddcs); all(abs(s-s2) < 1e-3)
      
      if nargin > 1
        self = Sqw2ddcs(ddcs, varargin{:}, 'inverse');
      else
        self = Sqw2ddcs(ddcs, [], 'inverse');
      end
    
    end % qw2ddcs
    
    function s = Sab(self, varargin)
      % SAB    Convert a 2D S(q,w) into an S(alpha,beta). 
      %   sab = SAB(sqw, M, T) transforms a 2D S(q,w) into its S(alpha,beta) 
      %   representation of the dynamic structure factor using unitless momentum
      %   and energy variables defined as a function of temperature T:
      %     alpha= h2q2/2MkT  = (Ei+Ef-2*mu*sqrt(Ei*Ef))/AkT
      %     beta = -hw/kT     = (Ef-Ei)/kT
      %     A    = M/m
      %     mu   = cos(theta) = (Ki.^2 + Kf.^2 - q.^2) ./ (2*Ki.*Kf)
      %   As alpha and beta are defined for a given temperature T, an S(alpha,beta)
      %   can only be used for it.
      %   The molar weight M of the atom/molecule can be given in [g/mol].
      %   When omitted or empty, it is searched as 'weight' or 'mass' in the object.
      %   The temperature T to use can be given in [K]. 1 meV=11.605 K.
      %   When not given or empty, the Temperature is searched in the object.
      %
      %   Conventions:
      %     w = omega = Ei-Ef = energy lost by the neutron [meV]
      %         omega > 0, neutron looses energy, can not be higher than Ei (Stokes)
      %         omega < 0, neutron gains energy, anti-Stokes
      %
      % References: M. Mattes and J. Keinert, IAEA INDC(NDS)-0470, 2005.
      %             R. E. MacFarlane, LA-12639-MS (ENDF-356), 1994.
      %
      % Example: sqw=iData_Sqw2D('SQW_coh_lGe.nc'); sab = Sab(sqw,72.6,1235); ...
      %   h=plot(log(sab)); tf=ishandle(h); ...
      %   close(gcf); isa(sab, 'iData_Sab') && tf

      s = Sqw2Sab(self, varargin{:});  % private
    end
    
    function f = saveas(self, varargin)
      % SAVEAS Save S(q,w) into a file.
      %   F = SAVEAS(Sqw, filename) saves the S(q,w) dataset into format determined
      %   from the filename extension. All iData formats as available, as well as
      %     'sqw'   McStas/McXtrace (for Isotropic_Sqw component)
      %     'inx'   INX/MuPhoCor
      %     'spe'   MSlice Horace
      %   The generated file name F is returned. 
      %   To get a full list of standard WRITER, use: ifitpref formats iswriter
      %
      % F = SAVEAS(Sqw, filename, writer) specifies the desired output format.
      %
      % Example: sqw = iData_Sqw2D('SQW_coh_lGe.nc'); f=saveas(sqw, '','mccode'); ~isempty(dir(f))
      % See also: iData/saveas
      if numel(varargin) >= 2
        switch lower(varargin{2})
        case {'mcstas','sqw','mcxtrace','mccode'}
          f = Sqw_McStas(self, varargin{1});
        case {'inx','nuphocor'}
          spe = Sqw_q2phi(self);  % S(phi, w)
          f = write_inx(spe, varargin{1});
        case {'spe','mslice','horace'}
          spe = Sqw_q2phi(self);  % S(phi, w)
          f = write_spe(spe, varargin{1});
        otherwise
          f = saveas@iData(self, varargin{:});
        end
      else
        f = saveas@iData(self, varargin{:});
      end
      
    end % saveas
    
    function h = plot(self, varargin)
      % PLOT   Display a S(q,w) 2D data set.
      %
      % Example: sqw = iData_Sqw2D('SQW_coh_lGe.nc'); h=plot(sqw); close(h);
      
      if all(size(axis(self,1)) == size(axis(self,0))) ...
      && all(size(axis(self,2)) == size(axis(self,0)))
        h=plot@iData(self, varargin{:});
      else
        h=plot@iData(transpose(self), varargin{:});
      end
    end
    
  end
  
end
