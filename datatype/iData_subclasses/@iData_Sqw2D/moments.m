function sigma=moments(data, varargin)
% MOMENTS S(q,w) moments/sum rules (harmonic frequencies).
%   M = MOMENTS(Sqw) computes the structure factor (moment 0), recoil 
%   energy (moment 1) and the collective, harmonic and mean energy transfer
%   dispersions.
%
%   The result is given as an iData array with data sets:
%   S(q) = \int S(q,w) dw = <S(q,w)>             structure factor [moment 0]
%   Er   = \int w*S(q,w) dw = <wS(q,w)> = h2q2/2M   recoil energy [moment 1]
%   Wc   = sqrt(2kT*Er/S(q))                collective/isothermal dispersion
%   Wl                                      harmonic/longitudinal excitation
%   Wq   = 2q*sqrt(kT/S(q)/M)                           mean energy transfer
%   M2   = <w2S(q,w)>                                             [moment 2]
%   M3   = <w3S(q,w)>                                             [moment 3]
%   M4   = <w4S(q,w)>                                             [moment 4]
%   When no output is used, a plot is shown.
%
%   M = MOMENTS(Sqw, m, T, classical) computes the moments using given
%   molar weight m [g/mol], temperature T [K] and 'classical' flag
%   0 for non symmetric S(a,b) [with Bose, from exp.], 1 for symmetric (from MD).
%   M = MOMENTS(Sqw, 'M', m, 'T', T, 'classical', classical)
%   Input arguments can be given in order, or with name-value pairs, or as a 
%   structure with named fields.
%
% References: 
%   Helmut Schober, Journal of Neutron Research 17 (2014) pp. 109
%   Lovesey, Theory of Neutron Scattering from Condensed Matter, Vol 1, p180 eq. 5.38 (w0)
%   J-P.Hansen and I.R.McDonald, Theory of simple liquids Academic Press New York 2006.
%
% Example: m=moments(iData_Sqw2D('SQW_coh_lGe.nc'), 72.6, 1235); h=subplot(m);...
%   tf=all(ishandle(h)); close(gcf); tf && isa(m,'iData_Sqw2D')

  sigma = []; NL = sprintf('\n');
  if isempty(data), return; end
  p = varargin2struct({'M' 'T' 'classical'}, varargin, true);
  
  % guess when omitted arguments
  if isempty(p.classical) && (isfield(data,'classical') || ~isempty(findfield(data, 'classical')))
    p.classical = get(data,'classical');
  end
  if isempty(p.m) || p.m<=0
    p.m = Sqw_getT(data, {'Masses','Molar_mass','Mass','Weight'});
  end
  if isempty(p.t)
    p.t = Sqw_getT(data);
  end
  
  % check input parameters
  if isempty(p.t) || p.t<=0
    error([ mfilename ': ERROR: Temperature undefined: The data set ' data.Tag ' ' data.Title ' from ' data.Source NL ...
    '    does not have any temperature defined. Use moments(data, M, T, classical).' ]);
  end
  
  kT      = p.t/11.604;   % kbT in meV;
  q       = axis(data,1);
  w       = axis(data,2);
  
  % clean low level data
  i=find(log(data) < -15);
  S.type='()';
  S.subs={ i };
  data = subsasgn(data, S, 0);
  
  sq      = abs(trapz(data,2)); % S(q) from the data itself
  M0      = sq;
  % w2R = 2 kT M1
  % w2R 1/2/kT = wS = M1 and w0^2 = 1/S(q) w2R = 1/S(q) 2 kT M1 = q2 kT/p.m/M0
  M1      = trapz(w.*data,2);    % = h2q2/2/p.m recoil when non-classical, 0 for classical symmetrized
  
  % check if symmetric
  if isempty(p.classical) && abs(mean(M1)) < 1e-6
    p.classical = 1;
  end
  
  if isempty(p.classical)
    error([ mfilename ': ERROR: The data set ' data.Tag ' ' data.Title ' from ' data.Source NL ...
    '   does not provide information about classical/quantum data set.' NL ...
    '   Use moments(data, M, T, classical=0 or 1)' ]);
  else p.classical=p.classical(1);
  end
  
  if ~p.classical && isempty(p.m)
    % try to extract a mass from the recoil
    mn      = 1.674927471E-027; % neutron mass [kg]
    e       = 1.60217662E-019;  % [C]
    HBAR    = 1.05457168e-34;   % Plank/2PI
    kb      = 1.38064852E-023;  % Boltzmann [J/K]
    q2toE   = HBAR*HBAR/2/mn/e*1000*1e20; % = 2.0721 = [Angs^-2] to [meV] 
    C       = e/1000/kb/p.t;
    p.m       = mean(q.*q*q2toE./M1);
    if p.m >= 1 && p.m < 2000
      warning([ mfilename ': INFO: The data set ' data.Tag ' ' data.Title ' from ' data.Source  sprintf('\n') '    Recoil provides a mass M=' num2str(p.m) ' [g/mol]. Wq may be wrong.' ]);
    else
      p.m = [];
    end
  end
  if isempty(p.m)
    warning([ mfilename ': WARNING: Mass undefined: The data set ' data.Tag ' ' data.Title ' from ' data.Source NL ...
    '    does not provide information about the material molar weight. Use Sqw_moments(data, M).' NL ...
    '    Ignoring: The Wq frequency will be empty.' ]);
  end
  M2      = abs(trapz(w.^2.*data,2)); % M2 cl = wc^2
  M3      =     trapz(w.^3.*data,2);
  M4      = abs(trapz(w.^4.*data,2));
  
  % half width from normalized 2nd frequency moment J-P.Hansen and I.R.McDonald 
  % Theory of simple liquids Academic Press New York 2006.
  if ~isempty(p.m) && isnumeric(p.m)
    wq      = 2*q.*sqrt(kT./M0/p.m);  % Lovesey p180 eq. 5.38 = w0
    if ndims(wq) == 2, wq=trapz(wq,2); end
    wq.Title='w_q=q \surd kB T/M S(q) mean energy transfer';
  else
    wq = iData_Sqw2D;
  end
  
  if p.classical
    % all odd moments are 0, even are to be multiplied by 2 when using S(q,w>0)
    % M2 = q.^2.*kT/M
    wc      = sqrt(M2./M0); % sqrt(<w2S>/s(q)) == q sqrt(kT/M/s(q)) collective/isothermal
    m3      = abs(trapz(abs(w).^3.*data,2));
    wl      = m3./M2; % maxima wL(q) of the longitudinal current correlation function ~ wl
  else
    wc      = sqrt(2*kT.*M1./M0); 
    wl      = sqrt(M3./M1); 
  end
  
  % a very crude estimate of S(q) may be obtained from (phenomenological):
  %   M2./(wl-min(wl)).^2 assuming wl == wc
  % or better
  %   M2./(wl-min(wl)).^2./sqrt(q) which works remarkably !
  % and normalize it.
  
  % structure factor estimate from pure inelastic
  sq_approx = M2./(wl-min(wl)).^2./sqrt(q); 
  if ndims(sq_approx) == 2, sq_approx=trapz(sq_approx,2); end
  mn=mean(sq_approx);
  if mn && isfinite(mn)
    sq_approx=sq_approx/mn;
  end
  
  sq.Title='S(q) structure factor';                 ylabel(sq, sq.Title );
  M1.Title='recoil E_r=h^2q^2/2M <wS> 1st moment';  ylabel(M1, M1.Title );
  wc.Title='w_c collective/isothermal dispersion';  ylabel(wc, wc.Title );
  wl.Title='w_l harmonic/longitudinal excitation';  ylabel(wl, wl.Title );
  M2.Title='<w2S> 2nd moment';                      ylabel(M2, M2.Title );
  M3.Title='<w3S> 3rd moment';                      ylabel(M3, M3.Title );
  M4.Title='<w4S> 4th moment';                      ylabel(M4, M4.Title );
  sq_approx.Title = 'S(q) structure factor estimate from pure inelastic';
  ylabel(sq_approx, sq_approx.Title );

  sigma =iData([ sq M1 wc wl wq M2 M3 M4 sq_approx ]);
  
  if nargout == 0
    fig=figure; 
    subplot(sigma(1:6));
    set(fig, 'NextPlot','new');
  end

