function s = structure_factor(s)
% STRUCTURE_FACTOR Compute the structure factor S(q).
%   The structure factor is the integral of the dynamic structure factor along
%   the energy axis. It is representative of the material structure.
%   Its Fourier transform is the pair distribution function g(r).
%
%   This function is basically a call to trapz(s,1)
%
% References: Fischer, Barnes and Salmon, Rev Prog Phys 69 (2006) 233
%
% Example: sq=structure_factor(iData_Sqw2D('SQW_coh_lGe.nc')); isa(sq, 'iData')

  if nargin == 0, return; end

  % handle array of objects
  if numel(s) > 1
    for index=1:numel(s)
      s(index) = feval(mfilename, s(index));
    end
    return
  end
  
  s = Sqw_check(s);
  if isempty(s), return; end

  % compute integral
  s = iData(trapz(s,2)); % on q
  
  % reset axes
  title(s,'S(q)');
  
  if nargout == 0
    fig=figure; 
    plot(s);
    set(fig, 'NextPlot','new');
  end
