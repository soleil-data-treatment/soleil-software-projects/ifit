function s=symmetrize(s0)
% SYMMETRIZE Extend the S(|q|,w) in both energy sides.
%   The resulting S(q,w) is the combination of S(q,w) and S(q,-w), which
%   is thus symmetric in energy:
%     S(q,w) = S(q,-w)
%
%   The incoming data set should NOT contain the Bose factor, that is it
%   should be 'classical'.
%   To obtain a 'classical' S(q,w) from an experiment, use first:
%     deBosify(s, T)
%
%   The positive energy values in the S(q,w) map correspond to Stokes processes, 
%   i.e. material gains energy, and neutrons loose energy when down-scattered.
%
% Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); symmetrize(s); isa(s,'iData_Sqw2D')
% See also: iData_Sqw2D/Bosify, iData_Sqw2D/deBosify, 
%           iData_Sqw2D/dynamic_range, iData_Sqw2D/scattering_cross_section

  if nargin == 0, return; end
  if ~isa(s0, 'iData'), s0=iData(s0); end

  % handle array of objects
  if numel(s0) > 1
    sqw = [];
    for index=1:numel(s)
      sqw = [ sqw feval(mfilename, s0(index)) ];
    end
    s = sqw;
    return
  end
  
  s = copyobj(s0);
  if isempty(s), return; end

  % test if classical
  if isfield(s,'classical') || ~isempty(findfield(s, 'classical'))
    classical = get(s,'classical');
    if ~isempty(classical) && classical(1) == 0
      warning([ mfilename ': WARNING: The data set ' s.Tag ' ' s.Title ' from ' s.Source ' does NOT seem to be classical.' sprintf('\n') ...
      ' It may already contain the Bose factor in which case the symmetrisation will be wrong.' ]);
    end
  end

  % test if the data set has single energy side: much faster to symmetrise
  w = axis(s,2); % should be a row vector
  w0= w;
  if isvector(w) && (all(w(:) >= 0) || all(w(:) <= 0))
    signal    = get(s, 'Signal');
    signal    = [ signal signal ];
    [w,index] = unique([ w0 -w0 ]);
    s.Axis{2} = w;
    signal = signal(:,index); % extend
    set(s,'Signal', signal);
    clear signal
    
    if ~isempty(get(s,'Error'))
       err = get(s, 'Error');
       if all(err(:) > 0 )
         err=[ err ; err ];
         s = set(s, 'Error', err(:,index));
       end
    end
    clear err

    if ~isempty(get(s,'Monitor')) && ~isscalar(get(s, 'Monitor'))
      m = get(s, 'Monitor');
      if all(m(:) > 0 )
        m=[ m ; m ];
        s = set(s, 'Monitor', m(:,index));
      end
    end
    clear m
  else
    % create a new object with an opposite energy axis
    s     = combine(s, axis(s, 2, -axis(s,2)));
  end
  
  
  s     = set(s,'classical', 1, '[0=from measurement, with Bose factor included, 1=from MD, symmetric]');
  % final object (and merge common area)
  
  % s     = iData_Sqw2D(s);
  
