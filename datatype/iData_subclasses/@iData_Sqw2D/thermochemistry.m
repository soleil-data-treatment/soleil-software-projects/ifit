function [t, fig]=thermochemistry(s, T, options)
% THERMOCHEMISTRY Thermodynamic quantities for 2D S(q,w) data sets.
%   T = THERMOCHEMISTRY(Sqw) computes thermodynamic quantities.
%   The function returns T as an array of iData objects:
%     entropy                           S [eV/K/cell]
%     internal_energy                   U [eV/cell]
%     Helmholtz_energy                  F [eV/cell]
%     heat_capacity at constant volume Cv [eV/K/cell]
%   When missing, the density of states (gDOS) is estimated.
%   When no output is used, a plot is shown.
%
%   T = THERMOCHEMISTRY(Sqw, T)
%   T = THERMOCHEMISTRY(Sqw, Tmin:Tmax) computes using given temperature 
%   range in K. Default is 0:500 [K].
%
% Reference: https://wiki.fysik.dtu.dk/ase/ase/thermochemistry/thermochemistry.html#background
%            D.A. McQuarrie. Statistical Mechanics. University Science Books, 2000.
%
% Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); t=thermochemistry(s); numel(t) == 4
% See also: iData_Sqw2D/dos, iData_vDOS

t=[]; fig=[];

if nargin < 2, T=[]; end
if nargin < 3, options=[]; end

if isempty(T)
  T = 1:500;  % default
end

DOS = dos(s); % compute or get already available DOS from 2D

if nargout == 0 || ~isempty(strfind(options, 'plot'))
  options = [ options ' plot' ];
end

t = thermochemistry(DOS, T, options); % we use the iData_vDOS/thermochemistry
