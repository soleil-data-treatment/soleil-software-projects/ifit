classdef iData_vDOS < iData
  % IDATA_VDOS Vibrational density of states data set (iData flavour)
  %
  % The iData_vDOS class is a 1D data set holding a density of states
  %   often labelled as g(w). This usually can store a 'true' vibrational density 
  %   of states (vDOS), or a generalised density of states (gDOS). The axis holds
  %   the energy in [meV].
  %
  % Example: g=iData_vDOS('SQW_coh_lGe.nc')
  %
  % Useful methods for this iData flavour:
  %
  % methods(iData_vDOS)
  %   all iData methods can be used.
  % iData_vDOS(g)
  %   convert input [e.g. a 2D or 4D SQW, or a 1D vDOS] into an iData_vDOS to give access to
  %   the methods below.
  %
  % d = dos(g)
  %   Get the vibrational density of states (vDOS).
  %
  % t = thermochemistry(g)
  %   Compute and display thermochemistry quantities from the vDOS.
  %
  % [SQW,Iqt,DW] = incoherent(g, q, T, sigma, m, n, DW)
  %   Compute the incoherent gaussian approximation scattering law S(q,w) from an initial 
  %   density of states vDOS.
  %
  % [Gw, Tsym] = multi_phonons(g, Ki, T, sigma, m, phi, n)
  %   compute the integrated multi-phonon generalised density of states (gDOS) 
  %   from an initial vibrational density of states (vDOS) in the neutron 
  %   scattering incoherent gaussian approximation.
  %   The generalised density of states (gDOS) is the sum of the terms in this expansion.
  %
  % gDOS = gdos(vDOS, Ki, T, sigma, m, phi, n)
  %   compute the generalised density of states (gDOS) from a vibrational 
  %   density of states (vDOS) in the neutron scattering incoherent gaussian approximation.
  %
  % input:
  %   can be an iData (1D vDOS, 2D SQW, 4D SQW) or filename to generate a 1D vDOS object.
  %
  % output: an iData_vDOS object
  %
  % See also: iData

  properties
  end
  
  methods
  
    % main instantiation method
    function obj = iData_vDOS(s, varargin)
      % IDATA_VDOS Create the iData_vDOS subclass for vibrational density of states.
      %
      % G = IDATA_VDOS(D) converts to vDOS any object:
      %    1D iData object [energy,density of states] 
      %    2D iData S(q,w), S(phi,t), S(alpha,beta) or S(phi,w) [iData_Sqw2D]
      %    4D iData S(q,w) [iData_Sqw4D]
      %    filename containing any of the above
      %
      % Example: s=iData_vDOS('SQW_coh_lGe.nc');
      
      % could also support
      %    2D iFunc model holding a S(q,w)
      %    4D iFunc model holding a S(q,w)
      
      obj = obj@iData;
      if ~nargin, return; end
      if ~isa(s, 'iData')
        s = iData(s, varargin{:});
      end
      
      % convert/test
      m = [];
      if isa(s, 'iData')
        if ndims(s) == 1
          m = s;
        elseif ndims(s) == 2
          m = dos(iData_Sqw2D(s));
        elseif ndims(s) == 4
          m = dos(powder(iData_Sqw4D(s)));
        end
      end
      
      if isempty(m)
        error([ mfilename ': the given input ' class(s) ' does not seem to be convertible to iData_vDOS.' ])
      end
      
      if ~isa(s, 'iData') || ~any(strcmp(varargin, 'cast'))
        m = Sqw_parameters(m);
      end
      
      % transfer properties
      % this is a safe way to instantiate a subclass
      warning off MATLAB:structOnObject
      m = struct(m);
      for p = fieldnames(m)'
        if ismethod(obj, p{1})
          error([ mfilename ': ERROR: setting property ' p{1} ' conflicts with ' class(obj) ' method with same name.' ])
        end
        if isempty(findprop(obj, p{1}))
          obj.addprop(p{1});
        end
        obj.(p{1}) = m.(p{1});
      end
 
    end % iData_vDOS constructor
    
    function d = dos(self)
      % DOS Return the object, as it is already a density of states.
      %   For easier syntax and compatibility with other classes.
      d = self;
    end % dos
    
    function g = gdos(self, varargin)
      % GDOS Generalised density of states (gDOS) from a density-of-states (vDOS).
      %   G = GDOS(g) computes the 'generalized' neutron weighted 
      %   density of states (gDOS) from a given "true" vibrational DOS.
      %   This implementation is in principle exact for an isotropic monoatomic material,
      %   e.g. a liquid or powder. The total gDOS [p>=1] G is returned.
      %   The gDOS is the 'experimental' density of states, which includes the
      %   dos(Sqw) added with multi-phonon estimates.
      %   This call is equivalent to sum(multi_phonons(g)).
      %
      %   G = GDOS(g, Ki, T, m, n, ...) specifies additional argments for 
      %   multi_phonons.
      %
      % Example: s=sqw_cubic_monoatomic; g=dos(s); d=gdos(g);
      % See also: iData_vDOS/multi_phonons

      g   = multi_phonons(self, varargin{:});
      % normalise the 1-phonon term to 1 and apply to others
      nrm = trapz(g(1));
      g   = g ./ nrm; % normalise to gDOS 1-phonon term
      g   = plus(g); % effective gDOS == \int q S(q,w) dq
      if (size(self,1) == 1 && size(g,2) == 1) ...
      || (size(self,2) == 1 && size(g,1) == 1)
        g = transpose(g);
      end
      g.Title = [ 'gDOS(' title(self) ')' ];
    end % gdos
    
    % parameters (search for parameters in iData)
    function parameters = parseparams(s)
      % PARSEPARAMS Search for physical quantities in object.
      %   P = PARSEPARAMS(g) searches in object Sqw for physical parameters.  
      %   The initial object is updated with a 'parameters' property, and the 
      %   parameters are returned as a structure P.
      %
      %   PARSEPARAMS(g, 'Bose') compute the Temperature from the Bose factor 
      %   (detailed balance). The data set should extend on both positive and 
      %   negative beta sides.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); d=dos(s); p=parseparams(d); isstruct(p)
      [s,parameters,fields] = Sqw_parameters(s);
    end % parseparams
    
    function f = iData(self)
      % IDATA  Convert a iData_vDOS back to iData.
      %   S = IDATA(g) returns a pure iData object. The iData_vDOS methods are 
      %   then not accessible anymore.
      %
      % Example: s=iData_Sqw2D('SQW_coh_lGe.nc'); d=dos(s); isa(iData(d),'iData')
      f = [];
      for index=1:numel(self)
        this = self(index);
        f1   = iData;
        this = struct(this);
        w = warning('query','iData:subsasgn');
        warning('off','iData:subsasgn');
        for p = fieldnames(this)'
          f1.(p{1}) = this.(p{1}); % generates a warning when setting Alias
        end
        warning(w);
        f = [ f f1 ];
      end
    end % iData
    
  end % methods
  
end % class
