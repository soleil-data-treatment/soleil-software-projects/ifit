function b = gradient(a, dim)
% GRADIENT Approximate gradient.
%   G = GRADIENT(A, DIM) computes the gradient (derivative) of iData objects.
%   The returned value is an iData array of the partials for dimension 
%   ranks 1,2,3..., that is G=[GY GX GZ ...] (remember X is rank 2, Y is 1).
%   When the dimension is specified, only that rank partial derivative is returned.
%
% Example: a=iData(peaks); g=gradient(a,1); round(max(g))==1
% See also iData, iData/diff, iData/del2, diff, gradient, iData/jacobian

if nargin <= 1,
  dim = 0;
end

if dim > ndims(a), b=[]; return; end

% compute the gradient and return it in g (cell)
s = get(a,'Signal'); % Signal
e = get(a,'Error');

gs = cell(1,ndims(a)); ge=gs; gm=gs;
[gs{:}]=gradient(s);
if any(abs(e))
  [ge{:}] = gradient(e);
else ge = 0; 
end

% create returned object(s): one per dimension (axes)

b = [];
for i=1:ndims(s)
  % build each partial: beware index 1 and 2 are to swap
  index = i;
  if ndims(a) > 1
    if     i == 1, index=2;
    elseif i == 2, index=1;
    end
  end
  if dim && index ~= dim,  continue; end
  g = copyobj(a);
  if iscell(ge), g.Error = ge{index}; 
  else           g.Error = 0; end
  g.Signal = gs{index}; g.Title = [  mfilename '(' a.Tag ',' num2str(index) ')' ];
  b = [ b g ];
end

