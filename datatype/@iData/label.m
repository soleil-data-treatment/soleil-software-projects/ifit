function v = label(self, varargin)
% LABEL  Get or set label for a given axis.
%   L = LABEL(s, rank) Get axis label. Rank 0 is for the Signal,
%   rank values 1:ndims(s) indicate an axis.
%
%   LABEL(s, rank, 'text') Set axis/signal label. Rank 0 is for the Signal,
%   rank values 1:ndims(s) indicate an axis.
%
%   LABEL(s) Returns the object Label (s.Label property).
%
% Example: s=iData(1:10); label(s,0,'text'); strcmp(label(s,0),'text')
% See also iData, iData/plot, iData/xlabel, iData/ylabel, iData/zlabel, iDala/clabel

if nargin == 3
  v = setlabel(self, varargin{:});
else
  v = getlabel(self, varargin{:});
end
