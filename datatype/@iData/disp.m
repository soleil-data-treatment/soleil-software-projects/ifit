function disp(s)
% DISP   Display object (details).
%   DISP(X) displays the object. Properties are displayed as well as Axes and 
%   Signal definition and sizes.

builtin('disp', s);
get_disp(s);

% ------------------------------------------------------------------------------

function get_disp(s)

  if numel(s) > 1
    for index=1:numel(s)
      get_disp(s(index));
    end
  else

    % display Signal and Aliases ---------------------------------------------------
    myisvector = @(c)length(c) == numel(c);
    disp([ num2str(ndims(s)) 'D object ' char(s) ]);
    disp('  [Rank]          [Size]               [Value]  [Label]');
    for index=0:length(s.Axis)
      v = axis(s, index); 
      sz= size(v);
      v = v(:)';
      l = label(s, index);
      if numel(v) > 3
        v = [ num2str(v(1),2) ' ... ' num2str(v(end),2) ];
      else
        v = num2str(v);
      end
      fprintf(1,'  %6i %15s [%19s]  %s\n', index, mat2str(sz), v, l);
      
    end
  end


