function T = getnametitle(a, fields)
% GETNAMETITLE Return a short Name/Title representation
%   GETNAMETITLE(S, FIELDS) returns a short char for Name/Title/etc.
%   Default FIELDS is {S.Name, S.Title}

  if nargin < 2, fields=''; end
  if isempty(fields) fields={a.Title, a.Name}; end
  
  T = '';
  for f=fields
    this = cleanchar(f{1});
    T    = strtrim([ T ' ' this ]);
  end


function this = cleanchar(this)

    if iscellstr(this), this=char(this); end
    if ischar(this)
      if size(this, 1) > 1, this = sprintf('%s ', this'); end
      % if numel(this) > 21,  this = [ this(1:18) '...' ]; end
      this(~isstrprop(this,'print') | this=='\' | this=='%')='';
      this = strtrim(this);
    else this = '';
    end
