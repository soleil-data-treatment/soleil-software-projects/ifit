function self = setaxis(self, index, v, lab)
% SETAXIS Set axis definition or value in object.
%   SETAXIS(a, rank, value) sets axis value. This is equivalent to
%   the syntax a{rank}=value. The axis rank 0 corresponds with the Signal/Monitor value.
%   The axis of rank 1 corresponds with rows, 2 with columns, 3 with pages, etc.
%
%   SETAXIS(a, rank, value, label) also sets the associated label.
%
%   SETAXIS(a) checks axes, Signal, Monitor, Error.
%
% Example: s=iData(peaks); axis(s,0,[1 2]); all(s.Signal == [1 2])
% See also iData, fieldnames, findfield, isfield, set, get, getaxis, setaxis

  if nargin >= 3 && isnumeric(index) && isscalar(index)
    if (isscalar(index) && ~index) || strcmp(index, 'Signal')
      % update value with Monitor
      m = get(self,'Monitor');
      if isnumeric(v) && ~isempty(m) ...
        && (isscalar(m) || isequal(size(m),size(v)))
        v=v.*m;
      end
      set(self,'Signal', v); 
      if nargin == 4 && (ischar(lab) || iscellstr(lab))
        self.Title = lab;
      end
    elseif index <= ndims(self)
      self.Axis{index} = v; 
      self.Axis = squeeze(self.Axis);
      if nargin == 4 && (ischar(lab) || iscellstr(lab))
        self.Label{index} = lab;
      end
    end
  elseif nargin ==1
    axescheck(self);
  end
end
