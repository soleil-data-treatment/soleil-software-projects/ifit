function v = getlabel(self, index)
% GETLABEL Get label for a given axis/signal.
%   L = GETLABEL(s, rank) Get axis label. Rank 0 is for the Signal,
%   rank values 1:ndims(s) indicate an axis.
%
%   GETLABEL(s) Returns the object Label (s.Label property).
%
% Example: s=iData(1:10); label(s,0,'text'); strcmp(getlabel(s,0),'text')
% See also iData, iData/plot, iData/xlabel, iData/ylabel, iData/zlabel, iDala/clabel

  v = [];
  if nargin == 1, v = self.Label;
  elseif isnumeric(index)
    if (isscalar(index) && ~index) || strcmp(index, 'Signal')
      v = self.Title; 
    elseif 0 < index && index <= numel(self.Label)
      v = self.Label{index}; 
    else v = []; end
  elseif ischar(index)
    attr = fileattrib(self, index);
    if isfield(attr, 'label')
      v = attr.label;
    end
  end
  if ~ischar(v), v = label2char(v); end
end

% ------------------------------------------------------------------------------
function v=label2char(v)

  if     isempty(v), v='';
  elseif iscellstr(v), v = sprintf('%s\n', v{:});
  elseif iscell(v)
    out = '';
    for index=1:numel(v)
      out = [ out sprintf('\n') label2char(v{index}) ];
    end
    v = out;
  elseif isnumeric(v)
    v = mat2str(v)
  else
    v = class2str(v,'short eval');
  end
  v(~isstrprop(v,'print') | v=='\' | v=='%')='';
end
