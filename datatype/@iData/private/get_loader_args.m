function varg = get_loader_args(filename, loader, varargin)
% GET_LOADER_ARGS assemble the arguments for the loader method
%
% Each format is specified as a structure with the following fields
%
%  description: description of the data format
%         read: function name/handle to read the file; data=method(file,...)
%          ext: a single or a cellstr of extensions associated with the format
%       istext: optional priority (int), e.g. for text files
%      options: optional list of arguments to pass to the method
%               If given as a string, they are catenated with file name
%               If given as a cell, sent to the method as additional arguments
%     patterns: optional cell of tokens to search in e.g. text headers
%               Method is qualified when all are found. The patterns can be  
%               regular expressions. When given as a string, the file is assumed
%               to contain "text" data.
%               A specification begining with `!` excludes the format (should not match).
%     callback: optional callback function(s) to remaster the data after importation 
  
  % we select the calling syntax which matches the number of input arguments
  if ~isfield(loader, 'options') || isempty(loader.options)
    varg = { filename, varargin{:} };
  elseif iscellstr(loader.options)
    varg = { filename, loader.options{:}, varargin{:} };
  elseif ischar(loader.options) && ~isempty(loader.options)
    if nargin(loader.read) == 1
      if ~isempty(loader.options), loader.options = [ ' ' loader.options ]; end
      varg = { [ filename loader.options ], varargin{:} };
    else
      varg = { filename, loader.options, varargin{:} };
    end
  end
  % reduce the number of input arguments to the one expected
  if nargin(loader.read) > 0 && ~any(strcmp(loader.read, {'text','read_anytext','looktxt'}))
    narg = min(length(varg), nargin(loader.read));
    varg = varg(1:narg);
  end
end
