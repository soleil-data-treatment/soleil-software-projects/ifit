function self = setlabel(self, index, v)
% SETLABEL Set label for a given axis/signal.
%   SETLABEL(s, rank, 'text') Get axis/signal label. Rank 0 is for the Signal,
%   rank values 1:ndims(s) indicate an axis.
%
% Example: s=iData(1:10); setlabel(s,0,'text'); strcmp(getlabel(s,0),'text')
% See also iData, iData/plot, iData/xlabel, iData/ylabel, iData/zlabel, iDala/clabel

  if nargin < 3, return; end
  if isnumeric(index) && isscalar(index) && ischar(v)
    if (isscalar(index) && ~index) || strcmp(index, 'Signal')
      self.Title = v; 
    else
      self.Label{index} = v; 
      self.Label = squeeze(self.Label);
    end
  elseif ischar(index) && ischar(v) && isfield(self, index)
    fileattrib(self, index, 'label', v);
  end
end
