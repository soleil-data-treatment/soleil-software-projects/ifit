function str = cleanupcomment(str,option)
% CLEANUPCOMMENT clean a string from EOL and duplicate spaces.
%   S = CLEANUPCOMMENT(C) removes non printable characters and duplicate spaces.
%
%   S = CLEANUPCOMMENT(C,'long') same as above, but keeps duplicate spaces. 

  if nargin < 2, option='short'; end
  if ~ischar(str), str=class(str); return; end
  
  % Replace linefeeds and carriage returns.
  str = strrep(str, '\', '/');
  isprint = isstrprop(str, 'print');
  str(~isprint) = ' ';

  if any(strcmp(option,{'short','compact'}))
    % Replace all double spaces with single spaces.
    while (strfind(str, '  '))
	    str = strrep(str, '  ', ' ');
    end
    % Remove leading and trailing space.
    str = strtrim(str);
  else
    str = deblank(str);
  end

  

