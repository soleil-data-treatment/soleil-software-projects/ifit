function [loader, isbinary, conf] = get_loaders(new, loader, filename)
% GET_LOADERS get the list of possible loaders for given file
%
% Each format is specified as a structure with the following fields
%
%  description: description of the data format
%         read: function name/handle to read the file; data=method(file,...)
%          ext: a single or a cellstr of extensions associated with the format
%       istext: optional priority (int), e.g. for text files
%      options: optional list of arguments to pass to the method
%               If given as a string, they are catenated with file name
%               If given as a cell, sent to the method as additional arguments
%     patterns: optional cell of tokens to search in e.g. text headers
%               Method is qualified when all are found. The patterns can be  
%               regular expressions. When given as a string, the file is assumed
%               to contain "text" data.
%               A specification begining with `!` excludes the format (should not match).
%     callback: optional callback function(s) to remaster the data after importation 

  isbinary = 0; conf = [];
  if strcmp(loader, 'auto')
    conf = ifitpref;
    [loader, isbinary] = get_loaders_auto(filename, conf);
  elseif strcmp(loader, 'gui')
    [dummy, filename_short, ext] = fileparts(filename);
    filename_short = [ filename_short ext];
    conf = ifitpref;
    [loader, isbinary] = get_loaders_auto(filename, conf);
    loader_names=[loader{:}];
    tmp         = cell(size(loader_names)); tmp(:)={' - '};
    loader_names= strcat({loader_names.name}, tmp,{loader_names.method});
    [loader_index,ok]=listdlg(...
      'PromptString',...
        {'Select suitable import methods',['to import file ' filename_short ]}, ...
      'SelectionMode','Multiple',...
      'ListString', loader_names, ...
      'ListSize', [300 160], ...
      'Name', ['Loader for ' filename_short ]);
    if isempty(loader_index), loader=[]; return; end
    loader=loader(loader_index);
  elseif (ischar(loader) || iscellstr(loader))
    % test if loader is the name of a function
    conf   = ifitpref;
    loader = ifitpref('formats', loader);
    loader = get_loaders_auto(filename, conf, loader); % restricted loaders
  elseif isa(loader, 'function_handle') loader = feval(loader);
  end
end

% ------------------------------------------------------------------------------

function [loaders, isbinary] = get_loaders_auto(file, conf, loader)
% GET_LOADERS_AUTO function to determine which parser to use to analyze content
%   when given 3rd arg, a restricted list of loaders is used (not ifitpref all)
  
  % get extension 
  [~, ~, ext] = fileparts(file);
  if ~isempty(ext) && ext(1)=='.', ext(1)=[]; end

  % read start of file
  [fid,message] = fopen(file, 'r');
  if fid == -1
    error([ mfilename ': Could not open file ' file ' for reading. ' ...
      message '. Check existence/permissions.' ]);
  end

  % get header (file start) and 'isbinary'
  file_start = fread(fid, 1000, 'uint8=>char')';
  if length(find(file_start >= 32 & file_start < 127))/length(file_start) < 0.4
    isbinary = 1; % less than 40% of printable characters
  else
    % clean file start with spaces, remove EOL
    file_start = [ file_start fread(fid, 9000, 'uint8=>char')' ];
    file_start(isspace(file_start)) = ' ';
    isbinary= 0;
  end
  fclose(fid);

  % restrict loaders using format extensions, also retain formats without extension
  %   this filter is needed to restrict formats in a fast way.
  if nargin == 2, loaders=conf.loaders; else loaders=loader; end
  [loaders,selected] = iLoad_loader_extension(conf.loaders, ext);

  % restrict loaders that match patterns (or no patterns). 
  % Keep extension exact match on top
  loaders = iLoad_loader_patterns(conf, file_start, isbinary, ext, selected);

end % get_loaders_auto

% ------------------------------------------------------------------------------
function [loaders,selected] = iLoad_loader_extension(formats, ext)
% iLoad_loader_extension: identify loaders for which any registered extension match 'ext'
%   or loader has no extension (all match), or '*'

  persistent exts
  if isempty(exts)
    exts = cellfun(@(c)getfield(c, 'ext'), formats, 'UniformOutput',false);
  end
  
  done = zeros(size(exts));
  loaders={};
  % first we add formats that exactly match extensions
  for index=1:numel(exts)
    if ~isempty(exts{index}) && isempty(ext), found = false; 
    else found=any(strcmpi(ext, exts{index}));
      if found, loaders{end+1} = formats{index}; done(index)=1; end
    end
  end
  % then we add other ones
  for index=1:numel(exts)
    if ~done(index)
      if ~isempty(exts{index}) && isempty(ext), found = false;
      else found=isempty(exts{index}) || (iscellstr(exts{index}) && any(strcmp(exts{index},'*')));
        if found, loaders{end+1} = formats{index}; done(index) = 1; end
      end
      
    end
  end
  selected = find(done);
  
end % iLoad_loader_extension

% ------------------------------------------------------------------------------
function loaders = iLoad_loader_patterns(conf, file_start, isbinary, ext, selected, formats)
% iLoad_loader_patterns: identify loaders for which all patterns appear in file_start 
% when given last argument, a restricted search is done.

  if nargin < 6, formats=[]; end
  % keep when patterns found in file_start
  % or format has patterns=='' and is text
  % or no pattern at all
  persistent exts0 pats0 istext0
  
  if (isempty(exts0) && isempty(formats)) || ~isempty(formats)
    if isempty(formats), f=conf.loaders; else f=formats; end
    exts   = cellfun(@(c)getfield(c, 'ext'),       f, 'UniformOutput',false);
    pats   = cellfun(@(c)getfield(c, 'patterns'),  f, 'UniformOutput',false);
    istext = cellfun(@(c)getfield(c, 'istext'),    f);
    if isempty(exts0) && isempty(formats)
      exts0=exts; pats0=pats; istext0=istext; % set cache
    end
  else % if isempty(formats)
    exts=exts0; pats=pats0; istext=istext0;   % use cache
  end
  
  if isempty(formats), formats=conf.loaders; end
  
  % restrict formats to selected set (from extension)
  exts   = exts(selected);
  pats   = pats(selected);
  istext = istext(selected);
  formats= formats(selected);
  
  done   = zeros(size(istext));
  nopat  = zeros(size(istext));
  loaders= {};
  % first put formats that match patterns
  for index=1:numel(pats)
    found = false;
    if (iscellstr(pats{index}) || ischar(pats{index})) && ~isbinary && istext{index}
      found=true;
      for pat=cellstr(pats{index})
        this  = pat{1}; if isempty(this), continue; end
        match = regexp(file_start, pat{1});
        match = xor(~isempty(match), this(1) == '!');
        if ~match
          found=false; nopat(index)=1; break; 
        end % a pattern was not found, or a NOT-pattern was found
      end
      if found
        loaders{end+1} = formats{index};
        done(index)=1;
      end
    end
  end
  
  % we keep formats that exactly match extension (but not if patterns do not match)
  for index=1:numel(exts)
    if ~done(index) && ~nopat(index)
      if ~isempty(exts{index}) && isempty(ext), found = false; 
      else found=any(strcmpi(ext, exts{index})); end
      if found
        loaders{end+1} = formats{index}; done(index)=1; 
      end
    end
  end
  
  % then put loaders without specific pattern
  for index=1:numel(pats)
    if ~isbinary && ~done(index)
      found = false;
      if (iscellstr(pats{index}) || ischar(pats{index})) && all(strcmp(pats{index},''))
        found = istext(index);  % text format without specific pattern
        if found, 
          loaders{end+1} = formats{index};
          done(index)=1;
        end
      end
      
    end
  end
  
  % then put loaders without patterns at all (including binary ones)
  for index=1:numel(pats)
    if isempty(pats{index}) && ~done(index) ...
    && ( (~isbinary && istext{index}) || ~istext{index} )
      loaders{end+1} = formats{index}; 
    end
  end

end % iLoad_loader_patterns

