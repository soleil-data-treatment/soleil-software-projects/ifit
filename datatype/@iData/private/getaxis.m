function v = getaxis(self, index)
% GETAXIS Get axis value in object.
%   GETAXIS(a, rank) Get axis value (and follow aliases). This is equivalent to
%   the syntax a{rank}. The axis rank 0 corresponds with the Signal/Monitor value.
%   The axis of rank 1 corresponds with rows, 2 with columns, 3 with pages, etc.
%
% Example: s=iData(rand(10,20)); setaxis(s,1,2:11); isnumeric(getaxis(s,1))
% See also iData, fieldnames, findfield, isfield, set, get, getaxis, setaxis

  if nargin == 1
    v = self.Axis;
    for index=1:numel(v); 
      if ischar(v{index}), v{index} = get(self,v{index}); end
    end
  elseif (isscalar(index) && ~index) || strcmp(index, 'Signal')
    v = get(self,'Signal'); 
    m = get(self,'Monitor');
    if isnumeric(v) && ~isempty(m) ...
      && (isscalar(m) || isequal(size(m),size(v))) && any(m) && any(isfinite(m))
      v=v./m;
    end
  elseif strcmp(index, 'Error')
    v = get(self,'Error'); 
    m = get(self,'Monitor');
    if isnumeric(v) && ~isempty(m) ...
      && (isscalar(m) || isequal(size(m),size(v))) && any(m) && any(isfinite(m))
      v=v./m;
    end
  elseif ischar(index) && 0 < str2num(index)
    index=str2num(index);
    if index <= numel(self.Axis), v=self.Axis{index}; 
    else v=[]; end
  elseif 0 < index && index <= ndims(self)
    if index <= numel(self.Axis), v = self.Axis{index}; 
    else v = []; end
    if ischar(v), v = get(self, v); end
    changed = false;
    % handle empty/nan axis
    if (isempty(v) || (isscalar(v) && isnan(v)))
      changed = true;
      if numel(find(size(self)>1)) == 1
        v = 1:prod(size(self));
      elseif index <= ndims(self)
        v = 1:size(self, index);
      else v=[];
      end
    end
    % check axis orientation
    n = size(v);
    if isscalar(find(n > 1))
      changed = true;
      if length(find(size(self) > 1)) ~= 1
        z = ones(1, length(n));
        z(index) = max(n);
        if prod(size(v)) == prod(z), v   = reshape(v, z); end
      else
        if prod(size(v)) == prod(size(self)), v = reshape(v, size(self)); end
      end
    end
    % update Axis value
    if changed, self.Axis{index} = v; end
  else v = []; end
end
