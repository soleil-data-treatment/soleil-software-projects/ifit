function filename = get_filenames(filename)
% GET_FILENAMES parse filename for URL, wildcards, etc

  if iscellstr(filename)
    filenames = {};
    for index=1:numel(filename)
      filenames = [ filenames ; get_filenames(filename{index}) ];
    end
    return
  end

  % handle ~ substitution for $HOME
  if filename(1) == '~' && (length(filename==1) || filename(2) == '/' || filename(2) == '\')
    filename(1) = '';
    if usejava('jvm')
      filename = [ char(java.lang.System.getProperty('user.home')) filename ];
    elseif ~ispc  % does not work under Windows
      filename = [ getenv('HOME') filename ];
    end
  end
  
  % handle / to \ substitution for Windows systems, not in URLs
  if strncmp(filename, 'file://', length('file://'))
    filename = filename(7:end); % remove 'file://' from local name
  elseif ~(strncmp(filename, 'http://', length('http://')) || ...
           strncmp(filename, 'https://',length('https://'))   || ...
           strncmp(filename, 'ftp://',  length('ftp://'))   )
    if    ~ispc, filename = strrep(filename, '\', filesep);
    elseif ispc, filename = strrep(filename, '/', filesep);
    end
  end
  
  if isdir(filename), filename = fullfile(filename,'*'); end % all elements in case of directory
  
  % handle single file name (possibibly with wildcard)
  if ~isempty(find(filename == '*')) || ~isempty(find(filename == '?'))  % wildchar !!#
    [filepath,name,ext]=fileparts(filename);  % 'file' to search
    if isempty(filepath), filepath = pwd; end
    this_dir = dir(filename);
    if isempty(this_dir), return; end % directory is empty
    % removes '.' and '..'
    index    = find(~strcmp('.', {this_dir.name}) & ~strcmp('..', {this_dir.name}));
    this_dir = char(this_dir.name);
    this_dir = (this_dir(index,:));
    if isempty(this_dir), return; end % directory only contains '.' and '..'
    rdir     = cellstr(this_dir); % original directory listing as cell
    rdir     = strcat([ filepath filesep ], char(rdir));
    filename = cellstr(rdir);
    filename = get_filenames(filename);
    return
  end
  
  % handle file on the internet
  if strncmp(filename, 'http://', length('http://')) ...
  || strncmp(filename, 'https://',length('https://')) ...
  || strncmp(filename, 'ftp://',  length('ftp://'))
    filename = read_url(filename);
  end
  
  if ~iscellstr(filename), filename = { filename }; end
      
  % remove SVN/GIT/CVS files
  index = ~cellfun(@isempty, strfind(filename, [ '.get' filesep ])) ...
        | ~cellfun(@isempty, strfind(filename, [ '.git' filesep ])) ...
        | ~cellfun(@isempty, strfind(filename, [ '.cvs' filesep ]));
  filename = filename(find(~index));

end % get_filenames
    
