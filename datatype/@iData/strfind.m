function [match, field] = strfind(s, varargin)
% STRFIND Find a string within object.
%   STRFIND is equivalent to FINDSTR.
%
% Example: s=iData; s.UserData='blah'; ischar(strfind(s, 'blah'))
% See also iData, findstr, set, get, findobj, findfield

[match, field] = findstr(s, varargin{:});
