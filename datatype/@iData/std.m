function [w,c] = std(a, dim)
% STD    Standard deviation of object.
%  [W, C] = STD(A) computes the standard deviation of object A, that is
%  their gaussian half width W (second moment). Optionally, the distribution  
%  center C (first moment) can be returned as well.
%  The minimum object value (background) is subtracted before estimating
%  moments.
%
%  [W,C] = STD(A, DIM) computes standard deviation along axis of rank 'dim'.
%  When omitted, dim is set to 1. DIM can be given as an array to iteratively
%  compute the width and center of projected distributions along all given axes.
%
%  [W,C] = STD(A, -DIM) does not subtract the minimum object value (background) before 
%  computing the width and center. This may result in imaginary values.
%
%  [W,C] = STD(A, 0) computes std(Signal) and mean(Signal).
%
% Example: a=iData(peaks); round(std(a))==14

% See also iData, iData/median, iData/mean
w = []; c = [];
if nargin < 2, dim=1; end
if numel(a) > 1
  for index=1:numel(a)
    [wi, ci] = std(a(index), dim);
    w = [ w wi ];
    c = [ c ci ];
  end
  return
end

if length(dim) > 1
  for index=1:length(dim)
    [wi, ci] = std(a, dim(index));
    w = [ w wi ];
    c = [ c ci ];
  end
  return
end

if abs(dim) > ndims(a)
  dim = 1;
end

if dim == 0
  w = double(a);
  c = mean(w(:));
  w = std(w(:));
  return
end

if ~isempty(w) && (nargout == 1 || ~isempty(c))
  return
end

b = meshgrid(a);
s = getaxis(b, 'Signal'); 
x = getaxis(b, abs(dim));

s=s(:); x=x(:);
if ~isfloat(s), s=double(s); end
if ~isfloat(x), s=double(x); end

% then we compute sum(axis{dim}.*Signal)/sum(Signal)
s = private_cleannaninf(s);
if (dim > 0)
  s = s - min(s);
end

sum_s = sum(s);
% first moment (mean)
c = sum(s.*x)/sum_s; % mean value

% second moment: sqrt(sum(x^2*s)/sum(s)-fmon_x*fmon_x);
w = sqrt(sum(x.*x.*s)/sum_s - c*c);

if verbosity(a) && ~isreal(w) && dim < 0
  warning('iData:std', [ mfilename ': The computed standard deviation is imaginary. ' ...
    'You should use std(' inputname(1) ', ' num2str(-dim) ')' ])
end

