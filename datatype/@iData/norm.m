function v = norm(a, varargin)
% NORM   Object norm.
%   Computes the norm of the object Signal. The default is to use the 
%   2-norm, defined as sqrt(sum( |a|^2 ))
%
%     NORM(V,P)    = sum(abs(V).^P)^(1/P).
%     NORM(V)      = norm(V,2).
%     NORM(V,inf)  = max(abs(V)).
%     NORM(V,-inf) = min(abs(V)).
%
%   In addition, one can get the normalized Signal and Error orver Monitor with
%     NORM(V, 'Signal') returns Signal/Monitor == axis(V, 'Signal') == axis(V,0)
%     NORM(V, 'Error')  returns Error/Monitor  == axis(V, 'Error')
%
% Example: X=iData([0 1 2 3]); round(norm(X))==4
% See also iData, iData/sum, iData/trapz, norm

if nargin > 1 && ischar(varargin{1})
  switch lower(varargin{1})
  case 'signal'
    v = axis(a, 0);
  case 'error'
    v = axis(a, 'Error');
  otherwise
    error([ mfilename ': unknown argument ' varargin{1} ]);
  end
else
  v = unary(a, 'norm', varargin{:});
end  
