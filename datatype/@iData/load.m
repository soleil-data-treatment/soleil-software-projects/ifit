function new = load(self, filename, loader, varargin)
% LOAD   Import data into Matlab. 
%
%   F = LOAD(iData, FILE) imports given FILE. FILE can be a single file or directory
%   a cell of files or directories. These may make use of wildcards (*,?).
%   Distant URL's are also supported, as well as compressed files. The imported
%   data is stored in the Data property, whereas the importation information is
%   stored in the Loader property. You may equally call IFITOPEN.
%
%   F = LOAD(iData, FILE, LOADER, ...) specifies the format loader to use. 
%   The LOADER can be a function name, a function handle, a file extension, 
%   'raw', 'auto' for a guess, 'gui' to popup a format selector, or a structure:
%             LOADER.read   = 'function name'
%             LOADER.options= string of options to catenate after file name
%                          OR cell of options to use as additional arguments
%                             to the method
%   additional arguments are passed to the import routine.
%   Any loader defined callbacks are executed to reorder the raw imported data.
%
%   F = LOAD(iData, ..., 'raw') only imports the data, but does not apply callbacks.
%   You may call AXESCHECK for a default Signal/Axes set.
%
%   F = LOAD(iData, ..., 'callback', CB) applies specific callbacks onto iData 
%   objects after data import. The callback CB can be given as funtion handle 
%   @(c)fun(c), a function name (string), an expression to evaluate (may use 
%   'this' as the object itself), or a cell of all these executed in order.
%
%   Default supported formats include: any text based including CSV, Lotus1-2-3, SUN sound, 
%     WAV sound, AVI movie, NetCDF, FITS, XLS, BMP GIF JPEG TIFF PNG ICO images,
%     HDF4, HDF5, MAT workspace, XML, CDF, JSON, YAML, IDL, Python Numpy
%   Other specialized formats include: McStas, ILL, SPEC, ISIS/SPE, INX, EDF, Mantid.
%     SIF, MCCD/TIFF, ADSC, CBF, Analyze, NifTI, STL,PLY,OFF, CIF/CFL,
%     EZD/CCP4, Bruker Varian and JEOL NMR formats, Bruker OPUS, LabView LVM and TDMS,
%     Agilent and Thermo Finnigan MS, Quantum Design VMS, ENDF/ACE, Agilent SDF
%   Compressed files are also supported, with on-the-fly extraction (zip, gz, tar, Z).
%
%   Distant files are supported through e.g. URLs such as 
%     file://, ftp:// http:// and https://
%
%   This may require proxy settings to be set (when behind a firewall)
%   ProxyHost='my.proxy.com'; % Proxy address if you are behind a proxy [e.g. myproxy.mycompany.com or empty]
%   ProxyPort=8080;           % Proxy port if you are behind a proxy [8888 or 0 or empty]
%   java.lang.System.setProperty('http.proxyHost', ProxyHost); 
%   com.mathworks.mlwidgets.html.HTMLPrefs.setUseProxy(true);
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyHost(ProxyHost);
%   java.lang.System.setProperty('http.proxyPort', num2str(ProxyPort));
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyPort(num2str(ProxyPort));
%
%   The configuration of the data formats is controled via 'ifitpref'.
%
%   Typical usage:
%     LOAD(iData,'file'); 
%     LOAD(iData,'file','loader'); 
%     LOAD(iData,{'file1','file2',...}); 
%     LOAD(iData,'file.zip')
%     LOAD(iData,'http://path/name'); 
%
%   F = LOAD(iData, X, Y, ... Signal) imports given numeric arrays as axes and signal.
%
%   F = LOAD(iData, arg1, arg2, ...) imports given arguments into the Data property.
%
% Example: a=load(iData,fullfile(ifitpath, 'data'), 'raw'); numel(a) > 1
% Example: a=load(iData,fullfile(ifitpath, 'data')); numel(a) > 1
% Example: a=iData(1:10,1:20, rand(10,20)); all(size(a)==[10 20])
% See also iData, set, get, findfield, importdata, load, ifitpref, ifitopen

% LOAD calls IFITOPEN to execute loader callbacks.
    
  new     = self;
  
  if nargin < 2, return; end
  if nargin < 3, loader={}; end
  
  if iscellstr(filename) || ischar(filename)
    new = ifitopen(filename, loader, varargin{:}); % in readers/openers
  else % mixed input arguments
    if ~iscell(filename), filename = { filename }; end
    if ~iscell(loader),   
      if ~isempty(loader), loader   = { loader }; 
      else loader = {}; end
    end
    args = { filename{:} loader{:} varargin{:} };
    if ~all(cellfun(@isnumeric, args))
      if numel(args) == 1, args = args{1}; end
      new.Data.Signal = args;
      if isa(args, 'link') && isscalar(args)
        new.Source = class(args.reference);
        new.Format = class(args);
      else
        new.Source = class(args);
      end
    else % we have numeric values (x,y ... ,Signal)
      new.Axis   = args(1:(end-1));
      new.Signal = args{end};
      new.Title = '';
      new.Label  = {};
      for index=2:nargin
        if index < nargin
          new.Label{index-1} = inputname(index);
        else
          new.Title = inputname(index);
        end
      end
      new.Format      = 'numeric';
      if ~isempty(new.Title) || ~all(cellfun(@isempty, new.Label))
        new.Source    = [ new.Title ...
          '(' deblank(sprintf('%s ', new.Label{:})) ')' ];
      else new.Source = class(new.Signal);
      end

    end
  end
  
end % load

