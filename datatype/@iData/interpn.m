function b = interpn(a, varargin)
% INTERPN N-D object interpolation (table lookup).
%    VI = INTERPN(V, Y1,Y2,Y3,...) interpolates object V onto new axes Y1,Y2,Y3, 
%    etc to find the underlying object VI values.
%    INTERPN works for all object dimensionalities. The multidimensional interpolation
%    is based on a Delauney tessellation using the Computational Geometry 
%    Algorithms Library, CGAL. Extrapolated data is set to NaN for the Signal, 
%    Error and Monitor. Also, in some cases, the triangulation interpolant creates
%    fake 'flat' area, especially when the axes area is concave. We then recommend
%    you try the HIST method.
%    For Event data sets, we recommend to use the HIST method which is much faster.
%
%    VI = INTERPN(V, {Y1,Y2,Y3,...}) is similar to the previous syntax.
%
%    VI = INTERPN(V, A) where A is an iData object interpolates V onto A axes,
%    i.e. Y1=A{1}, Y2=A{2}, ... with the usual iData axis notation.
%
%    VI = INTERPN(...,METHOD) specifies alternate methods.  The default
%    is linear interpolation.  Available methods are:
% 
%      'nearest' - nearest neighbor interpolation
%      'linear'  - linear interpolation (default)
%      'spline'  - spline interpolation
%      'cubic'   - cubic interpolation as long as the data is uniformly
%                  spaced, otherwise the same as 'spline'
%      'safe'    - use best of (interpn TriScatteredInterp
%                   griddedInterpolant scatteredInterpolant griddata) methods.
%
%    VI = INTERPN(..., 'safe') evaluates a series of interpolants (interpn, 
%    TriScatteredInterp, griddedInterpolant, scatteredInterpolant, griddata) to
%    return the one minimizing NaN's.
%
% Example: a=iData(peaks); [Xq,Yq] = meshgrid(1:.1:49,1:.1:49); b=interpn(a,Xq,Yq); abs(mean(a,0)-mean(b,0)) < 0.015
% See also iData, interp1, interpn, ndgrid, iData/setaxis, iData/getaxis,
%          iData/hist, iData/resize, iData/reshape, iData/fill

% private_interp and private_meshgrid are in private

% handle input arrays
if numel(a) > 1
  b = zeros(iData, numel(a), 1);
  for index=1:numel(a)
    b(index) = interpn(a(index), varargin{:});
  end
  b = reshape(b, size(a));
  return
end

% default axes and signal of input object
i_axes = cell(1,ndims(a)); 
for index=1:ndims(a)
  i_axes{index} = getaxis(a, index);  % loads object axes, or 1:end if not defined
end

method='';

% interpolation axes
f_axes           = i_axes;
f_axes_changed   = 0;

% parse varargin to overload defaults and set manually the axes ----------------
axis_arg_index   = 0;
for index=1:length(varargin)
  c = varargin{index};
  if ischar(c) && ~isempty(c)         	% method (char)
    if isempty(method), method = c;
    else method = [ method ' ' c ]; end
  elseif isa(varargin{index}, 'iData')  % object axes
    f_axes = getaxis(c); f_axes_changed=1;
  elseif isnumeric(c) && length(c) ~= 1 % vector/matrix axes
    axis_arg_index = axis_arg_index+1;
    f_axes{axis_arg_index} = c;
    f_axes_changed = 1;
  elseif iscell(c)                      % cell(vector/matrix) axes
    f_axes = c;
    f_axes_changed = 1;
  end
  clear c
end % input arguments parsing

if ~f_axes_changed, b=a; return; end

% check for method to be valid
if ~any(strcmp(method, {'linear','cubic','spline','nearest','v4','safe'})) && ~isempty(method)
  if verbosity(a)
    warning('iData:interp', [ mfilename ': Interpolation method "' method '" is not supported. Use: linear, cubic, spline, nearest, v4.']);
  end
end

i_signal  = get(a,'Signal');
i_error   = get(a,'Error');
i_monitor = get(a,'Monitor');

% interpolation takes place here ------------------------------------------
[f_signal, meth] = private_interp(i_axes, i_signal, f_axes, method);
if isscalar(f_signal), b=a; return; end % single scalar value

if isnumeric(i_error) && length(i_error) > 1,
     f_error = private_interp(i_axes, i_error,  f_axes, method);
else f_error = i_error; end
clear i_error
if isnumeric(i_monitor) && length(i_monitor) > 1,
     f_monitor = private_interp(i_axes, i_monitor,f_axes, method);
else f_monitor = i_monitor; end
clear i_monitor
clear i_axes

% transfer Data and Axes --------------------------------------------------
b = copyobj(a);  % new object derived from 'a'
set(b, 'Signal' , f_signal);  clear f_signal
set(b, 'Error',   f_error);   clear f_error
set(b, 'Monitor', f_monitor); clear f_monitor
b.Axis = f_axes;
set(b, 'Interpolation', meth);

