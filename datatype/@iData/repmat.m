function s = repmat(self, varargin)
% REPMAT Replicate and tile an array.
%    B = repmat(A,M,N) creates a large matrix B consisting of an M-by-N
%    tiling of copies of A. The size of B is [size(A,1)*M, size(A,2)*N].
%    The statement repmat(A,N) creates an N-by-N tiling.
%
%    B = REPMAT(A,[M N]) accomplishes the same result as repmat(A,M,N).
%
%    B = REPMAT(A,[M N P ...]) tiles the array A to produce a
%    multidimensional array B composed of copies of A. The size of B is
%    [size(A,1)*M, size(A,2)*N, size(A,3)*P, ...].
%
% Example: numel(repmat(iData, 11,10)) == 110
% See also: iData.reshape, iData.ones
  
  if numel(self) > 1
    error([ mfilename ': repmat(iData, M,N,...) only works with a single iData.' ])
  end
  % we create M*N copies and then reshape the array.
  s = [];
  for index=1:prod([ varargin{:}])
    this = copyobj(self);
    if index == 1, s = this; else s = [ s this]; end
  end
  s = reshape(s, varargin{:});
end % repmat
