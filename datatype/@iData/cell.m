function c = cell(s)
% CELL   Convert structure array to cell array.
%    C = CELL(S) converts the M-by-N structure S (with P fields)
%    into a P-by-M-by-N cell array C.
% 
%    If S is N-D, C will have size [P SIZE(S)].
% 
% Example: s=iData; iscell(cell(s))
% See also iFile, set, get, findstr


w = warning('off','MATLAB:structOnObject');
c = struct2cell(struct(s));
warning(w)
