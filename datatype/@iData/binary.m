function c = binary(a, b, op, varargin)
% BINARY Handles binary operations.
%   Operator may apply on an iData array and:
%     a scalar
%     a vector/matrix: if its dimensionality is lower than the object,
%       it is replicated on the missing dimensions
%     a single iData object, which is then used for each iData array element
%       operator(a(index), b)
%
% binary operator may be:
%   combine conv convn deconv eq ge gt isequal ldivide le lt
%   minus mldivide mpower mrdivide mtimes ne plus power rdivide times xcorr
%   and or xor
%
% Example: s=iData(1:10); ops=binary(s); c=0; for op=ops; ...
%   c=c+(~isempty(binary(s,s,op{1}))); end; c == numel(ops)
% see also iData, getaxis, setaxis, unary

% for the estimate of errors, we use the Gaussian error propagation (quadrature rule),
% or the simpler average error estimate (derivative).
%
% Contributed code (Matlab Central):
%   genop: Douglas M. Schwarz, 13 March 2006

if nargin < 3 || strcmp(char(op), 'identify')
  c = {'combine','conv','convn','deconv','eq','ge','gt','isequal','ldivide', ...
       'le','lt','minus','mldivide','mpower','mrdivide','mtimes','ne','plus',...
       'power','rdivide','times','xcorr','and','or','xor'};
  return
end
opc = char(op);
if ischar(op), opf = str2func(op); else opc = op; end

% handle vectorial operations
if (isa(a, 'iData') & numel(a) > 1)
  % vector+single
  if isa(b, 'iData') & numel(b) == numel(a)
    % add element to element
    c = zeros(iData, numel(a), 1);
    for index=1:numel(a)
      c(index) = binary(a(index), b(index), op);
    end
  elseif isempty(b)
    % process all elements in vector elements
    c = a(1);
    for index=2:numel(a)
      c = binary(c, a(index), op);
    end
  elseif isa(b, 'iData') & numel(b) ~= numel(a) & numel(b) ~= 1
    error(...
    [ mfilename ': ERROR: Dimension of object arrays do not match for operator ' op ...
      ': 1st is ' num2str(numel(a)) ' and 2nd is ' num2str(numel(b)) ]);
  else
    c = arrayfun(mfilename, a, b, op, varargin{:});
  end
  return
end

% detect when objects are orthogonal
sa = size(a); sb = size(b);
if     numel(sa) < numel(sb), sb=sb(1:numel(sa));
elseif numel(sb) < numel(sa), sa=sa(1:numel(sb));
end
index=find((sa == 1 & sb > 1) | (sb ==1 & sa > 1));
orthogonal_ab = (numel(index) == numel(sa));  % all orthogonal

% get common axes
% get Signal, Error and Monitor for 'a' and 'b'
if isa(a, 'iData') && isa(b, 'iData') && ~orthogonal_ab
  if strcmp(opc, 'combine')
    [a,b] = union(a,b);     % perform combine on union
  else
    % compute intersection when objects are not orthogonal
    [a,b] = intersect(a,b); % perform operation on intersection
  end
end


% the p1 flag is true when a monitor normalization is required (not for combine)
if strcmp(opc, 'combine'), p1 = 0; else p1 = 1; end
if ~isa(a, 'iData')   % constant/scalar
  s1 = a; e1=0; m1=0;
  c  = copyobj(b); m4=b.Monitor;
else
  s1 = get(a,'Signal');
  e1 = get(a,'Error');
  m1 = get(a,'Monitor');
  c  = copyobj(a); m4=m1;
end

if ~isa(b, 'iData') % constant/scalar
  s2 = b; e2=0; m2=0;
else
  s2 = get(b,'Signal');
  e2 = get(b,'Error');
  m2 = get(b,'Monitor');
end

[m1,m1is0] = check_em(m1); % true when []/nan/0
[e1,e1is0] = check_em(e1);
[m2,m2is0] = check_em(m2);
[e2,e2is0] = check_em(e2);
 m4        = check_em(m4);

% do test on dimensionality for a vector/matrix input
% use vector duplication to fill iData dimensionality (repmat/kron)

% the 'real' signal is 'Signal'/'Monitor', so we must perform operation on this.
% then we compute the associated error, and the final monitor
% finally we multiply the result by the monitor.

% 'y'=normalized signal, 'd'=normalized error, 'p1' set to true when normalization
% to the Monitor is required (i.e. all operations except combine).

if p1 && ~m1is0
  y1 = genop(@rdivide, s1, m1); d1 = genop(@rdivide,e1,m1);
else y1=s1; d1=e1; end

if p1 && ~m2is0
  y2 = genop(@rdivide, s2, m2); d2 = genop(@rdivide,e2,m2);
else y2=s2; d2=e2; end

% operation ============================================================
switch opc
case {'plus','minus','combine'}
  if strcmp(opc, 'combine'),
    s3 = genop( @plus,  y1, y2); % @plus without Monitor nomalization (y=s)
  else 
    s3 = genop( opf,    y1, y2); 
  end
  i1 = isnan(y1(:)); i2=isnan(y2(:));
  % if NaN's are found, use non NaN values in the other data set
  if any(i1) && ~isscalar(y2), s3(i1) = y2(i1); end
  if any(i2) && ~isscalar(y1), s3(i2) = y1(i2); end

  try
    e3 = sqrt(genop(@plus, d1.^2,d2.^2));
    if any(i1) && numel(i1) <= numel(e2), e3(i1) = e2(i1); end
    if any(i2) && numel(i2) <= numel(e1), e3(i2) = e1(i2); end
  catch
    e3 = [];  % set to sqrt(Signal) (default)
  end

  if     m1is0, m3 = m2;
  elseif m2is0, m3 = m1;
  else
    try
      m3 = genop(@plus, m1, m2);
    catch ME
      m3 = [];
    end
    try; if any(i1) && numel(i1) <= numel(m2), m3(i1) = m2(i1); end; end
    try; if any(i2) && numel(i2) <= numel(m1), m3(i2) = m1(i2); end; end
  end
  clear i1 i2

case {'times','rdivide', 'ldivide','mtimes','mrdivide','mldivide', ...
      'conv','convn','xcorr','deconv'}
  % Signal
  if any(strcmp(opc, {'conv','deconv','xcorr','convn'}))
    s3 = fconv(y1, y2, varargin{:});  % pass additional arguments to fconv
    if nargin >= 4 && ~isempty(strfind(varargin{1}, 'norm'))
      m2 = 0;
    end
  else
    if strcmp(opc, 'mtimes'), opf=@times; end
    try
      s3 = genop(opf, y1, y2);
    catch ME
      getReport(ME)
      whos y1 y2
      error('iData:binary', ME.message);
    end
  end
  
  % Error = s3*sqrt(e1/s1^2+e2/s2^2)
  % when e.g. s1 is scalar, e1 is 0 then s3=s1*s2, and the e3 error should just be s1*e2
  try
    if e1is0 || ~any(s1), e1s1=0; else e1s1 = genop(@rdivide,e1,s1).^2; e1s1(find(s1 == 0)) = 0; end
    if e1is0 || ~any(s2), e2s2=0; else e2s2 = genop(@rdivide,e2,s2).^2; e2s2(find(s2 == 0)) = 0; end
    e3 = genop(@times, sqrt(genop(@plus, e1s1, e2s2)), s3);
  catch
    e3=[]; 
  end

  % Monitor
  if     m1is0, m3 = m2;
  elseif m2is0, m3 = m1;
  elseif p1
    try
      m3 = genop(@times, m1, m2);
    catch
      m3 = [];  % set to 1 (default)
    end
  else m3=m4; end

case {'power','mpower'}
  if strcmp(opc, 'mpower'), opf=@power; end
  try
    s3 = genop(opf, y1, y2);
  catch ME
    warning('iData:binary', ME.message); return;
  end

  try
    e2logs1 = genop(@times, e2, log(s1)); e2logs1(find(s1<=0))   = 0;
    s2e1_s1 = genop(@times, s2, genop(@rdivide,e1,s1));  s2e1_s1(find(s1 == 0)) = 0;
    e3      = s3.*genop(@plus,  s2e1_s1, e2logs1);
  catch
    e3      = [];  % set to sqrt(Signal) (default)
  end

  if     m1is0, m3 = m2;
  elseif m2is0, m3 = m1;
  elseif p1
    try
      m3 = genop(@times, m1, m2);
    catch
      m3 = [];  % set to 1 (default)
    end
  else m3=c.Monitor; end

case {'lt', 'gt', 'le', 'ge', 'ne', 'eq', 'and', 'or', 'xor' }
  s3 = logical(genop(opf, y1, y2));
  try
    e3 = sqrt(genop( opf, d1.^2, d2.^2));
    e3 = 2*genop(@divide, e3, genop(@plus, y1, y2)); % normalize error to mean signal
  catch
    e3=0; % no error
  end
  m3 = m4;
case {'isequal','isequaln','isequalwithequalnans'}
  c = logical(feval(opf, y1, y2));
  return
otherwise
  if isa(a,'iData'), al=a.Tag; else al=num2str(a(1:10)); end
  if isa(b,'iData'), bl=b.Tag; else bl=num2str(b(1:10)); end
  error([ mfilename ': Can not apply operation ' opc ' on ' al ' and ' bl '.' ]);
end

clear e1 e2 m1 m2 y1 y2

% set Signal label
s1=s1(1:min(10, numel(s1)));
if isa(a, 'iData'), al = title(a);
else
  al=num2str(s1(:)'); if length(al) > 10, al=[ al(1:10) '...' ]; end
end
s2=s2(1:min(10, numel(s2)));
if isa(b, 'iData'), bl = title(b);
else
  bl=num2str(s2(:)');
  if length(bl) > 10, bl=[ bl(1:10) '...' ]; end
end
clear s1 s2

% ensure that Monitor and Error have the right dimensions
[m3,m3is0] = check_em(m3);
[e3,e3is0] = check_em(e3);

% operate with Signal/Monitor and Error/Monitor (back to Monitor data)
if p1 && ~m3is0
  s3 = genop(@times, s3, m3); 
  e3 = genop(@times,e3,m3);
end

% update object (store result)
c.Signal  = s3;
c.Error   = abs(e3);
if m3is0
  c.Monitor = 1;
else
  c.Monitor = m3;
end
c.Title = [  opc '(' al ',' bl ')' ];

% ------------------------------------------------------------------------------
function [m,mis0] = check_em(m)
% CHECK_EM Check monitor/error for valid values (or constant)
  mis0 = ~any(m(:)); % true when []/nan/0
  if ~mis0 && ~isempty(m) && ~isscalar(m) && all(m == m(1)), m=m(1); end
