function v = subsref(a,S)
% SUBSREF Subscripted reference.
%   B = SUBSREF(A,S) is called for the syntax A(I), A{I}, or A.I
%    when A is an object.  S is a structure array with the fields:
%        type -- string containing '()', '{}', or '.' specifying the
%                subscript type.
%        subs -- Cell array or string containing the actual subscripts.
%
%   A(I1,I2,...)
%     Return the object A restricted to index ranges I1,I2,...
%     The Signal, Error, Monitor and Axes are reduced to the given ranges.
%
%   A{axis_rank}
%     Get an axis of given rank. The rank 0 corresponds with Signal/Monitor
%     Equivalent to AXIS(A, axis_rank)
%
%   A.FIELD1.FIELD2 ... 
%     Retrieve raw Property values.
%     The property value can itself be an internal PATH/ALIAS given as a string 
%     including '.', '()' and '{}' as in SUBSTRUCT and SUBSASGN, and referencing
%     an other part of the object A.
%     The property value can be a string as an internal ALIAS/PATH in
%     which case its fully resolved value is obtained using
%     Use GET(A,'FIELD1'...) or A.FIELD_... for full path resolution.
%
%   A.FIELD1_... (add underscore '_' at end of 1st field name)
%     Retrieve Property values and follow aliases, same as GET(A, 'FIELD1'...).
%
% Example: s=iData(peaks); isnumeric(s{0}) && isa(s(1:2),'iData')
% Example: s=iData; s.Data=peaks; s.Signal='Data'; ischar(s.Signal) && isnumeric(s.Signal_)
% See also iData, fieldnames, findfield, isfield, set, get, getaxis, setaxis

% For speed, we rely on the default subsref behaviour, and keep SIMPLE. 
% We just prepend the missing information to the 'path' in the object.

  if numel(a) > 1
    v = builtin('subsref',a, S);
    return
  end
  
  % handle array/cell of fields
  if iscell(S)
    sout = cell(size(S));
    for index=1:numel(S)
      sout{index} = subsref(a, S{index});
    end
    sout = reshape(sout, size(S));
    v = sout; 
    return
  end

  if ischar(S) || iscellstr(S), S=str2subs(S); end
  if isstruct(S) && numel(S) == 1 && strcmp(S.type,'.') && iscellstr(S.subs) && numel(S.subs) == 1
    S = str2subs(S.subs);
  end

  switch S(1).type
  case '.'
    f = S(1).subs;
    if f(end) == '_' && isfield(a, f(1:(end-1)))
      S(1).subs = f(1:(end-1));
      v = get(a, S);
    else
      v = builtin('subsref', a, S);
    end
    
  case '()' % object ROI: we extract Signal, Error, Monitor and Axes
    % get the Signal
    % new object
    v = copyobj(a,'fast');
    
    v.Signal = builtin('subsref', get(a, 'Signal'), S );
    % for each Axis, we must check that given indices do not exceed Axis dimension
    % add {1} for non given dimensions
    nd = my_ndims(get(v,'Signal'));
    S1subs = cell(1, nd); S1subs(:) = {1};
    S1subs = [ S(1).subs S1subs ];
    for index=1:min(numel(a.Axis), nd)
      ax = axis(a,index);
      if isscalar(ax), v.Axis{index} = ax;
      elseif my_isvector(ax)
        S(1).subs       = S1subs(index);
        v.Axis{index}   = subsref(ax, S); % ax(S)
      else
        S(1).subs       = S1subs(1:my_ndims(ax));
        v.Axis{index}   = subsref(ax, S);
      end
      clear ax
    end
    % remove any unused Axis
    v.Axis((nd+1):end) = [];
    
    % do the same for Error and Monitor
    for f={'Error','Monitor'}
      if ~isempty(a.(f{1}))
        ax = get(a,  f{1}); % value
        if isscalar(ax), v.(f{1})=ax;
        else
          S(1).subs = S1subs(1:my_ndims(ax));
          v.(f{1})  = builtin('subsref', ax, S); % ax(S)
        end
        clear ax
      end
    end
    
  case '{}' % get the axis rank or Signal when rank==0
    if numel(S(1).subs{1}) == 1 && isnumeric(S(1).subs{1})
      if ~S(1).subs{1} % rank == 0: Signal ref
        S = [ substruct('.', 'Signal') S(2:end) ];
        m = get(a,'Monitor');
      else
        if S(1).subs{1} > ndims(a)
          v = []; return; 
        end
        if S(1).subs{1} > numel(a.Axis)
          axis(a, S(1).subs{1}); % create undefined axis
        end
        S = [ substruct('.', 'Axis') S ]; 
        m = 1;
      end
      v = builtin('subsref', a, S);
      if ~isnumeric(v), v=get(a, v); end % resolve alias
      if m ~= 1
        try; v = v./m; end
      end
    else
      error([ mfilename ': A{rank} indexing must use a single integer index.' ]);
    end
  end
  
end
  
% ----------------------------------------------------------------------------
function m=my_ndims(s)
  if isvector(s), m=1; else m=ndims(s); end
end

function tf = my_isvector(c)
  tf = (length(c) == numel(c));
end

