function b = cumsum(a,dim)
% CUMSUM Cumulative sum of elements.
%   S = CUMSUM(A) computes the cumulative sum of objects elements along
%   columns. S has the same size as A.
%
%   CUMSUM(A,DIM) operates along axis of rank DIM. When DIM=0 the total 
%   cumulated sum is returned.
%
% Example: a=iData(peaks); s=cumsum(a); abs(sum(s,0)+1.79e3) < 0.5
% See also iData, iData/plus, iData/sum, iData/prod, iData/cumprod


if nargin < 2, dim=1; end

b = unary(a,'cumsum',dim);
