function [m,id] = max(a,b, dim)
% MAX    Largest component.
%   MAX(A) returns a single value as the maximum value of the object signal.
%
%   MAX(A,B) returns an object which signal is the highest of A and B.
%
%   MAX(A,[], DIM) returns maximum value along dimension 'DIM'
%
%   [M,I] = MAX(A,...) returns the maximum value/object in M, and the indices
%   of the maximum values in vector I.
%
% Example: a=iData(peaks); round(max(a))==8
% See also iData, max, iData/min

if nargin == 1
  b = [];
end
if nargin <= 2
  dim = [];
end
id=[];

% handle input iData arrays
if numel(a) > 1 & isa(a,'iData')
  m = zeros(size(a)); id= m;
  for index=1:numel(a)
    [m(index), id(index)] = max(a(index), b, dim);
  end
  return
end

if ~isa(a, 'iData')
  [m,id] = max(b, a, dim);
  return
end

% return a scalar for max(a)
if isempty(b)
  m = getaxis(a, 0);
  if isempty(dim), dim=1; end
  [m,id] = max(m(:),[],dim);
  return
end

% find intersection between iData objects
if isa(b, 'iData')
  m = copyobj(a);
  setaxis(m, 0, max(getaxis(a,0), getaxis(b,0)));
  return
else
% handle iData and scalar/vector/matrix min/max
  m = copyobj(a);
  if isempty(dim) || ~isempty(b)
    y = max(getaxis(a,0), b);
    id=[];
    set(m, 'Signal', y);
  else
    rmaxis(m); % delete all axes
    % copy all axes except the one on which operation runs
    ax_index=1;
    for index=1:ndims(a)
      if index ~= dim
        setaxis(m, ax_index, getaxis(a, num2str(index)));
        ax_index = ax_index+1;
      end
    end
    [y,id] = max(getaxis(a,'Signal'), [], dim);
    set(m, 'Signal', y);     % Store Signal
  end
end


