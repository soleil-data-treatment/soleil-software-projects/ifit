function a = zlabel(a, lab)
% ZLABEL Z-axis object label (rank 3, pages).
%   ZLABEL(s, 'label') changes the Z axis (rank 3, pages) label
%
%   ZLABEL(s) returns the current Z axis label
%
% Example: s=iData(1:10); zlabel(s,'Z'); strcmp(zlabel(s),'Z')
% See also iData, iData/plot, iData/xlabel, iData/ylabel, iData/label, iData/clabel

if nargin ==1
	a = getlabel(a, 3);
else
	a = setlabel(a, 3, lab);
end
