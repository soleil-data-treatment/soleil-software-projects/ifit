function [filename,writer] = saveas(self, varargin)
% SAVEAS Save data in desired output format
%   FILENAME = SAVEAS(DATA, FILENAME) exports DATA.
%     The file format is set from the file extension.
%     When FILENAME is empty, a temporary file is created, HDF5 format.
%     When FILENAME is 'gui', a file selector is displayed.
%     When FILENAME is a directory, random filenames are created therein.
%
%   [FILENAME,WRITERS] = SAVEAS(DATA, FILE, WRITER, ...) specifies the file format.
%     The WRITER can be a token, function name, function handle, file extension
%     or structure (see ifitwrite).
%     Additional arguments are passed to the export routine. The returned WRITERS
%     contains the actual list of used writers.
%     To get a full list of supported WRITER, use: ifitpref formats iswriter
%     The recommended data formats for large datasets are NRRD (the fastest, with 
%     metadata), and HDF5 (saves full data structure). XML, YAML and JSON are 
%     fast text-based formats (and retain whole structure).
%
%   WRITERS = SAVEAS(DATA, FILE, 'writers') returns the WRITERS that can write the file.
%
%   The configuration of the data formats is controled via 'ifitpref'.
%
% Example: y=saveas(iData(rand(10))); ~isempty(dir(y))
% Example: y=saveas(iData(rand(10)),'','nrrd'); ~isempty(dir(y))
% See also ifitwrite, ifitread, ifitpref, ifitopen

if numel(self) > 1
  filename = cell(size(self));
  writer   = filename;
  for index=1:numel(self)
    [filename{index},writer{index}] = ifitwrite(self(index), varargin{:});
  end
  return
end
[filename,writer] = ifitwrite(self, varargin{:});
