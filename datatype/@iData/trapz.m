function s = trapz(a,dim, varargin)
% TRAPZ  Trapezoidal numerical integration.
%   S = TRAPZ(A,DIM) integrates along axis of rank DIM. The axis is then removed.
%   Default is to use DIM as the first non-singleton dimension. 
%   TRAPZ is complementary to SUM, but takes into account axes values.
%
%   S = TRAPZ(A,0) integrates along all axes and the total is returned as a 
%   scalar value. 
%
%   S = TRAPZ(A,'radial') integrates radially (R=sqrt(sum(axes^2)).
%
%   TRAPZ(A,'radial', center) specifies the 'center' of the integration 
%   (vector of coordinates) or a single value used as center on all axes 
%   (for instance 0). All axes are assumed to be distances.
%
% Example: a=iData(peaks); b=trapz(a); ndims(b) == 1
% See also iData, iData/cumsum, iData/sum, iData/cart2sph

if nargin < 2, 
  dim=find(size(a)>1,1);
end

if strcmp(dim, 'radial')
  s = cart2sph(a, varargin{:});
else
  s = unary(a, 'trapz', dim, varargin{:});
end

