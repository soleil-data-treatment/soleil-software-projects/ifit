function a = subsasgn(a,S,val)
% SUBSASGN Subscripted assignment.
%   A = SUBSASGN(A,S,B) is called for the syntax A(I)=B, A{I}=B, or
%     A.I=B when A is an object.  S is a structure array with the fields:
%       type -- string containing '()', '{}', or '.' specifying the
%               subscript type.
%       subs -- Cell array or string containing the actual subscripts.
%
%   A{axis_rank} = VALUE
%     Set an axis of given rank to a value.
%     The rank 0 corresponds with Signal/Monitor.
%
%   A(I1,I2,...) = VALUE
%     Set the object A on restricted index ranges I1,I2,...
%     The VALUE must match the given range size. 
%     When given as a scalar or an ordinary array, the Signal/Monitor is set. 
%     When given as an iData, Signal, Error, and Monitor are set. Axes are kept.
%
%   A.I = B
%     Set or create field I in object A. 
%     The value B can itself be an internal PATH/ALIAS given as a string 
%     including '.', '()' and '{}' as in SUBSTRUCT and SUBSREF, and referencing
%     an other part of the object A.
%     Use SET(A,'I',B) or A.I_ = B for property assignment with path resolution.
%
%   A.I_ = B
%     Set field I and follow aliases, equivalent to SET(A, 'I', B).
%
% Example: s=iData(peaks); s.x = 1:10; s(1:2)=1; isnumeric(s.x) && all(s{0}(1:2) == 1)
% See also iData, fieldnames, findfield, isfield, set, get, getaxis, setaxis

% For speed, we rely on the default subsref behaviour, and keep SIMPLE. 
% We just prepend the missing information to the 'path' in the object.

  if numel(a) > 1 && isa(val, 'iData')
    a = builtin('subsasgn',a, S, val);
    return
  end

  if ischar(S) || iscellstr(S), S=str2subs(S); end
  if isstruct(S) && numel(S) == 1 && strcmp(S.type,'.') && iscellstr(S.subs) && numel(S.subs) == 1
    S = str2subs(S.subs);
  end
  
  set_signal=false;
  set_axis  =false;

  switch S(1).type
  case '.'
    % add property/field if not there yet, before assignment
    f = S(1).subs;
    if f(end) == '_' && isfield(a, f(1:(end-1))) % obj.field_ -> set (follow links)
      S(1).subs = f(1:(end-1));
      S = set(a, S, val);
    elseif ~isfield(a, S(1).subs) && isempty(findprop(a, S(1).subs))
      if isvarname(S(1).subs) && ~ismethod(a, S(1).subs)
        a.addprop(S(1).subs);
      else
        error([ mfilename ': ERROR: can not set invalid property name ' S(1).subs ]);
      end
    end
    a = builtin('subsasgn', a, S, val);
    
  case '()' % object ROI
    if isa(val, class(a))
    
      % object ROI: we set Signal, Error, Monitor
      for f={'Signal','Error','Monitor'}
        if ~isempty(val.(f{1}))
          a = builtin('subsasgn', a, [ struct('type','.','subs', f{1}) S ], get(val,f{1}));
        end
      end
      
    elseif isnumeric(val)
    
      % assign Signal, weight with Monitor
      S = [ struct('type','.','subs', 'Signal') S ];
      % update value with Monitor
      m = get(a,'Monitor');
      if isnumeric(val) && ~isempty(m) ...
        && (isscalar(m) || isequal(size(m),size(val)))
        val=val.*m;
      end
      a = set(a, S, val);
    else
      n = inputname(1); if isempty(name), name=class(a); end
      error([ mfilename ': ERROR: can not set ' name '()=' class(val) ]);
    end
    set_signal = true;
    
  case '{}' % set the axis rank or Signal when rank==0
    if numel(S(1).subs{1}) == 1 && isnumeric(S(1).subs{1})
      if ~S(1).subs{1} % rank == 0: Signal ref
        S = [ struct('type','.','subs', 'Signal') S(2:end) ];
        % update value with Monitor
        m = a.Monitor;
        if isnumeric(val) && ~isempty(m) ...
          && (isscalar(m) || isequal(size(m),size(val)))
          val=val.*m;
        end
        set_signal = true;
      elseif S(1).subs{1} <= ndims(a)
        S = [ struct('type','.','subs', 'Axis') S ];
        set_axis = true;
      else
        error([ mfilename ': Can not set A{rank} for rank above dimension. Use A.Axis{rank}=value to force.' ]);
      end
      v = builtin('subsasgn', a, S, val);
    else
      error([ mfilename ': A{rank}=value assignment must use a single integer index.' ]);
    end
  end
  notify(a, 'ObjectUpdated')
  % update fieldnames when a field has changed (in case we use findfield/isfield 
  % on the same object further on)
  if numel(S) > 1 && ~set_axis && ~set_signal
    findfield(a, '', 'fieldnames');
  end
  
end % subsasgn


