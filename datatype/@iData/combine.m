function c = combine(a,b)
% COMBINE Combines (merge) objects Signal and Axes.
%   C = COMBINE(A,B) combines all objects in [A B] into a single object.
%   The merge is carried out on Signal and Axes.
%   The combine operator (aka merge) of two objects is defined as:
%
%     (S1+S2) over monitor (M1+M2)
%
%   where S1,M1 and S2,M2 and the Signal and Monitor of the two objects.
%
%   Alternatively, the addition (PLUS) is defined as the normalised sum:
%
%     (M1+M2)*(S1/M1+S2/M2) over monitor(M1+M2)
%
%   A\B and A.\B are equivalent to COMBINE(A,B).
%
% Example: a=iData(peaks); a.Monitor=1; b=combine(a,a); max(b) == max(a)
% See also iData, iData/minus, iData/plus, iData/times, iData/rdivide

c = binary(a, b, 'combine');
