function d = display(s_in, name)
% DISPLAY Display object.
%   DISPLAY(X) is called for the object X when the semicolon is not used
%   to terminate a statement. 
%
%   S = DISPLAY(X) same as above and return a string with the object description.
%
% Example: display(iData); 1
% See also iData, iData/disp, iData/get

if nargin == 2 && ~isempty(name)
  iname = name;
elseif ~isempty(inputname(1))
  iname = inputname(1);
else
  iname = 'ans';
end

% build the header -----------------------------------------------------
d = [ sprintf('%s = ',iname) ];
sz= size(s_in);
if numel(s_in) > 1
  d = [ d sprintf(' array [%s]',num2str(sz)) ];
end
if isdeployed || ~usejava('jvm') || ~usejava('desktop'), id=class(s_in);
else           id=[ '<a href="matlab:help ' class(s_in) '">' class(s_in) '</a> ' ...
                    '(<a href="matlab:methods ' class(s_in) '">methods</a>,' ...
                    '<a href="matlab:doc(''' class(s_in) ''')">doc</a>,' ...
                    '<a href="matlab:figure;subplot(' iname ');">plot</a>,' ...
                    '<a href="matlab:figure;subplot(log(' iname '));">plot(log)</a>,' ...
                    '<a href="matlab:double(' iname ')">values</a>,' ...
                    '<a href="matlab:disp(' iname ');">more...</a>)' ];
end
if length(s_in) == 0 || (numel(s_in) == 1 && isempty(s_in.Data) && isempty(s_in.Signal))
  d = [ d sprintf(' %s 0D object: empty\n',id) ];
else
  d = [ d sprintf(' %s %dD object:\n\n', id, numel(sz)) ];
  if numel(s_in) > 1
    d = [ d 'Index ' ];
  else d=[d '   '];
  end
  d = [ d sprintf(' [Tag]        [Size] [Name]                [Source]') ];
  
  d = [ d sprintf('\n') ];


  % add the objects representation
  d = [ d char(s_in) sprintf('\n') ];
end

if nargout == 0
  fprintf(1,d);
end
