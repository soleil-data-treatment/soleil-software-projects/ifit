function s = axescheck(s, varargin)
% AXESCHECK Check the Signal and Axes of an object.
%   AXESCHECK(s) Checks Signal and Axes. When not set, the largest numeric
%   field is assigned as the Signal, and missing Axes are searched accordingly.
%   Monitor and Error are also checked to match Signal dimension.
%
%   AXESCHECK(s, PATH) Cheks Signal and Axes, assuming they are below given PATH.
%   for instance AXESCHECK(s, 'Data') will check/search below s.Data
%
%   AXESCHECK(.., 'force') force to re-assign the Signal and Axes.
%
%   AXESCHECK(.., 'force signal') force to re-assign the Signal only (usually the
%   largest numeric array).
%
% Example: s=iData(1:10); isa(axescheck(s),'iData')
% see also iData, getaxis, setaxis

% handle array of objects
if numel(s) > 1
  for index=1:numel(s)
    s(index) = axescheck(s(index), varargin{:});
  end
  return
end

% extract 'force' option
option='';
for index=1:numel(varargin)
  if strncmp(varargin{index}, 'force', 5)
    option=varargin{index};
    varargin(index)=[];
    break;
  end
end

notify(s, 'ObjectUpdated');

% get the size of the Signal, Monitor, Error (follow links)
if nargin == 2 && ~isempty(strfind(option, 'force'))
  % clear current settings
  signal_sz = 0;
  monitor_sz= 0;
  error_sz  = 0;
  if isempty(strfind(option, 'signal')), s.Axis    = {}; end
else
  signal_sz = size(s);
  monitor_sz= size(s.Monitor);
  error_sz  = size(s.Error);
  axes_id   = [];
end

% get all fields in object as well as their size
if isempty(strfind(option, 'force'))
  [fields, ~, dims, sz] = findfield(s,'','numeric cache');
else
  [fields, ~, dims, sz] = findfield(s,'','numeric'); % force -> no cache
end

% restrict check/search to given PATH
if ~isempty(varargin)
  keep_fields=[];
  for index=1:numel(varargin)
    if ~ischar(varargin{index}), continue; end
    this_field_match=find(strncmp(varargin{index}, fields, numel(varargin{index})));
    keep_fields = [ keep_fields this_field_match ];
  end
  fields=fields(keep_fields);
  dims  =dims(keep_fields);
  sz    =sz(keep_fields);
end

% get definition of Signal, Error and Monitor, so that we do not use these.
not_use = {'Signal','Error','Monitor'};

% define Signal,Error,Monitor when not yet so
if all(signal_sz == 0) || all(error_sz == 0) || all(monitor_sz == 0) % empty ?

  % identify signal, error, monitor and axes based on names.
  % Signal can also be set as the biggest field.
  [signal_id, error_id, monitor_id, axes_id] = axescheck_find_signal(s, fields, dims, sz);

  % change those which are not set (set definition)
  if all(signal_sz == 0) && ~isempty(signal_id)
    if verbosity(s) > 1
      disp([ mfilename ': setting Signal to ' fields{signal_id} ' for object ' s.Tag ]);
    end
    s.Signal = fields{signal_id};
    if isempty(s.Title), s.Title = fields{signal_id}; end
    not_use{end+1} = fields{signal_id};
    signal_sz  = sz{signal_id};
  end
  if ~isempty(signal_sz)
    if all(error_sz == 0) && ~isempty(error_id)
      s.Error = fields{error_id};
      not_use{end+1} = fields{error_id};
      error_sz   = sz{error_id};
    end
    if all(monitor_id == 0) && ~isempty(monitor_id)
      s.Monitor = fields{monitor_id};
      not_use{end+1} = fields{monitor_id};
      monitor_sz = sz{monitor_id};
    end
  end
end

if isempty(signal_sz), return; end

if all(error_sz) && prod(signal_sz) ~= prod(error_sz) && ~all(error_sz ==1)
  s.Error = [];
end
if all(monitor_sz) && prod(signal_sz) ~= prod(monitor_sz) && ~all(monitor_sz ==1)
  s.Monitor = [];
end

if ~isempty(strfind(option, 'signal')), return; end

%% check Axes: must be size(Signal) or expression or size(rank)
% first search amongst axes_id, then blind search.
if ischar(s.Signal), not_use{end+1} = s.Signal; end
[ok, not_use] = axescheck_find_axes(s, fields(axes_id), dims(axes_id), sz(axes_id), not_use);
if ok < numel(signal_sz)
  % some axes were not found from reduced Axes search. Try with full object.
  axescheck_find_axes(s, fields,          dims,          sz, not_use);
end


% ------------------------------------------------------------------------------
function [signal_id, error_id, monitor_id, axes_id] = axescheck_find_signal(self, fields, dims, sz, not_use)
  % AXESCHECK_FIND_SIGNAL Search for Signal, Error, Monitor and Axes based on their names
  %   Signal can also be set as the biggest field.
  %   Return the index in the field cell.
  signal_id = []; error_id = []; monitor_id=[]; axes_id = [];

  if      isempty(dims), return;
  elseif isscalar(dims), signal_id=1; return;
  end

  % clean fields with 'Protected' stuff.
  for index=1:numel(fields) % decreasing size
    if isempty(fields{index}), continue; end  % skip empty fields/already tagged
    % remove fields that can not be used/cross-referenced
    field_root = strtok(fields{index},'.');
    field      = fliplr(strtok(fliplr(fields{index}),'.'));
    if any(strncmp(field_root, {'Error','Signal','Monitor'}, 5)) ...
    || any(strcmp(field,    {'Axis','Label','Axes','Error','Monit'}))
      fields{index} = ''; dims(index) = 0; sz{index} = 0;
    end
  end

  % we search for Monitor, Error and Axes by name.
  % we identify named numeric fields, and remove them from our definitions
  for index=1:numel(fields) % decreasing size
    if isempty(fields{index}), continue; end  % skip empty fields/already tagged
    if isempty(error_id) && ~isempty(strfind(lower(fields{index}), 'err'))
      error_id = index;   % if found, dimension must match signal, or scalar
      fields{index} = '';
    elseif isempty(monitor_id) && ~isempty(strfind(lower(fields{index}), 'mon')) ...
      monitor_id = index; % if found, dimension must match signal, or scalar
      fields{index} = '';
    elseif ~isempty(strfind(lower(fields{index}), 'axis')) ...
      ||   ~isempty(strfind(lower(fields{index}), 'axes'))
      axes_id(end+1) = index; % if found, dimension must match signal, size(rank)
      fields{index} = '';
    end
  end

  % We search for the Signal. Its size must be max(dims). Its name must not match
  % 'error','monitor' or any other alias.
  maxdim = max(dims); signal_id_candidate = []; field_indexes=1:numel(fields);
  % we reorder the fields when similar sizes are found
  index = find(dims == maxdim);
  if numel(index) > 1
    [~,i] = sort(fields(index));  % e.g. x,y,z,t,...
    i=i(end:-1:1);                % reverse order. All dims are the same=max
    field_indexes(index) = field_indexes(index(i));
  end
  
  for index=field_indexes % decreasing size
    if isempty(fields{index}), continue; end  % skip empty fields/already tagged
    field      = fliplr(strtok(fliplr(fields{index}),'.'));
    if isempty(signal_id) && ~strcmp(fields{index}, 'Signal') ...
      && ( ~isempty(strfind(lower(field), 'signa')) ...
        || ~isempty(strfind(lower(field), 'int')) ...
        || ~isempty(strfind(lower(field), 'ampl')))
      signal_id_candidate(end+1) = index;  % signal is named (Signal, Amplitude, Intensity)
      fields{index} = '';
    elseif dims(index) == maxdim
      signal_id_candidate(end+1) = index;  % max signal size
    else break; % not max size, not a nice name.
    end
  end
  if ~isempty(signal_id_candidate), signal_id = signal_id_candidate(1); end

  % no Signal found: then nothing can be defined, return
  if isempty(signal_id)
    error_id = []; monitor_id=[]; axes_id = [];
    return
  end

  % now check dimensions wrt signal
  if ~isempty(error_id) && ~all(dims(signal_id) == dims(error_id)) && dims(error_id) ~= 1
    error_id = [];  % found error does not match signal nor scalar
  end
  if ~isempty(monitor_id) && ~all(dims(signal_id) == dims(monitor_id)) && dims(monitor_id) ~= 1
    monitor_id = [];  % found monitor does not match signal nor scalar
  end

% ------------------------------------------------------------------------------
function [ok, not_use] = axescheck_find_axes(self, fields, dims, sz, not_use)

  ok = 0;
  if isempty(dims), return; end

  % scan Axes/dimensions
  for index=1:ndims(self)
    % get current axis definition
    if index <= numel(self.Axis)
      def = self.Axis{index};
    else def=[]; end

    % when not empty, we check that it is:
    %   size(Signal) or size(rank)
    if ~isempty(def)
      if ischar(def)
        try
          ax_sz = size(get(self, def));
        catch
          ax_sz = [];
        end
      elseif isnumeric(def) && isscalar(def) && isnan(def)
        ax_sz = nan;
      elseif isnumeric(def)
        ax_sz = size(def);
      end

      if axescheck_size_axes(self, index, ax_sz)
        ok = ok + 1;
        continue
      end

      % if we reach here, axis is invalid
      def=[]; self.Axis{index} = [];
    end

    if isempty(def)
    % when empty we search for one amongst fields that is:
    %   size(Signal) or size(rank)

      for findex = 1:numel(fields)
        % check for field
        if ~any(strcmp(fields{findex}, not_use)) && axescheck_size_axes(self, index, sz{findex})
          % found a valid axis definition/value
          self.Axis{index} = fields{findex}; % this is a link
          % check that this Axis is not constant/nan
          ax=axis(self,index); mi=min(ax(:)); ma=max(ax(:)); hasnan=any(isnan(ax(:))); clear ax;
          if abs(mi-ma) <= 1e-10*(abs(ma)+abs(mi)) || hasnan
            % this axis is not suitable
            self.Axis{index}=[]; continue;
          elseif verbosity(self) > 1
            disp([ mfilename ': setting Axis{' num2str(index) '} to ' fields{findex} ]);
          end
          if numel(self.Label) <= index || isempty(self.Label{index})
            self.Label{index}= fields{findex};
          end
          not_use{end+1}   = fields{findex};
          ok = ok + 1;
          break
        end
      end
    end
  end % for

  % ----------------------------------------------------------------------------
  function tf = axescheck_size_axes(self, index, ax_sz)
    tf = false;
    % size(Signal) or size(rank) as vector
    if (numel(ax_sz) == numel(size(self)) && all(ax_sz == size(self))) ...
    || (isscalar(ax_sz) && isnan(ax_sz)) ...
    || (numel(ax_sz) >= index && isscalar(find(ax_sz>1)) && prod(ax_sz) == size(self, index))
      tf = true;  % valid axis definition/value
    end
