function [b,sigma] = sum(a, dim)
% SUM    Sum of in object Signal.
%    S = SUM(X) is the sum of the Signal of the object X. If
%    X is a N-D object, SUM(X) operates along the first
%    dimension.
%
%    S = SUM(X,DIM) sums along the dimension DIM. When DIM=0 the total sum is
%    returned.
%
% Example: s=iData(-10:10); sum(s,0) == 0
% See also iData, iData/uminus, iData/abs, iData/real, iData/imag, iData/uplus

if nargin < 2, dim=1; end

b = unary(a, mfilename, dim);

