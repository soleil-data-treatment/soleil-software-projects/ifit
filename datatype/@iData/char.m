function d = char(self, option, varargin)
% CHAR   Convert object to character string.
%   S = CHAR(X) converts the object X into a character representation,
%   showing its Tag, Title/Name, last command, and Label.
%
%   S = CHAR(X, 'short') and S = CHAR(X, 'compact') produces a compact representation
%
%   S = CHAR(X, 'clean', value1, ...) produces a cleaned version of the given values
%   for a single object.
%
%   Use CELLSTR to obtain an exact string representation which evaluation
%   rebuilds the object.
%
% Example: s=iData; ischar(char(s))

  if nargin < 2, option=''; end
  
  if nargin > 2 && strcmp(option, 'clean')
    d = getnametitle(self, varargin);
    return
  end
  
  % build the output string
  d = '';
  for index=1:numel(self)
    s = self(index);
    if numel(self) > 1,  d1 = index;   else d1=[]; end        % index
    if isempty(s.Tag),   d2 = '<nul>'; else d2=s.Tag; end     % Tag
    d3 = mat2str(size(s));                                    % size

    t = getnametitle(s);
    if length(t) > 21, t = [ t(1:18) '...' ]; end             % Format

    d4 = t;

    if ~isempty(s.Source)
      h = char(s.Source); h = strtrim(h); h(~isstrprop(h,'print') | h=='\')='';
      d6 = h;
    else d6=[]; end

    % build the final string
    if isempty(d4) && isempty(d6), d4='empty'; end
    if nargin == 1 || ~any(strcmp(option,{'short','compact'}))
      d = [ d sprintf('%3i %7s %13s %-21s %s', d1, d2, d3, d4, d6) ];
    else % compact form
      d = [ d sprintf('%i %s %s %s', d1, d2, d4, d6) ];
    end
    if numel(self) > 1
      d = [ d sprintf('\n') ];
    end

  end
