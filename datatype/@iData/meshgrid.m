function b = meshgrid(a, vect)
% MESHGRID   Cartesian grid object in 2-D/3-D/n-D space.
%    B = meshgrid(A) creates a new object which axes are rectangular grids.
%
%    B = meshgrid(A,'vector') creates a new object which grid axes are vectors.
%
% Example: a=iData(peaks); b=meshgrid(a); isvector(axis(a,1)) && ~isvector(axis(b,1))
% See also iData, iData/median, iData/mean

% check if any axis is a vector but signal is not
if isvector(a), b = a; return; end

s = numel(get(a,'Signal'));
flag_meshgrid_needed = 0;
for dim=1:ndims(a)
  x = axis(a, dim);
  if s ~= numel(x)
    flag_meshgrid_needed = 1;
  end
end

if flag_meshgrid_needed % not a regular meshgrid
  X = cell(1, ndims(a));
  Y = axis(a);
  [X{:}] = ndgrid(Y{:});
  b = copyobj(a);
  if nargin > 1 && strcmp(vect,'vector')
    X = cellfun(@unique, X,'UniformOutput',false);
  end
  b.Axis = X;
else
  b = a;
end

