function a = squeeze(a)
% SQUEEZE Remove singleton dimensions from objects.
%   B = SQUEEZE(A) returns an array B with the same elements as
%   A but with all the singleton dimensions removed.  A singleton
%   is a dimension such that size(A,dim)==1.  2-D arrays are
%   unaffected by squeeze so that row vectors remain rows.
%
% Example: c=squeeze(iData(rand(5,1,3))); all(size(c) == [5 3])
% See also iData, iData/pack, iData/fill, iData/resize, iData/size,
%   iData/reducevolume

d = size(a);
d = d(find(d > 1));
d = [d ones(1,2-length(d))]; % Make sure size is at least 2-D

if numel(a) == 1
  sz = isvector(a);
  if sz > 1, return; end
  if ~sz, sz = length(size(a)); end
  % squeeze the signal, error, monitor
  if ndims(a) > 2 || length(size(a)) > 2
    a.Signal = squeeze(get(a,'Signal'));
    % do the same on Error and Monitor
    de=get(a,'Error'); 
    if numel(de) > 1 && isnumeric(de) 
      d=squeeze(de); a.Error = d;
    end
    clear de

    dm=a.Monitor;
    if numel(dm) > 1 && isnumeric(dm)
      d=squeeze(dm); a.Monitor = d;
    end
    clear dm
  end

  % move the un-used/scalar axes to the end (code from subsref)

  % get the scalar axes and those not
  axis_scalar    = [];
  axis_notscalar = [];
  axis_remove    = [];
  % check if the sub indices suppress an axis: move it further
  for index=1:length(a.Axis)
    if index <= length(a.Axis)
      % axes to be moved: scalar, or all unique
      x = getaxis(a, index);
      if     isempty(x), continue;
      elseif isscalar(x) || isempty(find(x ~= x(1), 1))
        % axis is constant, or scalar
        a.Axis{index}=x(1);  % make it scalar
        axis_scalar    = [ axis_scalar index ];
      else
        a.Axis{index}=squeeze(x);
        axis_notscalar = [ axis_notscalar index ];
      end
    end
  end
  % reorder axes
  new_index = [ axis_notscalar axis_scalar ];
  a.Axis = a.Axis(new_index);
  % we remove the empty axes after axis_notscalar
  index = find(cellfun(@isempty, a.Axis) > numel(axis_notscalar));
  a.Axis(index) = [];
  % reorder labels
  labs = a.Label;
  a.Label={};
  for index=1:numel(labs)
    if index <= numel(new_index) && new_index(index) <= numel(labs) && ~isempty(labs{new_index(index)})
      a.Label(index) = labs(new_index(index));
    end
  end

else
  % this is an iData array
  a = builtin('reshape',a, d);
  for index=1:numel(a)
      a(index) = squeeze(a(index));
  end
end

