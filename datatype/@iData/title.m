function t = title(self, t)
% TITLE  Signal title.
%   TITLE(s) returns the current Signal title. 
%
%   TITLE(s, 'text') sets  the Signal title. 
%
%   The object name is obtained with s.Name
%
% Example: s=iData(1:10); title(s, 'argh'); strcmp(title(s), 'argh')
% See also iData, iData/plot

  if nargin == 1, t=getlabel(self, 0);
  else 
    self.Title=t; 
  end
end
