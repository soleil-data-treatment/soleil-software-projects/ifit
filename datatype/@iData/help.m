function c = help(s)
% HELP   Display object information as a Dialogue window or a structure.
%   HELP(S) displays the object information in a Dialogue window
%
%   H = HELP(S) returns the object information as a structure
%
% Example: s=iData(1:10); isstruct(help(s))
% See also  iData/cell, iData/double, iData/struct, 
%           iData/char, iData/size, iData/get
%

% this function is used to gather info for the contextual menu in plots.

c={};
for index=1:numel(s)
  if numel(s) > 1, a=s(index); disp(a); 
  else a=s; end

  % info about the plot --------------------------------------------------
  T = getnametitle(a);
  if ~ischar(T) || isempty(T), T=char(a,'short'); end
  if ~isvector(T), T=transpose(T); T=T(:)'; end
  T = regexprep(T,'\s+',' '); % remove duplicate spaces

  S   = a.Source;
  [pS, fS, eS] = fileparts(S);
  if length(pS) > 13, pS=[ '...' pS(end-10:end) ]; end
  if length(fS) > 13, fS=[ '...' fS(end-10:end) ]; end
  if ~isempty(pS), S = [ pS filesep ];
  else             S = '';
  end
  S = [ S fS ];
  if ~isempty(eS), S = [ S '.' eS ]; end
  
  % ----------------------------------------------------------------------------
  % make up title string and Properties dialog content
  properties={ [ 'Data '    char(a) ], ...
               [ 'Name: "'  char(T) '"' ], ...
               [ 'Source: ' a.Source ]};

  % axes and Signal stuff
  tproperties = {};
  properties{end+1} = '[Rank]         [Value] [Description]';
  % code from iData.disp -----------------------------------------------------
  myisvector = @(c)length(c) == numel(c);
  for index=0:length(a.Axis)
    l=getlabel(a,index);
    if ~index,                     sz = size(a);
    elseif index <= numel(a.Axis), sz = size(a.Axis{index});
    else                           sz = []; end
    if length(l) > 20, l = [l(1:18) '...' ]; end
    t = sprintf('%6i %15s  %s %s\n', index, l, mat2str(sz));
    properties{end+1}  = t;
    tproperties{end+1} = t;
  end
  
  % attach contexual menu to plot with UserData storage
  ud.properties=properties;
  ud.tproperties = tproperties;
  ud.xlabel = xlabel(a);
  ud.ylabel = ylabel(a);
  ud.zlabel = zlabel(a);
  ud.title  = cleanupcomment(T);
  ud.source = S;
  ud.name   = char(a,'short');
  ud.handle = [];
  
  % now open the dialogue when not used in nargout=help(object)
  if nargout == 0
    h = helpdlg(ud.properties, [ mfilename ': Data ' a.Tag ': ' num2str(ndims(a)) 'D object ' mat2str(size(a)) ]);
    ud.handle = h;
  end
  
  
  if numel(s) == 1, c=ud; else c{index} = ud; end
end

