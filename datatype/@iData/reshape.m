function b = reshape(a, varargin)
% RESHAPE Reshape object (reorder).
%   B = RESHAPE(A, M,N,P,...) returns the object A as an M*N*P*... array. 
%   An error results if X does not have M*N elements.
%   The resulting object has the elements of the initial data reordered
%   so that the final size is that requested. The number of elements must
%   not change. To change the number of elements, use RESIZE (with interpolation)
%   or REDUCEVOLUME instead. To permute dimensions, use PERMUTE.
%
%   B = RESHAPE(A, [m n p ...]) is the same thing as above.
%
% Example: a=iData(peaks(60)); b=reshape(a, 75, 48); all(size(b) == [75 48])
% See also iData, iData/squeeze, iData/size, iData/permute, iData/resize,
% iData/reducevolume


b = unary(a, 'reshape', varargin{:});
