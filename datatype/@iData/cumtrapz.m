function b = cumtrapz(a,dim, varargin)
% CUMTRAPZ Cumulative trapezoidal numerical integration.
%   S = CUMTRAPZ(A) computes the cumulative integral (primitive) of the data set.
%
%   CUMTRAPZ(A,DIM) operates along axis of rank DIM. The axis is then removed.
%   Default is to use DIM=1. If DIM=0, integration is done on all axes and 
%   the total is returned as a scalar value. 
%   CUMTRAPZ is complementary to CUMSUM, but takes into account axes.
%
% Example: a=iData(peaks); t=cumtrapz(a); abs(sum(t,0)+2.1330e+03) < 0.1
% See also iData, iData/plus, iData/sum, iData/prod, iData/cumsum

if nargin < 2, dim=1; end

b = unary(a, 'cumtrapz', dim, varargin{:});
