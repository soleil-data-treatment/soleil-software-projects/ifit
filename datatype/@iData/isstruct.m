function tf = isstruct(self)
% ISSTRUCT True for structures.
%      ISSTRUCT(S) returns logical true (1) if S is a structure
%      and logical false (0) otherwise.
%
% Example: s=iData; isstruct(s)
% See also iData, isfield, iscell, isnumeric, isobject.

  tf = true; % isa(self, mfilename); always true
end
