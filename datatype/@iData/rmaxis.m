function self=rmaxis(self, f)
% RMAXIS Remove an axis from object.
%     RMAXIS(S, AX) removes the axis AX specified as a single rank index.
%
% Example: s=iData(1:10); axis(s,1,2:11); rmaxis(s,1); isempty(s.Axis{1})

  if nargin ~= 2 || ~isnumeric(f), return; end
  self.Axis{f} = []; % we do not allow to remove the Signal (rank=0)
  self.Axis = squeeze(self.Axis);
  setlabel(self, f, []);
  sel.Label = squeeze(self.Label);
end % rmaxis
