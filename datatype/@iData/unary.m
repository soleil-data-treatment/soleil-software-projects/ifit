function b = unary(a, op, varargin)
% UNARY Handles unary operations.
%   UNARY(A,OP, ...) applies operator OP on A and returns a new object.
%
% Supported operations can be obtained with: ops = unary(A)
%
% Example: s=iData(1:10); ops=unary(s); c=0; for op=ops; ...
%   c=c+~isempty(unary(s,op{1})); end; c == numel(ops)-1
% see also iData, getaxis, setaxis, binary

if nargin < 2 || strcmp(op, 'identify')
  b = {'acos','acosh','asin','asinh','atan','atanh','cos','cosh','exp','log', ...
    'log10','sin','sinh','sqrt','tan','tanh','transpose','ctranspose',...
    'sparse','full','flipdim','flipud','fliplr','floor','ceil','round','not',...
    'del2','sign','isfinite','isnan','isinf','isscalar','isvector','issparse',...
    'isreal','isfloat','isnumeric','isinteger','uminus','abs','real','imag',...
    'uplus','conj', 'find', 'double','single','logical', 'norm', ...
    'int8','int16','int32','int64', 'uint8','uint16','uint32','uint64', ...
    'flipdim','sum','mean','median','var','permute','reshape',...
    'trapz','cumtrapz','cumsum','cumprod','prod'};
  return
end

if numel(a) > 1
  b = arrayfun('unary', a, op, varargin{:});
  return
end

if nargin > 2 && isequal(varargin{1},0) ...
    && any(strcmp(op, {'sum','mean','var','median','prod','trapz', ...
      'cumsum','cumprod','cumtrapz'}))
  % use iterative integral over ndims
  b = trapz(a);
  varargin(1) = [];
  while isa(b, 'iData') && ndims(b) >= 1
    b = trapz(b);
  end
  return
end

% get Signal Error and Monitor
s = get(a,'Signal');

if isempty(s), b=a; return; end

if strcmp(op(1:2), 'is') || ...
  any(strcmp(op, {'sign','double','single','logical','find', ...
    'all','any','nonzeros'}))
  e = 0; m = 1;
else
  e = get(a,'Error');
  m = get(a,'Monitor');
end
[m,mis0] = check_em(m);
[e,eis0] = check_em(e);

% make sure sparse is done with 'double' type
if strcmp(op, 'sparse')
  if ndims(s) > 2
    error([ mfileame ': Operation ' op ' can only be used on 1d/2d data sets. Object ' a.Tag ' ' a.Name ' is ' num2str(ndims(s)) 'd.' ]);
  end
  if ~strcmp(class(s), 'double') && ~strcmp(class(s), 'logical')
    s = double(s);
  end
  if ~strcmp(class(e), 'double') && ~strcmp(class(e), 'logical')
    e = double(e);
  end
  if ~strcmp(class(m), 'double') && ~strcmp(class(m), 'logical')
    m = double(m);
  end
elseif any(strcmp(op, {'sum','mean','var','median','prod','trapz', ...
      'cumsum','cumprod','cumtrapz'}))
  s = private_cleannaninf(s);
end

% non-linear operators should perform on the Signal/Monitor
% and then multiply again by the Monitor

% operate with Signal/Monitor and Error/Monitor
if ~mis0 && any(strcmp(op, {'norm','asin', 'acos','atan','cos','sin','exp','log',...
 'log10','sqrt','tan','asinh','atanh','acosh','sinh','cosh','tanh'})) ...
  s = genop(@rdivide, s, m);
  if ~eis0, e = genop(@rdivide, e, m); end
end

% new Signal value is set HERE <================================================
if ~isfloat(s) && ~strcmp(op(1:2), 'is') && ...
  ~any(strcmp(op, {'sign','double','single','logical', ...
    'int8','int16','int32','int64', 'uint8','uint16','uint32','uint64', ...
    'find', 'all','any','nonzeros'}))
  s = double(s);
elseif strcmp(op, 'norm') % norm only valid on vectors/matrices
  s = s(:);
end

if isempty(varargin)
  if strcmp(op, 'permute')
    varargin{1} = [1 2];
  elseif strcmp(op, 'reshape')
    warning('iData:unary', [ mfilename ': ' op ': missing ORDER argument' ])
    b = a;
    return
  elseif any(strcmp(op, {'sum','mean','median','flipdim','prod','trapz', ...
    'cumsum','cumprod','cumtrapz'}))
    varargin{1} = 1;
  elseif strcmp(op, 'var')
    varargin{1} = 1;
  end
end

% handle trapz stuff that may use the given axis
new_s = [];
if any(strcmp(op, {'trapz', 'cumtrapz'}))
  x = axis(a, varargin{1});
  if numel(x) == size(s, varargin{1})
    new_s = feval(op, x, s, varargin{:});
    % TODO: create grid when sizes do not match ?
  end
end
if isempty(new_s)
  new_s = feval(op, s, varargin{:});
end

if isscalar(new_s) || any(strcmp(op, {'isscalar','isvector','issparse','isreal', ...
  'isfloat','isnumeric','isinteger', 'islogical', ...
  'find','norm','all','any','nonzeros','sign','isfinite','isnan','isinf'}))
  % result is a single value or array / or no error/monitor is needed
  b = new_s;
  return
end

% new object -------------------------------------------------------------------
b = copyobj(a);
b.Axis   = a.Axis;

% handle error/monitor stuff
try
  switch op
  case 'acos'
    e = -e./sqrt(1-s.*s);
  case 'acosh'
    e = e./sqrt(s.*s-1);
  case 'asin'
    e = e./sqrt(1-s.*s);
  case 'asinh'
    e = e./sqrt(1+s.*s);
  case 'atan'
    e = e./(1+s.*s);
  case 'atanh'
    e = e./(1-s.*s);
  case 'cos'
    e = -e.*sin(s);
  case 'cosh'
    e = e.*sinh(s);
  case 'exp'
    e = e.*exp(s);
  case 'log'
    e = e./s;
  case 'log10'
    e = e./(log(10)*s);
  case 'sin'
    e = e.*cos(s);
  case 'sinh'
    e = e.*cosh(s);
  case 'sqrt'
    e = e./(2*sqrt(s));
  case 'tan'
    c = cos(s);
    e = e./(c.*c);
  case 'tanh'
    c = cosh(s);
    e = e./(c.*c);
  case 'del2'
    new_s = new_s*2*ndims(s);
    if ~isscalar(e)
      e = 2*ndims(s)*del2(e);
    end
  % case {'uminus','abs','real','imag','uplus','conj','floor','ceil','round','not'}
    % retain error, do nothing
  case {'sparse','full','flipud','fliplr'}
    if ~isscalar(e),  e = feval(op, e); end
    if ~isscalar(m),  m = feval(op, m); end
  case {'double','single','logical', ...
    'int8','int16','int32','int64', 'uint8','uint16','uint32','uint64'}
    e = feval(op, e); % do it in conversion cases
    m = feval(op, m);
% following operators change the Signal size. Axes must follow.
  case {'sum','mean','median','var','cumsum','cumprod','prod','trapz','cumtrapz'}
    % these op remove DIM
    if strcmp(op, 'var') && numel(varargin) > 1
      varargin(1)=[];
    end
    if any(strcmp(op, {'cumprod','prod'}))
      if ~eis0 && any(s)
        es = genop(@rdivide,e,s).^2; 
        es(find(s == 0)) = 0;
        e  = genop(@times, sqrt(sum(es, varargin{:})), new_s); % see binary 'times'
      else e = 0; end
      if ~isscalar(m),  m = prod(m, varargin{:}); end
    else
      if ~isscalar(e),  e = sqrt(sum(e.^2, varargin{:})); end
      if ~isscalar(m),  m = sum(m, varargin{:}); end
    end
    if varargin{1} <= numel(b.Axis),  b.Axis( varargin{1}) = []; end
    if varargin{1} <= numel(b.Label), b.Label(varargin{1}) = []; end
  case {'permute','reshape','flipdim','private_resize'}
    % these op resize with ORDER
    if ~isempty(varargin)
      if ~isscalar(e),  e = feval(op, e, varargin{:}); end
      if ~isscalar(m),  m = feval(op, m, varargin{:}); end
      if ~strcmp(op, 'flipdim')
        index = [ varargin{:} ];
        index(find(index > numel(b.Axis))) = [];
        b.Axis = b.Axis(index);
      end
    end
  case { 'transpose', 'ctranspose'}
    % these op swap [1 2]
    if ~isscalar(e),  e = feval(op, e); end
    if ~isscalar(m),  m = feval(op, m); end
    if numel(b.Axis) > 1,  
      b.Axis( 1:2) = b.Axis( [ 2 1 ]); 
    end
    for index=1:numel(b.Axis)
      if ndims(b.Axis) <=2
        b.Axis{index} = feval(op, b.Axis{index});
      else
        b.Axis{index} = permute(b.Axis{index}, [2 1]);
      end
    end
    if numel(b.Label) > 1, 
      b.Label(1:2) = b.Label([ 2 1 ]); 
    end
  end
end
clear s

% operate with Signal/Monitor and Error/Monitor (back to Monitor data)
if ~mis0 && any(strcmp(op, { ...
  'norm','asin', 'acos','atan','cos','sin','exp','log',...
  'log10','sqrt','tan','asinh','atanh','acosh','sinh','cosh','tanh'}))
  new_s = genop(@times, new_s, m);
  if ~eis0, e = genop(@times, e, m); end
end

% set Signal and Axes
b.Signal = new_s;
b.Error  = abs(e);
b.Monitor= m;
b.Title  =  [ char(op) '(' title(a) ')' ];

% ------------------------------------------------------------------------------
function [m,mis0] = check_em(m)
% CHECK_EM Check monitor/error for valid values (or constant)
  mis0 = ~any(m(:)); % true when []/nan/0
  if ~mis0 && ~isempty(m) && ~isscalar(m) && all(m == m(1)), m=m(1); end
