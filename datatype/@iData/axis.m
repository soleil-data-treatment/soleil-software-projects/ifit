function v = axis(self, varargin)
% AXIS   Get/set axis definition or value in object.
%   AXIS(a, rank) gets axis value (and follow aliases). This is equivalent to
%   the syntax a{rank}. The axis rank 0 corresponds with the Signal/Monitor value.
%   The axis of rank 1 corresponds with rows, 2 with columns, 3 with pages, etc.
%
%   AXIS(a, rank, value) sets axis value. This is equivalent to
%   the syntax a{rank}=value. 
%
%   AXIS(a, rank, value, label) also sets the associated label.
%
%   AXIS(a, 'rank') returns the raw axis definition (numeric or alias/link).
%
%   AXIS(A) returns all defined axes values.
%
%   AXIS(A, 'check') checks the Signal and Axes of an object.
%
%   AXIS(A, 'force') forces to re-assign the Signal and Axes.
%   AXIS(A, 'force signal') forces to re-assign the Signal only (biggest numeric array).
%
% Example: s=iData(rand(10,20)); axis(s,1,2:11); isnumeric(axis(s,1))
% See also iData, fieldnames, findfield, isfield, set, get, getaxis, setaxis, axescheck

if numel(self) > 1
  v = cell(size(self));
  for index=1:numel(self)
    v{index} = feval(mfilename, self(index), varargin{:});
  end
  return
end

v=[];
if nargin == 3
  v = setaxis(self, varargin{:});
else
  if nargin == 2
    if strcmp(varargin{1}, 'check')
      axescheck(self);
    elseif ~isempty(strfind(varargin{1}, 'force'))
      axescheck(self, varargin{1});
    end
  end
  v = getaxis(self, varargin{:});
end
