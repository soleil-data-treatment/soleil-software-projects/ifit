function y=size(s, varargin)
% SIZE   Get object size.
%   D = SIZE(X) for a single object returns the size of its Signal. For
%   an array of objects, the size of the array is returned.
%
%   M = SIZE(X,DIM) returns the length of the dimension specified
%   by the scalar DIM.  For example, SIZE(X,1) returns the number
%   of rows. If DIM > NDIMS(X), M will be 1.
%
%   To get the size of all objects in an array S, use ARRAYFUN('size',S)
%
% Example: s=iData(rand(5)); all(size(s) == size(s.Signal))
% See also iData, get, length, ndims

if numel(s) > 1  % this is an array of iData
  y = builtin('size', s, varargin{:});
  return
end

if numel(s) == 0
  y=[0 0];
else
  y = size(get(s,'Signal'));
  if nargin > 1
    try
      y = y(varargin{:});
    catch
      y = 1;
    end
  end
end
