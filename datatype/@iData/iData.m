classdef iData < dynamicprops
% IDATA Create an IDATA object from given arguments.
%   IDATA objects hold numeric information (arrays, structures, etc), and
%   are manipulated just as single numbers. IDATA objects carry a signal, 
%   axes, error bars, and metadata. You may create IDATA objects by
%   converting Matlab variables, or by accessing e.g. a file or distant
%   resource. IDATA objects may handle internal aliases (char that point
%   to an other location in the object), or external links (see the LINK
%   object).
%
%   Main object properties
%   ----------------------
%
%     Signal:     The object 'value'. The Signal may be an alias.
%     Title:      The Signal label
%     Axis:       A list (cell) of axes for ranks 1-ndims. May be aliases.
%     Error:      The Error bar on the Signal. May be an alias.
%     Monitor     The Monitor, e.g. counting time, weight, etc. May be an alias.
%     Name:       The Name of the object.
%     Data:       The object unstructured data (usually imported).
%
%   Usual methods
%   -------------
%
%   F = IDATA(FILE)             imports given FILE or directory.
%
%   F = IDATA(X, Y, ... Signal) imports given numeric arrays as axes and Signal.
%
%   F = IDATA(MODEL, P, X,...)  imports given iFunc MODEL evaluated with 
%   parameters P and axes X... Any empty argument is guessed.
%
%   IDATA FORMATS               lists all known file formats.
%
%   FILE = save(IDATA, ...)     saves IDATA objects
%
%   Mathematical operators (abs, sin, fft, etc).
%
%   The configuration of the data formats is controled with 'ifitpref'.
%
% Example: a=iData; numel(a) == 1
% Example: a=iData(peaks); ndims(a) == 2
% Example: a=iData(gauss); ndims(a) == 1
% Example: a=iData(gauss2d); ndims(a) == 2
% See also set, get, findfield, importdata, load, ifitpref, ifitopen,
% iData/iData, iData/load, iData/saveas

  properties
    Signal          = [];         % Signal (vs Axes).
    Title           = '';         % Label for_Signal.
    Axis            = {};         % Axes for the Signal.
    Label           = {};         % Labels of Axes.
    Error           = [];         % Error bar on the Signal.
    Monitor         = 1;          % Monitor, e.g. counting time, weight, etc.
    
    Name            = '';         % Name of the object.
    Data            = [];         % Imported data.
    Date            = clock;      % Date of creation of the object.
    Format          = '';         % Format of the initial data, as text.
    Loader          = [];         % Loader configuration (struct).
    Source          = '';         % Source of the data, e.g. file name, URL, etc.
    Tag             = '';         % Object unique ID.
    UserData        = [];         % An area to store what you want.
  end
  
  properties (Access=protected)
    Attributes      = struct();   % Attributes (structure that mimics the Data)
    cache           = struct();
  end
  
  events
    % These are inherited from dynamicprops
    % PropertyAdded
    % PropertyRemoved
    % ObjectBeingDestroyed
    FileLoaded % Triggered when a new file is loaded
    ObjectUpdated % triggered when the object is updated (Signal, Axes)
  end
  
  methods
  
    function new = iData(varargin)
% IDATA Create an IDATA object from given arguments.
%   F = IDATA(FILE) imports given FILE. FILE can be a single file or directory
%   a cell of files or directories. These may make use of wildcards (*,?).
%   Distant URL's are also supported, as well as compressed files. The imported
%   data is stored in the Data property, whereas the importation information is
%   stored in the Loader property. You may equally call IDATA.LOAD or IFITOPEN.
%
%   F = IDATA(FILE, LOADER, ...) specifies the format loader to use. It can be a
%   function name, a function handle, an extension of keyword, 'auto' for 
%   automatic guess, 'gui' to popup a format selector, or a structure:
%             LOADER.method = 'function name'
%             LOADER.options= string of options to catenate after file name
%                          OR cell of options to use as additional arguments
%                             to the method
%   additional arguments are passed to the import routine.
%
%   F = IDATA( ..., 'raw') only imports the data, but does not apply callbacks.
%   You may call AXESCHECK for a default Signal/Axes set.
%
%   F = IDATA(X, Y, ... Signal) imports given numeric arrays as axes and signal.
%
%   F = IDATA(arg1, arg2, ...) imports given arguments into the Data property.
%
%   IDATA FORMATS         lists all known file formats.
%   IDATA FORMATS TOKEN   lists all known file formats matching TOKEN.
%   IDATA VERBOSE         sets verbosity to display more information.
%   IDATA SILENT          sets verbosity to keep silent.
%   IDATA CONFIG          returns the configuration in the Loader property.
%
%   Default supported formats include: any text based including CSV, Lotus1-2-3, SUN sound, 
%     WAV sound, AVI movie, NetCDF, FITS, XLS, BMP GIF JPEG TIFF PNG ICO images,
%     HDF4, HDF5, MAT workspace, XML, CDF, JSON, YAML, IDL, Python Numpy
%   Other specialized formats include: McStas, ILL, SPEC, ISIS/SPE, INX, EDF, Mantid.
%     SIF, MCCD/TIFF, ADSC, CBF, Analyze, NifTI, STL,PLY,OFF, CIF/CFL,
%     EZD/CCP4, Bruker Varian and JEOL NMR formats, Bruker OPUS, LabView LVM and TDMS,
%     Agilent and Thermo Finnigan MS, Quantum Design VMS, ENDF/ACE, Agilent SDF
%   Compressed files are also supported, with on-the-fly extraction (zip, gz, tar, Z).
%
%   Distant files are supported through e.g. URLs such as 
%     file://, ftp:// http:// and https://
%
%   The configuration of the data formats is controled with 'ifitpref'
%
% Typical usage:
%   iData('file'); 
%   iData('file','loader'); 
%   iData({'file1','file2',...}); 
%   iData('file.zip')
%   iData('http://path/name'); 
%   iData(x,y,signal);
%
% Example: a=iData; numel(a) == 1
% Example: a=iData(peaks); ndims(a) == 2
% Example: a=iData(gauss); ndims(a) == 1
% Example: a=iData(gauss2d); ndims(a) == 2
% See also set, get, findfield, importdata, load, ifitpref, ifitopen

      new.Tag = [ 'iD' sprintf('%0.f', private_id()) ]; % unique ID
      if ~nargin, return; end
      
      % handle "commands"
      if ~isempty(varargin), filename = varargin{1}; else filename = []; end
      if ~isempty(filename) && ischar(filename) && size(filename, 1) == 1 && any(strcmpi(filename, ...
          {'silent','normal','debug','verbose','compile','check','config','formats'}))
        if numel(varargin) > 1, loader = varargin{2}; else loader = []; end
        if any(strcmpi(filename, {'find','formats'})), filename = 'display'; end
        if ~isempty(loader) && ~strcmp(loader,'auto')
          new.Data   = ifitpref(filename, loader);
        else
          new.Data   = ifitpref(filename);
        end
        new.Format = [ mfilename ' configuration' ];
        new.Loader = new.Data;
        if isstruct(new.Loader) && isfield(new.Loader,'FileName') 
          new.Source = new.Loader.FileName;
        else
          new.Source = [ 'File formats matching ' loader ];
        end
      elseif isa(filename, 'iFunc')
        % we evaluate the model 'filename' and set iData value and axes
        [signal, model, ax, name] = feval(varargin{:});
        new      = iData(ax{:}, signal);
        new.Name = name;
        new.Data.Model           = model;
        new.Data.ModelParameters = cell2struct(model);
      else
        new = load(new, varargin{:}); % handles files and numeric inputs
      end
      
    end
    
% Converters -------------------------------------------------------------------
    
    function v = cast(self, typ)
      %   CAST  Cast object to a different data type or class.
      %     B = CAST(A,NEWCLASS) casts A to class NEWCLASS. A must be convertible to
      %     class NEWCLASS. NEWCLASS must be the name of one of the builtin data types:
      %       double single logical 
      %       int8 int16 int32 int64 uint8 uint16 uint32 uint64
      %     B is returned as a simple array of type NEWTYPE.
      %
      %   To convert the Signal but keep the object use: unary(A,NEWCLASS)
      %
      % Example: s=iData(1:10); isa(cast(s, 'uint64'), 'uint64')
      % See also: iData.double, iData.int32, iData.uint32
      if nargin < 2, typ=''; end
      if isempty(typ), typ='double'; end
      if numel(self) == 1
        v = cast(get(self,'Signal'), typ);
      else
        v = cell(size(self));
        for index=1:numel(self); v{index} = cast(self(index), typ); end
      end
    end % cast

    function v = double(self)
      %   DOUBLE Convert to double precision.
      %      DOUBLE(X) returns the double precision value of the object (float64).
      %
      %      To convert the Signal but keep the object use: unary(X, 'double')
      %
      % Example: s=iData(logical(-5:5)); isa(double(s), 'double')
      % See also: iData.single, iData.cast
      v = cast(self, 'double');
    end

    function v = single(self)
      %   SINGLE Convert to SINGLE precision.
      %      SINGLE(X) returns the single precision value of the object (float32).
      %
      %      To convert the Signal but keep the object use: unary(X, 'single')
      %
      % Example: s=iData(logical(-5:5)); isa(single(s), 'single')
      % See also: iData.double, iData.cast
      v = cast(self, 'single');
    end

    function v = logical(self)
      %   LOGICAL Convert to logical (0=false, any other=true).
      %      LOGICAL(X) returns the logical value of the object.
      %
      %      To convert the Signal but keep the object use: unary(X, 'logical')
      %
      % Example: s=iData(-5:5); isa(logical(s), 'logical')
      %
      % See also: iData.double, iData.cast
      v = cast(self, 'logical');
    end

    function v = uint32(self)
      %   UINT32 Convert to uint32, e.g. for indexing.
      %      UINT32(X) returns the unsigned integer alue of the object.
      %
      %      To convert the Signal but keep the object use: unary(X, 'uint32')
      %
      % Example: s=iData(-5:5); isa(uint32(s), 'uint32')
      % See also: iData.double, iData.cast
      v = cast(self, 'uint32');
    end

    function v = int32(self)
      %   INT32 Convert to int32.
      %      INT32(X) returns the integer value of the object.
      %
      %      To convert the Signal but keep the object use: unary(X, 'int32')
      %
      % Example: s=iData(-5:5); isa(int32(s), 'int32')
      % See also: iData.double, iData.cast
      v = cast(self, 'int32');
    end
    
% simple initializers ----------------------------------------------------------

    function o = ones(self, varargin)
      % ONES   Ones array.
      %    ONES(S,N) is an N-by-N matrix of 'S' structure.
      %
      %    ONES(S, M,N) or ONES(S, [M,N]) is an M-by-N matrix of 'S' structure.
      %
      %    ONES(S,M,N,P,...) or ONES(S,[M N P ...]) is an M-by-N-by-P-by-... array of
      %    'S' structure.
      %
      % Example: numel(ones(iData, 11,10)) == 110
      % See also: iData.reshape
      if nargin == 1, o=self(1); return; end
      o = repmat(self, varargin{:});
      if iscell(o)
        o = [ o{:} ];
      end
    end % ones
    
    function z = zeros(self, varargin)
      % ZEROS  Zeros array.
      %    ZEROS(S,N) is an N-by-N matrix of empty structures.
      %
      %    ZEROS(S,M,N) or ZEROS(S,[M,N]) is an M-by-N matrix of empty structures.
      %
      %    ZEROS(S,M,N,P,...) or ZEROS(S,[M N P ...]) is an M-by-N-by-P-by-... array of
      %    empty structures.
      %
      %    ZEROS(S) removes all properties except base ones, and keep metadata.
      %
      % Example: numel(zeros(iData, 11,10)) == 110
      % See also: iData.reshape
      if nargin == 1, z=iData; return; end
      z = ones(iData, varargin{:});
    end % zeros
    
  end

end
