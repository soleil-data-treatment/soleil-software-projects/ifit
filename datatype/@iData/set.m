function s = set(s, varargin)
% SET    Set object properties.
%    SET(S,'PropertyName','Value') set the value of the specified
%    property/field in the object.  
%    The 'PropertyName' can be a full structure path, such as 'field1.field2' in
%    in which case the value assigment follows the path/alias. The Value can be 
%    an alias/path given as a string including '.', '()' and '{}' as in SUBSTRUCT
%    and SUBSASGN, and referencing an other part/value of the object S.
%    SET(S,'PropertyName','Value','Label') also sets the label attribute for property.
%
%    SET(S, SUBS, V) is similar to SUBSASGN, except that it fully evaluates
%    every level of the inner object path, to sets the final value. 
%    The SUBS can be an alias/path given as a string or SUBSTRUCT including 
%    '.', '()' and '{}', and referencing an other part/value of the object S.
%    The '/' separator in the path can also be used, e.g. 'level1/level2.level3'.
%    The '/../' token is used to travel one level up, as in a file path, but
%    array/cell indexing must not be used for the parent (e.g. no 'a.b(1)/../cc')
%    SET(S, 'FIELD1...',V) is equivalent to S.FIELD1_...=V (NOTE _ at end).
%    To set the raw field value, use S.FIELD1=V or SUBSASGN(S,'FIELD1',V)
%
%    [S, FQF] = SET(S,...) returns the fully qualified (resolved) field FQF.
% 
%    SET(S) displays all structure field names.
%
% Example: s=iData; set(s,'Data',[1 2]); numel(s.Data)==2
% Example: s=iData(peaks); set(s,'Signal(2)',-0.01); get(s, 'Signal(2)') == -0.01
% See also iData, iData/get, iData/subsref, iData/subsasgn, findfield, tree

  if nargin == 1
    disp([ class(s) ' object properties:' ]);
    disp('Signal          (value/alias)   Signal (vs Axes)');
    disp('Title           (string)        Label for_Signal');
    disp('Axis            (cell)          Axes for the Signal');
    disp('Label           (cellstr)       Labels of Axes');
    disp('Error           (value/alias)   Error bar on the Signal');
    disp('Monitor         (value/alias)   Monitor, e.g. counting time, weight, etc');
    disp('Name            (string)        Name of the object');
    disp('Data            (anything)      Imported data');
    disp('Date            (datevec)       Date of creation of the object');
    disp('Format          (string)        Format of the initial data, as text');
    disp('Loader          (struct)        Loader configuration (struct)');
    disp('Source          (string)        Source of the data, e.g. file name, URL, etc');
    disp('Tag             (string)        Object unique ID');
    disp('UserData        (anything)      An area to store what you want');
    return
  end
  
  % handle array of objects
  if numel(s) > 1
    for index=1:numel(s)
      s(index) = set(s(index), varargin{:});
    end
    return
  end
  
  % handle multiple name/value pairs
  % main loop on input arguments
  for index=1:2:numel(varargin)
    field = varargin{index};
    if index >= numel(varargin)
      if ischar(varargin{index}) && index >=3
        % this may be a label for past field
        label(s, varargin{index-2}, varargin{index});
      end
      break
    end
    value = varargin{index+1};
  
    if ischar(field) && size(field, 1) > 1
      field = cellstr(field);
    end
    
    % handle array/cell of fields
    if iscell(field)
      for index=1:numel(field)
        s = set(s, field{index}, value);
      end
      return
    end
    
    if ~ischar(field) && ~isstruct(field)
      error([ mfilename ': field to set in object must be a struct/char/cell, not ' class(field) ]);
    end

    % evaluate link recursively
    if ischar(field) && any(strcmp(field, {'Title','Name','Label','Format','Loader','Source','Tag'}))
      s   = subsasgn(s, field, value);
    else
      s = myset(s, field, value);
    end
  end
  
end

% ------------------------------------------------------------------------------

function s = myset(s, f, v)
  % MYSET Evaluate s.(f) = v
  % We assume 'f' is a string or a SUBS
  
  if isempty(f), return; end
  
  if ischar(f)
    % handle a path/alias 'aaa.bbb.ccc(...)' -> resolve 'aaa.bbb.ccc'
    [alias,indexing] = split_alias_index(f);  % up to indexing part
    [val,last_alias] = get_alias(s, alias);   % recursive s.(last_alias)=val
    % we assign val(f)=v in case of further indexing 
    if ~isempty(indexing)
      f   = str2subs(indexing);       % further indexing
      v   = subsasgn(val, f, v);      % val(f)=v -> v
    end
    % and then assign s.(last_alias)=v
    if isempty(last_alias) && ~isempty(alias)
      last_alias = alias;
    end
    s = subsasgn(s, str2subs(last_alias), v);
    return
    
  elseif isstruct(f) && isfield(f, 'subs') % rarely used as e.g. f is char
    % we concatenate the '.' members and evaluate it
    index   = find(~strcmp({f.type}, '.'),1); % position of not '.' i.e. (){} 
    if isempty(index) || index > 1 %  no (){} or one after a '.'
      alias   = { f.subs };               % all subs, will use '.'
      if ~isempty(index), alias = alias(1:(index-1)); end
      alias   = sprintf('.%s', alias{:}); % concatenate all '.field'
      alias(1)= [];                       % remove '.' at start
      [val, fqf]= get(s, alias);        % recursive val = s.(alias) = s.(fqf)
      if ~isempty(index) && index > 1
        f       = f(index:end);           % indexing part
        v       = subsasgn(val, f, v);    % then set   val(f) = v -> v
      end
      s = subsasgn(s, fqf, v);        % and store s.(fqf) = v
      return
      
    else % ~isempty(index) && index == 1: start with a (){}
      s         = subsasgn(s, f, v); % we only have indexing in 'f' s(f) = v
      fqf       = { f }; f=[];
    end
    
  else % f is not char, nor struct
    error([ mfilename ': invalid set property ' class(f) ]);
  end
    
end % myset

% ------------------------------------------------------------------------------
function [alias, last_alias] = get_alias(s, f)
% GET_ALIAS recursively resolve alias 'f'

  maxResolve=10; % max levels of recursion/resolve
  
  alias   = f; last_alias = alias;
  resolve = 1; previous_alias={}; previous_indexing='';
  while resolve < maxResolve
    % exit: alias is not a char
    if ~ischar(alias) || isempty(alias)
      break; 
    end
    if alias(1)=='.', alias(1)=[]; end  % no '.' at start for isfield and strcmp to work
    
    % exit: first part of alias is not a field (path exact case isfield)
    [alias1,indexing1] = split_alias_index(alias);
    if ~isfield(s, alias1) || any(strcmp(alias1, previous_alias))
      break; 
    end
    previous_alias{end+1} = alias1;
    
    % exit: failure to evaluate with subsref/isfield
    % else: evaluate 'alias' value, and loop
    if isfield(s, alias1)
      % failed global eval. Try in parts
      alias1 = subsref(s, alias1); % only field path
      if ~ischar(alias1)
        if ~isempty(indexing1)
          alias1 = subsref(alias1, str2subs(indexing1));
        end
      else
        alias1 = [ alias1 indexing1 ]; % retain indexing
      end
      alias = alias1;
    end
    resolve = resolve+1;
    if ischar(alias), last_alias=alias; end
    
  end % while

end % get_alias

% ------------------------------------------------------------------------------
function [alias1,indexing1] = split_alias_index(alias)

  indexing1 = ''; alias1 = alias;
  index     = find(alias == '(' | alias == '{', 1); % up to indexing part
  if ~isempty(index)
    if index >= 2
      alias1 = alias(1:(index-1));
    end
    indexing1= alias(index:end);
  end
end % split_alias_index

