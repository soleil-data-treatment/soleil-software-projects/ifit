function r = runtests(s, m, option)
% RUNTESTS Run a set of tests on object methods.
%   RUNTESTS(s) checks all object methods. 
%   A test can be written as:
%     An 'Example:' line in the header for which following expressions are executed.
%       The test can span on more lines with the '...' continuation symbol. 
%     A script named 'test_<class>_<method>'.
%   In deployed versions, only the 'test' functions can be used.
%   Each test result must return e.g. a non-zero value, or 'OK'.
%
%   RUNTESTS(s, 'class/method') and RUNTESTS(s, {'class/method1',..}) checks only
%   the given methods. When not given or empty, all methods are checked.
%
%   RUNTESTS(s, DIR) tests all m-files in given directory.
%
%   RUNTESTS(..., 'testsuite') generates a test suite (list of 
%   'test_<class>_<method>' functions).
%
%   RUNTESTS(..., 'debug') shows more information for each test. This is 
%   equivalent to ifitpref('verbose')
%
%   R = RUNTESTS(...) performs the test and returns a structure array with test results.
%
%   RUNTESTS(s, R) where R is a RUNTESTS structure array, displays the test results.

if nargin <2
  m = [];
end

if isstruct(m) % print report for past tests
  runtests_report(m)
  return
end

if nargin < 3 
  if ischar(m) && ~isempty([ strfind(m, 'testsuite') strfind(m, 'debug') strfind(m, 'verbose') ])
    option=m; m = '';
  else option = ''; end
end
option=lower(option);
if ifitpref('verbosity') > 1 || ~isempty(strfind(option, 'verbose'))
  option = [ option ' debug' ];
end

if isempty(m)
  m = runtests_getmethods(s);
elseif ischar(m) && isdir(m)
  w=what(m); m={};
  for mindex=1:numel(w.m)
    [p,f,e] = fileparts(w.m{mindex});
    m{mindex}= f;
  end
elseif ischar(m)
  m = cellstr(m);
elseif ~iscellstr(m)
  error([ mfilename ': require char/cellstr/dirname or empty argument.' ])
end

% perform all tests in a tmp directory
pw = pwd;
p = tempname; mkdir(p); cd(p);
onCleanup(@()cd(pw));
r = runtests_dotest(s, m, option);

% display results
fprintf(1, '%s: %s %i tests from %s\n', mfilename, class(s), numel(r), pwd);
disp('--------------------------------------------------------------------------------')
runtests_report(r);
cd(pw)

% generate testsuite
if ~isdeployed && ischar(option) && ~isempty(strfind(option, 'testsuite'))
  runtests_testsuite(s, r);
end

% ------------------------------------------------------------------------------
function r = runtests_dotest(s, m, option)
% RUNTESTS_DOTEST performs a test for given method 'm'
  r = [];
  c = class(s);
  n = num2str(numel(c)+20);
  fprintf(1,'Running %s (%s) for %i methods (from %s)\n',...
    mfilename, c, numel(m), pwd);
  ntest=0;
  

  for mindex=1:numel(m)
    % test if the method is functional
    if isempty(m{mindex}) || ~ischar(m{mindex}), continue; end
    if strcmp([ c '/' mfilename ], m{mindex}) ...
    || strcmp([ c '.' mfilename ], m{mindex}) , continue; end % not myself
    res = [];
    
    % check if code is valid (not in deployed). 
    % Must be convertible to pcode (in temporary dir)
    if ~isdeployed
      pw = pwd;
      p = tempname; mkdir(p); cd(p)
      failed = [];
      try
        pcode(which(m{mindex}));
      catch ME
        failed= ME;
      end
      rmdir(p, 's'); cd(pw); % clean temporary directory used for the p-code
      if ~isempty(failed)
        h   = []; % invalid method (can not get help)
        res = runtests_init(m{mindex});
        res.Details.Status = 'ERROR';
        res.Details.Output = failed;
        res.Incomplete     = true;
        res.Failed         = true;
        r = [ r res ];
        if ischar(option) && ~isempty(strfind(option, 'debug'))
          fprintf(1, [ '%-' n 's %s (syntax error)\n' ], m{mindex}, res.Details.Status);
        end
        continue
      end
    end
    
    % get the HELP text (not in deployed)
    if ~isdeployed
      failed = [];
      try
        h   = help([ m{mindex} ]);
      catch ME
        failed = ME;
      end
      if ~isempty(failed) % invalid method (can not get help)
        res = runtests_init(m{mindex});
        res.Details.Status = 'ERROR';
        res.Details.Output = failed;
        res.Incomplete     = true;
        res.Failed         = true;
        r = [ r res ];
        if ischar(option) && ~isempty(strfind(option, 'debug'))
          fprintf(1, [ '%- ' n 's %s (invalid help)\n' ], m{mindex}, res.Details.Status);
        else 
          fprintf(1,'X');
          if rem(ntest, 80) == 0, fprintf(1,'\n'); end
        end
        continue
      end

      % get the Example lines in Help
      if ~isempty(h)
        h = textscan(h, '%s','Delimiter','\n'); h=strtrim(h{1});
        % get the Examples: lines
        ex= strfind(h, 'Example:');
        ex= find(~cellfun(@isempty, ex));
      else 
        ex=[];
      end

      % perform test for each Example line in Help. Ex is an index.
      for this_example_index=1:numel(ex)
        index = ex(this_example_index); % index of line starting with Example.
        if isempty(index) || isempty(h{index})
          continue;
        end
        h{index} = strrep(h{index}, 'Example:','');
        % we add lines. First must start with Example, and may go on with '...'
        code = ''; iscontinuation = false;
        for lindex=index:numel(h)
          rline = h{lindex};
          if (lindex == index) || iscontinuation
            code = [ code strrep(rline, '...','') ];
          end
          if numel(rline) > 3 && strcmp(rline(end:-1:(end-2)),'...'), iscontinuation = true;
          else break; end
        end
        if ischar(option) && ~isempty(strfind(option, 'debug'))
          if numel(r) == 0
            ns = repmat(' ',1,numel(c)+14);
            fprintf(1, [ 'TEST ' ns '| STATUS | TIME | CODE\n' ]);
            ns = repmat('-',1,numel(c)+19);
            fprintf(1, [ ns '|--------|------|------\n' ]);
          end
          fprintf(1, [ '%-' n 's ' ], m{mindex});
        end
        res = runtests_dotest_single(s, m{mindex}, code);
        if isempty(res) || isempty(code)
          res = runtests_init(m{mindex});
          res.Details.Status = 'notest';
          res.Passed         = true;
          res.Incomplete     = true;
        end
        r = [ r res ];
        if ischar(option) && ~isempty(strfind(option, 'debug'))
          fprintf(1, '%-8s %-5.2f %s\n', res.Details.Status, res.Duration, code);
        else 
          fprintf(1,runtests_symbol(res));
          ntest = ntest+1;
          if rem(ntest, 80) == 0, fprintf(1,'\n'); end
        end
      end
    end % if not deployed
    
    % now test if we have a 'test' function as well
    if exist([ 'test_' c '_' m{mindex} ],'file')
      code = [ 'test_' c '_' m{mindex} ];
      res = runtests_dotest_single(s, m{mindex}, code);
      r = [ r res ];
      if ischar(option) && ~isempty(strfind(option, 'debug'))
        fprintf(1, [ '%-' n 's %-8s  %-5.2f %s\n' ], ...
          m{mindex}, res.Details.Status, res.Duration, code);
      else 
        fprintf(1,runtests_symbol(res));
        ntest = ntest+1;
        if rem(ntest, 80) == 0, fprintf(1,'\n'); end
      end
    end
    
    % check if there is no test for a given method
    if isempty(res)
      res = runtests_init(m{mindex});
      res.Details.Status = 'notest';
      res.Passed         = true;
      res.Incomplete     = true;
      r = [ r res ];
      if ischar(option) && ~isempty(strfind(option, 'debug'))
        fprintf(1, [ '%-' n 's %-8s (pass)\n' ], m{mindex}, res.Details.Status);
      else 
        fprintf(1,runtests_symbol(res));
      end
    end

  end % mindex
  fprintf(1, '\n');
  disp([ 'Done ' mfilename ' (' c ')' ])
  
% ------------------------------------------------------------------------------
function res = runtests_init(m)
% RUNTESTS_INIT return an empty test result structure

  res.Name      =char(m);
  res.Passed    =false;
  res.Failed    =false;
  res.Incomplete=false;
  res.Duration  =0;
  res.Details.Name   = m;
  res.Details.Code   =''; 
  res.Details.Status ='';
  res.Details.Output ='';
  res.Details.Code   ='';
  
% ------------------------------------------------------------------------------

function m = runtests_getmethods(s)
% RUNTESTS_GETMETHODS return a list of class methods

  m = methods(s);
  % we should remove superclass methods, to only test what is from that class
  for super=superclasses(s)'
    ms = methods(super{1});
    for sindex=1:numel(ms)
      mindex=find(strcmp(ms{sindex}, m));
      if ~isempty(mindex)
        m{mindex}='';
      end
    end
  end
  m = cellstr(m);
  for mindex=1:numel(m)
    if ~isempty(m{mindex}) && ~strcmp(class(s),m{mindex})
      m{mindex} = [ class(s) '/' m{mindex} ];
    end
  end

% ------------------------------------------------------------------------------
function res = runtests_dotest_single(s, m, code)
% RUNTESTS_DOTEST_SINGLE text a single 'Example:' line or 'test_' function

  % test if the method is functional
  res = [];
  if nargin < 2, m = []; end
  if isempty(m) || (~ischar(m) && ~isa(m, 'function_handle')), return; end
  if strcmp(mfilename, m), return; end % not myself
  if nargin < 3, code = m; end
  if isempty(code), return; end
  
  % init result to 'empty'
  res = runtests_init(m);
  res.Details.Code = strtrim(code);
  
  % perform test <<< HERE
  t0 = clock;
  T = runtests_sandbox(res.Details.Code);
  res.Passed   = T.passed;
  res.Failed   = T.failed;
  res.Duration = etime(clock, t0);
  res.Details.Output = T.output;
  if T.passed,     res.Details.Status = 'passed';
  elseif T.failed, 
    if isempty(T.output) || ischar(T.output) res.Details.Status = 'FAILED';
    else res.Details.Status = 'ERROR'; res.Incomplete=true; end
  end
  

% ------------------------------------------------------------------------------
function runtests_report(r)
% RUNTESTS_REPORT Display the test results

  if ~isstruct(r), return; end
  % we display details for Incomplete and Failed tests
  n=0;
  for index=1:numel(r)
    n = max(n, numel(r(index).Name));
  end
  n = num2str(n);
  for index=1:numel(r)
    res = r(index);
    if res.Failed || res.Incomplete
      % get the error message
      if isa(res.Details.Output,'MException')
        ME = res.Details.Output;
        if ~isempty(ME.stack) && ~strcmp(ME.stack(1).name, 'runtests_sandbox')
          msg = sprintf('%s in %s:%i', ...
            ME.message, ME.stack(1).file, ME.stack(1).line);
        else
          msg = ME.message;
        end
      else msg = res.Details.Output; end
      fprintf(1, [ '%-' n 's %-7s %s %s\n' ], ...
        res.Name, res.Details.Status, char(res.Details.Code), cleanupcomment(msg));
    elseif res.Passed && strcmp(res.Details.Status, 'notest')
      fprintf(1, [ '%-' n 's %-7s %s %s\n' ], ...
        res.Name, res.Details.Status, char(res.Details.Code), '');
    end
  end
  disp('Totals:')
  fprintf(1, '  %i Passed, %i Failed, %i Incomplete.\n', ...
    sum([ r.Passed ]), sum([ r.Failed ]), sum([ r.Incomplete ]));
  fprintf(1, '  %.2f seconds testing time.\n', sum([ r.Duration ]));

% ------------------------------------------------------------------------------
function T = runtests_sandbox(ln)
% RUNTESTS_SANDBOX Sandbox execution, outside of the class to be able to 
% properly evaluate strings.
  
  T = evals(ln);
  
function c = runtests_symbol(res)
% RUNTESTS_SYMBOL Symbol to use during tests as a progress bar.

  switch lower(res.Details.Status)
  case {'error','failed'}
    c='X';
  case 'notest'
    c='0';
  otherwise
    c='.';
  end
  
% ------------------------------------------------------------------------------ 
function runtests_testsuite(s, r)
% RUNTESTS_TESTSUITE generate a set of functions from the Example lines

  p = tempname; mkdir(p); 
  disp([ mfilename ': generating test suite...' ]);
  for index=1:numel(r)
    res = r(index);
    f   = strrep(res.Name, '/', '_');
    filename = [ 'test_' f ];
    if strcmp(res.Details.Code, filename), continue; end
    if isempty(res.Details.Code), continue; end
    
    % create the file when it does not exist
    if isempty(dir(fullfile(p,[ filename '.m' ])))
      fid = fopen(fullfile(p,[ filename '.m' ]),'w');
      if fid == -1, continue; end
      fprintf(fid, 'function ret=%s\n', filename);
      fprintf(fid, '%% %s checks for class %s, method ''%s''\n', ...
        upper(filename), class(s), res.Name);
      fprintf(fid, '%%   %s returns ERROR when the test could not run\n%%   OK when test is passed, FAILED otherwise.\n',...
        upper(filename));
    else
      % the test already exists. We have multiple tests for same method. catenate.
      fid = fopen(fullfile(p,[ filename '.m' ]),'a');
      if fid == -1, continue; end
      fprintf(fid, '%% -------------------------------------------------\n');
    end
    fprintf(fid, 'clear ans\n');
    fprintf(fid, 'try\n');
    fprintf(fid, '  %s\n', res.Details.Code);
    fprintf(fid, 'catch ME\n');
    fprintf(fid, '  ret=[ ''ERROR '' mfilename ]; return\n');
    fprintf(fid, 'end\n');
    fprintf(fid, 'result = ans;\n');
    fprintf(fid, 'if (isnumeric(result) || islogical(result)) && all(result(:))\n');
    fprintf(fid, '  ret=[ ''OK '' mfilename ];\n');
    fprintf(fid, 'elseif ischar(result) && any(strcmpi(strtok(result),{''OK'',''passed''}))\n');
    fprintf(fid, '  ret=[ ''OK '' mfilename ];\n');
    fprintf(fid, 'else\n');
    fprintf(fid, '  ret=[ ''FAILED '' mfilename ]; return\n');
    fprintf(fid, 'end\n');
    fclose(fid);
  end
  disp([ mfilename ': test suite generated in <a href="' p '">' p '</a>' ]);
% ------------------------------------------------------------------------------
