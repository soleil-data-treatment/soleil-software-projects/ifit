function a=moveaxis(a, dim, shift)
% MOVEAXIS Shifts an object axis of specified rank by a value.
%   A=MOVEAXIS(A, RANK, SHIFT) shifts object axes RANK by SHIFT.
%   This is equivalent to A{RANK} = A{RANK}+SHIFT;
%
% Example: a=iData(peaks); moveaxis(a, 1, 5); max(axis(a,1))==54
% See also  iData/getaxis, iData/setaxis

if nargin < 3
  return
end

a = setaxis(a, dim, getaxis(a, dim)+shift);
