function [v, fqf] = get(s, varargin)
% GET    Get object property values.
%    V = GET(S,'PropertyName') returns the evaluated value of the specified
%    property/field in the object.  If S is an array, then GET will 
%    return a cell array of values.
%    If 'PropertyName' is replaced by a 1-by-N or N-by-1 cell array of strings 
%    containing property names, then GET will return an M-by-N cell array of
%    values.
%
%    V = GET(S, SUBS) is similar to SUBSREF, except that it fully evaluates
%    every level of the inner object path, to return the final value. 
%    The SUBS can be an alias/path given as a string or SUBSTRUCT including 
%    '.', '()' and '{}', and referencing an other part/value of the object S.
%    The '/' separator in the path can also be used, e.g. 'level1/level2.level3'.
%    The '/../' token is used to travel one level up, as in a file path, but
%    array/cell indexing must not be used for the parent (e.g. no 'a.b(1)/../cc')
%    V = GET(S, 'FIELD1...') is equivalent to V = S.FIELD1_...
%    To get the raw field value, use V=S.FIELD1 or V=SUBSREF(S,'FIELD1')
%    The Properties 'Title','Name','Label','Format','Loader','Source','Tag' are
%    not evaluated.
%
%    [V, FQF] = GET(...) returns the fully qualified (resolved) field FQF.
% 
%    GET(S) displays object signal and axes information (see also TREE(S)).
%
% Example: s=iData; s.Data=[1 2]; isnumeric(get(s,'Data'))
% Example: s=iData(peaks); x=get(s,'Signal'); set(s,'Signal(2)',-0.01); all(get(s,'Signal(1:2)')==[x(1) -0.01])
% See also iData, iData/set, iData/subsref, iData/subsasgn, findfield, tree
  fqf = []; v=[];
  if nargin == 1,    field = ''; 
  elseif nargin ==2, field = varargin{1}; 
  else               field = varargin; 
  end
  if nargin == 1 % view object
    disp(s);
    v=s;
    return; 
  elseif isempty(field)
    return
  end
  
  % handle array of objects/struct
  if numel(s) > 1
    v = cell(size(s)); fqf = v;
    parfor index=1:numel(s)
      [v{index}, fqf{index}] = get(s(index), field);
    end
    return
  end
  
  if ischar(field) && size(field, 1) > 1
    field = cellstr(field);
  end
  
  % handle array/cell of fields
  if iscell(field)
    v = cell(size(field)); fqf=v;
    for index=1:numel(field)
      [v{index}, fqf{index}] = get(s, field{index});
    end
    return
  end
  
  if ~ischar(field) && ~isstruct(field) && ~isobject(field) && ~isa(field, 'function_handle')
    error([ mfilename ': field to get in object must be a struct/char/cell, not ' class(field) ]);
  end
  
  % evaluate link recursively, except 'protected' fields (that can not carry links)
  if ischar(field) && any(strcmp(field, {'Title','Name','Label','Format','Loader','Source','Tag'}))
    fqf = field;
    v   = subsref(s, field);
  else
    [v, fqf] = myget(s, field);
  end
  if iscell(fqf) && numel(fqf) == 1, fqf=fqf{1}; end
  
end

% ------------------------------------------------------------------------------

function [v, fqf] = myget(s, f)
  % MYGET Evaluate s.(f), return value and fully qualified field path (resolved)
  % We assume 'f' is a string or a SUBS
  
  v = []; fqf=[];
  if isempty(f), v=s; return; end
  
  if ischar(f)
    if strncmp(f, 'matlab:', 7) % Evaluate expression
      T   = evals(f, 'self', s, 'this', s, 'ans', true);
      if ~T.failed, v = T.output; else v=T; end
      fqf ={ T }; f = [];
    else % handle a path/alias 'aaa.bbb.ccc(...)' -> resolve 'aaa.bbb.ccc'
      [alias,indexing] = split_alias_index(f);  % up to indexing part
      [v,last_alias]   = get_alias(s, alias);   % s.(last_alias)=v
      f   = str2subs(indexing);   % further indexing -> get_value -> v(f)
      fqf = { str2subs(last_alias) }; if ~isempty(f), fqf = { fqf{:} f }; end
    end
    
  elseif isstruct(f) && isfield(f, 'subs') % rarely used as e.g. f is char
    % we concatenate the '.' members and evaluate it
    index   = find(~strcmp({f.type}, '.'),1); % position of not '.' i.e. (){} 
    if isempty(index) || index > 1 %  no (){} or one after a '.'
      alias   = { f.subs };               % all subs, will use '.'
      if ~isempty(index), alias = alias(1:(index-1)); end
      alias   = sprintf('.%s', alias{:}); % concatenate all '.field'
      alias(1)= [];                       % remove '.' at start
      [v, fqf]= myget(s, alias);          % recursive v = s.(alias -> fqf)
      if ~isempty(index) && index > 1
        f       = f(index:end);           % indexing part
        fqf     = { fqf{:} f };
        %         then v=get_value(v, f) below
      else f=[]; % and v=get_value(v)    below
      end
    else % ~isempty(index) && index == 1: start with a (){}
      v         = subsref(s, f); % we only have indexing in 'f' v=s(f)
      fqf       = { f }; f=[];
    end
    
  else % f is not char, nor struct
    error([ mfilename ': invalid get property ' class(f) ]);
  end
  
  % when here, the alias has been resolved, but not as indexing
  v=get_value(v, f); % evaluate final value (including (){} indexing)
    
end % myget

% ------------------------------------------------------------------------------
function [value, last_alias] = get_alias(s, f)
% GET_ALIAS recursively resolve alias 'f'

  maxResolve=10; % max levels of recursion/resolve
  alias   = f; last_alias = alias; value   = alias;
  resolve = 1; previous_alias={}; previous_indexing='';
  while resolve < maxResolve
    % exit: alias is not a char
    if ~ischar(alias) || isempty(alias)
      break; 
    end
    if alias(1)=='.', alias(1)=[]; end  % no '.' at start for isfield and strcmp to work
    
    % exit: first part of alias is not a field (path exact case isfield)
    [alias1,indexing1] = split_alias_index(alias);
    if ~isfield(s, alias1) || any(strcmp(alias1, previous_alias))
      break; 
    end
    previous_alias{end+1} = alias1;
    
    % exit: failure to evaluate with subsref/isfield
    % else: evaluate 'alias' value, and loop
    if isfield(s, alias1)
      % failed global eval. Try in parts
      alias1 = subsref(s, alias1); % only field path
      if ~ischar(alias1)
        if ~isempty(indexing1)
          alias1 = subsref(alias1, str2subs(indexing1));
        end
      else
        alias1 = [ alias1 indexing1 ]; % retain indexing
      end
      alias = alias1;
    end
    resolve = resolve+1;
    if ischar(alias), last_alias=alias; end
    
  end % while
  value = alias; % final value or other (not resolvable)

end % get_alias

% ------------------------------------------------------------------------------
function [alias1,indexing1] = split_alias_index(alias)

  indexing1 = ''; alias1 = alias;
  index     = find(alias == '(' | alias == '{', 1); % up to indexing part
  if ~isempty(index)
    if index >= 2
      alias1 = alias(1:(index-1));
    end
    indexing1= alias(index:end);
  end
end % split_alias_index

% ------------------------------------------------------------------------------
function [v2,f] = get_value(v,f)
% GET_VALUE evaluate final value in subsref call for member
%   The value V must initially be an object, with either feval or double method
%   to return e.g. a numeric array.

  v2 = [];
  if ischar(f), f=str2subs(f); end
  if strncmp(v, 'matlab:', 7)     % Evaluate expression
    T = evals(v, 'self', s, 'this', s, 'varargin',f,'ans', true);
    if ~T.failed, v = T.output; end
  end
  if ~isstruct(f) || isempty(f)   % Evaluate object 'v' (f is invalid)
    if builtin('isnumeric',v)
      v2 = v;
    elseif ismethod(v, 'double')
      v2 = double(v);
    else
      v2 = v; % default
      if ismethod(v, 'feval')
        try
          v2 = feval(v);
        end
      end
    end
  else % we have a subs. Evaluate v(f)
    v2 = v;
    for index=1:numel(f)          % Go through all subs levels iteratively
      f1=f(index);
      if     isstruct(v2) || ischar(v2) || ismethod(v2, 'subsref')
        v2 = subsref(v2, f1);
      elseif ismethod(v2, 'feval')
        try
          v2 = feval(v2, f1);           % v(f)
        catch
          v2 = subsref(feval(v2), f1);  % subsref(v, f)
        end
      elseif builtin('isnumeric',v2) || ismethod(v2, 'double')
        v2 = subsref(double(v2), f1);
      else
        warning('iData:get', [ mfilename ': failed to get value for ' class(v2) f1.type ])
        break
      end
    end
  end
  
end % get_value

