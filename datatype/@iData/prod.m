function b = prod(a,dim)
% PROD Product of elements.
%   P = PROD(A) computes the product of objects elements along
%   columns. P has the same size as A.
%
%   CUMPROD(A,DIM) operates along axis of rank DIM. When DIM=0 the total product
%   is returned.
%
% Example: a=iData(peaks); prod(a,0) == 0
% See also iData, iData/plus, iData/sum, iData/prod, iData/cumprod

if nargin < 2, dim=1; end

b = unary(a, 'prod',dim);
