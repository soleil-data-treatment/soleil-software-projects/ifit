function v = verbosity(self, level)
% VERBOSITY Get or set the verbosity level.
%   VERBOSITY(s) gets the current verbosity level
%
%   VERBOSITY(s,level) sets the current verbosity level, and return previous
%   verbosity level.
%     0 silent
%     1 normal
%     2 debug/verbose
%
% Example: a=iData; verbosity(a,1); verbosity(a)== 1
% See also set, get, ifitpref

  persistent verb
  
  % use ifit preferences
  if exist('ifitpref')
    v = ifitpref('verbosity');
    if nargin > 1 && isnumeric(level)
      ifitpref('verbosity', level);
    end
  else % fallback: no ifit ?? use persistent/global for all objects
    if isempty(verb), verb=1; end
    v = verb;
    if nargin > 1 && isnumeric(level)
      verb = level;
    end
  end
  
end
