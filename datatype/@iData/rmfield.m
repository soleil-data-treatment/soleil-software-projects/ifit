function self=rmfield(self, f)
% RMFIELD Remove fields from a structure array.
%      S = RMFIELD(S,'field') removes the specified field from the
%      m x n structure array S. The size of input S is preserved.
%
%      Only additional fields can be removed. To add a field use:
%        S.FIELD = value;
%
%      S = RMFIELD(S,FIELDS) removes more than one field at a time
%      when FIELDS is a character array or cell array of strings.  The
%      changed structure is returned. The size of input S is preserved.
%
%      S = RMFIELD(S,'all') removes all fields except base ones.
%
% Example: s=iData; s.test =1; ...
%          isfield(s,'test') && ~isfield(rmfield(s,'test'),'test')
% See also setfield, getfield, isfield, fieldnames.

  if nargin ~= 2, return; end
  if ischar(f) && strcmp(f, 'all')
    f = fieldnames(self);
  end
  if ~iscellstr(f), f = cellstr(f); end
  for index=1:numel(f)
    delete(findprop(self, f{index}));
  end
end % rmfield
