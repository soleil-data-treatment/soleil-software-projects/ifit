classdef link < handle
% LINK A container to handle inner objects, functions, or strings to evaluate.
%  The LINK can be evaluated to get its content. It can also be referenced 
%  using sub-indexing (e.g. L(1:10) or L{1} or L.field), as well as assigned
%  when the initial object supports it. The evaluation is only performed when 
%  it is required, so that the LINK instantiation is immediate (aka lazy loading).
%
%  L = LINK(object, ...) connects to an object, that must have a feval or 
%  double method. Additional arguments are passed for the evaluation.
%
%  L = LINK(function_handle, ...) connects to a function, which can be evaluated.
%  Additional arguments are passed for the evaluation.
%
%  L = LINK(expr) evaluates the expression (given as string or cellstr). The 
%  expression can be preceeded with a URL specifying the type of the content, 
%  such as a 
%    'file:','http:','https','ftp:'       to load a distant raw content.
%    'matlab:','python:','perl:','julia:' to use a language.
%  The default content is supposed to be 'matlab:' expression.
%  Linked expressions usually support sub-indexing, and assignments when the 
%  target object does.
%  When using a language expression other than matlab, the link value is always 
%  converted into a numeric vector (empty when no number is found).
% 
% Example: a=link('matlab: rand(5)'); isnumeric(a(1))
% Example: a=link(@iData, fullfile(ifitpath,'data/POSCAR_Si')); isa(feval(a),'iData')
% See also memmapfile, importdata, perl, function_handle, evals

% link('url:')
% link('matlab: python: perl: or julia: code') to get numeric values
% link(MappedTensor(file))
% link(process(cmd)): convert stdout to numeric
% link(@()obj.Signal(10))
%
% link(iFunc, args)
% link(function_handle, args)

  properties
    reference         % The object/data source.
    arguments={};     % Additional arguments for e.g. function_handle.
  end
  
  properties (Access=private)
    evalfcn     = ''; % function for object evaluation
    subsreffcn  = ''; % function to get object inner reference
    subsasgnfcn = ''; % function to set object inner reference
  end
  
  methods
    function obj = link(v, varargin)
    % LINK Create a link to an object.
    %   LINK(object)
    %     The object may support SUBSREF SUBSASGN FEVAL and DOUBLE methods.
    %   LINK(function_handle)
    %   LINK(expression as char or cellstr)
      if nargin == 0
        obj.reference = [];
        return
      end
      obj.reference = v;
      obj.arguments = varargin;
      if isobject(v)
        % e.g. @process, @MappedTensor, @iData, @hdf5prop
        %      @link, @iFunc, function_handle with args
        % Optionnal methods: subsref, feval, double, ...
        if ismethod(v, 'subsref')
          obj.subsreffcn  = @subsref;
        end
        if ismethod(v, 'subsasgn')
          obj.subsasgnfcn = @subsasgn;
        end
        if     ismethod(v,'feval')  obj.evalfcn = @feval;
        elseif ismethod(v,'double') obj.evalfcn = @double;
        else
          error([ mfilename ': class ' class(v) ' must provide method feval or double. Not available.' ]);
        end
      elseif isa(v, 'function_handle')
        obj.evalfcn = @feval;
      elseif ~ischar(v) && ~iscellstr(v)
        error([ mfilename ': must provide object, function_handle, char or cellstr.' ]);
      end
      
    end % link
    
    function v = feval(self, varargin)
    % FEVAL Evaluate a LINK object.
    %   FEVAL(LINK,...) evaluates the LINK object and return its value, e.g as a 
    %   numeric value.
    %
    %   Example: a=link('rand(5)'); isnumeric(feval(a))
      if ischar(self.reference) || iscellstr(self.reference)
        T = evals(self.reference, self.arguments{:}, varargin{:});
        % and get values when not failed
        if ~T.failed && T.passed
          if ischar(T.output)
            v = regexp(T.output,'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?','Match');
            v = cellfun(@(c)str2double(c), v);
          else
            v = T.output;
          end
        else
          warning([ mfilename ': feval: expression evaluation failed.' ]);
          v = T.output;
        end
      elseif ~isempty(self.evalfcn)
        if nargin(self.evalfcn) == 1
          v = self.evalfcn(self.reference);          
        else
          v = self.evalfcn(self.reference, self.arguments{:}, varargin{:});
        end
      else
        error([ mfilename ': feval: linked object ' class(self.reference) ' can not be evaluated (no feval).' ]);
      end
    end % feval
    
    function v = double(self)
    % DOUBLE Evaluate a LINK object.
    %   DOUBLE(LINK) evaluates the LINK object and return its value, e.g as a 
    %   numeric value.
    %
    %   Example: a=link('rand(5)'); isnumeric(double(a))
      v = feval(self);
    end
    
    function v = subsref(self, S)
    % SUBSREF Subscripted reference.
    %   B = SUBSREF(A,S) is called for the syntax A(I), A{I}, or A.I
    %    when A is an object.  S is a structure array with the fields:
    %        type -- string containing '()', '{}', or '.' specifying the
    %                subscript type.
    %        subs -- Cell array or string containing the actual subscripts.
    %
    %   A(I1,I2,...) Returns the object A restricted to index ranges I1,I2,...
    %
    %   A{axis_rank} Returns inner cell value (when LINK returns a cell).
    %
    %   A.FIELD1.FIELD2 Retrieves Property value (when LINK returns a struct).
    %
    %   Example: a=link('rand(5)'); isnumeric(subsref(a,substruct('()',{2:3})))
    
      if isempty(self.subsreffcn)
        % evaluate link
        v = feval(self);
        % forward to default subsref
        v = subsref(v, S);
      elseif strcmp(S(1).type,'.') && any(strcmpi(S(1).subs,{'reference','arguments'}))
        v = self.(S(1).subs);
      else
        % usual case: we forward to reference subsref method
        v = self.subsreffcn(self.reference, S);
      end
    end
    
    function self = subsasgn(self, S, value)
    % SUBSASGN Subscripted assignment.
    %   A = SUBSASGN(A,S,B) is called for the syntax A(I)=B, A{I}=B, or
    %     A.I=B when A is an object.  S is a structure array with the fields:
    %       type -- string containing '()', '{}', or '.' specifying the
    %               subscript type.
    %       subs -- Cell array or string containing the actual subscripts.
    %
    %   A(I1,I2,...)=val Assigns the object A restricted to index ranges I1,I2,...
    %
    %   A{axis_rank}=val Assigns inner cell value (when LINK returns a cell).
    %
    %   A.FIELD1.FIELD2=val Assigns Property value (when LINK returns a struct).
    %
    %   NOTE: ONLY links with objects supporting assignment (subsasgn) are handled.
    
      % can not set links
      if ~isempty(self.subsasgnfcn)
        self.reference = self.subsasgnfcn(self.reference, S, value);
      else
        error([ mfilename ': subsasgn: linked object ' class(self.reference) ' is set as read-only (no subsasgn method).' ]);
      end
    end
  
  end % methods

end % class
