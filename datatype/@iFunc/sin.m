function a = sin(a)
% SIN    Sine of argument in radians.
%   SIN(X) is the sine of the model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=sin(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'sin');

