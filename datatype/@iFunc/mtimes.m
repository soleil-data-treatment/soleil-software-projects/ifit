function c = mtimes(a,b)
% *     Model matrix-multiply.
%   A*B denotes the model matrix-multiplication, which assumes model axes are 
%   orthogonal. The resulting model is of dimension NDIMS(A)+NDIMS(B). The 
%   syntax C = MTIMES(A,B) is equivalent. When one of the argument is a character
%   string, it is used as-is in the operator expression.
%
% Example: m=iFunc('p(1)*x'); z=m*m; ndims(z) == 2*ndims(m)
% See also iFunc, iFunc/minus, iFunc/plus, iFunc/times, iFunc/rdivide

if nargin ==1
	b=[];
end
c = binary(a, b, 'mtimes');

