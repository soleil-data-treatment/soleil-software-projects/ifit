function a = tanh(a)
% TANH   Hyperbolic tangent.
%   TANH(X) is the hyperbolic tangent of the model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=tanh(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'tanh');

