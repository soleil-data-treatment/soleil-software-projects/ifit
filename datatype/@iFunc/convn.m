function c = convn(a,b)
% CONVN  Convolution of model with a response function (normalized).
%   C = CONVN(A,B) computes the convolution model of A and B with 
%   automatic centering and normalization of the filter. This is a 
%   shortucut for:
%       CONV(A,B, 'same pad background center normalize')
%   When used with a single scalar value, it is used as a width to build a 
%   gaussian function.
%   When one of the arguments is a character string, it is used as-is in the 
%   operator expression.
%
%   C = CONVN(A) is the convolution of model A with normalized A.
%
% Example: b=convn(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/times, iData/convn, iFunc/xcorr, fconv, fconvn, fxcorr
if nargin ==1
	b=a;
end
c = conv(a, b, 'same pad background center normalize');


