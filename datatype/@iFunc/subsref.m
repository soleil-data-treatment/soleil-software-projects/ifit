function b = subsref(a,S)
% SUBSREF Subscripted reference.
%   B = SUBSREF(A,S) is called for the syntax A(I), A{I}, or A.I
%    when A is an object.  S is a structure array with the fields:
%        type -- string containing '()', '{}', or '.' specifying the
%                subscript type.
%        subs -- Cell array or string containing the actual subscripts.
%
%   A(P, X,Y,...) returns the object A evaluated with arguments P, X, Y, ...
%     This is equivalent to FEVAL(A, P, X,Y,...)
%
%   A{RANK}       returns the guessed axis of given RANK.
%
%   A.PROP        retrieves Property PROP value.
%     A.p     returns the model parameter values, same as A.ParameterValues
%     A.Par   returns the model parameter "Par" value
%
% Example: m=iFunc('p(1)*x+p(2)'); m.Amplitude=2; isnumeric(m.Amplitude)
% Example: m=iFunc('p(1)*x+p(2)'); m.p(2)=2; m.p(2)==2
% Example: m=iFunc('p(1)*x+p(2)'); isnumeric(m{1})
% Example: m=iFunc('p(1)*x+p(2)'); p=[1 2]; x=-10:10; isnumeric(m(p,x))
% See also iFunc, iFunc/subsasgn

  if numel(a) > 1
    b = builtin('subsref',a, S);
    return
  end

  % handle array/cell of fields
  if iscell(S)
    sout = cell(size(S));
    for index=1:numel(S)
      sout{index} = subsref(a, S{index});
    end
    sout = reshape(sout, size(S));
    v = sout; 
    return
  end

  if ischar(S) || iscellstr(S), S=str2subs(S); end
  if isstruct(S) && numel(S) == 1 && strcmp(S.type,'.') && iscellstr(S.subs) && numel(S.subs) == 1
    S = str2subs(S.subs);
  end

  switch S(1).type
  case '.'  % ======================================================== structure
    if strcmp(S(1).subs, 'p')   % a.p
      S(1).subs = 'ParameterValues';
    else
      index = find(strcmp(S(1).subs, strtok(a.Parameters)));
      if ~isempty(index)        % a.<parameter name> (should be single match)
        if index <= length(a.ParameterValues) % last parameter value used
          b = a.ParameterValues(index);
        else
          b = [];
        end
        return
      end
    end
    % normal fields
    b = builtin('subsref',a, S);
  
  case '()' % ======================================================== array
    % syntax a(p, axes{:}, varargin) -> evaluate
    b = feval(a, S.subs{:});
  
  case '{}' % ======================================================== axes (guessed)
    [~,~,ax] = feval(a);
    b        = ax{S.subs{:}};
  
  end % switch


