function a = acos(a)
% ACOS   Arc cosine of iFunc model.
%   ACOS(X) the inverse cosine of model X (in radians).
%
% Example: b=acos(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'acos');

