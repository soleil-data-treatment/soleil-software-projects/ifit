function a = log(a)
% LOG    Natural logarithm.
%   LOG10(X) is the ntural logarithm of the model X.
%   Complex results are produced where X is not positive.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=log(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/exp, iFunc/log, iFunc/log10, iFunc/sqrt

a = unary(a, 'log');

