function b = unary(a, op)
% UNARY  Handles unary model operations.
%   B = UNARY(A, OP) applies unary operator OP on model A and
%   returns the new model B.
%
%   Supported operators can be obtained with OP=UNARY(iFunc), returning
%     abs acosh acos asinh asin atanh atan ceil conj cosh cos ctranspose del2 exp 
%     find fliplr flipud floor full imag isfinite isfloat isinf isinteger islogical 
%     isnan isnumeric isreal isscalar issparse log10 logical log norm not 
%     real round sign single sinh sin sparse sqrt tanh tan transpose uminus uplus 
%     single double find logical
%
% Example: s=iFunc('p(1)*x+p(2)'); ops=unary(s); c=0; for op=ops; ...
%   c=c+isa(unary(s, op{1}), 'iFunc'); end; c >= numel(ops)-3

% just modify the Name and the Expression

if nargin < 2 || strcmp(op, 'identify')
  b = {'abs', 'acosh', 'acos', 'asinh', 'asin', 'atanh', 'atan', 'ceil', ...
       'conj', 'cosh', 'cos', 'ctranspose', 'del2', 'exp', 'find', 'fliplr', ...
       'flipud', 'floor', 'full', 'imag', 'isfinite', 'isfloat', 'isinf', ...
       'isinteger', 'islogical', '', '', 'isnan', 'isnumeric', 'isreal', ...
       'isscalar', 'issparse', 'log10', 'logical', 'log', 'norm', 'not', ...
       'real', 'round', 'sign', 'single', 'sinh', 'sin', 'sparse', 'sqrt', ...
       'tanh', 'tan', 'transpose', 'uminus', 'uplus', 'single', 'double', ...
       'find', 'logical'};
  return
end

% handle input iFunc arrays
if numel(a) > 1
  b = [];
  for index=1:numel(a)
    this = unary(a(index), op);
    b = [ b  this ];
  end
  b = reshape(b, size(a));
  return
end

ax = 'x,y,z,t,u,v,w,'; ax = ax(1:(a.Dimension*2));
if isa(a.Expression, 'function_handle')
  if nargin(a.Expression) > 1
    a.Expression = sprintf('signal = feval(%s, p, %s);', func2str(a.Expression), ax(1:(end-1)));
  else
    a.Expression = sprintf('signal = feval(%s, p);', func2str(a.Expression));
  end
  
end

if ~isempty(a.Name)
   a.Name       = [ op '(' a.Name ')' ];
else
  u = a.Expression; u=strtrim(u); u(~isstrprop(u,'print'))='';
  u = strrep(u, ';', '');
  % remove any 'signal='
  [s,e] = regexp(u, '\<signal\>\s*=');
  for index=1:length(s)
   u = strrep(u, u(s(index):e(index)),'');
  end
  if length(u) > 20, u = [ u(1:17) '...' ]; end
  a.Name       = [ op '(' u ')' ];
end
if iscell(a.Expression)
  a.Expression = [ a.Expression(:) ; sprintf('\nsignal=%s(signal);', op) ];
else
  a.Expression = [ a.Expression sprintf('\nsignal=%s(signal);', op) ];
end

a.Eval=cellstr(a); % trigger new Eval

b = copyobj(a);
