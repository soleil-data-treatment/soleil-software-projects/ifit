function a = not(a)
% ~      Logical not.
%   ~A performs a logical NOT of model A, and returns a model
%   which values are set to 1 where A contains a zero value.
%   Otherwise, that element is set to 0. NOT(A) is equivalent to '~A'.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=not(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/or, iFunc/and

a = unary(a, 'not');

