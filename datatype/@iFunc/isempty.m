function y = isempty(s)
% ISEMPTY True for empty model.
%   ISEMPTY(X) returns 1 if X is an empty model and 0 otherwise. An empty model
%   has no parameter or no expression (Signal).
%
% Example: isempty(iFunc)
% See also iFunc, iFunc/disp, iFunc/get

if numel(s) > 1
  y = zeros(size(s));
elseif ~numel(s), y=1; return;
end
for index = 1:numel(s)
  t = s(index);
  if     isempty(t.Expression) && isempty(t.Parameters), empty = 1;
  else empty = 0; end
  y(index) = empty;
end

y=uint8(y);


% expression  parameters  empty
%     no         no         yes
