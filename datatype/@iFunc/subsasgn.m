function a = subsasgn(a,S,val)
% SUBSASGN Subscripted assignement.
%   A = SUBSASGN(A,INDEX,B) function defines indexed assignement
%   such as A.FIELD = B. The updated model is returned.
%
%   A.p(index)  = val               sets the value of the indexed parameter
%   A.p         = val               sets all parameter values
%   A.Par       = val               sets the value of the parameter named "Par"
%   A.Par       = 'fix'  or 'lock'  locks the value of the Parameter "Par"
%   A.Par       = 'free' or 'clear' frees/unlocks the value of the Parameter "Par"
%   A.Par       = [min max]         restraints the Par parameter between bounds [min max]
%   A.Par       = [min val max]     restraints the Par parameter between bounds [min max]
%                                     and set its value to val
%   A.p         ='fix'              fixes/locks all parameter values
%   A.p         ='free'             frees/unlocks all parameter values
%
% Example: b=iFunc('p(1)*x+p(2)'); b.p(1)=1; b.ParameterValues(1) == 1
% Example: b=iFunc('p(1)*x+p(2)'); b.Constant=2; b.ParameterValues(2) == 2
% Example: b=iFunc('p(1)*x+p(2)'); b.Constant='fix'; l=mlock(b); l(2)==1
% Example: b=iFunc('p(1)*x+p(2)'); b.Amplitude=[1 10]; b.Constraint.min(1)==1 && b.Constraint.max(1)==10
% See also iFunc, iFunc/subsref

  if numel(a) > 1
    a = builtin('subsasgn',a, S, val);
    return
  end

  if ischar(S) || iscellstr(S), S=str2subs(S); end
  if isstruct(S) && numel(S) == 1 && strcmp(S.type,'.') && iscellstr(S.subs) && numel(S.subs) == 1
    S = str2subs(S.subs);
  end
  
  switch S(1).type
  case '.'
    if strcmp(S(1).subs, 'p')
      S(1).subs = 'ParameterValues';
    else
      index = find(strcmp(S(1).subs, strtok(a.Parameters)));
      if ~isempty(index)
        S(1).subs = 'ParameterValues'; 
        s.type='()'; s.subs= { index };
        S = [ S(1) s S(2:end) ];
      end
    end
    
    if strcmp(S(1).subs, 'ParameterValues')
      % handle different parameter values:
      % {'fix','lock'}, {'free','unlock'}
      % [min     max]
      % [min val max]
      % and return
      if ischar(val)
        switch val
        case {'fix', 'lock'}
          val = 1;
        case {'free','unlock','clear'}
          val = 0;
        otherwise
          error([ mfilename ': Invalid value ' val ' to set ' class(a) S(1).type S(1).subs ]);
        end
        S = [ substruct('.','Constraint','.','fixed') S(2:end) ];
        if numel(S) == 2  % fix/free all
          val = ones(size(a.Parameters))*val;
        end
      elseif isnumeric(val)
        if numel(S) == 2 && strcmp(S(2).type,'()') && numel(S(2).subs{1}) == 1
          if numel(val) == 2
            % [min max]
            S = [ substruct('.','Constraint','.','min') S(2:end) ];
            a = builtin('subsasgn', a, S, val(1));
            S = [ substruct('.','Constraint','.','max') S(3:end) ];
            val = val(2);
          elseif numel(val) == 3
            S = [ substruct('.','Constraint','.','min') S(2:end) ];
            a = builtin('subsasgn', a, S, val(1));
            S = [ substruct('.','Constraint','.','max') S(3:end) ];
            a = builtin('subsasgn', a, S, val(3));
            S = [ substruct('.','ParameterValues') S(3:end) ];
            val = val(2);
          end
        end
      else
        error([ mfilename ': Invalid value ' class(val) ' to set ' class(a) S(1).type S(1).subs ]);
      end
    end
    a = builtin('subsasgn', a, S, val);
    
  otherwise
    error([ mfilename ': Can not set ' class(a) ' ' S(1).type ]);
  end
  

