function a = acos(a)
% COS    Cosine of model in radians.
%   COS(X) is the cosine of model X (using radians).
%
% Example: a=iFunc('p(1)*x+p(2)'); b=cos(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'cos');

