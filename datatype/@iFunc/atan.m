function a = atan(a)
% ATAN   Arc tangent of iFunc object.
%   ATAN(X) is the inverse tangent of model X (in radians).
%
% Example: b=atan(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'atan');

