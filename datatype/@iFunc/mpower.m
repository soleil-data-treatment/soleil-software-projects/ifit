function c = mpower(a,b)
% ^      Model matrix-power.
%   A^B and MPOWER(A,B) denote model matrix-power.
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression. B can be a model or a scalar.
%   When the second argument B is an integer, the model A is extended
%   orthogonaly, creating a new model with dimension NDIMS(A)*B
%
% Example: m=iFunc('p(1)*x'), n=m^2; ndims(n) ==2

if nargin ==1
	b=[];
end
c = binary(a, b, 'mpower');

