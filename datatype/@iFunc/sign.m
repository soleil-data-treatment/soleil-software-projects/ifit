function a = sign(a)
% SIGN   Signum function.
%   SIGN(X) returns a model which value is 1 where X
%   is greater than zero, 0 if it equals zero and -1 if it is
%   less than zero.  For the nonzero elements of complex X,
%   SIGN(X) = X ./ ABS(X).
%
% Example: a=iFunc('p(1)*x+p(2)'); b=sign(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/floor, iFunc/ceil, iFunc/round

a = unary(a, 'sign');

