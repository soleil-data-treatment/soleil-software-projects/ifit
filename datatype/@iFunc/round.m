function a = round(a)
% ROUND  Round towards nearest integer.
%   ROUND(X) rounds the model X to the nearest integers.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=round(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/floor, iFunc/ceil, iFunc/sign

a = unary(a, 'round');

