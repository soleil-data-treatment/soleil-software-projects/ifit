function c = mrdivide(a,b)
% ./     Orthogonal matrix-divide.
%   A/B and MRDIVIDE(A,B) denote model orthogonal (matrix) division. The axes
%   of A and B are assumed to be orthogonal for the division.
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression. 
%
% Example: m=iFunc('p(1)*x+1'); n=m/m; ndims(n) == 2
% See also iFunc, iFunc/minus, iFunc/plus, iFunc/times, iFunc/rdivide

if nargin ==1
	b=[];
end
c = binary(a, b, 'mrdivide');

