function a = log10(a)
% LOG10  Common (base 10) logarithm.
%   LOG10(X) is the base 10 logarithm of the model X.
%   Complex results are produced where X is not positive.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=log10(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/exp, iFunc/log, iFunc/log10, iFunc/sqrt

a = unary(a, 'log10');

