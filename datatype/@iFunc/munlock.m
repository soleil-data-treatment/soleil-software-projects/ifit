function a = munlock(a, varargin)
% MUNLOCK Set model parameter unlocked (free) state.
%   X = MUNLOCK(X, PARAMETER) locks model PARAMETER during further fit process.
%
%   X = MUNLOCK(X, {'Parameter1', 'Parameter2', ...}) unlocks multiple parameters.
%
%   X = MUNLOCK(X, regexp(model.Parameters, 'token1|token2|...')) unlocks multiple
%   parameters matching given tokens.
%
%   X = MUNLOCK(X, [1 2 ...])   unlocks/free parameters given their index.
%
%   X = MUNLOCK(X, 'all')       unlocks/free all parameters.
%
%   X = MUNLOCK(X, 'none')      locks/fix all parameters.
%
%   STATE = MUNLOCK(X)          displays free parameters, and returns a STATE 
%   array with 0=free, 1=fixed.
%
% Example: b=iFunc('p(1)*x+p(2)'); b.Parameters{1}='Intensity'; ...
%   b=munlock(b,'Intensity'); l=munlock(b); l(1) == 0
% See also iFunc, iFunc/fits, iFunc/mlock, iFunc/xlim

% calls subsasgn with 'clear' for each parameter given

% handle array of objects
if numel(a) > 1
  for index=1:numel(a)
    a(index) = feval(mfilename, a(index), varargin{:});
  end
  if nargout == 0 && ~isempty(inputname(1)) % update array inplace
    assignin('caller', inputname(1), a);
  end
  return
end

if nargin == 1 % display free parameters
  count = 0;
  if ~isempty(a.Parameters)
    for p=1:length(a.Parameters)
      [name, R] = strtok(a.Parameters{p}); % make sure we only get the first word (not following comments)
      if length(a.Constraint.min) >=p
        this_min = a.Constraint.min(p);
      else
        this_min = -Inf;
      end
      if length(a.Constraint.max) >=p
        this_max = a.Constraint.max(p);
      else
        this_max = Inf;
      end
      
      if length(a.Constraint.fixed) >=p && ~a.Constraint.fixed(p)
        if (count == 0)
          fprintf(1, 'Unlocked/Free Parameters in %s %iD model "%s"\n', a.Tag, a.Dimension, a.Name)
        end
        count = count +1;
        if any(isfinite([this_min this_max]))
          fprintf(1,'%20s (free) in %s\n', name, mat2str([this_min this_max]));
        else
          fprintf(1,'%20s (free)\n', name);
        end
      end
    end
  end
  if count == 0
    fprintf(1, 'No unlocked/free Parameters in %s %iD model "%s"\n', a.Tag, a.Dimension, a.Name);
  end
  a = a.Constraint.fixed;
  return
end

% handle case where names are obtained from regexp = cell with length=Parameters
if length(varargin) == 1 && iscell(varargin{1}) && length(varargin{1}) == length(a.Parameters)
  varargin{1} = a.Parameters(find(~cellfun(@isempty, varargin{1})));
end

% handle multiple parameter name arguments
if length(varargin) > 1
  for index=1:length(varargin)
    a = feval(mfilename, a, varargin{index});
  end
  return
else
  name = varargin{1};
end

% now with a single input argument
if iscell(name) && numel(name) == 1 && isnumeric(name{1}), name = strtok(a.Parameters(name{1})); end

if ischar(name) && strcmp(name, 'all'), name = strtok(a.Parameters);
elseif ischar(name) && strcmp(name, 'none'), 
  a=mlock(a,'all'); 
  if nargout == 0 && ~isempty(inputname(1))
    assignin('caller',inputname(1),a);
  end
  return
end
if ~ischar(name) && ~iscellstr(name)
  error([ mfilename ': can not unlock model parameters with a Parameter name of class ' class(name) ' in iFunc model ' a.Tag '.' ]);
end
if ischar(name), name = cellstr(name); end
% now with a single cellstr
for index=1:length(name)
  s    = struct('type', '.', 'subs', name{index});
  a = subsasgn(a, s, 'clear');
end

if nargout == 0 && ~isempty(inputname(1))
  assignin('caller',inputname(1),a);
end

