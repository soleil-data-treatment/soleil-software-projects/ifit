function a = uminus(a)
% -      Unary minus.
%   -A negates the model A.
%
%   B = UMINUS(A) is called for the syntax '-A' when A is a model.
%
% Example: b=uminus(iFunc('p(1)*x')); isa(b, 'iFunc')
% See also iFunc, iFunc/uminus, iFunc/abs, iFunc/real, iFunc/imag, iFunc/uplus

a = unary(a, 'uminus');

