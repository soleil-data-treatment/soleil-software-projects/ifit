function tf = isfield(a, field)
% ISFIELD True if field or parameter is in model.
%   TF = ISFIELD(MODEL, FIELD) returns true if the string FIELD is the name of a
%   field or parameter in MODEL. It is also possible to search for model
%   parameters with e.g.:
%     regexp(MODEL.Parameters, 'token1|token2|...')
%
% Example: isfield(iFunc, 'Name')
% Example: m=iFunc('p(1)*x'); m.Parameters{1}='Slope'; isfield(m, 'Slope')
% See also iFunc, isfield, iFunc/findfield

persistent fn
if isempty(fn)
  fn = fieldnames(a);
end

if any(strcmp(field, fn)) || any(strcmp(field, a.Parameters))
  tf = true;
else
  tf = false;
end
