function c = plus(a,b)
% +      Plus.
%   A+B and PLUS(A,B) denote model sum (addition).
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression. 
%
%   C = A+'expression' appends 'expression' to the model A. The 'expression' 
%   may contain equal sign (=) for assignment, and ';' after statements.
%
%   C = 'expression'+B prepends 'expression' to the model B. The 'expression' 
%   may contain equal sign (=) for assignment, and ';' after statements.
%
% Example: a=iFunc('p(1)*x'); b=a+1; all(feval(b) == feval(a)+1)
% Example: a=iFunc('p(1)*x'); b=a+'disp(''Hello after'');'; isnumeric(feval(b))
% Example: a=iFunc('p(1)*x'); b='disp(''Hello before'');'+a; isnumeric(feval(b))
% See also iFunc, iFunc/minus, iFunc/times, iFunc/rdivide

if nargin ==1
	b=[];
end
c = binary(a, b, 'plus');

