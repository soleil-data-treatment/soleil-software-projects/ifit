function c = power(a,b)
% .^     Model power.
%   A.^B and POWER(A,B) denote model power.
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression. B can be a model or a scalar.
%
% Example: m=iFunc('p(1)*x'); n=m.^0; all(feval(n)==1)
% See also iFunc, iFunc/minus, iFunc/plus, iFunc/times, iFunc/rdivide

if nargin ==1
	b=[];
end
c = binary(a, b, 'power');

