function a = tan(a)
% TAN    Hyperbolic tangent in radians.
%   TAN(X) is the tangent of the model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=tan(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'tan');

