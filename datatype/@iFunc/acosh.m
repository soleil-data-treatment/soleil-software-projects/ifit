function a = acosh(a)
% ACOSH  Inverse hyperbolic cosine of iFunc model.
%   ACOSH(X) is the inverse hyperbolic cosine of model X.
%
% Example: b=acosh(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'acosh');

