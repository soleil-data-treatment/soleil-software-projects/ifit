function a = mlock(a, varargin)
% MLOCK  Set model parameter locked (fixed) state.
%   X = MLOCK(X, PARAMETER) locks model PARAMETER during further fit process.
%
%   X = MLOCK(X, {'Parameter1', 'Parameter2', ...}) locks multiple parameters.
%
%   X = MLOCK(X, regexp(model.Parameters, 'token1|token2|...')) locks multiple
%   parameters matching given tokens.
%
%   X = MLOCK(X, [1 2 ...])   locks/fix parameters given their index.
%
%   X = MLOCK(X, 'all')       locks/fix all parameters.
%
%   X = MLOCK(X, 'none')      unlocks/free all parameters.
%
%   STATE = MLOCK(X)          displays fixed parameters, and returns a STATE 
%   array with 0=free, 1=fixed.
%
% Example: b=iFunc('p(1)*x+p(2)'); b.Parameters{1}='Intensity'; ...
%   b=mlock(b,'Intensity'); l=mlock(b); l(1) == 1 
% See also iFunc, iFunc/fits, iFunc/munlock, iFunc/xlim

% calls subsasgn with 'fix' for each parameter given

% handle array of objects
if numel(a) > 1
  for index=1:numel(a)
    a(index) = feval(mfilename, a(index), varargin{:});
  end
  if nargout == 0 && ~isempty(inputname(1)) % update array inplace
    assignin('caller', inputname(1), a);
  end
  return
end

if nargin == 1 % display fixed parameters
  count = 0;
  if ~isempty(a.Parameters)
    for p=1:length(a.Parameters)
      [name, R] = strtok(a.Parameters{p}); % make sure we only get the first word (not following comments)
      
      if (length(a.Constraint.fixed) >=p && a.Constraint.fixed(p)) || ...
         (length(a.Constraint.set) >=p && ~isempty(a.Constraint.set{p})) 
        if (count == 0)
          fprintf(1, 'Locked/Fixed Parameters in %s %iD model "%s"\n', a.Tag, a.Dimension, a.Name)
        end
        count = count +1;
        if length(a.Constraint.set) >=p && ~isempty(a.Constraint.set{p}) && ...
          (ischar(a.Constraint.set{p}) || isa(a.Constraint.set{p}, 'function_handle'))
          name = [ name ' set from ' char(a.Constraint.set(p)) ];
        end
        fprintf(1,'%20s (fixed)\n', name);
      end
    end
  end
  if count == 0
    fprintf(1, 'No locked/fixed Parameters in %s %iD model "%s"\n', a.Tag, a.Dimension, a.Name);
  end
  a = a.Constraint.fixed;
  return
end

% handle case where names are obtained from regexp = cell with length=Parameters
if length(varargin) == 1 && iscell(varargin{1}) && length(varargin{1}) == length(a.Parameters)
  varargin{1} = a.Parameters(find(~cellfun(@isempty, varargin{1})));
end

% handle multiple parameter name arguments
if length(varargin) > 1
  for index=1:length(varargin)
    a = feval(mfilename, a, varargin{index});
  end
  return
else
  name = varargin{1};
end

% now with a single input argument
if isempty(name), return; end
if isnumeric(name), all_names = strtok(a.Parameters); name=all_names(name); end
if iscell(name) && numel(name) == 1 && isnumeric(name{1}), name = strtok(a.Parameters(name{1})); end
if ischar(name) && strcmp(name, 'all'), name = strtok(a.Parameters); 
elseif ischar(name) && strcmp(name, 'none'), 
  a=munlock(a,'all'); 
  if nargout == 0 && ~isempty(inputname(1))
    assignin('caller',inputname(1),a);
  end
  return
end
if ~ischar(name) && ~iscellstr(name)
  error([ mfilename ': can not lock model parameters with a Parameter name of class ' class(name) ' in iFunc model ' a.Tag '.' ]);
end

if ischar(name), name = cellstr(name); end
% now with a single cellstr
for index=1:length(name)
  s = struct('type', '.', 'subs', name{index});
  a = subsasgn(a, s, 'fix');
end

if nargout == 0 && ~isempty(inputname(1)) && isa(a,'iFunc')
  assignin('caller',inputname(1),a);
end

