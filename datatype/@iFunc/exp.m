function a = exp(a)
% EXP    Exponential.
%   EXP(X) is the exponential of the model X, e to the X value.
%   For complex Z=X+i*Y, exp(Z) = exp(X)*(COS(Y)+i*SIN(Y)).
%
% Example: a=iFunc('p(1)*x+p(2)'); b=exp(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/exp, iFunc/log, iFunc/log10, iFunc/sqrt

a = unary(a, 'exp');

