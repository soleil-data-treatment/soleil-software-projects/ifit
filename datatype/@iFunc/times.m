function c = times(a,b)
% .*     Model multiply.
%   A.*B denotes the model multiplication. C = TIMES(A,B) is equivalent.
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression.
%
% Example: m=iFunc('p(1)*x'); z=m.*m; numel(z.Parameters) == 2
% See also iFunc, iFunc/minus, iFunc/plus, iFunc/mtimes, iFunc/rdivide

if nargin ==1
	b=[];
end
c = binary(a, b, 'times');

