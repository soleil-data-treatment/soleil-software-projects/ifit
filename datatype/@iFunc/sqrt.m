function a = sqrt(a)
% SQRT   Square root.
%   SQRT(X) is the square root of the model X. Complex 
%   results are produced where X is not positive.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=sqrt(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/sqrt, iFunc/power

a = unary(a, 'sqrt');

