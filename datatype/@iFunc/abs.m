function a = abs(a)
% ABS    Absolute value of iFunc model.
%   ABS(X) is the absolute value of model X.
%   This function computes the absolute value/modulus of the object 's'.
%
% Example: b=abs(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/uminus, iFunc/abs, iFunc/real, iFunc/imag, iFunc/uplus

a = unary(a, 'abs');

