function a = copyobj(a)
% COPYOBJ  Make copy of model.
%   B = COPYOBJ(A) makes a copy of model A into a new model B.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=copyobj(a); isa(b, 'iFunc') && ndims(b) == ndims(a)
% See also iFunc, iFunc/uplus, iFunc/findobj

% handle input iFunc arrays
if numel(a) > 1
  b = [];
  for index=1:numel(a)
    b = [ b copyobj(a(index)) ];
  end
  a = reshape(b, size(a));
  return
end

t     = iFunc;
a.Tag = t.Tag;

