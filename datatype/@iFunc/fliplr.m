function a = fliplr(a)
% FLIPLR Flip model in left/right direction.
%   FLIPLR(A) returns model A with row preserved and columns flipped
%   in the left/right direction, e.g. the X axis (horizontal) is inverted.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=fliplr(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/fliplr, fliplr, iFunc/flipud, flipud

a = unary(a, 'fliplr');

