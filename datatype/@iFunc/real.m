function a = real(a)
% REAL   Complex real part.
%   REAL(X) is the real part of model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=real(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/uminus, iFunc/abs, iFunc/real, iFunc/imag, iFunc/uplus

a = unary(a, 'real');

