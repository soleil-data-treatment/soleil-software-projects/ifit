function a = ceil(a)
% CEIL   Upper integer round of iFunc model.
%   CEIL(X) Round the elements of model X to the nearest integers
%   towards plus infinity.
%
% Example: b=ceil(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/floor, iFunc/ceil, iFunc/round

a = unary(a, 'ceil');

