function a = asinh(a)
% ASINH  Inverse hyperbolic sine of iFunc model.
%   ASINH(X) is the inverse hyperbolic sine of model X.
%
% Example: b=asinh(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'asinh');

