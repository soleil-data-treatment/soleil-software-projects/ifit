function c = minus(a,b)
% -      Minus.
%   A-B and MINUS(A,B) denote model subtraction.
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression. 
%
% Example: a=iFunc('p(1)*x'); b=a-1; all(feval(b) == feval(a)-1)
% Example: a=iFunc('p(1)*x'); b=a-a; all(feval(b) == 0)
% See also iFunc, iFunc/minus, iFunc/plus, iFunc/times, iFunc/rdivide

if nargin ==1
	b=[];
end
c = binary(a, b, 'minus');

