function a = uplus(a)
% +      Unary plus, makes a copy of model.
%   +A for model is a copy of A. 
%
%   B = UPLUS(A) is called for the syntax '+A' when A is an object.
%
% Example: b=uplus(iFunc('p(1)*x')); isa(b, 'iFunc')
% See also iFunc, iFunc/uminus, iFunc/abs, iFunc/real, iFunc/imag, iFunc/uplus

a = unary(a, 'uplus');

