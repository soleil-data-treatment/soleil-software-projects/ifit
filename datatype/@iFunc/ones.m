function s = ones(iFunc_in,varargin)
% ONES   Ones model array.
%   ONES(X, M,N,P,...) is an M-by-N-by-P-by-... model array using X
%   The model X is duplicated into an array. Use X=iFunc for empty models.
%
% Example: a=ones(iFunc,5,5); numel(a) == 25
% Example: a=ones(iFunc('p(1)*x+p(2)'),5,5); numel(a) == 25
% See also iFunc/zeros

if nargin == 1
    s = zeros(iFunc_in, size(iFunc_in));
else
    s = zeros(iFunc_in, varargin{:});
end

