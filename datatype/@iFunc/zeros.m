function s = zeros(iFunc_in,varargin)
% ZEROS  Zeros model array.
%   ZEROS(X, M,N,P,...) is an M-by-N-by-P-by-... model array using X
%   The model X is duplicated into an array. Use X=iFunc for empty models.
%   ZEROS(iFunc...) is the same as ONES(iFunc...).
%
% Example: a=zeros(iFunc,5,5); numel(a) == 25
% Example: a=zeros(iFunc('p(1)*x+p(2)'),5,5); numel(a) == 25
% See also iFunc/zeros

% EF 27/07/00 creation
% EF 23/09/07 iFunc implementation

if nargin == 1, 
  if numel(iFunc_in) == 1, s=iFunc; return; end
  s=zeros(size(iFunc_in));
  iFunc_in = iFunc;
else s = zeros(varargin{:}); end

if isempty(s), s=iFunc; return; end

s_index = 1;
long_s  = iFunc_in(1);

for i = 2:numel(s)
  s_index = s_index +1;
  if s_index > numel(iFunc_in), s_index = 1; end
  long_s = [ long_s copyobj(iFunc_in(s_index)) ];
end

s = reshape(long_s, size(s));

