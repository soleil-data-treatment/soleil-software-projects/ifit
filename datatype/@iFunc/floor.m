function a = floor(a)
% FLOOR  Round towards minus infinity.
%   FLOOR(X) rounds the elements of X to the nearest integers
%   towards minus infinity.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=imag(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/floor, iFunc/ceil, iFunc/round

a = unary(a, 'floor');

