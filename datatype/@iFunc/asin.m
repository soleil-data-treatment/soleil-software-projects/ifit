function a = asin(a)
% ASIN   Inverse sine of iFunc model.
%   ASIN(X) is the inverse sine of model X (in radians).
%
% Example: b=asin(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'asin');

