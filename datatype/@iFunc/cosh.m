function a = cosh(a)
% COSH   Hyperbolic cosine of model.
%   COSH(X) is the hyperbolic cosine of model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=cosh(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'cosh');

