function a = norm(a)
% NORM   Model norm.
%   NORM(X) returns the 2-norm of model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=norm(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, norm

a = unary(a, 'norm');

