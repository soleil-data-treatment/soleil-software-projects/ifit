function [ret, header] = char(s)
% CHAR   Convert iFunc model into character array.
%   CHAR(X) converts iFunc model into char array with expression to evaluate.
%
% Example: m=iFunc('p(1)*x+p(2)'); ischar(char(m))
% See also  iFunc/struct, iFunc/cellstr


ret=[];
if numel(s) > 1
  ret = {};
  for index=1:numel(s)
    ret{index} = char(s(index));
  end
  return
end
  
[ret, header] = cellstr(s);
ret = char(ret); % as a single char line
