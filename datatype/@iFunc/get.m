function [varargout] = get(this,varargin)
% GET    Get model properties.
%   V = GET(M, 'PropertyName') returns the value of the specified
%   property for the model M. The PropertyName may be a model property or 
%   parameter name. If M is an array, then GET will return a cell array of values.
%   If 'PropertyName' is replaced by a 1-by-N or N-by-1 cell array of strings 
%   containing property names, then get will return an M-by-N cell array of
%   values. A similar syntax for the GET method is M.PropertyName
%
%   GET(M) displays full object structure..
%
% Example: m=iFunc('p(1)*x'); ischar(get(m,'Name'))
% Example: m=iFunc('p(1)*x'); set(m,'Amplitude',4); m.Amplitude==4 
% See also iFunc, iFunc/set

% calls: subsref (mainly): get is a wrapper to subsref

persistent fields

if isempty(fields), fields=fieldnames(iFunc); end

% handle array of objects
varargout = {};

if numel(this) > 1
  varg = {};
  for index=1:numel(this)
    varg{end+1} = get(this(index), varargin{:});
  end
  varargout{1} = varg;
  return
end

if nargin == 1
  disp(this, inputname(1));
  varargout{1} = display(this, inputname(1));
  return
end

% handle single object
for index=1:length(varargin)
  property = varargin{index}; % get PropertyName
  if isempty(property), continue; end
  if iscellstr(property)
    b = {};
    for p=property(:)'
      b{end+1} = get(this, p{1});
    end
    varargout{1} = b;
    return
  end
  if ~ischar(property)
    error([ mfilename ': PropertyName should be char strings in object ' inputname(1) ' ' this.Tag ' "' this.Name '" and not ' class(property) ]);
  end
  % test if this is a unique property, or a composed one
  if isvarname(property)  % extract iFunc field/alias
    if any(strcmp(property, fields))
      b = this.(property);               % direct static field
      if isnumeric(b) && any(strcmp(property, {'Date','ModificationDate'}))
        b = datestr(b);
      end
      varargout{1} = b;
    else
      s = struct('type','.','subs', property);      % MAIN TIME SPENT
      varargout{1} = subsref(this, s);              % calls subsref directly (single subsref level)
    end
  else
    s = [];
    split = textscan(property,'%s','Delimiter','.'); split=split{end};
    split = split(:)';
    for k=split
      s(end+1).type='.';
      s(end).subs=k{1};
    end
    % MAIN TIME SPENT   
    varargout{1} = subsref(this, s);              % calls subsref directly (single subsref level)
  end
end

if isempty(varargout)
  varargout={[]};
end
