function s = edit(s)
% EDIT   Edit a model with a window dialogue.
%   EDIT(X) displays a window dialogue to view/edit properties of a
%   model definition
%
% See also  iFunc/struct, iFunc/char

s = ifitmakefunc(s);
