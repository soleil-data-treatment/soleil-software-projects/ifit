function a=shiftdim(a, dim, parname)
% SHIFTDIM Shift model axis by a value.
%   B = SHIFTDIM(A, RANK) shifts model A axis of given RANK by a new parameters. 
%   Default RANK is 1.
%
%   B = SHIFTDIM(A, RANK, 'offset') adds a new model parameter 'offset', which is
%   used to parameterize the shift of axis RANK, e.g. x=x+offset when RANK is 1.
%   When the offset name is empty (''), the associated parameters is Offset_x
%   for RANK=1. When the offset name starts with '-' the value is subtracted,
%   e.g. x=x-offset, so that the x=offset is shifted to 0 (centered).
%
% Example: m=iFunc('p(1)*x'); n=shiftdim(m, 1, 'offset'); n.offset=1; all(n{1} == m{1}+1)
% See also  iFunc

if nargin < 2, dim=[]; end
if nargin < 3, parname=''; end

if isempty(dim), dim=1; end
if ~ischar(parname) && ~isempty(parname), parname=''; end

% only when shifting an axis which exists in object
if isnumeric(dim) && dim > a.Dimension && a.Dimension > 0, return; end

centre = false;
if ~isempty(parname) && parname(1) == '-'
  centre = true;
  parname = parname(2:end);
end
ax = 'xyztuvw';
if isnumeric(dim),  ax = ax(dim);
elseif ischar(dim); ax = dim;
else return; end

if isempty(parname)
  if centre
    parname = ['Centre_' ax ];
  else
    parname = ['Offset_' ax ];
  end
end

% now we add the code which shifts the axes
a.Parameters{end+1} = parname;
index=numel(a.Parameters);
if centre
  code = [ ax '=' ax '-p(' num2str(index) ')' ];
else
  code = [ ax '=' ax '+p(' num2str(index) ')' ];
end
a = iFunc(plus(code, a)); % add code and check for redundent parameter names
  
if nargout == 0 & length(inputname(1))
  assignin('caller',inputname(1),a);
end
