function a = transpose(a)
% .'     Complex non-conjugate transpose of model.
%   X.' is the transpose of model X, which is the same as TRANSPOSE(X).
%
% Example: a=iFunc('p(1)*x+p(2)'); b=a.'; isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/transpose

if numel(a) > 1
  a = builtin('transpose', a);
elseif ndims(a) <=2
  a = unary(a, 'transpose');
else
  a = permute(a, [2 1]);
end

