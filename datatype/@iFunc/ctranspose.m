function a = ctranspose(a)
% '      Complex conjugate transpose of model.
%   X' is the complex conjugate transpose of model X, which is the 
%   same as CTRANSPOSE(X).
%
% Example: a=iFunc('p(1)*x+p(2)'); b=a'; isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/transpose

if numel(a) > 1
  a = builtin('ctranspose', a);
elseif ndims(a) <=2
  a = unary(a, 'ctranspose');
else
  a = permute(a, [2 1]);
end
