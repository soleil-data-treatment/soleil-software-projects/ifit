function [p, model, ax] = guess(s, varargin)
% GUESS  Guess starting parameters for model fit.
%   P = GUESS(M) makes a guess P of the starting parameters for model M. The
%   guess is not a fit but rather a quick estimate of sensible parameter values.
%
%   P = GUESS(M, X,Y,..., SIGNAL) makes a guess with given axes and signal.
%
%   [P, M, AX] = GUESS(M,...) returns as well the updated model and guessed axes.
%
% Example: m=iFunc('p(1)*x+p(2)'); p=guess(m);
% See also iFunc, iFunc/feval

p = feval(s, 'guess_only', varargin{:});

if ~isempty(inputname(1))
  assignin('caller',inputname(1),s);
end
