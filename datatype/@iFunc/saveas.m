function [filename,writer] = saveas(self, varargin)
% SAVEAS Save model in desired output format.
%   FILENAME = SAVEAS(MODEL, FILENAME) exports MODEL.
%     The file format is set from the file extension.
%     When FILENAME is empty, a temporary file is created, HDF5 format.
%     When FILENAME is 'gui', a file selector is displayed.
%     When FILENAME is a directory, random filenames are created therein.
%
%   [FILENAME,WRITERS] = SAVEAS(MODEL, FILE, WRITER, ...) specifies the file format.
%     The WRITER can be a token, function name, function handle, file extension
%     or structure (see ifitwrite).
%     Additional arguments are passed to the export routine. The returned WRITERS
%     contains the actual list of used writers.
%     To get a full list of supported WRITER, use: ifitpref formats iswriter
%     The recommended data formats for modela are M, MAT, HDF5, XML, YAML and JSON.
%
%   WRITERS = SAVEAS(MODEL, FILE, 'writers') returns the WRITERS that can write the file.
%
%   The configuration of the data formats is controled via 'ifitpref'.
%
% Example: m=iFunc('p(1)*x'); y=saveas(m); ~isempty(dir(y))
% See also ifitwrite, ifitread, ifitpref, ifitopen, save

if numel(self) > 1
  filename = cell(size(self));
  writer   = filename;
  for index=1:numel(self)
    [filename{index},writer{index}] = ifitwrite(struct(self(index)), varargin{:});
  end
  return
end
[filename,writer] = ifitwrite(struct(self), varargin{:});
