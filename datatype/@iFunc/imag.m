function a = imag(a)
% IMAG   Complex imaginary part.
%   IMAG(X) is the imaginary part of model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=imag(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/uminus, iFunc/abs, iFunc/real, iFunc/imag, iFunc/uplus

a = unary(a, 'imag');

