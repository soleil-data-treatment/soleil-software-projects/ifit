function a = sinh(a)
% SINH   Hyperbolic sine.
%   SINH(X) is the hyperbolic sine of the model X.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=sinh(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'sinh');

