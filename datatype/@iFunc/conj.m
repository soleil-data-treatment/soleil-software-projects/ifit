function a = conj(a)
% CONJ   Conjugate of iFunc model.
%   CONJ(X) is the conjugate of model X.
%
% Example: b=conj(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/transpose, iFunc/ctranspose, iFunc?imag, iFunc/real

a = unary(a, 'conj');


