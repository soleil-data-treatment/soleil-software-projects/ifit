function c = conv(a,b, shape)
% CONV   Convolution of models.
%   C = CONV(A,B) convolves models A and B (FFT based).
%   When B is a single scalar value, B is further built as a gaussian 
%   with given width. When A or B is a character string, it is used as-is in the 
%   operator expression. 
%
%   C = CONV(A) is the auto-convolution of model A.
%
%   C = CONV(A, B, SHAPE) returns the convolution with size/option
%   specified by SHAPE:
%          full         Returns the full two-dimensional convolution.
%          same         Returns the central part of the convolution of the same size as a.
%          valid        Returns only those parts of the convolution that are computed
%                       without the zero-padded edges. Using this option, y has size
%                       [ma-mb+1,na-nb+1] when all(size(a) >= size(b)).
%          deconv       Performs an FFT deconvolution.
%          pad          Pads the 'a' signal by replicating its starting/ending values
%                       in order to minimize the convolution side effects
%          center       Centers the 'b' filter so that convolution does not shift
%                       the 'a' signal.
%          normalize    Normalizes the 'b' filter so that the convolution does not
%                       change the 'a' signal integral.
%          background   Remove the background from the filter 'b' (subtracts the minimal value)
%     Default SHAPE is 'same'. Multiple options can be given as words in SHAPE.
%
%   C = CONV(A, 'tas') convolves the model A with the TAS resolution function
%   computed from ResLibCal. The dispersion is either a 2D model S(|q|,w)
%   or a 4D model S(qh,qk,ql,w). The returned model can then further be used
%   to fit its parameters to match a measurement scan.
%
% Example: b=conv(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/convn, iFunc/xcorr, fconv, fconvn, fxcorr, ResLibCal
if nargin ==1
	b = a;
end
if isnumeric(b) && isscalar(b)
  g = b;
  b = gauss;
  b.Guess = [ 1 0 double(g) 0]; % use input as a width
  c = convn(a,b);
  return
elseif isnumeric(a) && isscalar(a)
  g = a;
  a = gauss;
  a.Guess = [ 1 0 double(g) 0]; % use input as a width
  c = convn(a,b);
  return
elseif ischar(b) && any(strcmpi(b, {'tas','reslibcal','rescal','reslib'}))
  c = ResLibCal(a);
  return
elseif ischar(a) && any(strcmpi(a, {'tas','reslibcal','rescal','reslib'}))
  c = ResLibCal(b);
  return
elseif isstruct(b) && isfield(b,'EXP')
  c = ResLibCal(a);
  return
elseif isstruct(a) && isfield(a,'EXP')
  c = ResLibCal(b);
  return
end
if nargin < 3, shape = 'same'; end

c = binary(a, b, 'fconv', shape);

