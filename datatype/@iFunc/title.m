function t = title(a, name)
% TITLE  Get or set the model Name.
%   T = TITLE(MODEL) gets the current model name/title.
%
%   TITLE(MODEL, 'text') sets the model Name to 'text'.
%
% Example: m=iFunc('p(1)*x'); title(m,'toto'); strcmp(title(m), 'toto')
% See also iFunc

if nargin == 1
  t = a.Name;
elseif ischar(name) || iscellstr(name)
  t = char(name);
  a.Name = t;
  
  if nargout == 0 && ~isempty(inputname(1)) && isa(a,'iFunc')
    assignin('caller',inputname(1),a);
  end
end

