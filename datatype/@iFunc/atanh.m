function a = atanh(a)
% ATANH  Inverse hyperbolic tangent of iFunc model.
%   ATANH(X) is the inverse hyperbolic tangent of model X.
%
% Example: b=atanh(iFunc('p(1)*x+p(2)')); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/cos, iFunc/acos, iFunc/sin, iFunc/asin, iFunc/tan, iFunc/atan

a = unary(a, 'atanh');

