function a = flipud(a)
% FLIPUD Flip model in up/down direction.
%   FLIPUD(A) returns model A with columns preserved and rows flipped
%   in the up/down direction. With 2D model, the Y axis (vertical) is inverted.
%
% Example: a=iFunc('p(1)*x+p(2)'); b=flipud(a); isa(b, 'iFunc') && isnumeric(feval(b))
% See also iFunc, iFunc/fliplr, fliplr, iFunc/flipud, flipud

a = unary(a, 'flipud');

