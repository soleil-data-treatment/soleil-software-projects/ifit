function v=ndims(s)
% NDIMS  Number of dimensions of model.
%   N = NDIMS(X) returns the number of dimensions in the model X.
%   A negative dimension is used to indicate a variable dimensionality.
%
% Example: ndims(iFunc) == 0
% Example: ndims(iFunc('p(1)*x+p(2)')) == 1

if numel(s) > 1
  v = zeros(size(s)); 
  for index=1:numel(s)
    v(index) =ndims(s(index));
  end
  return
end

v = s.Dimension;


