function a = del2(a)
% DEL2 Discrete Laplacian of model.
%   L = DEL2(M) returns the Discrete Laplacian model of M.
%
% Example: m=iFunc('p(1)*x+p(2)*y'); L=del2(m); ndims(m) == 2
% See also iFunc, del2, gradient

% make sure axes are regularly binned

a = unary(a, 'del2');

