function c = rdivide(a,b)
% ./     Divide.
%   A./B and RDIVIDE(A,B) denotes model division.
%   When one of the argument is a character string, it is used as-is in the 
%   operator expression. 
%
% Example: m=iFunc('p(1)*x+1'); n=m./m; all(feval(n) == 1)
% See also iFunc, iFunc/minus, iFunc/plus, iFunc/times, iFunc/divide

if nargin ==1
	b=[];
end
c = binary(a, b, 'rdivide');

