function [s,f] = str2subs(str)
% STR2SUBS Create structure argument for SUBSREF or SUBSASGN from a string.
%   SUBS = STR2SUBS(STR) creates a structure for SUBSREF or SUBSASGN by cutting
%   the STR into '.', '()' and '{}' bits. The ':' value can be used to specify
%   a full index range.
%   The output S is a structure array containing the fields:
%        type -- subscript types '.', '()', or '{}'
%        subs -- actual subscript values (field names or cell arrays
%                of index vectors)
%   The '/' separator in the path can also be used, e.g. 'level1/level2.level3'.
%   The '/../' token is used to travel one level up, as in a file path, but
%   array/cell indexing must not be used for the parent (e.g. no 'a.b(1)/../cc').
%   In case of error (invalid substruct string), SUBS is returned empty. 
%
%   [SUBS,STR] = STR2SUBS(STR) returns the fully qualified path (when
%   e.g. containing '/../' in path).
%
% Example: s=str2subs('Data.Signal(1,:)'); isstruct(s)
%
% See also subsref, subsasgn, substruct, str2struct
 

  s=[]; f=[]; failed = false;
  if nargin ==0 || isempty(str) || (~ischar(str) && ~iscellstr(str)), return; end
  if iscellstr(str)
    str = sprintf('%s', str{:});
  end
  str(str == '/') = '.'; % replace / by .
  
  % search for a substruct separator
  index=0; s={};

  while 1
    [tok, r] = strtok(str, '.({');
    
    % test the next token: can be a name or '(...)' or '{...}'
    if isempty(tok), break; end
    if isvarname(tok)
      % member name
      s{end+1} = '.';
      s{end+1} = strtrim(tok);
      f{end+1} = [ '.' s{end} ];
    elseif str(1) == '(' || str(1) == '{'
      % search for the other side ) or }
      [t1, r1] = strtok(str, ')}');
      if str(1)=='(', s{end+1} = '()'; else s{end+1} = '{}'; end
      s{end+1} = str2struct_tonum(t1(2:end));
      if str(1)=='(', f{end+1} = [ '(' t1(2:end) ')' ]; 
      else            f{end+1} = [ '{' t1(2:end) '}' ]; end
    else
      s = []; % invalid substruct
      break
    end
    if strncmp(r,'...',3) % one level above
      s(end)=[]; s(end)=[]; f(end)= []; % remove last (type,subs)
      str = r(5:end);
    else
      str = r;
    end
    index=index+1;
  end % while
  if ~isempty(s)
    try
      s = substruct(s{:}); % may fail again...
      f = [ f{:} ];
    catch
      s = [];
    end
  end
  if isempty(s)
    f=[]; failed = true;
  end
  
end % str2struct

% ------------------------------------------------------------------------------
function s=str2struct_tonum(s)
% STR2STRUCT_TONUM cut the string s into bits separated by ',', and accept ':'
  split = textscan(s,'%s','Delimiter',',');
  s=cellfun(@num_or_colon, split{1}, 'UniformOutput', false);
end % str2struct_tonum

function c=num_or_colon(c)
  if ~strcmp(c,':'), c=str2num(c); end
end


