function s = strsplit(str, delim)
% STRSPLIT Split string or character vector at specified delimiter.
%   C = STRSPLIT(str) splits str at whitespace into C. A whitespace character 
%   is equivalent to any sequence in the set {' ','\f','\n','\r','\t','\v'}.
%
%   C = STRSPLIT(str,delimiter) splits str at the delimiters specified by 
%   delimiter.given as a string, e.g. sprintf(';\n')
%
% Example: s=strsplit('Hello,world',','); numel(s) == 2

  if nargin < 2, delim=''; end
  if isempty(delim), delim=sprintf(' \f\n\r\t\v'); end
  s = regexp(str,regexptranslate('escape',delim),'split');
