function T = evals(ln, varargin)
% EVALS Evaluate expression similarly as EVALC but in a sandbox.
%   T = EVALS(expr) evaluates expression, and returns a structure holding the
%   evaluation. The given expression can be a char (string) or a cellstr.
%   The default language to use is Matlab. However, it is possible to specify
%   which interpreter to use when the expression to evaluate starts with:
%
%     'matlab:' evaluates Matlab code (default). Result should contain OK or be numeric
%     'perl:'   evaluates Perl code
%     'julia:'  evaluates Julia code. Default is to use 'julia' as interpreter
%               but you may as well define the JULIAEXECUTABLE environment variable.
%               julia must have been installed on the system.
%     'python:' evaluates Python code. Default is to use 'python3' as interpreter
%               but you may as well define the PYTHONEXECUTABLE environment variable.
%               python must have been installed on the system.
%     'http:'.'https','ftp:','file:' indicates an URL
%
%   T = EVALS(expr, 'name', value, ...) same as above, and passes given name/value
%   pairs as variables for the expression evaluation (only for matlab: expr).
%
%   The result is a structure with members:
%     T.passed is true when the result is true/non-zero, or the output contains
%             'OK' or 'passed' as first word.
%     T.failed is true when the result was null/not OK, or failed execution.
%     T.output holds anything that would normally be written to the command
%              window, or a MException object (when failed execution).
%
% Example: T=evals('a=1; disp(a);'); T.passed
% Example: T=evals('python: a=1; print(a);'); T.passed
% Example: T=evals('perl: my $a=1; print($a);'); T.passed
% Example: T=evals('julia: a=1; print ("OK");'); T.passed

  passed=false; failed=false; output=''; T=[];
  
  if isempty(ln), return; end
  if ~ischar(ln) && ~iscellstr(ln)
    error([ mfilename ': expect a char/cellstr as expression to evaluate' ]);
  end
  
  % identify which type of evaluation to use
  eval_type = '';
  if ~iscellstr(ln), ln = { ln }; end
  [eval_type, rem] = strtok(ln{1}, ':');
  if isempty(rem) || ~isvarname(eval_type), eval_type = 'matlab';
  else
    ln{1} = strtrim(rem(2:end));
  end
  ln = sprintf('%s\n', ln{:});
  result = 0;

  try
    clear ans
    % evaluate code ------------------------------------------------------------
    switch eval_type
    case 'matlab'
      [result, output] = eval_matlab(ln, varargin{:});
      if isempty(output) output=result; end
    case 'perl'
      [failed, output] = eval_perl(ln, varargin{:});
    case 'julia'
      [failed, output] = eval_julia(ln);
    case 'python'
      [failed, output] = eval_python(ln); % TODO: should support variable assignement as in "pyrun"
    case {'http','https','ftp','file'}
      ln = [ eval_type ':' ln ]; % put back URL scheme
      [failed, output] = eval_url(ln);
    otherwise
      error([ mfilename ': unsupported evaluation for ' eval_type ': ' ln ]);
    end

    % interpret result ---------------------------------------------------------
    if isobject(result) || isstruct(result) || (isnumeric(result) || islogical(result) && all(result(:)))
      passed=true;
    elseif ischar(result) && any(strcmpi(strtok(result),{'OK','passed','ans','done','success','1'}))
      passed=true;
    elseif ischar(output) && any(strcmpi(strtok(output),{'OK','passed','ans','done','success','1'}))
      passed=true;
    elseif strcmp(eval_type,'matlab')
      failed=true;
    end
  catch ME
    output = ME;
    failed = true;
  end
  if ~failed && ~passed, passed=true; end
  T.passed = passed;
  T.failed = failed;
  T.output = output;
  T.code   = ln;
  
% ----------------------------------------------------------------------------
function [eval_matlab_result, eval_matlab_output] = eval_matlab(eval_matlab_ln, varargin)
% EVAL_MATLAB Execute code (char) with Matlab.
    
  % first set variables in the local scope
  for eval_matlab_output = 1:2:numel(varargin)
    eval([ varargin{eval_matlab_output} '= varargin{' num2str(eval_matlab_output+1) '};' ]);
  end

  % we evaluate statements one-by-one so that we can get the output
  eval_matlab_result=[];
  eval_matlab_output='';
  try
    [eval_matlab_output,eval_matlab_result] = evalc(eval_matlab_ln);
  catch
    eval_matlab_output = evalc(eval_matlab_ln);
    eval_matlab_result=0;
    if ~isempty(strfind(eval_matlab_output,sprintf(' =\n'))) || isempty(eval_matlab_output)
      eval_matlab_result = 'OK';
    end
  end

  
% ----------------------------------------------------------------------------
function [failed, output] = eval_perl(ln, varargin)
% EVAL_PERL Execute code (char) with Perl.
%
% Example: T=evals('perl: my $a=1; print($a);')

  failed = true; output='';
  
  file = [ tempname '.pl' ];
  fid = fopen(file,'w');
  if fid ~= -1
    % handle any input argument
    for index=1:2:numel(varargin)
      fprintf(fid, '$%s=$ARGV[%i];\n', varargin{index}, index+1);
    end
    fprintf(fid, '%s\n', ln);
    fclose(fid);

    [output,st] = perl(file, varargin{:});
    failed=(st ~= 0);
  end
  delete(file);
  
% ----------------------------------------------------------------------------
function [failed, output] = eval_julia(ln)
% EVAL_JULIA Execute code (char) with Julia.
%   The JULIAEXECUTABLE env var can be set to specify which Julia interpreter
%   to call. Default is julia.
%
% Example: T=evals('julia: a=1; print("OK");')
  
  % required to avoid Matlab to use its own libraries
  if ismac,      precmd = 'DYLD_LIBRARY_PATH= ; DISPLAY= ; ';
  elseif isunix, precmd = 'LD_LIBRARY_PATH= ; DISPLAY= ; '; 
  else           precmd=''; end
  failed = true; output='';
  
  exe = getenv('JULIAEXECUTABLE');
  if isempty(exe), exe='julia'; end
  
  file = [ tempname '.jl' ];
  fid = fopen(file,'w');
  if fid ~= -1
    fprintf(fid, '%s\n', ln);
    fclose(fid);

    [st, output] = system([ precmd exe ' ' file ]);
    failed=(st ~= 0);
  end
  delete(file);
  
% ----------------------------------------------------------------------------
function [failed, output] = eval_python(ln)
% EVAL_PYTHON Execute code (char) with Python.
%   The PYTHONEXECUTABLE env var can be set to specify which Python interpreter
%   to call. Default is python3.
%
% Example: T=evals('python: a=1; print(a);')

  % required to avoid Matlab to use its own libraries
  if ismac,      precmd = 'DYLD_LIBRARY_PATH= ; DISPLAY= ; ';
  elseif isunix, precmd = 'LD_LIBRARY_PATH= ; DISPLAY= ; '; 
  else           precmd=''; end
  failed = true; output='';
  
  exe = getenv('PYTHONEXECUTABLE');
  if isempty(exe), exe='python3'; end
  
  file = [ tempname '.py' ];
  fid = fopen(file,'w');
  if fid ~= -1
    fprintf(fid, '%s\n', ln);
    fclose(fid);

    [st, output] = system([ precmd exe ' ' file ]);
    failed=(st ~= 0);
  end
  delete(file);

% ----------------------------------------------------------------------------
function [failed,output] = eval_url(ln)
% EVAL_URL Get distant URL content
  filename = read_url(ln); % from iofun
  try
    output = fileread(filename);
    failed = false;
    if ~strncmp(ln, 'file:', 5) && ~strcmp(ln,filename)
      delete(filename); % remove temporary file
    end
  catch ME
    output = ME; failed = true; 
  end
