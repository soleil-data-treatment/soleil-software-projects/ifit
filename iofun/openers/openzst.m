function out = openzst(filename)
%OPENZST Open a Zstandard compressed data set, display it
%        and set the 'ans' variable to an iData object with its content
% 

out = ifitopen(filename, 'zst');

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
