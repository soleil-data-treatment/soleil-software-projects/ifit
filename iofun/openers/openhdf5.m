function out = openhdf5(filename)
%OPENHDF5 Open an HDF5 file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','rita22010n021140.hdf'); a=openhdf5(f); numel(a)==3 && all(size(a(1))==[128 128 21])

out = openhdf(filename, @read_hdf5);

if ~isdeployed
  assignin('base','ans',out);
  ans = out
end
