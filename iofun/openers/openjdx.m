function out = openjdx(filename)
%OPENJDX  Open a Electronic Chemistry Exchange/JCAMP data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','2Methyl1Propanol.jdx'); a=openjdx(f); all(size(a)==[1 2153]) && strncmp(a.Title,'2-methyl',8)

out = ifitopen(filename, @read_dx);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
