function out = opennc(filename)
%OPENNC Open a NetCDF/CDF file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','SQW_coh_lGe.nc'); a=opennc(f); strcmp(a.Signal,'Data.Variables.dsf') && all(size(a) == [2000 95])

out = opencdf(filename);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
