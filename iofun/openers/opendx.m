function out = opendx(filename)
%OPENDX  Open a Electronic Chemistry Exchange/JCAMP data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','Adamantane.dx'); a=opendx(f); strcmp(a.Signal,'Data.Blocks.YData') && all(size(a)==[1 1791])

out = ifitopen(filename, @read_dx);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
