function out = openply(filename)
%OPENPLY Open an PLY 3D ascii File, display it
%        and set the 'ans' variable to an iData object with its content
% 
%  Example: f=fullfile(ifitpath,'data','socket.ply'); a=openply(f); all(size(a) == [836 1])

out = openstl(filename, 'PLY');

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
