function out = openhdr(filename)
%OPENHDR Open a Analyze volume dataset (medical imaging), display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','cyno_atlas.4dint.hdr'); a=openhdr(f); strcmp(a.Axis{1},'x') && all(size(a)==[128 160 128])

if ~isa(filename,'iData')
  out = ifitopen(filename, @read_analyze,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input object arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
elseif isfield(out.Data,'hdr')
  % this is an Analyze HDR/IMG file. Proceed.
    
  hdr=out.Data.hdr;
  x = ([1:hdr.dim(1)]-hdr.origin(1))*hdr.siz(1);
  y = ([1:hdr.dim(2)]-hdr.origin(2))*hdr.siz(2);
  z = ([1:hdr.dim(3)]-hdr.origin(3))*hdr.siz(3);

  out.Signal = 'Data.img';
  out.x=x;
  out.y=y;
  out.z=z;
  axis(out,1,'x');
  axis(out,2,'y');
  axis(out,3,'z');
else
  warning([ mfilename ': The loaded data set ' out.Tag ' from ' out.Source ' is not an HDR/IMG Analyze Mayo Clinic data format.' ]);
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

