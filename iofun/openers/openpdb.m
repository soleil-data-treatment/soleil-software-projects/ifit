function out = openpdb(filename)
%OPENPDB Open a Protein Data Bank file, display its structure factor
%        and set the 'ans' variable to an iData object with its content
% 
%  Example: f=fullfile(ifitpath,'data','PDBSilk.pdb'); a=openpdb(f); all(size(a) == [500 1])

if ~isa(filename,'iData')
  out = ifitopen(filename, @read_pdb,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
end

if isfield(out, 'Data.StructureFactor')
  out.Signal='Data.StructureFactor';
  out.Title ='I(Q)';
end
if isfield(out, 'Data.Momentum_invAngs')
  out.Axis{1} = 'Data.Momentum_invAngs';
  out.Label{1}= 'Q [1/Angs]';
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

