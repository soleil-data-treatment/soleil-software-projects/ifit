function out = openh5(filename)
%OPENH5 Open an HDF5 file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','peaks.hdf5'); a=openh5(f); strcmp(a.Signal,'Data.iData.Data.Signal') && all(size(a)==[49 49])

out = openhdf(filename, @read_hdf5);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

