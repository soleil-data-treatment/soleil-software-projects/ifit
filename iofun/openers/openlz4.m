function out = openlz4(filename)
%OPENLZ4 Open a LZ4 compressed data set, display it
%        and set the 'ans' variable to an iData object with its content
% 

out = ifitopen(filename, @read_compressed);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
