function out = opentdms(filename)
%OPENTDMS  Open a LabView TDMS data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','simple_test.tdms'); a=opentdms(f); all(size(a)==[1000 1])

out = ifitopen(filename, @read_tdms);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
