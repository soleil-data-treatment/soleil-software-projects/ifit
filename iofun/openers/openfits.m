function out = openfits(filename)
%OPENNII Open a FITS image data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','30dor.fits'); a=openfits(f); isstruct(a.Data.PrimaryData) && all(size(a)==[151 151])

out = ifitopen(filename, @read_fits);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
