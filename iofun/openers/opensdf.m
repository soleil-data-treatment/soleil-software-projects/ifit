function out = opensdf(filename)
%OPENSDF Open a HP/Agilent/Keysight data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
%  Example: f=fullfile(ifitpath,'data','example.sdf'); a=opensdf(f); all(size(a) == [118848 1])

out = ifitopen(filename, @read_sdf);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
