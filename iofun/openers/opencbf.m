function out = opencbf(filename)
%OPENCBF Open a Crystallographic Binary File, display it
%        and set the 'ans' variable to an iData object with its content
%
% Example: f=fullfile(ifitpath,'data','insulin_pilatus6mconverted_orig.cbf'); a=opencbf(f); isstruct(a.Data.parameters)

if ~isa(filename,'iData')
  out = ifitopen(filename,@read_cbf,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
else
  axescheck(out);
  if isfield(out.Data, 'header')
    out.Data.parameters = str2struct(out.Data.header);
  end
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
