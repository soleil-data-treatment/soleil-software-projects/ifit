function out = opensim(filename)
%OPENSIM Open a McStas/McXtrace SIM file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','Diff_BananaPSD_1314088587.th_y'); a=opensim(f); all(size(a)==[25 170]) && strcmp(a.Label{1},'y [m]')
% Example: f=fullfile(ifitpath,'data','Diff_BananaTheta_1314088587.th'); a=opensim(f); all(size(a)==[340 1]) && strncmp(a.Label{1},'Lon',3)
% Example: f=fullfile(ifitpath,'data','Monitor_GV_1330447143_list.L.x.y.z'); a=opensim(f); all(size(a)==[7778 1]) && isfield(a,'L')

if ~isa(filename,'iData')
  out = ifitopen(filename,'mccode','raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
end

if numel(out) == 1
  if ~isempty(findstr(out,'simulation description file'))
    % this is a McCode SIM file
    
    % Find filename fields in sim struct:
    filenames = findstr(out,'filename');
    if ~iscellstr(filenames), filenames = { filenames }; end
    dirname   = fileparts(out.Source);

    a=[];
    if length(filenames(:)) > 0
      % This is a McCode 'overview' plot
      for j=1:length(filenames(:))
        filename = filenames{j};
        filename(~isstrprop(filename,'print')) = '';
        filename(1:length('filename: '))='';
        filename(findstr(';',filename):length(filename))='';
        filename=strtrim(filename);
        filename(findstr(' ',filename):length(filename))='';
        if isempty(filename), continue; end
        filename = fullfile(dirname,filename);
        a = [ a load(iData, filename) ];
      end
    else
      % This is a .sim from a scan
      filename = 'mccode.dat';
      filename = fullfile(dirname,filename);
      if exist(filename)
        a = load(iData, filename);
      end
    end
    out = a;
    clear a;
  elseif ~isempty(findstr(out,'Isotropic_Sqw'))
    out = opensqw(out);
  elseif ~isempty(findstr(out, 'multiarray_1d'))
    out = load_mccode_scan(out);
  else
    out = load_mccode_dat(out); % private, below
  end
  
  if ~isempty(out) && isempty(findstr(out,'McStas')) && isempty(findstr(out,'McCode'))
    warning([ mfilename ': The loaded data set ' out.Tag ' from ' out.Source ' is not a McCode data format.' ]);
    return
  end

end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

% ------------------------------------------------------------------------------
function a=load_mccode_dat(a)
% function a=load_mccode_dat(a)
%
% Returns an iData style dataset from a McCode 1d/2d/list monitor file
% as well as simple XYE files
% Some labels are also searched.
%
% Version: $Date$ $Version$ $Author$
% See also: iData/load, save, iData/saveas

% inline: load_mccode_param

% Find proper labels for Signal and Axis

xlab=''; ylab=''; zlab='';
d = a.Data;

i = findfield(a, {'xlabel','x_label'}, 'char exact flat');
if ~isempty(i)
  if iscell(i) l=get(a,i{1}); else l=get(a,i); end
  [l, xlab] = strtok(char(l), ':='); xlab=strtrim(xlab(2:end));
  if isempty(xlab), xlab=l; end
end

i = findfield(a, {'ylabel','y_label'}, 'char exact cache flat');
if ~isempty(i)
  if iscell(i) l=get(a,i{1}); else l=get(a,i); end
  [~, r] = strtok(char(l), ':='); ylab=strtrim(r(2:end));
  if isempty(ylab), ylab=l; end
end

i = findfield(a, {'xlabel','x_label'}, 'char exact cache flat');
if ~isempty(i)
  if iscell(i) l=get(a,i{1}); else l=get(a,i); end
  [l, zlab] = strtok(char(l), ':='); zlab=strtrim(zlab(2:end));
  if isempty(zlab), zlab=l; end
end

i = findfield(a, 'component','char exact cache');
if ~isempty(i)
  if iscell(i) l=get(a,i{1}); else l=get(a,i); end
  [l, lab] = strtok(char(l), ':='); lab=strtrim(lab(2:end));
  if isempty(lab), lab=l; end
  a.Title = lab;
  a.Data.Component = lab;
  a.Component='Data.Component'; % 'Component name';
end

i = findfield(a, 'Creator','char exact cache'); 
if iscell(i) && length(i) > 1
  l=get(a,i{end});
  [l, creator] = strtok(char(l), ':='); creator=strtrim(creator(2:end));
  if isempty(creator), creator=l; end
  a.Creator=creator; 
end

i = findfield(a, 'type','char exact cache'); 
if ~isempty(i)
  if iscell(i) t=get(a,i{1}); else t=get(a,i); end
  a.Format = [ a.Format ' ' char(t) ];
end

clear d

% check that guessed Signal is indeed what we look for
signal = a.Signal;
if isempty(signal) || (ischar(signal) && (~isempty(strfind(signal, 'MetaData')) ...
    || ~isempty(strfind(signal, 'errors')) ...
    || ~isempty(strfind(signal, 'events'))))
  % biggest field is not the list but some MetaData, search other List 
  % should be 'Data.MataData.variables' or 'Data.I'
  for search = {'I','N', 'Sqw','data','variables'} 
    if isfield(a,[ 'Data.' search{1} ]);
      search=[ 'Data.' search{1} ];
      signal = search;
      break; 
    end
  end
  if isempty(signal)
    [match, types, dims] = findfield(a, '', 'numeric cache biggest');
    if iscellstr(match)
      signal = match{1};
    elseif ischar(match)
      signal = match;
    end
  end
  if ~isempty(signal), a.Signal=signal; end
end
siz = size(a);

% treat specific data formats 1D, 2D, List for McCode ==========================
if ~isempty(strfind(a.Format,'0D monitor'))
  if verbosity(a) > 1
    disp([ mfilename ': 0D monitor ' a.Source ]);
  end
  a.Signal= a.Data.values(1);
  a.Error=  a.Data.values(2);
  a.I='Signal';
  a.E='Error';
  a.N=a.Data.values(3);
  
elseif (~isempty(strfind(a.Format,'1D monitor')) || ~isempty(strfind(a,'array_1d'))) && size(a,2) ~= 4

  if verbosity(a) > 1
    disp([ mfilename ': 1D monitor ' a.Source ]);
  end
  a.Label{1}=xlab;
  a.Title=ylab;
  a.I='Signal';
  a.E='Error';
  siz = numel(get(a,'Signal')');
  % set axes
  i = findfield(a, {'xylimits','xlimits'}, 'numeric exact cache flat');
  if iscell(i) && ~isempty(i), i = i{1}; end
  lims = get(a,i);
  if numel(lims)>= 2, 
    xax = linspace(lims(1),lims(2),siz);
    a.x = xax;
    a.Axis{1}='x'; a.Label{1}=xlab;
  end
  
elseif ~isempty(strfind(a,'array_1d')) && size(a,2) == 4
  % recover a wrongly imported McCode 1D, using header, and set default columns
  if verbosity(a) > 1
    disp([ mfilename ': 1D monitor ' a.Source ]);
  end
  sig = a.Signal;
  a.Axis_1=[ sig '(:,1)' ];
  a.Signal=[ sig '(:,2)' ];
  a.Error =[ sig '(:,3)' ];
  a.Events=[ sig '(:,4)' ];
  a.Axis{1}='Axis_1';
  header = findfield(a,'Attributes','char cache');
  if ~isempty(header)
    if iscell(header), header = get(a, header{1});
    else               header = get(a, header); end
    header = textscan(header, '%s','Delimiter','#');
    header = str2struct(header{1});
    a.Header=header;
    a.Format = 'McCode 1D monitor';
    a = opensim(a); % this will now make a better job
    return
  end
  
elseif (~isempty(strfind(a.Format,'2D monitor')) || ~isempty(strfind(a,'array_2d'))) ...
    && isempty(strfind(a.Format,'list monitor'))

  if verbosity(a) > 1
    disp([ mfilename ': 2D monitor ' a.Source ]);
  end
  % Get sizes of x- and y- axes:
  i = findfield(a, 'variables', 'numeric exact');
  if iscell(i) && ~isempty(i), i = i{1}; end
  if numel(get(a, i)) >= prod(siz)    % e.g. i='Data.MetaData.variables'
    a.Signal=i;
    a.Title=zlab;
  end
  siz = size(a);
  a.I='Signal';
  i = findfield(a, 'errors', 'numeric exact cache');
  if ~isempty(i)
    a.Error=i{1};
  else a.Error=0;
  end
  a.E='Error';
  i = findfield(a, 'events', 'numeric exact cache');
  if ~isempty(i) 
    a.N=i{1};
  end
  
  % set axes
  i = findfield(a, {'xylimits','xlimits'}, 'numeric exact cache flat');
  if iscell(i) && ~isempty(i), i = i{1}; end
  lims = get(a,i);
  if numel(lims)>= 2, 
    xax = linspace(lims(1),lims(2),siz(2));
    a.y=xax; a.Label{2}=xlab;
    a.Axis{2}='y';
  end
  if numel(lims)>= 4, 
    yax = linspace(lims(3),lims(4),siz(1));
    a.x=yax; a.Label{1}=ylab;
    a.Axis{1}='x';
  end

elseif ~isempty(strfind(a.Format,'list monitor'))

  if verbosity(a) > 1
    disp([ mfilename ': list monitor ' a.Source ]);
  end
  
  % the Signal should contain the List
  list = a.Signal;
  if ischar(list) && ~isfield(a, 'List')
    a.List=list; %  'List of events';
    % column signification is given by tokens from the ylab
    columns = strread(ylab,'%s','delimiter',' ');
    index_axes = 0;
    for index=1:length(columns)
      a.(columns{index})=[ list '(:,' num2str(index) ')' ];
      if index==1
        a.Signal=columns{index};
      elseif index_axes < 3
        index_axes = index_axes +1;
        a.Axis{index_axes}=columns{index};
      end
    end
    if ~isfield(a, 'N'), a.N=length(a{0}); end
  end
end

% build the title: 
%   sum(I) sqrt(sum(I_err^2)) sum(N)

values = get(a, findfield(a, 'values','cache'));
if ~isempty(values)
  if iscell(values) values=values{1}; end
  t_sum = sprintf(' I=%g I_err=%g N=%g', values);
else
  t_sum = ''; 
end
%   X0 dX, Y0 dY ...

t_XdX = get(a, findfield(a, 'statistics', 'exact cache'));
if ~isempty(t_XdX)
  if iscell(t_XdX) t_XdX=t_XdX{1}; end
else
  t_XdX = '';
  if ~isscalar(a)
    if ndims(a) == 1; ax='X'; else ax = 'YXZ'; end
    for index=1:ndims(a)
      [dx,x0]=std(a,index);
      t_XdX = [t_XdX sprintf(' %c0=%g d%c=%g;', ax(index), x0, ax(index), dx) ];
    end
  end
end
a.Title = [ a.Title t_sum, t_XdX ];
a.statistics=t_XdX; % 'Center and Gaussian half width';
a.values=values;    % 'I I_err N';

% get the instrument parameters
param = load_mccode_param(a, 'Param');
a.Data.Parameters = param;
a.Parameters='Data.Parameters'; % 'Instrument parameters';

% end of loader

% ------------------------------------------------------------------------------
function a=load_mccode_scan(a0)
% function a=load_mccode_scan(a0)
%
% Returns iData style datasets from a McCode scan output file
%
% See also: iData/load, save, iData/saveas

% Define alias for the 'raw' datablock
a0.Datablock=a0.Signal;

% get the column labels
cnames=strread(a0.Data.Attributes.MetaData.variables,'%s','delimiter',' ');
cnames=cnames(3:end);

if ~isempty(findfield(a0, 'xlabel','cache')) 
  xlab = deblank(a0.Data.Attributes.MetaData.xlabel);
  xlab(1:length('# xlabel: '))='';
else xlab=''; end
if ~isempty(findfield(a0, 'ylabel', 'cache')) 
  ylab = deblank(a0.Data.Attributes.MetaData.ylabel);
  ylab(1:length('# ylabel: '))='';
else ylab=''; end
if ~isempty(findfield(a0, 'xvars', 'cache')) 
  xvars = deblank(a0.Data.Attributes.MetaData.xvars);
  xvars(1:length('# xvars: '))='';
else xvars=''; end

if ~isempty(xvars)
  xvars_i = find(cellfun('isempty', strfind(cnames,xvars)) == 0);
  if ~isempty(xvars_i)
    if length(xvars_i) > 1
      cnames=cnames(xvars_i(end):end);
      xvars_i=xvars_i(1);
    end
    a0.x=[ a0.Signal '(:,' num2str(xvars_i) ')' ]; a0.Label{1}=xvars; % create an alias for xvars
    a0.xvars='x';   % xvars; % create an alias for xvars
    % locate xvars label and column
    xlab=xvars;
  end

  % Define scanning variable
  a0.Axis{1}='x';
end


param = load_mccode_param(a0, 'Param');
a0.Data.Parameters = param;
a0.Parameters='Data.Parameters';  % 'Instrument parameters';

siz = size(a0);
siz = (siz(2)-1)/2;

a = [];
for j=1:siz
  b = copyobj(a0);
  ylab=cnames(2*j);
  b.Signal=[ a0.Signal '(:,' num2str(2*j) ')'];
  b.Title=ylab;
  if ~isempty(findfield(a0, '_ERR', 'cache')) 
    b.Error=[ a0.Signal '(:,' num2str(1+2*j) ')'];
  end
  b.Name = [ char(ylab) ': ' char(b.Title) ];
  a = [a b];
end
% ------------------------------------------------------------------------------
% build-up a parameter structure which holds all parameters from the simulation
function param=load_mccode_param(a, keyword)
  if nargin == 1, keyword='Param:'; end
  param = [];
  
  par_list = findfield(a, keyword,'case cache');
  if ~isempty(par_list)
    for index=1:numel(par_list)
      try; this=get(a,par_list{index}); catch; this=[]; end
      if iscellstr(this), this=char(this); end
      if isscalar(this) && (isnumeric(this) || ischar(this))
        name         = fliplr(strtok(fliplr(par_list{index}),sprintf('.')));
        param.(name) = this; 
      end
    end
  end

  par_list = findstr(a, keyword, 'case');
  if ischar(par_list), par_list=cellstr(par_list); end
  
  % search strings of the form 'keyword' optional ':', name '=' value
  for index=1:length(par_list)
    line         = par_list{index};
    reversed_line= line(end:-1:1);
    equal_sign_id= find(reversed_line == '=');
    if isempty(equal_sign_id), continue; end % incomplete line: skip it
    name         = fliplr(strtok(reversed_line((equal_sign_id+1):end),sprintf(' \n\t\r\f;#')));
    if isempty(name)
      column_sign_id = findstr(line, keyword);
      name = strtok(line((column_sign_id+length(keyword)+1):end));
    end
    
    if isfield(param, name) % was set with a Previous 'Param name=value' token
      continue;
    elseif isfield(a.Data, name) && any(getfield(a.Data, name))
      value = getfield(a.Data, name);
    else
      value = strtok(fliplr(reversed_line(1:(equal_sign_id-1))),sprintf(' \n\t\r\f;#'));
      if ~isempty(str2num(value)), value = str2num(value); end
    end
    if ~isempty(value) && ~isempty(name) && ischar(name)
      param = setfield(param, sanitize_name(name), value);
    end
  end

% ------------------------------------------------------------------------------
function name = sanitize_name(name)
  name(~isstrprop(name,'print')) = '';
  name(~isstrprop(name,'alphanum')) = '_';
  if name(1) == '_'
    name = name(find(name ~= '_', 1):end);
  end
  if isstrprop(name(1),'digit')
    name = [ 'x' name ];
  end
  name = regexprep(name ,'_{2,}','_'); % shorten multiple _

