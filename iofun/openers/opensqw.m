function out = opensqw(filename)
%OPENSQW Open a McCode Sqw file (isotropic dynamic stucture factor, text file) 
%        or Horace Sqw file, display it and set the 'ans' variable to an iData
%        object with its content
% 
% Example: f=fullfile(ifitpath,'data','Horace_sqw_1d.sqw'); a=opensqw(f); all(size(a)==[9 4324])

if ~isa(filename,'iData')
  out = ifitopen(filename,@read_sqw,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
elseif ~isempty(findstr(out,'Sqw'))
  % this is a SQW file
  
  % check if this is an ISIS SQW with a @sqw object
  if isa(out.Data, 'sqw')
    out.Data=struct(out.Data);
    axescheck(out,'force signal'); % get biggest field
  end

  % Find proper axes and Signal
  [fields, types, dims] = findfield(out, '', 'numeric cache');
  % get Signal
  [~,index]=max(dims);
  
  sz = size(subsref(out, fields{index}));
  % get Momentum
  q_index = find(dims == sz(1));
  w_index = find(dims == sz(2));
  xylimits= findfield(out, 'xylimits','cache numeric exact case');
  if ~isempty(xylimits)
    xylimits=out.(xylimits);
  end
  
  if ~isempty(xylimits) && numel(xylimits) == 4
    if isempty(q_index)
      q_values = linspace(xylimits(1), xylimits(2), sz(2));
      out.q=q_values; axis(out,1,'q'); label(out, 1, 'Q [AA-1]'); 
    end
    if isempty(w_index)
      w_values = linspace(xylimits(3), xylimits(4), sz(1));
      out.w=w_values; axis(out,2,'w'); label(out, 2, 'w [meV]');  
    end
  end

  if ~isempty(q_index) && ~isempty(w_index)
    % case: 2D S(q,w)
    % out.(fields{index}) = transpose(out.(fields{index}));
    if ~isempty(q_index) 
      q_values= fields{q_index}; 
      out.q=q_values; axis(out,1,'q'); label(out, 1, 'Q [AA-1]'); 
    end
    % get Energy
    
    if ~isempty(w_index)
      w_values= fields{w_index}; 
      out.w=w_values; axis(out,2,'w'); label(out, 2, 'w [meV]');  
    end
    out.Signal=fields{index};
  elseif size(out.Signal, 2) == 5
    % case: 4D S(hkl,w)
    % convert to event list
    sig = out.Signal; % get the definition of the Signal
    ax={'H','K','L','W','Sqw'};
    for index=1:numel(ax)
      if index <= 3
        out.(ax{index})=sprintf('%s(:,%i)', sig, index);
        label(out, index, [ 'Momentum ' ax{index} ' [rlu]' ]);
      elseif index==4
        out.(ax{index})=sprintf('%s(:,%i)', sig, index);
        label(out, index, [ 'Energy ' ax{index} ' [meV]' ]);
      else
        out.(ax{index})=sprintf('%s(:,%i)', sig, index);
        label(out, index, [ 'S(hkl,w)' ]);
      end
    end
    % now set signal and all axes
    axis(out, 0, 'Sqw');
    for index=1:4
      axis(out, index, ax{index});
    end
  end
  if     ~isempty(findstr(out, 'incoherent')) out.Title = 'Sqw (inc)';
  elseif ~isempty(findstr(out, 'coherent'))   out.Title = 'Sqw (coh)';
  else out.Title ='Sqw';
  end
  try
    t = out.Data.Attributes.MetaData.title;
  catch
    t = findstr(out, 'title'); 
  end
  out.Name = char(out, 'clean', t);

  out.Error=0;
else
  warning([ mfilename ': The loaded data set ' out.Tag ' from ' out.Source ' is not a McCode Sqw data format.' ]);
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

