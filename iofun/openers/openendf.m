function out = openendf(filename)
%OPENENDF Open an Evaluated Nuclear Data File, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','tsl_0012_para-d.endf'); a=openendf(f); a.T==19 && strcmp(a.Axis{1},'alpha') && all(size(a)==[56 219])

  if ~isa(filename,'iData')
    in = ifitopen(filename, @read_endf,'raw');
  else
    in = filename;
  end
  clear filename;

  if numel(in) > 1
    % handle input iData arrays
    out = []; % the number of elements may change, can not simply replace
    for index=1:numel(in)
      out = [ out feval(mfilename, in(index)) ];
      in(index) = iData; % free memory
    end
    return
  end
  
  % we search for 'MT' and 'MF' fields, and then access their parent.
  % first look for MT=451 and MF=1 to initiate base object.
  mt_links = findfield(in, 'MT','case numeric cache');
  if ~isempty(mt_links)
    mt_values= cell2mat(get(in, mt_links));
    mt_info  = find(mt_values == 451);
    if ~isempty(mt_info)
      [~,mt_info_path]  = str2subs([ mt_links{mt_info} '/../' ]); % parent MT451
      mt_info_451  = get(in, mt_info_path); % this is MT=451
      mt_links{mt_info}=[]; % remove from list of MT/MF
    end
    mf_links = findfield(in, 'MF','case numeric cache');
    mf_values= cell2mat(get(in, mf_links));

    % except for MT=451 (COMMENT), we pass through all other sections,
    % copy the base object, and define aliases that point to data.
    out = [];
    for index=1:numel(mt_links)
      % get parent, MT and MF
      if isempty(mt_links{index}), continue; end
      [section_subs,section_path] = str2subs([ mt_links{index} '/../' ]);
      % create a copy of base object
      new = copyobj(in, 'fast');
      if     ~isempty(mt_info), new.Data.info=mt_info_451;
      elseif ~isempty(mf_info), new.Data.info=mf_info_1; 
      end
      new = open_endf_aliases(new,           mf_values(index), mt_values(index));
      new = open_endf_mf7(new, section_path, mf_values(index), mt_values(index));
      
      out = [ out new ];
    end
  else
    % ACE file
    out = in;
  end

  if ~nargout
    figure; subplot(log10(out));
    
    if ~isdeployed
      assignin('base','ans',out);
      ans = out
    end
  end

% ------------------------------------------------------------------------------
function out=open_endf_aliases(out, MF, MT)
  % OPEN_ENDF_ALIASES Set generic aliases for MF/MT
  
  out.MT=MT; % 'ENDF Section';
  out.MF=MF; % 'ENDF File';
  
  if verbosity(out) > 1
    disp([ mfilename ': MF=' num2str(MF) ' MT=' num2str(MT) ' section: ' 'common' ])
  end
  
  if ~isfield(out, 'MAT') && ~isempty(findfield(out, 'MAT','first exact cache'))
    out.MAT=findfield(out, 'MAT','first exact cache'); %   'ENDF Material number';
  end
  if ~isfield(out, 'ZSYMAM') && ~isempty(findfield(out, 'ZSYMAM','first  exact cache'))
    out.Material=findfield(out, 'ZSYMAM','first exact cache'); % 'ENDF Material description (ZSYMAM)';
  end
  if ~isfield(out, 'EDATE') && ~isempty(findfield(out, 'EDATE','first exact cache'))
    out.EDATE=findfield(out, 'EDATE','first exact cache'); %  'ENDF Evaluation Date (EDATE)';
  end
  if ~isfield(out, 'ZA') && ~isempty(findfield(out, 'ZA','first exact cache'))
    out.charge=findfield(out, 'ZA','first exact cache'); % 'ENDF material charge Z (ZA)';
  end
  if ~isfield(out, 'AWR') && ~isempty(findfield(out, 'AWR','first exact cache'))
    out.mass=findfield(out, 'AWR','first exact cache'); %   'ENDF material mass A [g/mol] (AWR)';
  end
  if ~isfield(out, 'DescriptiveData') && ~isempty(findfield(out, 'DescriptiveData','first case cache'))
    out.DescriptiveData=findfield(out, 'DescriptiveData','first case cache'); %  'ENDF DescriptiveData (MF1/MT451)';
  elseif isfield(out, 'info')
    out.DescriptiveData=findfield(out, 'info','first exact case cache'); % 'ENDF DescriptiveData (MF1/MT451)';
  elseif ~isfield(out, 'description') && ~isempty(findfield(out, 'description','first cache'))
    out.DescriptiveData=findfield(out, 'description','first cache'); % 'ENDF DescriptiveData (MF1/MT451)';
  elseif ~isfield(out, 'comment') && ~isempty(findfield(out, 'comment','first exact cache'))
    out.DescriptiveData=findfield(out, 'description','first case cache'); % 'ENDF DescriptiveData (MF1/MT451)';
  end
  % search for aliases that we need
  tokens={'S','E','T','W','SB','Sab','alpha','beta','NP','INT','MT','MF','Teff'};
  for index=1:numel(tokens)
    tok = tokens{index};
    if ~isfield(out, tok) && ~isempty(findfield(out, tok,'first case exact cache'))
      out.(tok)= findfield(out, tok,'first case exact cache');
    end
  end
  
% ------------------------------------------------------------------------------
function out=open_endf_mf7(out, section_path, MF, MT)
% OPEN_ENDF_MF7 Handle ENDF TSL MF=7 cases (MT=2 and 4)

  if MF ~= 7, return; end
  if verbosity(out) > 1
    disp([ mfilename ': MF=' num2str(MF) ' MT=' num2str(MT) ' section: ' section_path ])
  end
  if MT == 2      % elastic
    if     get(out, [ section_path '.LTHR' ]) == 1
      out.Signal     =[ section_path '.S' ];
      out.Title      ='S(E,T) Coherent Elastic Scattering Bragg Edges';
      out.Energy     =[ section_path '.E' ];      % 'Neutron Incident Energy [eV]';
      out.Axis{1}    ='Energy';
      out.Label{1}   ='Neutron Incident Energy [eV]';
      out.Temperature=[ section_path 'T' ]; % 'Temperature [K]';
      out.Scattering ='coherent elastic';
    elseif get(out, [ section_path '.LTHR' ]) == 2
      out.Signal     =[ section_path '.W' ];
      out.Title      ='Debye-Waller integral divided by the atomic mass [eV-1] ) (W)';
      out.Sigma      =[ section_path '.SB' ];      % 'Characteristic bound cross section [barns] (SB)';
      out.Temperature=[ section_path '.T' ]; % 'Temperature [K]';
      out.Scattering ='incoherent elastic';
      out.Axis{1}    ='Temperature';
      out.Label{1}   ='Temperature [K]';
    end
  elseif MT == 4  % inelastic
    % assign axes: alpha, beta, Sab for MF7 MT4
    out.Signal      =[ section_path '.Sab' ];
    out.Title       ='S(alpha,beta,T) Inelastic Scattering';
    out.alpha       =[ section_path '.alpha' ]; 
    out.beta        =[ section_path '.beta' ]; 
    out.Axis{1}     ='alpha';
    out.Label{1}    ='Unit-less wavevector [h2q2/AkT] alpha';
    out.Axis{2}     ='beta';
    out.Label{2}    ='Unit-less energy [hw/kT] beta';
    out.Temperature =[ section_path '.T' ];       % 'Temperature [K]';
    out.classical   =~get(out, [ section_path '.LASYM' ]);  % 'classical/symmetric[1] or quantum/asymmetric[0]';
    B = get(out, [ section_path '.B' ]);
    out.weight      =B(3);         % 'Standard material mass [g/mol]';
    out.sigma_free  =B(1);     % 'total free atom cross section [barns]';
    out.sigma_inc   =B(1)*(B(3)+1)^2/B(3)^2; % 'total bound cross section [barns]';
    out.multiplicity=B(6);   % 'the number of principal scattering atoms in the material';
    out.B           =B;                 % 'Analytical model physical constants (B)';
    out.Scattering  ='(in)coherent inelastic';
  end
  % assign axes: alpha, beta, Sab for MF7 MT4
  % must check if the input object should be split into temperatures
  if size(out, 3) > 1 || (isfield(out, 'T') && numel(get(out,'T') > 1))
    out = open_endf_mf7_array(out);
  end

% ------------------------------------------------------------------------------
function t0=open_endf_mf7_array(t)
  % open_endf_mf7_array: split an array of ENDF entries vs Temperature
  t0 = []; E = [];
  t.T=get(t, t.T);
  disp(sprintf('%s: MF=%3i   MT=%i TSL T=%s', mfilename, t.MF, t.MT, mat2str(t.T)));
  % if numel(t.T)==1, t0=t; return; end
  
  for index=1:numel(t.T)
    t1     = copyobj(t);
    t1.T   = t.T(index);
    if isfield(t1, 'ZSYMAM')
      t1.Name = [ t1.Name ' ' t1.ZSYMAM ];
    end
    t1.Name = [ t1.Name ' T=' num2str(t1.T) ' [K]' ];
    if isfield(t1, 'description')
      t1.Name = [ t1.Name ' ' t1.description ];
    end
    if isfield(t1, 'ZSYMAM') t1.Title=t1.ZSYMAM; end
    t1.Title = [ t1.Title ' T=' num2str(t1.T) ' [K]' ];
    
    if t1.MT == 2 % Incoherent/Coherent Elastic Scattering
      if isstruct(t.Data.S)
        f = fieldnames(t.Data.S);
        for index=1:numel(f)
          tstr = [ 'T' num2str(floor(t1.T)) ];
          if strncmp(f{index}, tstr, numel(tstr))
            S = t.Data.S.(f{index});
            if isstruct(S)
              E = S.x;
              S = S.y;
            end
            t1.Data.S = S;
            if ~isempty(E), t1.Data.E = E; end
          end
        end
      else
        t1.NP  = t.NP(index);
        t1.S   = t.S(index,:);
        t1.INT = t.INT(index);
      end
      t1.Title = [ t1.Title ' TSL elastic' ];
      axis(t1, 3, t1.T);
      
    elseif t1.MT == 4 % Incoherent Inelastic Scattering
      if isfield(t1, 'Sab')
        t1.Sab = t.Sab(:,:,index);
      elseif isfield(t1, 'scattering_law')
        t1.Sab = t.scattering_law(:,:,index);
      end
      
      if isstruct(t.Teff) && isfield(t.Teff, 'y');
        t1.Teff     = t.Teff.y(index);
      else
        t1.Teff     = t.Teff(index);
      end
      t1.Title = [ t1.Title ' TSL inelastic' ];
      axis(t1, 3, t1.T);
    end
    
    t0 = [ t0 t1 ];
  end
