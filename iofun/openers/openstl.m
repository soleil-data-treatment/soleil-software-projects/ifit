function out = openstl(filename, format)
%OPENSTL Open an STL/SLP 3D ascii stereolithography data file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','Bunny-LowPoly2.stl'); a=openstl(f); all(size(a)==[876 1]) && ndims(a)==3


if ~isa(filename,'iData')
  if nargin < 2
    format = 'STL';
  end

  if ischar(format)
    switch upper(format)
    case 'STL'
      format=@read_stl;
    case 'PLY'
      format=@read_ply;
    case 'OFF'
      format=@read_off;
    end
  end
  out = ifitopen(filename,format,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
else
  % this may be an OFF/PLY/STL file. Proceed.
    
  out = private_load_stl(out); % see private
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

