function out = ifitopen(filename, varargin)
% IFITOPEN import data into Matlab. 
%   F = IFITOPEN(FILE) imports given FILE. FILE can be a single file or directory
%   a cell of files or directories. These may make use of wildcards (*,?).
%   Distant URL's are also supported, as well as compressed files. The imported
%   data is stored in the Data property, whereas the importation information is
%   stored in the Loader property. This is equivalent to iData.LOAD
%
%   F = IFITOPEN(FILE, LOADER, ...) specifies the format loader to use. 
%   The LOADER can be a function name, a function handle, a file extension, 
%   'auto' for a guess, 'gui' to popup a format selector, or a structure:
%             LOADER.read   = 'function name'
%             LOADER.options= string of options to catenate after file name
%                          OR cell of options to use as additional arguments
%                             to the method
%   additional arguments are passed to the import routine.
%   Any loader defined callbacks are executed to reorder the raw imported data.
%
%   F = IFITOPEN( ..., 'raw') only imports the data, but does not apply callbacks.
%   You may call AXESCHECK for a default Signal/Axes set.
%
%   F = IFITOPEN(..., 'callback', CB) applies specific callbacks onto iData 
%   objects after data import. The callback CB can be given as funtion handle 
%   @(c)fun(c), a function name (string), an expression to evaluate (may use 
%   'this' as the object itself), or a cell of all these executed in order.
%
%   Default supported formats include: any text based including CSV, Lotus1-2-3, SUN sound, 
%     WAV sound, AVI movie, NetCDF, FITS, XLS, BMP GIF JPEG TIFF PNG ICO images,
%     HDF4, HDF5, MAT workspace, XML, CDF, JSON, YAML, IDL, Python Numpy
%   Other specialized formats include: McStas, ILL, SPEC, ISIS/SPE, INX, EDF, Mantid.
%     SIF, MCCD/TIFF, ADSC, CBF, Analyze, NifTI, STL,PLY,OFF, CIF/CFL,
%     EZD/CCP4, Bruker Varian and JEOL NMR formats, Bruker OPUS, LabView LVM and TDMS,
%     Agilent and Thermo Finnigan MS, Quantum Design VMS, ENDF/ACE, Agilent SDF
%   Compressed files are also supported, with on-the-fly extraction (zip, gz, tar, Z).
%
%   Distant files are supported through e.g. URLs such as 
%     file://, ftp:// http:// and https://
%
%   This may require proxy settings to be set (when behind a firewall)
%   ProxyHost='my.proxy.com'; % Proxy address if you are behind a proxy [e.g. myproxy.mycompany.com or empty]
%   ProxyPort=8080;           % Proxy port if you are behind a proxy [8888 or 0 or empty]
%   java.lang.System.setProperty('http.proxyHost', ProxyHost); 
%   com.mathworks.mlwidgets.html.HTMLPrefs.setUseProxy(true);
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyHost(ProxyHost);
%   java.lang.System.setProperty('http.proxyPort', num2str(ProxyPort));
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyPort(num2str(ProxyPort));
%
%   The configuration of the data formats is controled via 'ifitpref'.
% Version: $Date$ $Version$ $Author$
% See also iData, set, get, findfield, importdata, uiopen, load, iData.load

  % check for load options: callback, raw
  removeme = []; flag_raw = false; callback0 = {}; ok = 0;
  for index=1:numel(varargin)
    if ischar(varargin{index})
      switch varargin{index}
      case 'raw'
        flag_raw = true;
        removeme = [ removeme index ];
      case {'callback','callbacks'}
        removeme = [ removeme index ];
        if index < numel(varargin), % add callbacks 
          this      = varargin{index+1};
          if iscell(this)
            callback0 = { callback0 this{:} };
          else
            callback0 = { callback0 this }; 
          end
          removeme  = [ removeme index+1 ];
        end
      end
    end
  end
  varargin(removeme) = [];
  
  % import data when needed
  if ~isa(filename,'iData')
    if ischar(filename) || iscellstr(filename)
      [data, loader] = ifitread(filename, varargin{:});
    else
      data   = filename;
      loader.description = [ class(filename) ' direct import' ];
      loader.read        = mfilename;
      loader.file.date   = datestr(now);
      loader.filename    = class(filename);
    end
    out            = private_raw2idata(data, loader, callback0);
  else
    out            = filename;
  end
  if flag_raw, return; end
  
  % post import when needed
  out = private_load_callback(out);

  if ~nargout
    figure; subplot(out);
    
    if ~isdeployed
      assignin('base','ans',out);
      ans = out;
    end
  end

% ------------------------------------------------------------------------------
function out = private_raw2idata(data, loader, callback0)
% RAW2IDATA Convert a 'raw' file imports to iData object(s)

  if ~iscell(data),   data   = { data };   end
  if ~iscell(loader), loader = { loader }; end
  out = cell(size(data));
  for index=1:numel(data)
    out{index} = private_raw2idata_single(data{index}, loader{index}, callback0);
  end
  out = [ out{:} ]; % to iData array
  
% ------------------------------------------------------------------------------
function out = private_raw2idata_single(data, loader, callback0)
% RAW2IDATA Convert a 'raw' file import to iData object

  out = [];
  if isempty(data), return; end
  
  % set callbacks
  if ~isempty(callback0)
    loader.callback = callback0;
  end
  
  out        = iData; % create iData object
  out.Data   = data;
  out.Loader = loader;
  if ~isempty(loader) && isfield(loader, 'description')
    out.Format = loader.description;
    out.Source = loader.filename;
    if numel(loader.file) && isfield(loader.file,'date')
      out.Date   = loader.file.date;
    end
  else
    out.Date   = datestr(now);
  end

% ------------------------------------------------------------------------------
function out = private_load_callback(out)
% PRIVATE_LOAD_CALLBACK evaluate the callback for all objects and their callbacks

  % apply callbacks
  out0 = [];
  for index=1:numel(out)
    if numel(out) == 1, this=out; else this=out(index); end
    if isfield(this.Loader,'callback') 
      callbacks = this.Loader.callback;
      if ~isempty(callbacks)
        if ~iscell(callbacks), callbacks={ callbacks }; end
        for cb=callbacks
          % the number of items in 'this' may change
          this = private_load_callback_single(this, cb{1}); % private fcn is below
        end
      end
    end
    out0 = [ out0 this ]; % update
  end
  out = out0;
  
  % final checks (Signal/axescheck, Name, Title)
  for index=1:numel(out)
    if numel(out) == 1, this=out; else this=out(index); end
    if isempty(this) % size == 0
      this = axescheck(this);
    end
    % set Title/Name when not set and found in Data
    for f = {'Title','Name'}
      if isempty(this.(f{1})) && isstruct(this.Data)
        t = findfield(this.Data, f{1}, 'exact char cache');
        if ~isempty(t)
          try
            str = get(this, [ 'Data.' t{1} ]);
            if iscellstr(str), str=char(str); end
            if ~isempty(str) && ischar(str)
              this.(f{1})=char(this, 'clean', str); 
            end
          end
        end
      end
    end
    if numel(out) == 1, out=this; else out(index)=this; end % update
  end

% ------------------------------------------------------------------------------
function this = private_load_callback_single(this, callback)
% PRIVATE_LOAD_CALLBACK evaluate the callback in a reduced environment, only with 'this'
%   The callback CB can be given as funtion handle @(c)fun(c, ...), a
%   function name (string), an expression to evaluate, or a cell of all these.

  if isempty(callback), return; end
  
  % must handle multiple iData objects. cb is here unique
  if numel(this) > 1
    this1 = [];
    for index=1:numel(this)
      if numel(this) == 1, this0=this; else this0=this(index); end
      this0 = private_load_callback_single(this0, callback);
      this1 = [ this1 this0 ]; % update
    end
    this = this1; % out
    return
  end

  % single object, single callback
  this0 = this;
  try
    t0=clock;
    if isa(callback, 'function_handle') || any(exist(callback) == [2 3 5 6 8])
      this = feval(callback, this);
    else
      T = evals(char(callback), 'this', this, 'self', this, 'ans', true);
      if T.failed, warning([ 'iData:' mfilename ], T.output); end
    end
    
    % restore initial object 'this' if it was destroyed at eval
    if ~isa(this, 'iData')
      me = MException([ 'iData:' mfilename ], 'WARNING: ifitopen: final object has changed from %s to %s', class(this0), class(this));
      throw(me);
    else
      if numel(this) > 1, this = this(:)'; end % as a row
    end
    if ~isfield(this.Loader,'callbackduration')
      this.Loader.callbackduration = [];
    end
    this.Loader.callbackduration = [ this.Loader.callbackduration etime(clock, t0) ];

  catch ME
    if ifitpref('verbosity') > 1
      disp(getReport(ME));
      warning([mfilename ': Error when calling loader callback ' char(callback) '. file: ' char(this0.Source) ]);
    end
    this = this0; % return initial
  end
  
