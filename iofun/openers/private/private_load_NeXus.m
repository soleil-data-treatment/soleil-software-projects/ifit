function out = private_load_NeXus(in)
% PRIVATE_LOAD_NEXUS format an HDF file with NeXus structure (signal, axes, ...)
%   See https://www.nexusformat.org/
%
% input:
%   in:  initial single HDF/NeXus data set loaded as a raw iData
% returns:
%   out: NXdata and NXdetector blocks
%
% called by: openhdf

% The elemnets we wish to identify and expose.
%   NXdetector
%   NXdata
%   NXsubentry
%   NXentry
%   NXinstrument
%   NXsample
%   NXprocess
%   NXuser
%
% Attributes can be as follows:
%   NXdata@signal= '<signal>'             name of field with signal (string)
%   NXdata@axes  = '<axis1>,<axis2>,...'  name of axes, in order (string, cellstr)
%                                         '.' indicates unspecified axis.
%
%   NXdata.<signal>@signal=1          indicates this is the signal
%   NXdata.<signal>@axes='<axis1>,<axis2>,...' name of axes for the signal
%   NXdata.<axis1>@axis=<rank>          indicates this is an axis of given rank

out     = [];

% We search for the 'NX_class' items. These identify the entries.
% These are defined as Attributes.
[NX_class, NX_path] = load_NeXus_class(in);
if isempty(NX_class),
  if verbosity(in) > 1, disp([ mfilename ': WARNING: Not a NeXus data set.' ]); end
  return; % Not a NeXus file
end

% location of NXdata stuff
NXdata_path = NX_path(strncmp(NX_class, 'NXdata',6)  | strncmp(NX_class, 'NXdetector',10));
if isempty(NXdata_path)
  if verbosity(in) > 1, disp([ mfilename ': WARNING: No NXdata/detector block found.' ]); end
  return;
end

% we search for 'signal' (should be attributes)
signal_path = findfield(in, 'signal','exact case');

% we loop on NXdata groups and extract signal and axes for each
if ~iscell(signal_path), signal_path = { signal_path }; end
for index=1:numel(signal_path)

  % the 'signal' should be an attribute, with value '1'
  if isempty(strfind(signal_path{index}, 'Attributes')), continue; end
  signal_value = subsref(in, signal_path{index});
  if ischar(signal_value), signal_value=str2num(signal_value); end
  if signal_value ~= 1, continue; end
  
  % we get the parent of the 'signal' attribute and remove the 'Attribute' from the path
  [~,signal_path{index}] = str2subs([ signal_path{index}, '/../' ]); % parent
  signal_attr            = fileattrib(in, signal_path{index});
  signal_path{index}     = strrep(signal_path{index}, '.Attributes',''); % real data
  
  this= copyobj(in);
  this.Signal=signal_path{index};
  if isfield(signal_attr, 'long_name')
    this.Title=signal_attr.long_name;
  else
    [p,f,e] = fileparts(strrep(signal_path{index},'.','/'));
    this.Title=f;
  end
  if isfield(signal_attr, 'units')
    this.Title = [ this.Title ' (' signal_attr.units ')' ];
  end
  
  % search axes at the level of the signal
  [~,signal_parent_path] = str2subs([ signal_path{index} '/../' ]); % signal parent
  axescheck(this, signal_parent_path);
  % get the axes attributes
  for ax_index=1:numel(in.Axis)
    ax=in.Axis{ax_index};
    if ischar(ax) || iscellstr(ax)
      ax_attr = fileattrib(in, ax);
      if isfield(ax_attr, 'long_name')
        this.Label{ax_index} = ax_attr.long_name;
      else
        [p,f,e] = fileparts(strrep(ax,'.','/'));
        this.Label{ax_index} = f;
      end
      if isfield(ax_attr, 'units')
        this.Label{ax_index} = [ this.Label{ax_index} ' (' ax_attr.units ')' ];
      end
    end
  end
  out = [ out this ];
end

% ------------------------------------------------------------------------------
  

% ------------------------------------------------------------------------------
function [NX_class, NX_path] = load_NeXus_class(in)
% LOAD_NEXUS_CLASS Determine the NeXus classes and their path as data sets
  NX_path  = [];
  NX_class = [ findfield(in, 'NX_class', 'exact case cache') ...
               findfield(in, 'class',    'exact case cache') ];
  if isempty(NX_class), return; end % Not a NeXus file

  % Then we remove the '.Attributes' and '.NX_class' tokens to get the true path.
  NX_path = strrep(NX_class, '.Attributes','');
  NX_path = strrep(NX_path,  '.NX_class',  ''); % path to NX entries
  NX_path = strrep(NX_path,  '.class',  '');    % path to NX entries (old style)
  NX_class= get(in, NX_class);                  % type of NX entries (NX_class)
