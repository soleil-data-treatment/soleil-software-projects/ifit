function out = private_load_hspy(in)
% PRIVATE_LOAD_HSPY format an HDF file with HypersPy structure (data, axes, ...)

% check if this is an HyperSpy as Data.Attributes.file_format = 'HyperSpy'
% Then search for 'data' and 'axis_N' in same group.

data = findfield(in,'data','exact case cache');
if ischar(data), data = { data }; end
out = [];

for index=1:numel(data)
  % ignore Attributes stuff
  if ~isempty(strfind(data{index}, 'Attributes')), continue; end
  
  % set output element
  if isempty(out), out1 = in;
  else             out1 = copyobj(in); end

  % shift dimensions of HypersPy data to put in [x, y, z, signal] order
  out1 = subsasgn(out1, data{index}, shiftdim(get(out1,data{index}), 1));
  
  % set the Signal
  out1.Signal = data{index}; % set alias to dataset
  
  % now get the axes
  % we get the 'data' root path. Then pass through axis0|1|2 and assign aliases
  % as axis_N.name, and values are build as offset + scale*index
  [~,p] = str2subs([ data{index} '/../' ]); % one level up
  hspy2idata_axis = [ 2 1 3 ];
  for index_ax=0:2
    axn = [ 'axis_' num2str(index_ax) ];  % field name
    axp = [ p '.' axn '.Attributes' ];    % path to axis Attributes
    if isfield(out1, axp)
      this = get(out1, axp);
      out1.(this.name) =      this.offset+(1:this.size)*this.scale;
      axis( out1, hspy2idata_axis(index_ax+1),   this.name);
      label(out1, hspy2idata_axis(index_ax+1), [ this.name ' ' this.units ]);
    end
  end
  % set metadata
  if isfield(out, [ p '.original_metadata.Attributes' ]), out1.parameters = get(out1, [ p '.original_metadata.Attributes' ]); end
  out1.Name       = get(out1, [ p '.metadata.General.Attributes.original_filename' ]);
  out1.Title      = label(out1, 0);
  
  out = [ out out1 ];
end
