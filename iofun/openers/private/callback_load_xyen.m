function   a=callback_load_xyen(a)
% function a=callback_load_xyen(a)
%
% Simple postprocessing for X Y E N files.
%
% Version: $Date$ $Version$ $Author$
% See also: iData/load, save, iData/saveas
% 

if ~isa(a,'iData')
  a = iData(a, 'raw'); % no post-processing
end

% handle input iData arrays
if numel(a) > 1
  for index=1:numel(a)
    a(index) = feval(mfilename, a(index));
  end
  return
end

% special case for McCode files and XYEN (2-4 columns) files
a=axescheck(a,'force signal');
n = size(a,2); % number of columns

if (ndims(a) == 2 && n >= 2 && n <= 4 && size(a,1) >= 2)
  if ~isempty(axis(a))
      xlab = label(a,1);
  else
      xlab = 'x [1st column]';
  end
  ylab = a.Title;

  Datablock = a.Signal;

  % First column is the scan parm, we denote that 'x'
  a.x=     [Datablock '(:,1)']; % xlab;
  a.Signal=[Datablock '(:,2)']; % ylab;
  label(a,0,ylab);
  if n>=3
    a.Error=[Datablock '(:,3)'];
  else
    a.Error=[];
  end
  a.E='Error';
  if ~isempty(findfield(a, 'Error','cache')) || n >= 4
    a.N=[Datablock '(:,4)']; % '# Events';
  end
  axis(a,1,'x'); 
  label(a,1,xlab);
end
