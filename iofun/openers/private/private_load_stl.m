function a=private_load_stl(a)
% function a=private_load_stl(a)
%
% Returns an iData style dataset from a STL file (ascii or binary)
% or an OFF, PLY file

% Here we get the number of vertices (xyz) 'nv' and the number of faces (polygons) 'nf'
% and we extract the corresponding 'vertices' and 'faces' arrays.

% STL provides: OK
%              faces: [292x3 double]
%           vertices: [876x3 double]
%    FaceVertexCData: [292x3 double]
%              Title: '/home/farhi/dev/ifit/data/Bunny-LowPoly2.stl VCG binary'
%            normals: [292x3 double]
%
% PLY provides: OK
%       faces: [1696x3 double]
%    vertices: [836x3 double]
%       elmts: [1x1 struct] -> .vertex.(x|y|z)=vectors ; .face.vertex_indices={cell} to be cleared -> aliases in iData ?
%    comments: {'VCGLIB generated '}
% 
% OFF provides: Not OK -> get numeric fields in order of appearance
%    [m,t,nel]=findfield(a, 'Data.Data'); in order OK, loop on t='double|single|uint|int' then get 1st then 2nd
%        find(strcmp( 'double', t) | strcmp( 'single', t) ...)
%        MetaData: [1x1 struct]
%          socket: [837x3 double]   -> vertices
%        socket_1: [1696x4 double]  -> faces (x3 here)
%
% Matlab: patch('Vertices',vertex_matrix,'Faces',faces_matrix)
% Matlab: [tri,vol]=convhull(X,Y,Z)
%
% Then create an iData with properties:
%   vertices
%   faces
%   x,y,z -> vertices(:,n) and set axes
%   signal=size(vertices)*numel(faces)

vertices = []; faces = [];

% get data from OFF importer. The others are OK.
if ~isempty(strfind(a.Loader.description, 'OFF'))
  [fields,types,nelemnts]=findfield(a, 'Data.Data');
  index = find(strcmp( 'double', types) | strcmp( 'single', types) ...
             | strcmp( 'uint',   types) | strcmp( 'int',    types));
  fields=fields(index);
  % then we get fields in order. 1st is vertices, 2nd is faces
  if numel(fields) < 2 && verbosity(a) > 1
    warning([ mfilename ': WARNING: The loaded data set ' a.Tag ' from ' a.Source ' is not an OFF data format (empty vertices/faces).' ]); 
    return
  end
  a.Data.vertices=fields{1};
  a.Data.faces   =fields{2};
else
  % make sure we have vertices and faces
  for fv={'vertices','faces'}
    if ~isfield(a, [ 'Data.' fv{1} ])
      f=findfield(a,fv{1});
      if ~isempty(f), a.Data.(fv{1})=f{1}; 
      else
        warning([ mfilename ': WARNING: The loaded data set ' a.Tag ' from ' a.Source ' is not a STL/SLP/PLY data format (empty vertices/faces).' ]); 
        return
      end
    end
  end
end

% format output
a.X='Data.vertices(:,1)';
a.Y='Data.vertices(:,2)';
a.Z='Data.vertices(:,3)';
a.Axis{1}='X'; 
a.Axis{2}='Y'; 
a.Axis{3}='Z'; 
set(a, 'Signal', ones(size( axis(a,1) )));

