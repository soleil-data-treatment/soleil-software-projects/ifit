function out = opennii(filename)
%OPENNII Open a NifTi-1 3D MRI/tomography data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','zstat1.nii'); a=opennii(f); all(size(a) == [64 64 21])

out = ifitopen(filename, @read_nii);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
