function out = openspe(filename)
%OPENSPE Open an ISIS/SPE tof data file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','008545.spe'); a=openspe(f); all(size(a)==1024) && isstruct(a.Data.header)
% Example: f=fullfile(ifitpath,'data','Example.spe'); a=openspe(f); a.Signal='Data.Data.w'; all(size(a)==[30 195])

if ~isa(filename,'iData')
  out = ifitopen(filename, @read_spe,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

