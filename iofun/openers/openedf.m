function out = openedf(filename)
%OPENEDF Open an EDF ESRF Data Format file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','Ag_3_a.edf'); a=openedf(f); strcmp(a.Signal,'Data.Signal') && all(size(a)==[55 71])


if ~isa(filename,'iData')
  out = ifitopen(filename, @read_edf);
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

