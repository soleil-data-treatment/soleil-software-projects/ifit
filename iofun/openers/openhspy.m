function out = openhspy(filename)
%OPENHSPY Open an HypersPy file, display it (derived from HDF5)
%        and set the 'ans' variable to an iData object with its content
%
% Example: f=fullfile(ifitpath,'data','stem_abf_data_imagestack.hspy'); a=openhspy(f); strcmp(a.Name,'004_ABF_movie_21.dm3') && all(size(a)==[512 5 512])

out = openhdf(filename, @read_hdf5); % hspy is an hdf5 file

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
