function out = opennxs(filename)
%OPENNXS Open a NeXus/HDF file, display it
%        and set the 'ans' variable to an iData object with its content
% 
%  Example: f=fullfile(ifitpath,'data','IRS21360_graphite002_ipg.nxs'); a=opennxs(f); all(size(a) == [1903 51])

out = openhdf(filename);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
