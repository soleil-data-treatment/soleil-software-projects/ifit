function out = opennpy(filename)
%OPENEDF Open an python Numpy NPY Format file, display it
%        and set the 'ans' variable to an iData object with its content
%
% To generate a NPY file, use python:
% >>> import numpy as np
% >>> x = np.arange(10)
% >>> np.save( 'outfile.npy', x)
% >>> np.savez('outfile.npz', x)
%
% Example: f=fullfile(ifitpath,'data','vkitti3d_0001_00000.npy'); a=opennpy(f); all(size(a) == [401326 7])


if ~isa(filename,'iData')
  out = ifitopen(filename, @read_npy,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
else
  out = axescheck(out);
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

