Processed data readers
======================

This directory contains functions that call raw loaders and may further apply 
'callbacks' so to return iData objects. The basic loaders sit in the root iofun
directory, and are scanned by ifitpref at first call.

All functions in here return iData objects.
