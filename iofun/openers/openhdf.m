function out = openhdf(filename, format)
%OPENHDF Open an HDF file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','nano_50K_4.8a_03_h4.hdf'); a=openhdf(f); all(size(a)==[268 302])

if nargin < 2
  format = 'HDF';
end

if ischar(format)
  switch (format)
  case 'HDF5'
    format=@read_hdf5;
  case 'HDF4'
    format=@read_hdf4;
  end
end

if ~isa(filename,'iData')
  out = ifitopen(filename,format,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  in = out;
  out = []; % the number of elements may change, can not simply replace
  for index=1:numel(in)
    out = [ out ; feval(mfilename, in(index)) ];
    in(index) = iData; % free memory
  end
  return
end

if ~isempty(findstr(out, 'NeXus')) || ~isempty(findfield(out, {'NX_class','class'},'cache'))
  % special stuff for NeXus files
  out1 = private_load_NeXus(out); % see private

  if isa(out1, 'iData')
    out = out1;
  end
  
end % if Nexus

% look for hyperspy
in = out;
out = [];
for index=1:numel(in)
  if numel(in) == 1, this = in; else this=in(index); end
  if ~isempty(findstr(this, 'HyperSpy'))
    out1 = private_load_hspy(this); % see private
  else out1 = []; end
  if isempty(out1), out1 = this; end
  out = [ out this ];
end
  
if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end

% ------------------------------------------------------------------------------

