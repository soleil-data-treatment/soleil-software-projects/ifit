function out = opennrrd(filename)
%OPENNRRD  Open a NRRD Nearly Raw Raster Data, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','fool.nrrd'); a=opennrrd(f); all(size(a) == 128)

out = ifitopen(filename, @read_nrrd);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
