function out = openimg(filename)
%OPENIMG Open an ADSC X-ray detector image or Analyze volume (medical imaging),
%         display it and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','Q1_Test_Protein.img'); a=openimg(f); isfield(a.Data,'header') && all(size(a)==[1152 1152])

out = ifitopen(filename, 'img');

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
