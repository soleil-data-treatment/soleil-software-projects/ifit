function out = openmap(filename)
%OPENMCCD  Open a EZD/MRC/CCP4 electronic density map, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','emd_0409.map'); a=openmap(f);

out = ifitopen(filename, @read_mrc);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
