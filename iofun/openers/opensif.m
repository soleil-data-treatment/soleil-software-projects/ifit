function out = opensif(filename)
%OPENSIF Open a Andor SIF CCD Camera, display it
%        and set the 'ans' variable to an iData object with its content
% 
%  Example: f=fullfile(ifitpath,'data','example.sif'); a=opensif(f); all(size(a)==[1024 1])

out = ifitopen(filename, @read_sif);
out = axescheck(out);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
