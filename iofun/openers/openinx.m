function out = openinx(filename)
%OPENINX Open an ILL INX tof data file, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','YIG_4A_corr_vana_spectre.inx'); a=openinx(f); all(size(a)==[384 1]) && strncmp(a.Title,'SJ,JO',5)

if ~isa(filename,'iData')
  out = ifitopen(filename, @read_inx,'raw');
else
  out = filename;
end
clear filename;

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
elseif isfield(out.Data, 'header') && isfield(out.Data, 'Par') && isfield(out.Data, 'Mat')
  % the data read with read_inx comes as:
  % s.Data.header: char
  % s.Data.Par:    double with parameters after the header/comment line
  %   Angle, incident energy, transfered wave-vector (Q), mean temperature, -, -, -, channel width (musec), -
  Data = out.Data;
  Data.angle              = Data.Par(:,1);
  Data.IncidentEnergy     = Data.Par(:,2);
  Data.IncidentWavevector = Data.Par(:,3);
  Data.temperature        = Data.Par(:,4);
  Data.signal      = squeeze(Data.Mat(:,2,:));
  Data.error       = squeeze(Data.Mat(:,3,:));
  Data.wavelength  = sqrt(81.805./Data.IncidentEnergy);
  % a few checks for consistency
  if abs(2*pi/mean(Data.wavelength) - Data.IncidentWavevector) > 0.01*Data.IncidentWavevector
    warning([ mfilename ': WARNING: The loaded data set ' out.Tag ' from ' out.Source ' has an inconsistent Energy:' ...
    sprintf('\n  Incident Energy=%g [meV] Wavevector=%g [Angs-1] Wavelength=%g [Angs]', ...
      mean(Data.IncidentEnergy), mean(Data.IncidentWavevector), mean(Data.wavelength)) ]);
  end
  if ~Data.angle  % this is an Angle data set, not Energy
    isAngleData = 1;
    Data.angle     = squeeze(Data.Mat(:,1,:));
    Data.angle     = Data.angle(:,1);
    Data.energy    = 81.805/Data.wavelength^2;
  else
    isAngleData = 0;
    Data.energy      = squeeze(Data.Mat(:,1,:));
    Data.energy      = Data.energy(:,1);
  end
  if numel(unique(Data.temperature)) == 1, Data.temperature=Data.temperature(1); end
  if numel(unique(Data.wavelength))  == 1, Data.wavelength =Data.wavelength(1); end
  if numel(unique(Data.IncidentWavevector))  == 1, Data.IncidentWavevector =Data.IncidentWavevector(1); end
  if numel(unique(Data.IncidentEnergy))  == 1,     Data.IncidentEnergy     =Data.IncidentEnergy(1); end
  out.Data = Data; 

  out.Signal='Data.signal';
  label(out, 0, out.Data.header(2,:,1));
  out.Error='Data.error';
  if isAngleData
    out.Angle='Data.angle';
  else
    out.Energy='Data.energy';
    out.Angle='Data.angle';                       % 'Angle [deg]';
  end
  out.IncidentEnergy=Data.IncidentEnergy;         % 'Incident Energy [meV]';
  out.Wavelength=Data.wavelength;                 % 'Incident Wavelength [Angs]';
  out.IncidentWavevector=Data.IncidentWavevector; % 'Incident Wavevector [Angs-1]'
  out.Temperature=Data.temperature;               % 'Sample Temperature [K]';

  if ndims(out) == 1
    if isAngleData
      axis(out, 1, 'Angle');
      label(out, 1, [ 'Angle [deg] T=' num2str(mean(out.Data.temperature)) ' lambda=' num2str(mean(out.Data.wavelength)) ]);
    else
      axis(out, 1, 'Energy');
      label(out,1, [ 'Energy [meV] T=' num2str(mean(out.Data.temperature)) ' lambda=' num2str(mean(out.Data.wavelength)) ]);
    end
  elseif ndims(out) == 2
    axis(out, 1, 'Energy');
    axis(out, 2, 'Angle');
  end
  out.Name   =[ out.Data.header(2,:,1) ' ' out.Name ];
  
  % update object fields cache
  findfield(out, '','fieldnames');
else
  warning([ mfilename ': The loaded data set ' out.Tag ' from ' out.Source ' is not an ILL INX data format.' ]);
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
