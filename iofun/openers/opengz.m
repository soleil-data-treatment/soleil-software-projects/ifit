function out = opengz(filename)
%OPENGZ Open a GZIP compressed data set, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','D2O_liq_290_inc.sqw.gz'); a=opengz(f); strcmp(a.Title,'Sqw (inc)') && all(size(a)==[2500 400])

out = ifitopen(filename, @read_compressed);

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
