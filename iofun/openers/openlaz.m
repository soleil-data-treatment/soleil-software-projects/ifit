function out=openlaz(a, format)
%OPENLAZ Open a McStas Lazy HKL Powder file
%        display it and set the 'ans' variable to an iData object with its content
%        such files can be obtained from Crystallographica and ICSD <icsd.ill.fr>
% 
% Example: f=fullfile(ifitpath,'data','C_diamond.laz'); a=openlaz(f);  all(size(a)==[16 1]) && strcmp(a.Signal,'FHKL')

if nargin < 2
  format = 'LAZ';
end

if ~isa(a,'iData')
  out = ifitopen(a,format,'raw');
else
  out = a;
end

if numel(out) > 1
  % handle input iData arrays
  for index=1:numel(out)
    out(index) = feval(mfilename, out(index));
  end
  
elseif ~isempty(findstr(out,'Lazy')) || ~isempty(findstr(out,'Crystallographica'))
  % set HKL axes and Intensity signal. No Error.
  data_definition = findfield(out,'','biggest numeric cache');
  columns_header  = fileattrib(out,data_definition);
  if isfield(out, 'Data.Attributes.MetaData.VALUE')
    this = out.Data.Attributes.MetaData.VALUE;
  elseif isfield(out, 'Data.Attributes.MetaData.F_squared')
    this = out.Data.Attributes.MetaData.F_squared;
  else
    this = findstr(out,'VALUE');
  end
  if ~isempty(this), 
    if iscell(this), this=this{1}; end
  else this = []; end
  
  % the header line may be split as it contains numerics. Prepend Attributes.VALUE.
  columns_header = [ this ' ' columns_header ];
  % the Lazy format has a column named 'D VALUE': remove the space so that columns are not shifted
  columns_header = strrep(columns_header, 'D VALUE','D_VALUE');
  columns = textscan(columns_header,'%s','delimiter',' ;#','MultipleDelimsAsOne',true);
  columns = columns(~cellfun('isempty', columns));
  if iscell(columns) && numel(columns)==1, columns=columns{1}; end
  for index=1:length(columns)
    % clean the column name so that it looks like a variable name
    columns{index} = strrep(columns{index}, '.','');
    columns{index} = strrep(columns{index}, '-','');
    columns{index} = strrep(columns{index}, '/','');
    columns{index} = strrep(columns{index}, '*','');
    columns{index} = strrep(columns{index}, '(','');
    columns{index} = strrep(columns{index}, ')','');
    columns{index} = genvarname(columns{index});
    if ~isfield(out, columns{index})
      out.(columns{index})  = [ data_definition '(:,' num2str(index) ')' ]; 
      this_axis = find(strcmpi(columns{index},{'h','k','l'}),1);
      if ~isempty(this_axis)
        this_axis = this_axis(1);
        out.Axis{this_axis}  = columns{index}; 
        out.Label{this_axis} = columns{index};
      end
      this_axis=[];
      if   ~isempty(strfind(columns{index}, 'FHKL')) ...
        || ~isempty(strfind(columns{index}, 'Fsquared'))
        out.Signal=columns{index};
        out.Title =columns{index};
      end
    end
  end

  out.Error=0;
  % out = transpose(out);
else
  warning([ mfilename ': The loaded data set ' out.Tag ' from ' out.Source ' is not an McCode LAZ/LAU data format.' ]);
end

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
