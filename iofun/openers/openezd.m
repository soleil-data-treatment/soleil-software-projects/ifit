function out = openezd(filename)
%OPENEZD Open an EZD electronic density map File, display it
%        and set the 'ans' variable to an iData object with its content
% 
% Example: f=fullfile(ifitpath,'data','GroEl.ezd'); a=openezd(f); strcmp(a.Signal,'Data.Data.MAP') && all(size(a)==[96 96 96])

if ~isa(filename,'iData')
  out = ifitopen(filename, @read_ezd);
end

fieldMAP    = findfield(out, 'Data.MAP','cache case first numeric');
fieldEXTENT = findfield(out, 'Data.EXTENT','cache case first numeric');
set(out, fieldMAP, reshape(get(out,fieldMAP), get(out,fieldEXTENT)));

if ~nargout
  figure; subplot(out);
  
  if ~isdeployed
    assignin('base','ans',out);
    ans = out
  end
end
