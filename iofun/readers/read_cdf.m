function s = read_cdf(filename)
% read_cdf Read a CDF file
% s = read_cdf(filename)
%
% http://en.wikipedia.org/wiki/Common_Data_Format
%
% Example: f=fullfile(ifitpath,'data','ulysses.cdf'); a=read_cdf(f); isstruct(a)
% See also: read_nc, read_hdf4, read_hdf5

s=[];

if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    CDF.description     ='Common Data Format CDF (.cdf)';
    CDF.ext             ='cdf';
    CDF.read            =mfilename;
    CDF.callback        ={'opencdf','callback_load_nmoldyn'};
    CDF.isbinary        =true;
    s = CDF;
    return
end

% turn OFF file validation to improve performance
cdflib.setValidate('VALIDATEFILEoff');

% gather fields and avoid creating Date objects
[data, info] = cdfread(filename, ...
  'CombineRecords',true, 'ConvertEpochToDatenum',true);

if iscell(data)
  % data is a cell array
  % info.Variables contains the field names
  
  % setup CDF structure and get Global Attributes
  s.Name       = '/';
  s.Filename   = filename;
  s.Group      = [];
  s.Format     = info.Format;
  s.Attributes = info.GlobalAttributes;

  % reconstruct all fields in there
  for index=1:length(data)
    this_field=info.Variables{index};
    if strncmp(this_field,'Data_',5)
      this_field = this_field(6:end);
    end
    this_field               = sanitize_name(this_field);
    s.Variables.(this_field) = data{index};

    % are there related attributes ?
    attribute = info.VariableAttributes;
    this_attr = [];
    if isstruct(attribute)
      for f=fieldnames(attribute)'
        ff = attribute.(f{1});
        ii = find(strcmp(info.Variables{index}, ff(:,1)));
        if ~isempty(ii)
          this_attr.(f{1}) = ff(ii,:);
        end
      end
    end

    if ~isempty(this_attr)
      % use location [ base group 'Attributes.' lastword ]
      s.Attributes.(this_field) = this_attr;
    end
    
  end

else
  s = data;
end

% ------------------------------------------------------------------------------
function name = sanitize_name(name)
  name(~isstrprop(name,'print')) = '';
  name(~isstrprop(name,'alphanum')) = '_';
  if isempty(name), return; end
  if name(1) == '_'
    name = name(find(name ~= '_', 1):end);
  end
  if isstrprop(name(1),'digit')
    name = [ 'x' name ];
  end
