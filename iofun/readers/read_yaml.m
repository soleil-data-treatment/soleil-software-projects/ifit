function s = read_yaml(filename)
% data=read_yaml(filename)
%
% read_yaml Wrapper to directly read YAML files using the YAML class
%
% Input:  filename: yaml/json file (string)
% output: structure
% Example: y=read_yaml(fullfile(ifitpath, 'data','iFit_iD80908.yaml')); isstruct(y)
%
% 
% See also: read_json, read_xml

s=[];

if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    s.description    = 'Yet Another Markup Language (YAML)';
    s.patterns       ='';
    s.read           = mfilename;
    s.ext            = {'yaml','yml'};
    s.istext         = true;
    return
end

s       = YAML.read(filename);

end

