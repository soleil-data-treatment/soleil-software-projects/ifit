function ace = read_ace(filename)
% data = read_ace(filename) Read MCNP/ACE file
%   Import MCNP/ACE files using PyNE/ACE from <http://pyne.io/>
%
% Get ACE files from: https://www.cab.cnea.gov.ar/nyr/tsl.html
%
% Example: f=fullfile(ifitpath,'data','od220.ace'); a=read_ace(f); isstruct(a)

% for MF7 MT4 from ACE
% lib = pyne.ace.Library('hwtr-293.ace' )
% lib.read()
% scipy.io.savemat('ace.mat',lib.tables)
% matlab> ace=load('ace.mat')
%
% The PyNE/ACE library is included in the ifit/iofun/readers/private directory.

ace = [];

if ~isempty(dir(fullfile(ifitpath,'iofun','readers','private','ace.py')))
  status = true;
else
  status = false;
end

if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
  if status
    ace_ascii.description     = 'ACE (A compact ENDF) nuclear data (text)';
    ace_ascii.read            = mfilename;
    ace_ascii.ext             = {'ace'};
    ace_ascii.callback        = 'openendf';
    ace_accii.patterns        = {'mat '};
    ace_ascii.istext          = true;
    
    ace_binary.description     = 'ACE (A compact ENDF) nuclear data (binary)';
    ace_binary.read            = mfilename;
    ace_binary.ext             = {'ace'};
    ace_binary.callback        = 'openendf';
    ace_binary.isbinary        = true;
    
    ace = { ace_ascii, ace_binary };
  end
  return
end

% use PyNE/ACE
if status
  target   = [ tempname '.mat' ];
  filename = strrep(filename, '~',getenv('HOME'));
  filename = java.io.File(filename);
  filename = char(filename.getCanonicalPath()); % fully qualified name

  script = { 'python:', ...
      'import sys', ...
    [ 'sys.path.append("' fullfile(ifitpath,'iofun','readers','private') '")' ], ...
      'from ace import ace2mat', ...
    [ 'ace2mat("' filename '","' target '")' ] };
    
  T= evals(script);

  if T.passed && ~T.failed
    ace = load(target);
    ace = read_endf_pyne2endf(ace);
    ace.script = script;
    ace.output = T;
  else
    disp(T.output)
    error([ mfilename ': ERROR reading ' filename ]);
  end
else
  error([ mfilename ': pyne/ace.py is not available.' ])
end

