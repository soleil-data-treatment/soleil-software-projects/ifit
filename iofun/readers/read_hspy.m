function s = read_hspy(filename)
% READ_HSPY Read an HypersPy HDF5 data set.
%   See https://hyperspy.org/
%
% Example: f=fullfile(ifitpath,'data','stem_abf_data_imagestack.hspy'); a=read_hspy(f); isstruct(a)
% See also: read_hdf4, read_nc, read_cdf, read_hdf5

% We use the standard hdf5 reader, and attach a callback to find the data sets.
% check if this is an HyperSpy as Data.Attributes.file_format = 'HyperSpy'
% Then search for 'data' and 'axis_N' in same group.

  s = [];

  if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    hspy.description  = 'HypersPy HDF5 data format';
    hspy.read         = 'read_hdf5';
    hspy.callback     = 'openhdf';
    hspy.ext          ={'hspy'};
    hspy.isbinary     = true;
    s = hspy;
    return
  end

  s = read_hdf5(filename);
  
