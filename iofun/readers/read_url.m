function [filename,result] = read_url(filename, option)
% READ_URL Reads a distant file and returns a local file for further processing.
%   filename=READ_URL(url) get a distant resource, and return a local copy path.
%   Temporary files are NOT removed afterwards. Think about removing them
%   after further processing. The URL supports file:// ftp:// http:// and https://
%
%   [filename,data]=READ_URL(url) same as above, and return the URL content.
%
%   [filename,data]=READ_URL(url, tmpdir) extract compressed data from given file, 
%   in given temporary directory. It is recommended to use a RAMdisk for higher
%   efficiency, such as '/dev/shm' or '/run/shm'.
%
%   This loader may require proxy settings to be set (when behind a firewall)
%   ProxyHost='my.proxy.com'; % Proxy address if you are behind a proxy [e.g. myproxy.mycompany.com or empty]
%   ProxyPort=8080;           % Proxy port if you are behind a proxy [8888 or 0 or empty]
%   java.lang.System.setProperty('http.proxyHost', ProxyHost); 
%   com.mathworks.mlwidgets.html.HTMLPrefs.setUseProxy(true);
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyHost(ProxyHost);
%   java.lang.System.setProperty('http.proxyPort', num2str(ProxyPort));
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyPort(num2str(ProxyPort));
%
% Example: y=read_url('http://www.jcamp-dx.org/lisms/cc/bruker1.dx'); delete(y); ischar(y)
% See also: read_compressed, iData

result='';
if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    s.description ='URL (file:// ftp:// http:// and https://)';
    s.read        =mfilename;
    s.ext         ={'url'};
    filename = s;
    return
end

if nargin < 2, option = ''; end

if isempty(option), option= tempdir; end
if ischar(option) && ~isempty(dir(option)) && ~isdir(option)
  option = fullparts(option); % get path
end

% handle file on the internet
if strncmp(filename, 'http://', length('http://')) ...
|| strncmp(filename, 'https://',length('https://')) ...
|| strncmp(filename, 'ftp://',  length('ftp://'))
  tmpfile = tempname(option);
  % Keep file extension, may be useful for iData load
  [filepath,name,ext] = fileparts(filename);
  tmpfile = [tmpfile ext];
  use_wget = false;
  if ~usejava('jvm')
    use_wget = true;
  else
    % access the net. Proxy settings must be set (if any).
    try
      % write to temporary file
      [result,status] = urldownload(filename, tmpfile, 'TimeOut', 5);
      if status
        filename = tmpfile;
      else
        error([ mfilename ': Can not get URL ' filename ' (urlread time-out).' ]);
      end
    catch ME
      disp(ME.message);
      use_wget = true;
    end
  end
  if use_wget
    % Fall back to using wget
    cmd = ['wget -T 5 "' filename '" -O ' tmpfile]; disp(cmd)
    [status, result] = system(cmd);
    if status
      disp(result);
      error([ mfilename ': Can not get URL ' filename ' (wget time-out).' ]);
    elseif nargout > 1
      result = fileread(tmpfile);
    end
  end
  filename = tmpfile;
end

if strncmp(filename, 'file://', length('file://'))
  filename = filename(7:end); % remove 'file://' from local name
end
