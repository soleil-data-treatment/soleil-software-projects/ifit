function s = read_ill(filename, varargin)
% data=read_ill(filename, options, ...) Read ILL data
%
% read_ill read Institut Laue-Langevin text file format
%
% Input:  filename: ILL Data text file (string)
% output: structure
% Example: y=read_ill(fullfile(ifitpath, 'data','ILL_IN6.dat')); isstruct(y)
%
% 
% See also: read_llb_tas, read_anytext, read_spec

s=[];

if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    ILL_normal.description       ='ILL Data (normal integers)';
    ILL_normal.patterns   ={'RRRR','AAAA','FFFF','IIII'};
    ILL_normal.options    ='--headers --fortran --catenate --fast --binary --makerows=IIII --makerows=FFFF --silent ';
    ILL_normal.read       =mfilename;
    ILL_normal.istext     = true;
    
    ILL_integers.description       ='ILL Data (large integers)';
    ILL_integers.patterns   ={'RRRR','AAAA','FFFF','JJJJ'};
    ILL_integers.options    ='--headers --fortran --catenate --fast --binary --makerows=JJJJ --makerows=FFFF --silent ';
    ILL_integers.read       =mfilename;
    ILL_integers.istext     = true;
    
    ILL_float.description          ='ILL Data (floats only)';
    ILL_float.patterns      ={'RRRR','AAAA','FFFF'};
    ILL_float.options       ='--headers --fortran --catenate --fast --binary --makerows=FFFF --silent ';
    ILL_float.read          =mfilename;
    ILL_float.istext        = true;
    
    ILL_general.description       ='ILL Data (general)';
    ILL_general.patterns   ={'RRRR','AAAA','SSSS'};
    ILL_general.options    ='--headers --fortran --catenate --fast --binary --makerows=FFFF --makerows=JJJJ --makerows=IIII --silent ';
    ILL_general.read       =mfilename;
    ILL_general.istext     = true;
    
    ILL_TAS_pol.description       ='ILL TAS Data (polarized)';
    ILL_TAS_pol.patterns   ={'PAL','POSQE:','PARAM:','DATA_:','USER_:'};
    ILL_TAS_pol.options    =['--fast --binary --headers --silent --fortran=0 --catenate ' ...
                        '--section=PARAM --section=VARIA --section=ZEROS --section=DATA ' ...
                        '--section=POLAN --section=STEPS ' ...
                        '--metadata=LOCAL --metadata=USER --metadata=EXPNO --metadata=DATE ' ...
                        '--metadata=INSTR --metadata=COMND --metadata=TITLE --metadata=MULTI --metadata=PNT'];
    ILL_TAS_pol.read      =mfilename;
    ILL_TAS_pol.callback  ='callback_load_ill_tas'; % load_ill_tas
    ILL_TAS_pol.istext    = true;
    
    ILL_TAS.description       ='ILL TAS Data';
    ILL_TAS.patterns   ={'POSQE:','PARAM:','DATA_:','USER_:'};
    ILL_TAS.options    =['--fast --binary --headers --silent --fortran=0 --catenate ' ...
                        '--section=PARAM --section=VARIA --section=ZEROS --section=DATA ' ...
                        '--section=STEPS ' ...
                        '--metadata=LOCAL --metadata=USER --metadata=EXPNO --metadata=DATE --metadata=DATA ' ...
                        '--metadata=INSTR --metadata=COMND --metadata=TITLE --metadata=MULTI --metadata=PNT '];
    ILL_TAS.read      =mfilename;
    ILL_TAS.callback  ='callback_load_ill_tas'; % load_ill_tas
    ILL_TAS.istext    = true;
    
    s = { ILL_normal, ILL_integers, ILL_float, ILL_general, ILL_TAS_pol, ILL_TAS };
    return
end

% now call read_anytext with given options

if isempty(varargin)
  varargin = { '--headers --fortran --catenate --fast --binary --makerows=FFFF --makerows=JJJJ --makerows=IIII --silent ' };
end
s       = read_anytext(filename, varargin{:});

end

