RAW data readers
================

This directory contains simple functions that read a file and return raw data,
usually as a struct. Such readers are scanned by `ifitpref` to list all available
data readers upon first call.

The iData, iData.load, openXXX and ifitopen functions all call these raw readers
and may further apply 'callbacks' so to return iData objects.

Readers definition
------------------

When given a unique 'identify' argument, the reader should return a structure or 
a cell of structures:

 field      | content
------------|----------------------------------------------------------------------
 description| description of the data format
        read| function name/handle to read the file; data=method(file,...)
       write|function name/handle to write the file; file=method(data,file,...)
         ext| a single or a cellstr of extensions associated with the format
      istext| optional priority (int), e.g. for text files
    isbinary| optional indicate that file is 'binary' 
     options| optional list of arguments to pass to the method. If given as a string, they are catenated with file name. If given as a cell, sent to the method as additional arguments
    patterns| optional (only for readers) cell of tokens to search in e.g. text headers. Method is qualified when all are found. The patterns can be regular expressions. When given as a string, the file is assumed to contain "text" data.  A specification begining with `!` excludes the format (should not match).
    callback| optional (only for readers) callback function(s) to remaster the data after importation 

This specification is inspired from the `imformats` documentation.

