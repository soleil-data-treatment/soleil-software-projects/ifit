function [data, readers, filenames] = ifitread(filename, reader, varargin)
% IFITREAD read filenames (char/cellstr)
%   DATA = IFITREAD(FILE) imports given FILE. FILE can be a single file or directory
%   a cell of files or directories. These may make use of wildcards (*,?).
%   When FILENAME is 'gui' a file selector is displayed (allowing multi-select).
%   Distant URL's are also supported, as well as compressed files.
%
%   [DATA,READERS] = IFITREAD(FILE, READER, ...) specifies the format loader to use. 
%   The READER can be a function name, a function handle, a file extension, 
%   'auto' or '' for a guess, 'gui' for a format selector, or a structure:
%             READER.read   = 'function name'
%             READER.options= string of options to catenate after file name
%                          OR cell of options to use as additional arguments
%                             to the method
%   additional arguments are passed to the import routine. The returned READERS
%   contains the actual list of used readers.
%   To get a full list of supported READER, use: ifitpref formats isreader
%
%   READERS = IFITREAD(FILE, 'readers') returns the readers that can read the file.
%
%   Default supported formats include: any text based including CSV, Lotus1-2-3, SUN sound, 
%     WAV sound, AVI movie, NetCDF, FITS, XLS, BMP GIF JPEG TIFF PNG ICO images,
%     HDF4, HDF5, MAT workspace, XML, CDF, JSON, YAML, IDL, Python Numpy
%   Other specialized formats include: McStas, ILL, SPEC, ISIS/SPE, INX, EDF, Mantid.
%     SIF, MCCD/TIFF, ADSC, CBF, Analyze, NifTI, STL,PLY,OFF, CIF/CFL,
%     EZD/CCP4, Bruker Varian and JEOL NMR formats, Bruker OPUS, LabView LVM and TDMS,
%     Agilent and Thermo Finnigan MS, Quantum Design VMS, ENDF/ACE, Agilent SDF
%   Compressed files are also supported, with on-the-fly extraction (zip, gz, tar, Z).
%
%   Distant files are supported through e.g. URLs such as 
%     file://, ftp:// http:// and https://
%
%   This may require proxy settings to be set (when behind a firewall)
%   ProxyHost='my.proxy.com'; % Proxy address if you are behind a proxy [e.g. myproxy.mycompany.com or empty]
%   ProxyPort=8080;           % Proxy port if you are behind a proxy [8888 or 0 or empty]
%   java.lang.System.setProperty('http.proxyHost', ProxyHost); 
%   com.mathworks.mlwidgets.html.HTMLPrefs.setUseProxy(true);
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyHost(ProxyHost);
%   java.lang.System.setProperty('http.proxyPort', num2str(ProxyPort));
%   com.mathworks.mlwidgets.html.HTMLPrefs.setProxyPort(num2str(ProxyPort));
%
%   The configuration of the data formats is controled via 'ifitpref'.
%
%   Typical usage:
%     IFITREAD('file'); 
%     IFITREAD('file','loader'); 
%     IFITREAD({'file1','file2',...}); 
%     IFITREAD('file.zip')
%     IFITREAD('http://path/name'); 
%
% IFITREAD supports parallel execution, e.g.: matlabpool; IFITREAD(filenames)
%
% Example: a=ifitread(fullfile(ifitpath, 'data')); numel(a) > 1
% Version: $Date$ $Version$ $Author$
% See also iData, set, get, findfield, importdata, load, ifitpref, ifitopen

  data = {}; readers = {}; filenames = [];
  if nargin < 1,    filename=''; end
  if nargin < 2,      reader=''; end
  if isempty(reader), reader='auto'; end
  if strcmp(filename,'identify') return; end
  
  % check for load options
  removeme = []; callback0 = {}; flag_readers_only = false;
  if strcmp(reader,'readers')
    reader   = 'auto';
    flag_readers_only = true;
  end
  for index=1:numel(varargin)
    if ischar(varargin{index})
      switch varargin{index}
      case {'loaders','loader','readers','reader'}
        flag_readers_only = true;
      end
    end
  end
  varargin(removeme) = [];

  % check filename. May return more than one.
  filenames = cellstr(get_filenames(filename));
  
  data    = cell(size(filenames));
  readers = data; % {}
  parfor i=1:numel(filenames)
  
    % import each file
    [ data{i}, readers{i} ] = ifitread_single(filenames{i}, reader, flag_readers_only, varargin{:});

  end % indexf filenames
  
  if numel(data) == 1
    data    = data{1};
    if ~isnumeric(data) && numel(data) > 1
      readers = repmat(readers, size(data));
    else
      readers = readers{1};
    end
  end
  
  if flag_readers_only
    data = readers;
  end

end % ifitread

% ------------------------------------------------------------------------------
function [data, reader] = ifitread_single(filename, reader, flag_readers_only, varargin)

  % check reader. May return more than one.
  [this_readers, isbinary, conf] = get_formats(reader, filename); % readers
  
  if flag_readers_only
    reader = this_readers;
    data   = reader;
    return
  end

  % scan all possible readers  until one works
  for index=1:numel(this_readers)
    if   iscell(this_readers), 
         this_reader=this_readers{index};
    else this_reader=this_readers(index);
    end
  
    % avoid calling text importer with binary
    if isbinary && ...
    (this_reader.istext || any(strcmp(this_reader.read, {'text','read_anytext','looktxt'})))
      continue; end
    % prepare arguments for reader method
    varg = get_format_args(this_reader, 'read', filename, varargin{:});
    
    % IMPORT HERE ============================================================
    try
      if isfield(conf, 'verbosity') && conf.verbosity > 1
        disp([ mfilename ': trying ' this_reader.read '(''' filename ''') ' this_reader.description ]);
      end
      t0 = clock;
      this_data = feval(this_reader.read, varg{:});
      % succeeds
      this_reader.duration   = etime(clock, t0);
      this_reader.importdate = datestr(now);
      this_reader.file       = dir(filename);
      this_reader.filename   = filename;
    catch ME
      if isfield(conf, 'verbosity') && conf.verbosity > 1
        disp(getReport(ME));
        disp([ mfilename ': FAILED ' this_reader.read '(''' filename ''') ' this_reader.description ]);
      end
      this_data   = []; % fails
      this_reader = [];
    end
    
    if ~isempty(this_data)
      data    = this_data;
      reader  = this_reader;
      % special case for compressed/containerized data
      if strcmp(reader.read,'read_compressed') && isfield(data,'Loader')
        reader0         = reader;
        reader          = data.Loader;
        reader.filename = filename;
        reader.duration = reader.duration + reader0.duration;
        reader.read     = { reader0.read reader.read };
        reader.file     = reader0.file;
      end
      return % succeeds
    end
    
  end % index reader

end % ifitread_single
