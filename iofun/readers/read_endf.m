function endf = read_endf(filename, option)
% data = read_endf(filename) import ENDF files
%
%   import ENDF files, either using PyNE or slower/partial Matlab reader.
%
% Useful tokens for neutron scattering
% MF1/MT451 as 'info':
%   ZA    Standard material charge
%   AWR   Standard material mass 
%   AWI   Mass of the projectile in neutron units
%   EMAX  Upper limit of energy range for evaluation.
%   TEMP  Target temperature (Kelvin) for Doppler broadening.
%
% MF7/MT4 as 'thermal_inelastic':
%   LAT   Flag indicating which temperature has been used to compute a and b
%           LAT=0, the actual temperature has been used.
%           LAT=1, the constant T0 = 0.0253 eV has been used (293 K).
%   LASYM Flag indicating whether an asymmetric S(a,b) is given
%           LASYM=0, S is symmetric.
%           LASYM=1, S is asymmetric
%   B=[sigma_free, E_elastic, A=mass/mn, E_max, ~, atom_multiplicity]
%   sigma_bound = sigma_free*(A+1)^2/A^2
%
% MF7/MT2 as 'thermal_elastic':
%   SB    Bound cross section (barns) [incoherent elastic LTHR=2]
%
% Format is defined at https://www.oecd-nea.org/dbdata/data/manual-endf/endf102.pdf
%
% By default, the PyNE reader is used. If you wish to use the pure Matlab reader:
%   data = read_endf(filename,'matlab')
%
% Input:  filename: ENDF filename (string)
% output: structure
% Example: y=read_endf(fullfile(ifitpath, 'data','tsl_0012_para-d.endf')); isstruct(y)
% Example: y=read_endf(fullfile(ifitpath, 'data','tsl_0012_para-d.endf'),'matlab'); isstruct(y)

% for MF7 MT4 from ACE
% lib = pyne.ace.Library('/home/farhi/Programs/MCNP/CAB-Sab/hwtr-293/hwtr-293.ace' )
% lib.read()
% scipy.io.savemat('ace.mat',lib.tables)
% matlab> ace=load('ace.mat')
%

persistent status
endf = [];
if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    endf.description  = 'ENDF';
    endf.read         = mfilename;
    endf.ext          = {'dat','tsl','endf'};
    endf.patterns     = {'EVAL','DIST','ENDF'};
    endf.callback     = 'openendf';
    endf.istext       = true;
    return
end

% ============================ Use PyNE ? ======================================
if ~exist('status') || isempty(status)
  status = ~isempty(dir(fullfile(ifitpath,'iofun','readers','private','endf.py'))); 
end

if nargin < 2, option='pyne'; end

if status && ~strcmp(option,'matlab')
  target   = [ tempname '.mat' ];
  filename = strrep(filename, '~',getenv('HOME'));
  filename = java.io.File(filename);
  filename = char(filename.getCanonicalPath()); % fully qualified name

  script = { 'python:', ...
      'import sys', ...
    [ 'sys.path.append("' fullfile(ifitpath,'iofun','readers','private') '")' ], ...
      'from endf import endf2mat', ...
    [ 'endf2mat("' filename '","' target '")' ] };
    
  T= evals(script);

  if T.passed && ~T.failed
    endf = load(target);
    endf = read_endf_pyne2endf(endf);
    endf.script = script;
    endf.output = T;
  else
    disp(T.output)
    disp([ mfilename ': using fallback ENDF reader for ' filename ]);
  end
  % endf = read_endf_pyne(filename);  % private function inline below
  if ~isempty(endf), return; end
end

% ============================ Use pure Matlab =================================
endf = read_endf_matlab(filename);

