% READ_XYZ read a Molecule XYZ file
%
%   Usage: s = read_xyz(filename)
%                    
% References:
%   https://en.wikipedia.org/wiki/XYZ_file_format
%   http://www.mathworks.com/matlabcentral/fileexchange/18233-nanovis--molecular-visualizer
%
% See also: read_poscar, read_pdb, read_mrc, read_cif
% Example: f=fullfile(ifitpath,'data','C6H14.xyz'); a=read_xyz(f); a.atomcount > 1

function s = read_xyz(filename)

  s = [];
  if nargin == 0 || any(strcmp(filename, {'identify','query','defaults'}))
    s.description ='XYZ chemical format';
    s.patterns    ='';
    s.read        =mfilename;
    s.ext         ='xyz';
    s.istext      = true;
    return
  end
  
  fid = fopen(filename,'r');


  if fid==-1
     return
  end

  % from http://www.mathworks.com/matlabcentral/fileexchange/18233-nanovis--molecular-visualizer/content/NanoVIs/NanoVis.m
  % Read a "xyz" file
    
  natom = fscanf(fid,'%i\n');
 
  comment = strtrim(fgetl(fid)); % comment line
  data    = zeros(natom,4);
  species = {};
 
  for k1=1:natom
     
    sp = fscanf(fid,'%c',2);
    I  = find(strcmp(sp,species)); % match previous atom ?
     
    if isempty(I)
        species{end+1}=sp;       
        data(k1,1)=numel(species); 
    else
        data(k1,1)=I(1);
    end
      
    %Read coordinates
    data(k1,2)=fscanf(fid,'%f',1);
    data(k1,3)=fscanf(fid,'%f',1);
    data(k1,4)=fscanf(fid,'%f\n',1); 

  end 
  
  fclose(fid);
  
  % assemble the 'geometry' structure in the style returned by vasplab/import_poscar
  % http://www.mathworks.com/matlabcentral/fileexchange/36836-vasplab/content/vasplab/import_poscar.m
  s.filename  = filename;
  s.symbols   = species;
  s.coords    = data;
  s.selective = 0;
  s.atomcount = natom;
  s.comment   = comment;
  
