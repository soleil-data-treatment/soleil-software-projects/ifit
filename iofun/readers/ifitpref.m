function c = ifitpref(varargin)
% IFITPREF iFit preferences and file formats configuration
%   IFITPREF returns the current configuration (struct).
%
%   IFITPREF('reload') forces to read the configuration again.
%
%   IFITPREF('filename') overrides the configuration from a file.
%
%   IFITPREF('defaults') removes Preferences/ifitpref_formats.ini and reloads formats.
%
%   IFITPREF('save') saves the current configuration to Preferences/ifitpref_formats.ini
%
%   IFITPREF('formats') displays the list of supported formats.
%
%   IFITPREF('formats', TOKEN, ...) displays the formats matching TOKEN
%     The search looks into description, read, write and extensions.
%     TOKEN can be given as a function_handle, char, or cell.
%     When TOKEN is given as a cell, an AND search is made (iterative narrow).
%     Multiple arguments correspond with an OR search      (additive).
%     TOKEN can also be 'isbinary' and 'istext' to select binary and text formats resp.
%     TOKEN can also be 'isreader' and 'iswriter' to search for readers and writers resp.
%     For instance "ifitpref formats isbinary iswriter" shows writers that 
%     generate binary files.
%
%   IFITPREF('silent|normal|verbose') changes the verbosity.
%   IFITPREF('verbosity', level) does the same. A level 0 is silent, 1 is normal
%     and 2 is verbose/debug.
%
%   IFITPREF('compile') checks for external importers.
%
% The format list is obtained from the following sources:
%   - the read_*  methods that return an identification structure
%   - the write_* methods that return an identification structure
%   - the default read methods listed in the ifitpref_formats.m file
%   - the user preferences stored in prefdir/ifitpref_formats.ini
%
% Each format is specified as a structure with the following fields:
%
%  description: description of the data format
%         read: function name/handle to read  the file; data=function(file,...)
%        write: function name/handle to write the file; file=method(data,file,...)
%          ext: a single or a cellstr of extensions associated with the format
%       istext: optional priority (int), e.g. for text files
%     isbinary: optional indicate that file is 'binary'
%      options: optional list of arguments to pass to the method
%               If given as a string, they are catenated with file name
%               If given as a cell, sent to the method as additional arguments
%     patterns: optional cell of tokens to search in e.g. text headers
%               Method is qualified when all are found. The patterns can be  
%               regular expressions. When given as a string, the file is assumed
%               to contain "text" data.
%               A specification begining with `!` excludes the format (should not match).
%     callback: optional callback function(s) to remaster the data after importation 
%
% READERS are sorted with text based ones with patterns first, then other text based,
% then other formats (e.g. binary). System wide formats are tested after user definitions.
%
% Example: c = ifitpref; isfield(c, 'formats')
% Version: $Date$ $Version$ $Author$
% See also set, get, findfield, ifitpref_formats

  persistent conf

  if isempty(conf)
    conf  = config_auto; % default config
  end
  
  % now handle multiple commands
  for index = 1:numel(varargin)
    if ischar(varargin{index}) && ~isempty(dir(varargin{index}))
      conf = config_load(arg{index});
    elseif isstruct(varargin{index}) && isconfig(varargin{index})
      conf = arg{index};
    elseif ischar(varargin{index})
      arg = lower(varargin{index});
      switch(arg)
      case {'auto','reload'}
        conf  = config_auto; %  read ifitpref_formats.ini, ifitpref_formats.m or default
      case 'defaults'
        if exist(fullfile(prefdir, 'ifitpref_formats.ini'), 'file')
          movefile(fullfile(prefdir, 'ifitpref_formats.ini'), fullfile(prefdir, [ 'ifitpref_formats.ini.' datestr(now,30) ]));
        end
        conf  = config_auto; %  read ifitpref_formats.ini, or default ifitpref_formats.m
      case 'save'
        config_save(conf);
      case 'silent'
        conf.verbosity = 0;
      case 'normal'
        conf.verbosity = 1;
      case {'verbose','debug'}
        conf.verbosity = 2;
      case 'verbosity'
        if index < numel(varargin) && isnumeric(varargin{index+1})
          conf.verbosity = varargin{index+1};
          index=index+1;
        end
        c = conf.verbosity;
        return
      case { 'display','find','formats' }
        if index < numel(varargin)
          c.formats = config_find(conf.formats, varargin{index+1});
          index=index+1;
        else
          c = conf;
        end
        if nargout == 0 || strcmp(arg,'display')
          config_display(c);
        end
        return
      case {'compile','check'}
        conf = config_check_compile(conf);
      end
    end
  end

  c = conf;

end % ifitpref

% ------------------------------------------------------------------------------
function conf = config_load_append(conf, new)
% CONFIG_LOAD_APPEND Concatenate NEW to CONF
  for f = {'readers','writers','formats','iofun'}
    if isfield(new, f{1})
      conf.formats = [ conf.formats(:) ; new.(f{1})(:) ];
    end
  end
  for f = {'UseSystemDialogs','FileName'}
    if isfield(new,f{1})
      conf.(f{1}) = new.(f{1});
    end
  end
end % config_load_append

% ------------------------------------------------------------------------------
function conf = config_auto
% CONFIG_AUTO load default configuration and format customization
  
  conf    = config_auto_init; % code defaults without readers/writers
  
  % read default ifitpref_formats.m configuration (system wide)
  if exist('ifitpref_formats', 'file')
    conf = config_load_append(conf, ifitpref_formats);
  end
  
  % add user ifitpref_formats.ini which is a struct of format descriptions
  if exist(fullfile(prefdir, 'ifitpref_formats.ini'), 'file')
    % there is an ifitpref_formats in the Matlab preferences directory: read it
    conf = config_load_append(conf, ...
      config_load(fullfile(prefdir, 'ifitpref_formats.ini')) );
  end

  % add the readers where ifitpref resides
  readers1     = config_load_dir(fileparts(which(mfilename)));
  conf.formats = [ conf.formats(:) ; readers1(:) ];

  % add the default pure-Matlab readers
  readers1     = config_auto_readers_default;
  conf.formats = [ conf.formats(:) ; readers1(:) ];
  
  % sort/check
  conf.formats = config_auto_sort(conf.formats);
  
  % add the default Matlab importer, but must be last
  conf.formats{end+1} = config_check_format( ...
    struct('description','Matlab importer','read','importdata'));

  % we sort formats with highest pattern counts and text on top
  patterns   = cellfun(@(c)getfield(c, 'patterns'),  conf.formats,'UniformOutput',false);
  istext     = cellfun(@(c)getfield(c, 'istext'),    conf.formats,'UniformOutput',false);
  ext        = cellfun(@(c)getfield(c, 'ext'),       conf.formats,'UniformOutput',false);
  istext     = cellfun(@double, istext);
  
  count      = cellfun(@(c)numel(char(c)), patterns)+istext+cellfun(@numel, patterns)+(~cellfun(@isempty,ext));
  [~,index]  = sort(count, 2, 'descend');
  
  conf.formats  = conf.formats(index); % updated list of formats
  
  % add the writers
  writers1      = config_load_dir(fileparts(which('ifitwrite')));
  conf.formats  = config_auto_sort([ conf.formats(:) ; writers1(:) ]);
  
end % config_auto

% ------------------------------------------------------------------------------
function conf = config_auto_init(conf)
% CONFIG_AUTO_INIT init default configuration
  
  if nargin < 1 || ~isstruct(conf), conf=[]; end
  
  if ~isfield(conf, 'verbosity'),        conf.verbosity         = 1; end
  if ~isfield(conf, 'UseSystemDialogs'), conf.UseSystemDialogs  = 'yes'; end
  if ~isfield(conf, 'FileName'),         conf.FileName          = ''; end
  if ~isfield(conf, 'formats'),          conf.formats           = {}; end
  
end % config_auto_init

% ------------------------------------------------------------------------------
function readers = config_auto_readers_default

% ADD default readers: method, ext, name, options
  % default importers, when no user specification is given. 
  % format = { method, extension, name, {options, patterns, callback} }
  formats = {...
    { 'csvread', 'csv', 'Comma Separated Values (CSV, suitable for Excel)',            '',''}, ...
    { 'dlmread', 'dlm', 'Numerical single block',                   '',''}, ...
    { 'xlsread', 'xls', 'Microsoft Excel (first spreadsheet, .xls)','',[]}, ...
    { 'load',    'mat', 'Matlab workspace (.mat)',                  '',[],'openhdf'}, ...
  };
  readers = {};
  for index=1:length(formats) % the default readers are addded after the INI file
    format = cell(1,6);
    [format(1:numel(formats{index}))] = deal(formats{index});

    reader.read         = format{1};
    reader.ext          = format{2};
    reader.description  = format{3};
    reader.options      = format{4};
    reader.patterns     = format{5};  % patterns=nan for binary formats; '' for text without pattern
    reader.callback     = format{6};
    readers{end+1}      = reader;
  end
end % config_auto_readers_default

% ------------------------------------------------------------------------------

function formats = config_auto_sort(formats)

  % make sure format definitions contain the proper fields
  s = {};
  for index=1:numel(formats)
    this = config_check_format(formats{index});
    if ~isempty(this), s{end+1} = this; else
      disp([ mfilename ': WARNING: Invalid format:' ]);
      disp(formats{index})
    end
  end
  formats = s;
  
  % sort entries
  [list, index] = sort(cellfun(@(c)getfield(c, 'description'), formats,'UniformOutput',false));
  formats = formats(index);
  
end

% ------------------------------------------------------------------------------
function conf = config_load(configfile)
% CONFIG_LOAD load the configuration and format customization from a file
%   CONFIG_LOAD(file) returns the configuration file as a structure
%
% The usual file is fullfile(prefdir, 'ifitpref_formats.ini')

  conf       = [];
  % read user list of formats which is a cell of format descriptions
  if ~isempty(dir(configfile))
    content    = fileread(configfile);
    % evaluate content of file
    try
      eval(content(:)'); % this makes a 'config' variable
    catch ME
      disp([ mfilename ': Can not read config file ' configfile]);
      disp(getReport(ME));
    end
    if ~exist('conf') || ~isconf(conf)
      disp([ mfilename ': WARNING: the configuration file ' configfile ]);
      disp(['  should define "conf" as a structure with "conf.formats"' ])
      disp(['  fields, being themselves cell arrays.' ]);
      conf = [];
      return
    end
    conf.FileName= configfile;
    disp([ '% ' mfilename ': Loaded file format descriptions from ' conf.FileName ]);
  end
 
end % config_load

% ------------------------------------------------------------------------------
function formats = config_load_dir(d)

  % read the output of M functions ('identify', return struct) in dir 'd'
  formats = {};
  if isempty(dir(d)), return; end
  if ~getfield(dir(d), 'isdir')
    d = fileparts(which(d));
  end
  if ~isdir(d), return; end
  p = what(d);
  for index=1:numel(p.m)
    [~,this] = fileparts(p.m{index});  % skip M extension
    id = [];
    try
      if ~strcmp(this,mfilename) && abs(nargin(this)) > 0
        id = feval(this, 'identify');
      end
    catch
      disp([ mfilename ': WARNING: ' this '(''identify''): invalid loader/saver information.' ]);
    end
    % the output can be an array: append elements to formats list
    for jj=1:numel(id)
      if iscell(id)
        this = id{jj};
      elseif isstruct(id)
        this = id(jj);
      else this=[];
      end
      if isstruct(this) && isfield(this, 'description') && (isfield(this, 'read') || isfield(this, 'write'))
        formats{end+1} = this;
      end
    end
  end

end % config_load_dir

% ------------------------------------------------------------------------------
function format = config_check_format(format)
% check that a given format (struct) is standardised
  
  if ~isstruct(format) , format=[]; return; end
  if ~isfield(format, 'description'),  format.description='';  end
  if ~isfield(format, 'read'),  format.read =''; end
  if ~isfield(format, 'write'), format.write=''; end
  if (isempty(format.read) && isempty(format.write)) || isempty(format.description)
    format=[]; return; % invalid
  end
  % get location of iofun
  p = fileparts(which('ifitpref_formats'));
  if ~isa(format.read,  'function_handle') && ~exist(format.read) ...
  && ~isa(format.write, 'function_handle') && ~exist(format.write)
    format=[]; return; 
  end

  % optional fields
  if ~isfield(format,'patterns'),  format.patterns =[]; end
  if ~isfield(format,'ext'),       format.ext      =[]; end
  if ~isfield(format,'options'),   format.options  =[]; end
  if ~isfield(format,'isbinary'),  format.isbinary =0;  end
  if ~isfield(format,'istext'),    format.istext   =0;  end
  % if ~isfield(format,'callback'),  format.callback =[]; end
  % test other naming (obsolete)
  for f={'name','postprocess','extension','extensions','method'}
    if isfield(format, f{1})
      disp([ mfilename ': WARNING: ' format.read ' has invalid ' f{1} ' member.' ]);
    end
  end
  
  if ischar(format.ext)
    format.ext = cellstr(format.ext);
  end
  
  % check if 'text'
  if ~isempty(format.patterns) && ischar(format.patterns)
    format.patterns = cellstr(format.patterns);
    % format.istext = true; % some formats may be binary with a text header
  end
  if ~isempty(strfind(lower(format.description),'text'))
    format.istext = 1;
  end
end % config_check_format

% ------------------------------------------------------------------------------
function config_display(conf)

    fprintf(1, ' EXT |          READ           |   WRITE   | TXT | BIN | DESCRIPTION\n');
    fprintf(1, '-----|-------------------------|-----------|-----|-----|------------\n');
    %           EXT=5           READ=25            WRITE=11 TXT=5  BIN=5
    
    % scan formats
    data=conf.formats(:);
    % sort descriptions in alpha order (so that readers and writers are nearby)
    [list, index] = sort(cellfun(@(c)getfield(c, 'description'), data,'UniformOutput',false));
    data=data(index);
    
    for index=1:length(data) % loop on formats
      this=data{index}; 
      
      % build output strings
      if isfield(this, 'read')
        this.read = char(this.read);
        if isfield(this,'callback')
          if ~isempty(this.callback)
            if iscell(this.callback)
              for i=1:length(this.callback)
                this.read = [ this.read '/' this.callback{i} ]; 
              end
            else
              this.read = [ this.read '/' this.callback ]; 
            end
          end
        end
      else this.read='';
      end
      if ~isfield(this,'write') this.write = ''; else this.write = char(this.write); end
      if length(this.read) >25, this.read  = [ this.read(1:25-3) '...' ]; end
      if length(this.write)>11, this.write = [ this.write(1:11-3) '...' ]; end
      
      if ~isfield(this,'ext') || isempty(this.ext), this.ext   = '*'; end
      this.ext=cellstr(this.ext);
      
      pat = this.patterns;
      if iscellstr(pat) && numel(pat) > 0, this.patterns = sprintf('"%s"', pat{:});
      elseif ~ischar(pat) || isempty(pat), this.patterns = '';
      else                                 this.patterns = sprintf('"%s"', pat); end
      
      if isfield(this,'istext')   && this.istext,   this.istext  ='TXT'; 
      else this.istext  =''; end
      if isfield(this,'isbinary') && this.isbinary, this.isbinary='BIN'; 
      else this.isbinary=''; end
      
      % display
      fprintf(1,'%5s|%25s|%11s|%5s|%5s| %s %s\n', upper(this.ext{1}), ...
        this.read, this.write, this.istext, this.isbinary, ...
        this.description, this.patterns);
      if length(this.ext) > 1
        s = '';
        for j=2:length(this.ext),s=[ s ' ' upper(this.ext{j}) ]; end
        fprintf(1, '   \\_|%25s|%11s|%5s|%5s| %s\n', '...', '...','...','...',[ '  other ext: ' s ]);
      end

    end
    
end % config_display

% ------------------------------------------------------------------------------
function tf = isconfig(conf)
  % ISCONFIG Return true when config is a struct with a config
  tf = false;
  if isempty(conf) || ~isstruct(conf), return; end
  for f = {'FileName','UseSystemDialogs','MeX','formats'}
    if ~isfield(conf, f{1}), return; end
  end
  if ~iscell(conf.formats),  return; end
  tf = true;
end

% ------------------------------------------------------------------------------
function conf = config_check_compile(conf)
% CONFIG_CHECK_COMPILE check for required executables/mex files

  if strcmp(conf, 'compile')
    % force compile
    read_anytext('compile');
    read_cbf('compile');
    cif2hkl('compile');
  else
    % make a check of installed MeX/binaries
    try
      conf.external.cbf = read_cbf('check');
      disp([ mfilename ': CBF     importer  is functional as ' conf.external.cbf ]);
    catch ME
      disp([ mfilename ': CBF     importer  is functional as read_cbf.m (failed mex)' ]);
    end
    try
      conf.external.looktxt = read_anytext('check');
      disp([ mfilename ': Text    importer  is functional as ' conf.external.looktxt ])
    catch ME
      warning('ifit:ifitpref', [ mfilename ': Text    importer  is NOT functional' ]);
      disp(getReport(ME))
    end
    try
      conf.external.cif2hkl = cif2hkl('check');
      disp([ mfilename ': CIF2HKL converter is functional as ' conf.external.cif2hkl ])
    catch ME
      warning('ifit:ifitpref', [ mfilename ': CIF2HKL converter is NOT functional' ]);
      disp(getReport(ME))
    end
      
  end % config_check_compile
end
% ------------------------------------------------------------------------------
    
function config = config_save(config)
% CONFIG_SAVE save the configuration and format customization

  data = config.formats;
  format_names  ={};
  format_readers={};
  format_writers={};
  format_unique =ones(1,length(data));
  % remove duplicated format definitions
  for index=1:length(data)
    % check for same description, 'read' and 'write'
    if ~isempty(data{index}.description) ...
      && any(strncmpi(data{index}.description,   format_names,   length(data{index}.description))) ... 
      && any(strncmpi(data{index}.read,  format_readers, length(data{index}.read))) ...
      && any(strncmpi(data{index}.write, format_writers, length(data{index}.write)))
      format_unique(index) = 0; % already exists. Skip it.
      format_names{index}  = '';
      format_readers{index}= '';
      format_writers{index}= '';
    else
      format_names{index}   = data{index}.description;
      format_readers{index} = data{index}.read;
      format_writers{index} = data{index}.write;
    end
  end
  data = data(find(format_unique));
  config.formats = data;
  
  % save ifitpref_formats.ini configuration file
  % make header for ifitpref_formats.ini
  config.FileName=fullfile(prefdir, 'ifitpref_formats.ini'); % store preferences in PrefDir (Matlab)
  NL = sprintf('\n');
  str = [ '% File format configuration script ' NL ...
          '%' NL ...
          '% Matlab ' version ' m-file ' config.FileName NL ...
          '% generated automatically on ' datestr(now) ' with ifitpref(''save'');' NL ...
          '%' NL ...
          '% The configuration may be specified as a structure' NL ...
          '%     config.formats = { format1 .. formatN }; (see below)' NL ...
          '%     config.UseSystemDialogs=''yes'' to use built-in Matlab file selector (uigetfile)' NL ...
          '%                             ''no''  to use iFit file selector           (uigetfiles)' NL ...
          '%' NL ...
          '% User definitions of specific import formats to be used by iFit' NL ...
          '% Each format is specified as a structure with the following fields' NL ...
          '%         read: function name/handle to read  the file; data=function(file,...)' NL ...
          '%        write: function name/handle to write the file; file=method(data,file,...)' NL ...
          '%          ext: a single or a cellstr of extensions associated with the format' NL ...
          '%       istext: optional priority (int), e.g. for text files' NL ...
          '%     isbinary: optional indicate that file is "binary"' NL ...
          '%      options: optional list of arguments to pass to the method' NL ...
          '%               If given as a string, they are catenated with file name' NL ...
          '%               If given as a cell, sent to the method as additional arguments' NL ...
          '%     patterns: optional (read) cell of tokens to search in e.g. text headers' NL ...
          '%               Method is qualified when all are found. The patterns can be  ' NL ...
          '%               regular expressions. When given as a string, the file is assumed' NL ...
          '%               to contain "text" data.' NL ...
          '%               A specification begining with `!` excludes the format (should not match).' NL ...
          '%     callback: optional (read) callback function(s) to remaster the data after importation ' NL ...
          '%' NL ...
          '% all formats must be arranged in a cell, sorted from the most specific to the most general.' NL ...
          '% Formats will be tried one after the other, in the given order.' NL ...
          '% System wide formats are tested after user definitions.' NL ...
          '%' NL ...
          '% NOTE: The resulting configuration must be named "config"' NL ...
          '%' NL ...
          class2str('config', config) ];
  [fid, message]=fopen(config.FileName,'w+');
  if fid == -1
    warning('ifit:ifitpref', ['Error opening file ' config.FileName ' to save Formats configuration.' ]);
    config.FileName = [];
  else
    fprintf(fid, '%s', str);
    fclose(fid);
    disp([ '% Saved Formats configuration into ' config.FileName ]);
  end
  
end % config_save

% ------------------------------------------------------------------------------
function formats = config_find(data, varargin)
% CONFIG_FIND searches in the config.formats for given names.
%   The search looks into description, read, write and extensions.
%   Token can also be 'isbinary' and 'istext' to search for binary and text formats resp.
%   Token can also be 'isreader' and 'iswriter' to search for readers and writers resp.
%
%   Token can be given as a function_handle, char, or cell.
%   When token is given as a cell, an AND search is made (iterative narrow).
%   Multiple arguments correspond with an OR search      (additive).
  
  if nargin > 2
    formats = {};
    for index=1:numel(varargin)
      this    = config_find(data, varargin{index});
      formats = [ formats ; this(:) ]; % additive search (OR)
    end
    return
  end
  token = varargin{1};
  if isa(token, 'function_handle'), token = func2str(token); end
  if iscell(token)
    formats = data;
    for index=1:numel(token)
      formats = config_find(formats, token{index}); % iterative narrow search (AND)
    end
  elseif ischar(token)
    % test if token is the user name of a function

    formats = {}; token = lower(token);
    for index=1:length(data)
      this = data{index};
      keepme=0;
      if ~isempty(strfind(lower(this.description), token)) || ...
      (isfield(this,'read')  && ~isempty(strfind(lower(char(this.read)),  token))) || ...
      (isfield(this,'write') && ~isempty(strfind(lower(char(this.write)), token)))
        keepme = 1;
      elseif isfield(this,'ext') && any(strncmpi(token, this.ext, length(token)))
        keepme = 1;
      elseif any(strcmp(token, {'isbinary','istext'})) && isfield(this,token) && this.(token)
        keepme = 1;
      elseif strcmp(token, 'isreader') && isfield(this,'read')  && ~isempty(this.read)
        keepme = 1;
      elseif strcmp(token, 'iswriter') && isfield(this,'write') && ~isempty(this.write)
        keepme = 1;
      end
      if keepme, formats = { formats{:} this }; end
    end
  end
  
end % config_find
