function varg = get_format_args(format, io, varargin)
% GET_FORMAT_ARGS assemble the arguments for the file format method
%   GET_FORMAT_ARGS(FORMAT, 'read',  FILE, ...)
%   GET_FORMAT_ARGS(FORMAT, 'write', DATA, FILE, ...)
%
% Each format is specified as a structure with the following fields
%
%  description: description of the data format
%         read: function name/handle to read  the file; data=method(file,...)
%        write: function name/handle to write the file; file=method(data,file,...)
%          ext: a single or a cellstr of extensions associated with the format
%       istext: optional, indicate this is a text-based format
%     isbinary: optional, indicate this is a binary-based format
%      options: optional list of arguments to pass to the method
%               If given as a string, they are catenated with file name
%               If given as a cell, sent to the method as additional arguments
%     patterns: optional (read) cell of tokens to search in e.g. text headers
%               Method is qualified when all are found. The patterns can be  
%               regular expressions. When given as a string, the file is assumed
%               to contain "text" data.
%               A specification begining with `!` excludes the format (should not match).
%     callback: optional (read) callback function(s) called after importation 

  varg = {};
  if ~isstruct(format) || ~any(strcmp(io, {'read','write'})), return; end
  % set index where to insert/concatenate options
  if strcmp(io, 'read') fi=1; else fi=2; end % file index

  % we select the calling syntax which matches the number of input arguments
  if ~isfield(format, 'options') || isempty(format.options)
    varg = varargin;
  elseif iscellstr(format.options)
    varg = { varargin{1:fi}, format.options{:}, varargin{(fi+1):end} };
  elseif ischar(format.options)
    varargin{fi} = [ varargin{fi} ' ' format.options ];
    varg = varargin;
  end
  % reduce the number of input arguments to the one expected
  if nargin(format.(io)) > 0 && ...
    ( ~strcmp(io, 'read') || ~any(strcmp(format.(io), {'text','read_anytext','looktxt'})) )
    narg = min(length(varg), nargin(format.(io)));
    varg = varg(1:narg);
  end
end
