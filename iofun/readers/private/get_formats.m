function [format, isbinary, conf] = get_formats(format, filename, opt)
% GET_FORMATS get the list of possible readers/writers for given file
%   READERS = GET_FORMATS('AUTO', FILENAME)
%     Returns a list of readers than can load the given FILENAME.
%   READERS = GET_FORMATS(FORMATS, FILENAME)
%     Checks given formats and return a qualified readers list.
%     Formats can be a char or cellstr.
%   WRITERS = GET_FORMATS(..., 'writers')
%     Same as above, but for writers (does not search for patterns).
%
% Each format is specified as a structure with the following fields
%
%  description: description of the data format
%         read: function name/handle to read  the file; data=method(file,...)
%        write: function name/handle to write the file; file=method(data,file,...)
%          ext: a single or a cellstr of extensions associated with the format
%       istext: optional, indicate this is a text-based format
%     isbinary: optional, indicate this is a binary-based format
%      options: optional list of arguments to pass to the method
%               If given as a string, they are catenated with file name
%               If given as a cell, sent to the method as additional arguments
%     patterns: optional (read) cell of tokens to search in e.g. text headers
%               Method is qualified when all are found. The patterns can be  
%               regular expressions. When given as a string, the file is assumed
%               to contain "text" data.
%               A specification begining with `!` excludes the format (should not match).
%     callback: optional (read) callback function(s) called after importation 

  isbinary = 0; conf = ifitpref;
  if nargin < 3,   opt=''; end
  if isempty(opt), opt='readers'; end
  if strcmpi(format, 'auto')
    [format, isbinary] = get_formats_auto(filename, conf, [], opt);
  elseif strcmpi(format, 'gui')
    [format, isbinary] = get_formats_auto(filename, conf, [], opt); % auto
    format_names=[format{:}];
    tmp         = cell(size(format_names)); tmp(:)={' - '};
    format_names= strcat({format_names.name}, tmp,{format_names.method});
    
    % display dialogue
    if strncmp(opt, 'read', 4)
      [dummy, filename_short, ext] = fileparts(filename);
      filename_short = [ filename_short ext];
    else
      filename_short = '';
    end
    [format_index,ok]=listdlg(...
      'PromptString',...
        {[ 'Select suitable ' opt ], ...
         [ filename_short ]}, ...
      'SelectionMode','Multiple',...
      'ListString', format_names, ...
      'ListSize', [300 160], ...
      'Name', [ filename_short ]);
    if isempty(format_index), format=[]; return; end
    format=format(format_index);
  elseif (ischar(format) || iscellstr(format) || isa(format, 'function_handle'))
    % test if format is the name of a function
    format = ifitpref('formats', format);
    format = get_formats_auto(filename, conf, format, opt); % restricted formats
  elseif isa(format, 'function_handle') format = feval(format);
  end
end

% ------------------------------------------------------------------------------

function [formats, isbinary] = get_formats_auto(file, conf, format, opt)
% GET_FORMATS_AUTO function to determine which parser to use to analyze content
%   FORMATS = GET_FORMATS_AUTO(FILE, CONF) Determines appropriate FORMATS to 
%   read FILE.
%
%   FORMATS = GET_FORMATS_AUTO(FILE, CONF, { FORMAT }) Determines appropriate 
%   FORMATS from cell FORMAT to read FILE. Empty given FORMAT checks all.
%
%   FORMATS = GET_FORMATS_AUTO(FILE, CONF, FORMAT, writers') Determines appropriate 
%   FORMATS to write FILE, restricting FORMAT to the cell given. 
%   Empty given FORMAT checks all.
%
%   when given 3rd arg, a restricted list of formats is used (not ifitpref all)
  
  if nargin < 3, format=[]; end
  if nargin < 4, opt   ='read'; end
  if isempty(format),          format=conf.formats; 
  elseif isfield(format,'formats')
    format = format.formats;
  elseif ~iscell(format)
    error([ mfilename ': format must be a cell, e.g. conf.formats, not ' class(format) ])
  end

  % restrict formats using format extensions, also retain formats without extension
  %   this filter is needed to restrict formats in a fast way.
  % get extension 
  [~, ~, ext] = fileparts(file);
  if ~isempty(ext) && ext(1)=='.', ext(1)=[]; end
  
  if ~isempty(ext) || strncmp(opt, 'read',4)
    selected = check_format_extension(format, ext);
  else
    selected=ones(size(format));
  end

  % readers: restrict formats that match patterns (or no patterns). 
  % Keep extension exact match on top
  if strncmp(opt, 'read',4)
    % restrict to readers only
    no_read = cellfun(@(c)isempty(c.read), format);
    selected(find(no_read)) = 0;
  
    % read start of file
    [fid,message] = fopen(file, 'r');
    if fid == -1
      error([ mfilename ': Could not open file ' file ' for reading. ' ...
        message '. Check existence/permissions.' ]);
    end

    % get header (file start) and 'isbinary'
    % WARNING: it is hard to determine if the file is binary. It may contain an 
    % ASCII header. When all is text, at least we may assume it is not binary...
    file_start = fread(fid, 100000, 'uint8=>char')'; fclose(fid);
    ispr = isstrprop(file_start, 'print') | isstrprop(file_start, 'wspace');
    if sum(ispr)/numel(file_start) >= 0.95
      isbinary = 0; % we should then skip all formats.isbinary==true
    else 
      isbinary = 1; % perhaps binary, may contain mixed text for patterns
    end

    % clean file start with spaces, remove EOL
    file_start(~ isstrprop(file_start,'print')) = ' ';
    
    formats = check_format_patterns(conf, file_start, isbinary, ext, selected, format);
  else
    isbinary = false;
    % restrict to writers only
    no_write = cellfun(@(c)isempty(c.write), format);
    selected(find(no_write)) = 0;
    formats = format(find(selected));
  end

end % get_formats_auto

% ------------------------------------------------------------------------------
function selected = check_format_extension(formats, ext)
% CHECK_FORMAT_EXTENSION: identify formats for which any registered extension 
%   match 'ext' or format has no extension (all match), or '*'
%   returns formats and selected (bool)

  exts = cellfun(@(c)getfield(c, 'ext'), formats, 'UniformOutput',false);
  
  selected = zeros(size(exts));
  % first we add formats that exactly match extensions
  for index=1:numel(exts)
    if ~isempty(exts{index}) && isempty(ext), found = false; 
    else found=any(strcmpi(ext, exts{index}));
      if found, selected(index)=1; end
    end
  end
  % then we add other ones
  for index=1:numel(exts)
    if ~selected(index)
      if ~isempty(exts{index}) && isempty(ext), found = false;
      else found=isempty(exts{index}) || (iscellstr(exts{index}) && any(strcmp(exts{index},'*')));
        if found, selected(index) = 1; end
      end
      
    end
  end
  
end % check_format_extension

% ------------------------------------------------------------------------------
function formats = check_format_patterns(conf, file_start, isbinary, ext, selected, formats)
% check_format_patterns: identify formats for which all patterns appear in file_start 
% when given last argument, a restricted search is done.
% warning: only isbinary=0 means this is full text. Other mixed state may exist.
%
% Chekcs:
%   isbinary == 0 (full text): exclude isbin
%   isbinary == 1 (mixed/bin): exclude istext
%   has patterns (for mixed binary/text == all cases) 
%   has matching extension (all cases)
%   formats without extension (all cases)
%   formats without pattern

  if nargin < 6, formats=[]; end
  % keep when patterns found in file_start
  % or format has patterns=='' and is text
  % or no pattern at all
  persistent exts0 pats0 istext0 isbin0
  
  if (isempty(exts0) && isempty(formats)) || ~isempty(formats)
    if isempty(formats), f=conf.formats; else f=formats; end
    exts   = cellfun(@(c)getfield(c, 'ext'),       f, 'UniformOutput',false);
    pats   = cellfun(@(c)getfield(c, 'patterns'),  f, 'UniformOutput',false);
    istext = cellfun(@(c)getfield(c, 'istext'),    f, 'UniformOutput',false);
    isbin  = cellfun(@(c)getfield(c, 'isbinary'),  f, 'UniformOutput',false);
    istext = cellfun(@double, istext);
    isbin  = cellfun(@double, isbin);
    if isempty(exts0) && isempty(formats)
      exts0=exts; pats0=pats; istext0=istext; isbin0=isbin; % set cache
    end
  else % if isempty(formats)
    exts=exts0; pats=pats0; istext=istext0; isbin=isbin0;   % use cache
  end
  
  if isempty(formats), formats=conf.formats; end
  
  % exclude binary formats when isbinary==0 from filestart (printable)
  if ~isbinary % pure text
    selected = selected & ~isbin & istext;
  else
    % isbinary = 1 mixed binary/text must exclude istext/read_anytext stuff.
    selected = selected & (isbin | ~istext);
  end
  if islogical(selected) || all(selected == 0 | selected == 1)
    selected = find(selected);
  end
  
  % restrict formats to selected set (from extension)
  exts   = exts(selected);
  pats   = pats(selected);
  istext = istext(selected);
  isbin  = isbin(selected);
  formats= formats(selected);
  
  done   = zeros(size(istext));
  nopat  = zeros(size(istext));
  out    = {};
  
  % put formats that match patterns
  for index=1:numel(pats)
    found = false;
    this_pats = pats{index};
    if ~isempty(this_pats) && iscell(this_pats) && isempty(this_pats{1})
      this_pats=[];
    end
    if ~isempty(this_pats) && (iscellstr(this_pats) || ischar(this_pats))
      found=true;
      for pat=cellstr(this_pats)
        this  = pat{1}; if isempty(this), continue; end
        if this(1) == '!'
          match = regexp(file_start, this(2:end));
        else
          match = regexp(file_start, this);
        end
        match = xor(~isempty(match), this(1) == '!');
        if ~match
          found=false; nopat(index)=1; break;
        end % a pattern was not found, or a NOT-pattern was found
      end
      if found
        out{end+1} = formats{index};
        done(index)=1;
      end
    end
  end
  
  % we keep formats that exactly match extension (but not if patterns do not 
  % match) or those without extension.
  for index=1:numel(exts)
    if ~done(index) && ~nopat(index)
      if isempty(ext)
        % formats without extension
        found = isempty(exts{index}) | any(strcmp(exts{index},{'*','*.*'}));
      else 
        % match extension
        found=any(strcmpi(ext, exts{index})); end
      if found
        out{end+1} = formats{index}; done(index)=1; 
      end
    end
  end
  
  % then put formats without pattern (may be text or binary)
  for index=1:numel(pats)
    if ~done(index)
      found = false;
      if  isempty(pats{index}) || ...
      ((iscellstr(pats{index}) || ischar(pats{index})) && all(strcmp(pats{index},'')))
        out{end+1} = formats{index};
        done(index)=1;
      end
      
    end
  end
  % return value
  formats = out;

end % check_format_patterns

