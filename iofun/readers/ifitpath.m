function d = ifitpath
% ifitpath iFit library location
%
% returns the iFit installation path (string)
%
% Example: d=ifitpath;
% Version: $Date$ $Version$ $Author$
% See also matlabpath, prefdir

persistent ifp

if isempty(ifp)
  % get the non fully qualified path
  d   = fullfile(fileparts(which(mfilename)),'..','..');
  % qualify it using Java
  if usejava('jvm')
    file= java.io.File(d);
    ifp = char(file.getCanonicalPath());
  else
    ifp = d;
  end
end
d = ifp;

