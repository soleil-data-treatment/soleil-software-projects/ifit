function config = ifitpref_formats
% config = IFITPREF_FORMATS User definitions of specific import/export formats
%
% Each format is specified as a structure with the following fields
%
%  description: description of the data format
%         read: function name/handle to read  the file; data=function(file,...)
%        write: function name/handle to write the file; file=method(data,file,...)
%          ext: a single or a cellstr of extensions associated with the format
%       istext: optional priority (int), e.g. for text files
%     isbinary: optional indicate that file is 'binary'
%      options: optional list of arguments to pass to the method
%               If given as a string, they are catenated with file name
%               If given as a cell, sent to the method as additional arguments
%     patterns: optional (readers) cell of tokens to search in e.g. text headers
%               Method is qualified when all are found. The patterns can be  
%               regular expressions. When given as a string, the file is assumed
%               to contain "text" data.
%               A specification begining with `!` excludes the format (should not match).
%     callback: optional (readers) callback function(s) to remaster the data after importation 
%
% Other formats are defined by each 'read_*' and 'write_*' function, and
% obtained with: read_FMT('identify') and write_FMT('identify')
%
% The list of formats is made available in the configuration through the
%   'formats' field of the structure 'conf'.
%
% Formats are sorted from the most specific to the most general.
% Formats will be sorted with text based ones with patterns first, then text based,
%   then other formats (e.g. binary).
% System wide readers/writers are tested after user definitions.
%
% These formats can be obtained using ifitpref('formats').
% The formats configuration file can be saved in the Preference directory
%   using ifitpref(i_file,'save'). It can also be loaded with ifitpref(i_file)
% A list of all supported formats is shown with ifitpref('formats');
%
% Example: conf = ifitpref; isstruct(conf)
% Version: $Date$ $Version$ $Author$
% See also: save, load
% 

% definition of formats ========================================================
    
    chalkriver.description='ChalkRiver CNBC';
    chalkriver.patterns   ={'Run ','Seq ','Rec ','Mode ','Temp:','File '};
    chalkriver.options    ='--fast --binary  --headers --comment=NULL --silent --section=Run --metadata=File --metadata=Sig';
    chalkriver.read       ='read_anytext';
    chalkriver.callback   ='callback_load_chalkriver';
    
    qd_vms.description    = 'Quantum Design VMS ppms/mpms';
    qd_vms.ext            = 'dat';
    qd_vms.read           = 'read_anytext';
    qd_vms.options        = '-H -m Headers -m INFO -m Data -m mm --catenate --fast --binary --silent';
    qd_vms.patterns       = {'Quantum Design'};
    
% definition of configuration ==================================================
% Must be stored in 'conf.formats' as a cell {}.

    config.formats          =  { ...
       chalkriver, ...
       qd_vms ...
    };
	       
    config.UseSystemDialogs = 'yes'; % no: use uigetfiles, else defaults to 'uigetfile'
    config.FileName         = [ mfilename ' (default configuration from ' which(mfilename) ')' ];
	  
