function filename = write_yaml(a, filename)
% WRITE_YAML Save dataset into a JavaScript Object Notation (JSON).
%
%   FILENAME = WRITE_YAML(DATA, FILENAME) 
%     When ommitted, FILENAME is set in TMDIR.
%
%   Open generated file with any text viewer.
%
% Credits: YAML, Kota Yamaguchi 2011
% Example: y=write_yaml(rand(100,100)); ~isempty(dir(y))
% See also: write_csv, write_xls, write_dat, write_json, write_xml
  
  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    dat.description  = 'Yet Another Markup Language (YAML)';
    dat.write        = mfilename;
    dat.ext          = {'yaml','yml'};
    dat.istext       = true;
    filename         = dat;
    return
  end
  
  if nargin < 2, filename = '';  end
  if isempty(filename), filename = [ tempname '.yaml' ]; end

  if isa(a, 'iData') || isstruct(a)
    warning('off','MATLAB:structOnObject');
    a=struct(a);
    if isfield(a, 'cache') a=rmfield(a,'cache'); end
  end

if usejava('jvm') && exist('YAML')
  YAML.write( filename, a ); % YAML object is in ifit/contrib
  if isempty(dir(filename)), filename = []; end
else
  error([ mfilename ': ERROR: requires java and YAML to be available from Matlab.' ])
end
    
