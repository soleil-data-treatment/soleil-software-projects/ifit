function filename=write_csv(a, filename, dlm)
% WRITE_CSV Save dataset into comma-separated value file (CSV suitable for Excel).
%
%   FILENAME = WRITE_CSV(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_CSV(DATA, FILENAME, 'DLM')
%     writes DATA into FILENAME using the character DLM as the delimiter.
%     Usual delimiters are ',', ' ', ';', '\t'. Default is ','.
%
%   Open generated file with Excel, LibreOffice, Gnumeric.
%
% Example: y=write_csv(rand(100,100)); ~isempty(dir(y))
% See also: write_dat, write_xls
      
  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      dlm.description = 'Comma Separated Values (CSV, suitable for Excel)';
      dlm.write       = mfilename;
      dlm.ext         = {'csv'};
      dlm.istext      = true;
      filename        = dlm;
      return
  end

  if nargin < 2, filename = [ tempname '.csv' ]; end
  if nargin < 3, dlm      = ',';  end
  
  if ~isnumeric(a), a=single(a); end

  dlmwrite(filename, a, dlm);
