function filename = write_pdf(a, filename, format, varargin)
% WRITE_PDF Save dataset into Portable Document Format (PDF).
%
%   FILENAME = WRITE_PDF(DATA, FILENAME)
%     DATA must be a iData object or a single numeric array.
%     The file format is infered from the FILENAME extension.
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_PDF(DATA, FILENAME, FORMAT)
%     FORMAT specifies the output data format. 
%     Supported formats are those supported by print and saveas:
%       JPEG PNG TIFF PDF EPS EPSC EPS2 EPSC2 SVG FIG
%
%   FILENAME = WRITE_PDF(DATA, FILENAME, 'FORMAT', OPTIONS)
%     OPTIONS indicates plot options:
%       tight, hide_axes, interp, view2, view3, transparent, light, clabel,
%       colorbar, whole, hide_errorbars (1D). Default is view2 axis tight.
%
%   Open generated file with many PDF viewers.
%
% Example: y=write_pdf(rand(100,100)); ~isempty(dir(y))
% See also: write_avi, write_eps, write_fits, write_image, write_svg, print, saveas

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      s1.description = 'Image/Picture (PS/PDF)';
      s1.write       = mfilename;
      s1.ext         = {'JPEG' 'PNG' 'TIFF' 'PDF' 'EPS' 'EPSC' 'EPS2' 'EPSC2' 'SVG' 'FIG'};
      filename = s1;
      return
  end

  % handle input arguments
  if nargin < 2, filename = '';    end
  if nargin < 3, format   = 'pdf'; end
  if nargin < 4 && isa(a,'iData'), varargin  = {'view2 axis tight'}; end

  if isempty(filename), filename = tempname; end
  if strncmp(format,'.',1), format(1)=[]; end
  format  = lower(format);
  [p,f,e] = fileparts(filename);
  if isempty(e)
    if ~isempty(format) filename = fullfile(p, [ f '.' format ]); end
  else format = e(2:end);
  end
  
  % handle input data type

  % plot, and export with saveas
  f=figure('visible','off','Renderer', 'zbuffer');
  plot(a, varargin{:});
  set(f, 'Renderer','zbuffer');
  saveas(f, filename, format);
  close(f);
  
  if isempty(dir(filename))
    error([ mfilename ': ERROR: could not save ' class(a) ' to ' format ]);
  end
