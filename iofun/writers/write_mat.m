function filename = write_mat(a, filename, varargin)
% WRITE_MAT Save dataset into a Matlab workspace (MAT).
%
%   FILENAME = WRITE_MAT(DATA, FILENAME) 
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_MAT(DATA, FILENAME, OPTIONS...) 
%     Where OPTIONS can be, as for SAVE:
%       '-struct'
%       '-append'
%       '-ascii'
%       '-v4' '-v6' '-v7' '-v7.3'
%
%   Open generated file with Matlab, Octave, or Scipy.
%
% Example: y=write_mat(rand(100,100)); ~isempty(dir(y))
% See also: write_m, write_fig, write_hdf, save
  
  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    dat.description  = 'Matlab workspace (.mat)';
    dat.write        = mfilename;
    dat.ext          = {'mat'};
    dat.isbinary     = true;
    filename         = dat;
    return
  end
  
  if nargin < 2, filename = '';  end
  if isempty(filename), filename = [ tempname '.mat' ]; end
    
  varg = { filename };
  if ~isempty(inputname(1))
    eval([ inputname(1) '= a;' ]);
    varg{2} = inputname(1);
  elseif isfield(a, 'Tag')
    eval([ a.Tag '= a;' ]);
    varg{2} = a.Tag;
  end

  save(varg{:}, varargin{:});
  
