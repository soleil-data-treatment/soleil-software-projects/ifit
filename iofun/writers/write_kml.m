function [filename, M] = write_kml(a, filename, varargin)
% WRITE_KML Save dataset into GoogleEarth 3D model. 
%   An additional Collada DAE file is written. File formats are XML based.
%
%   FILENAME = WRITE_KML(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array
%     or any structure with fields any of:
%       x,y,z
%       faces,vertices
%
%   FILENAME = WRITE_KML(DATA, FILENAME, 'PROP','VAL'...)
%     Specifies additional property value pairs for export:  
%      color:      color, default=[0.7 0.7 0.7] OR.. 
%                  image, RGB(A)|BW(A)|filename (optional)
%      alpha:      opacity, default=[1] (opaque) OR..
%                  image, BW|filename (optional)
%      latlon:     flag to interpret xyz as Lat,Lon,Alt, default=0
%      twosided:   flag to render faces from both sides, default=1
%      position:   [latitude_deg longitude_deg altitude_m]
%      altmode:    default='absolute', 'clampToGround', 'relativeToGround',
%                  'relativeToSeaFloor', 'clampToSeaFloor'
%      orientation:[heading_NtoE pitch_NtoUp roll_UPtoE] (deg), default=[0,0,0]
%      scale:      [x y z] scale factors, default=[1,1,1]
%      camera:     [heading_deg tilt_deg range_m]
%
%   Open generated file with Google Earth.
%
% Credits: mesh2kml, Sergey Kharabash 2017.
% Example: y=write_kml(rand(100,100)); ~isempty(dir(y{1})) && ~isempty(dir(y{2}))
% See also: write_vtk, write_dae, write_off, write_ply, write_stl, write_x3d, write_vrml

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    kml.description  = 'GoogleEarth 3D XML model';
    kml.write        = mfilename;
    kml.ext          = {'kml'};
    kml.istext       = true;
    filename         = kml;
    return
  end

  if nargin < 2, filename = ''; end
  if isempty(filename), filename = tempname; end

  [filename, M] = write_dae(a, filename, 'kml', varargin{:});
