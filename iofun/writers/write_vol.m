function filename = write_vol(a, hdr, filename)
% WRITE_VOL Save dataset into ESRF PyHST2 volume (.vol/.par).
%
%   FILENAME = WRITE_VOL(DATA, FILENAME)
%     DATA must be a iData object or a single numeric array.
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_VOL(DATA, HDR, FILENAME)
%     HDR is a structure which fields are put into the .PAR file.
%
%   Open with e.g. ImageJ, TomoPy, Astra_Toolbox, Plastimatch, 
%   This format is very fast to read and write, and can carry some MetaData.
%
% References:
%   Alessandro Mirone et al, NIM B 324 (2014) 41. DOI: 10.1016/j.nimb.2013.09.030
%
% Example: y=write_vol(rand(100,100)); ~isempty(dir(y{1}))
% See also: write_nrrd, write_edf, write_nii, write_hdf, write_fits

if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
  ESRF_vol.description= 'RAW 3D volume (tomography/PyHST2)';
  ESRF_vol.write      = mfilename;
  ESRF_vol.ext        = {'vol','par','info'};
  ESRF_vol.isbinary   = true;
  filename            = ESRF_vol;
  return
end

% test on input arguments
if nargin == 1
  filename = '';
  hdr      = [];
end
if nargin == 2 && ischar(hdr)
  filename = hdr;
  hdr      = [];
end
if isempty(filename), filename = [ tempname '.edf' ]; end

% iData -> struct fields
if isa(a, 'iData')
  % we compose the header struct from the iData fieldnames
  hdr.Label = { xlabel(a), ylabel(a) };
  hdr.Name  = char(a,'short');
  m = get(a,'Monitor'); 
  if ~isempty(m) && not(all(m==0| m==1 | isnan(m)))
    m = mean(m(:));
    hdr.Title                     = [ title(a) ' per monitor' ];
    hdr.SignalNormalizedToMonitor ='Yes';
    hdr.Monitor                   = m;
  else
    hdr.Title                     = title(a);
  end
  e = get(a,'Error'); 
  if ~isempty(e) && not(all(e==0 | isnan(e)))
    e=mean(e(:));
    hdr.Error = e;
  end
end

% fill-in header from 'a' into 'hdr'. Only first level fieldnames.
if isa(a, 'iData') || isstruct(a)
  for f=fieldnames(a)'
    if  ~isfield(hdr, f{1})
      hdr.(f{1}) = a.(f{1});
    end
  end
end

ax    = {};
axlab = {};

% get img (signal)
if isa(a, 'iData')
  % set axes
  for index=1:ndims(a)
    ax{index}     = axis(a, index);
    axlab{index}  = label(a, index); 
  end
  % get 'Signal'
  a = axis(a,0);
elseif isnumeric(a)
  % set default axes
  for index=1:ndims(a)
    ax{index}=1:size(a, index);
  end
else
  error([ mfilename ': ERROR: Can only write from iData/numeric array, not ' class(a) ]);
end

% now 'a' is an array
if isempty(a) || ~isnumeric(a), return; end
[p,f,e] = fileparts(filename);
vol = fullfile(p, [f '.vol' ]);
par = fullfile(p, [f '.info' ]);

% write the header
s = size(a);
hdr.BYTEORDER = 'LOWBYTEFIRST';
for index=1:numel(s)
  hdr.([ 'START_VOXEL_' num2str(index) ]) = 1;
  hdr.([ 'END_VOXEL_'   num2str(index) ]) = s(index);
end
fid = fopen(par,'w+');
if (fid<0)
  error([ mfilename ': ERROR: unable to write the file ' par]);
end
fprintf(fid, '! PyHST_SLAVE VOLUME INFO FILE\n');
for f=fieldnames(hdr)'
  this = hdr.(f{1});
  if isempty(this), continue; end
  if ischar(this) && numel(this)<100
    this = cellstr(this);
  end
  if iscellstr(this)
    if numel(this) > 1, c='%s;'; else c='%s'; end
    this = sprintf(c, this{:});
  elseif isnumeric(this)
    if isscalar(this)
      this = num2str(this);
    elseif numel(this) < 10
      this = mat2str(this);
    else this = ''; end
  else this='';
  end
  if ~isempty(this)
    fprintf(fid, [ upper(f{1}) ' = ' this sprintf('\n') ]);
  end
end
fclose(fid);

% write the data (image/volume)
% Open the image file
fid = fopen(vol,'w+'); 
if (fid<0)
  error([ mfilename ': ERROR: unable to write the file ' vol]);
end

% read the full volume as a float32 binary content
count = fwrite(fid, a, 'float32'); % can be large
fclose(fid);
if ~count || isempty(dir(vol)), filename = '';
else filename = { vol par }; end

