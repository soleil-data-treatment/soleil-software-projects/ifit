function filename = write_nc(a, filename)
% WRITE_NC Save dataset into NetCDF.
%
%   FILENAME = WRITE_NC(DATA, FILENAME)
%     DATA must be an iData object or a struct. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   Open the HDF5 file with NCView. NetCDF is not CDF.
%   This format is rather fast to read and write, and retains most data.
%
% Example: y=write_nc(rand(100,100)); ~isempty(dir(y))
% See also: write_hdf, write_cdf

if nargin == 0
  ncdf.description= 'NetCDF (.nc)';
  ncdf.write      = mfilename;
  ncdf.ext        = {'nc','cdf'};
  ncdf.isbinary   = true;
  filename        = ncdf;
  return
end

if nargin < 2, filename = ''; end
filename = write_hdf(a, filename, 'nc');
