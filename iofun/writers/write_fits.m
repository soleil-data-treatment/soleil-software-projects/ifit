function filename = write_fits(a, filename, varargin)
% WRITE_FITS Save dataset into a Flexible Image Transport System image (FITS).
%
%   FILENAME = WRITE_FITS(DATA, FILENAME)
%     DATA must be a iData object or a single numeric array.
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_FITS(DATA, FILENAME,'PARAM','VALUE') 
%     writes DATA to the FITS file according to the specified parameter 
%     value pairs. The parameter names are as follows:
%        'WriteMode'    One of these strings: 'overwrite' (the default)
%                       or 'append'. 
%        'Compression'  One of these strings: 'none' (the default), 'gzip', 
%                       'gzip2', 'rice', 'hcompress', or 'plio'.
%
%   Open generated file with ImageJ, GIMP, GCX, GinGa.
%
% Example: y=write_fits(rand(100,100)); ~isempty(dir(y))
% See also: write_avi, write_eps, write_pdf, write_image

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      s1.description = 'Flexible Image Transport System (FITS)';
      s1.ext         = {'fits' 'fit' 'fts'};
      s1.write       = mfilename;
      filename       = s1;
      return
  end
  
  % handle input arguments
  if nargin < 2, filename = '';    end
  if isempty(filename), filename = [ tempname '.fits' ]; end
  
  if isnumeric(a), b=a; else
    b = double(a);
  end
  fitswrite(b', filename, varargin{:});

