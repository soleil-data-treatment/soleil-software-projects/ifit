function filename = write_ply(a, filename)
% WRITE_PLY Save dataset into ply geomview.
%
%   FILENAME = WRITE_PLY(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array
%     or any structure with fields any of:
%       x,y,z
%       faces,vertices
%     When ommitted, FILENAME is set in TMDIR.
%
%   This file text-based format is rather slow to write, with large file.
%   Open generated file with Meshlab, F3d, view3dscene, Geomview.
%
% Example: y=write_ply(rand(100,100)); ~isempty(dir(y))
% See also: write_vtk, write_dae, write_kml, write_off, write_stl, write_x3d, write_vrml

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    ply.description  = 'PLY 3D ascii';
    ply.write        = mfilename;
    ply.ext          = {'ply'};
    ply.istext       = true;
    filename         = ply;
    return
  end

  if nargin < 2, filename = ''; end
  if isempty(filename), filename = tempname; end

  filename = write_stl(a, filename, 'ply');
