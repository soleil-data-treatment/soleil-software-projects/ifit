function filename = write_cdf(a, filename)
% WRITE_CDF Save dataset into Common Data Format (CDF).
%
%   FILENAME = WRITE_CDF(DATA, FILENAME)
%     DATA must be an iData object or a struct. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   Open with GDAL. CDF is not NetCDF.
%   This format is rather fast to read and write, and retains most data.
%
% Example: y=write_cdf(rand(100,100)); ~isempty(dir(y))
% See also: write_hdf, write_nc

if nargin == 0
  cdf.description = 'Common Data Format CDF (.cdf)';
  cdf.write       = mfilename;
  cdf.ext         = {'cdf'};
  cdf.isbinary    = true;
  filename        = cdf;
  return
end

if nargin < 2, filename = ''; end
filename = write_hdf(a, filename, 'cdf');
