function filename = write_m(a, filename, varargin)
% WRITE_M Save dataset into a flat text file with comments.
%
%   FILENAME = WRITE_M(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_M(DATA, FILENAME, OPTIONS)
%   OPTIONS can contain 
%     'no comments' remove comments from the output file,
%     'eval' create a compact evaluable string of the initial data
%     'short' strip-down large elements to reduce the file size. 
%       The 'short' output does not contain the full initial object.
%
%   Open the generated file with Matlab or Octave.
%
% Example: y=write_m(rand(100,100)); ~isempty(dir(y))
% See also: write_mat, write_fig, class2str

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    dat.description  = 'Matlab script/function (M-file)';
    dat.write        = mfilename;
    dat.ext          = {'m'};
    dat.istext       = true;
    filename         = dat;
    return
  end

  if nargin < 2, filename = '';  end
  if isempty(filename), filename = [ tempname '.m' ]; end
  
  name = inputname(1);
  if isempty(name)
    if isa(a, 'iData')
      name = a.Tag;
    else
      name = 'this';
    end
  end

  NL = sprintf('\n');
  if ~isdeployed
    [p,f] = fileparts(filename);
    str = [ 'function ' name '=' f NL ];
  else
    str = '';
  end
  str = [ str ...
          '% Original data: ' NL ...
          '%   class:    ' class(a) NL ...
          '%   variable: ' name NL ...
          '%   size:     ' size(a) NL ];
  if isa(a, 'iData')
    str = [ str ...
          '%   tag:      ' a.Tag NL ...
          '%   source:   ' a.Source NL ];
  end
  str = [ str ...
          '%   Matlab:   ' version NL ... 
          '%   file:     ' filename NL ...
          '%   date:     ' datestr(now) NL ...
          '%   creator:   ifit/' mfilename NL ...
          '%' NL ...
          '% To use/import data, type ''' name ''' at the Matlab prompt.' NL ...
          '%' NL ...
          class2str(name, a, varargin{:}) ];
  [fid, message]=fopen(filename,'w+');
  if fid == -1
    warning([ mfilename ': Error opening file ' filename ' to save object ' a.Tag 'in format ' format ]);
    disp(message)
    return
  end
  fprintf(fid, '%s', str);
  fclose(fid);
  if isdeployed
    warning([ 'WARNING: The standalone/deployed version of iFit does not allow to read back' NL ...
           '  function definitions. This M-file has been converted to a script that you can' NL ...
           '  import as "this" by typing: run ' filename ]);
  end
