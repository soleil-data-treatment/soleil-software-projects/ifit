function [filename, writers] = ifitwrite(data, filename, writer, varargin)
% IFITWRITE write data
%
%   FILENAME = IFITWRITE(DATA, FILENAME) exports DATA.
%     The file format is set from the file extension.
%     When FILENAME is empty, a temporary file is created, HDF5 format.
%     When FILENAME is 'gui', a file selector is displayed.
%     When FILENAME is a directory, random filenames are created therein.
%     To get a full list of supported WRITER, use: ifitpref formats iswriter
%
%   [FILENAME,WRITERS] = IFITWRITE(DATA, FILE, WRITER, ...) specifies the file format.
%     The WRITER can be a token, function name, function handle, file extension
%     or structure:
%             WRITER.write     = 'function name' or function_handle
%             WRITER.extension = {'ext1',...}
%             WRITER.options   = string of options to catenate after file name
%                          OR cell of options to use as additional arguments
%                             to the method
%     Additional arguments are passed to the export routine. The returned WRITERS
%     contains the actual list of used writers.
%     The recommended data formats for large datasets are NRRD (the fastest, with 
%     metadata), and HDF5 (saves full data structure). XML, YAML and JSON are 
%     fast text-based formats (and retain whole structure).
%
%   WRITERS = IFITWRITE(DATA, FILE, 'writers') returns the WRITERS that can write the file.
%
%   The configuration of the data formats is controled via 'ifitpref'.
%
% Example: y=ifitwrite(rand(10), fullfile(tempdir, 'data.hdf5')); ~isempty(dir(y))
% Example: y=ifitwrite(rand(10), '', 'nrrd'); ~isempty(dir(y))
% Version: $Date$ $Version$ $Author$
% See also save, ifitread, ifitpref, ifitopen

  writers = {};
  if nargin < 2,    filename=''; end
  if nargin < 3,      writer=''; end
  if isempty(writer), writer='auto'; end
  if ischar(data) && strcmp(data,'identify') return; end
  
  % loops for multiple formats/filenames ---------------------------------------
  if iscell(writer)
    filenames = cell(size(writer));
    writers   = filenames;
    parfor index=1:numel(writer)
      [filenames{index}, writers{index}] = ifitwrite(data, filename, writer{index}, varargin{:})
    end
    filename = filenames;
    return
  end
  if iscellstr(filename)
    filenames = cell(size(filename));
    writers   = filenames;
    parfor index=1:numel(filename)
      [filenames{index}, writers{index}] = ifitwrite(data, filename{index}, writer, varargin{:})
    end
    filename = filenames;
    return
  end
  
  % single data, filename and writer -------------------------------------------
  if ischar(writer) && strncmp(writer,'.',1), writer(1)=[]; end
  if isdir(filename), [p,f,e] = fileparts(tempname); filename = fullfile(filename, f); end
  [p,f,e] = fileparts(filename);
  if isempty(f), filename=tempname; end
  if isempty(e) && ischar(writer) && strcmp(writer,'auto')
    filename = [ filename '.' 'hdf5' ];
  end
  
  % check for format options
  removeme = []; flag_writers_only = false;
  if strcmp(writer,'writers')
    writer   = 'auto';
    flag_writers_only = true;
  end
  for index=1:numel(varargin)
    if ischar(varargin{index})
      switch varargin{index}
      case {'savers','saver','writers','writer'}
        flag_writers_only = true;
      end
    end
  end
  varargin(removeme) = [];

  filename = get_filenames(filename);
  
  % export into each file/format
  [filename, writers] = ifitwrite_single(data, filename, writer, flag_writers_only, varargin{:});
  if isempty(writers)
    disp([ mfilename ': WARNING: Could not find any writer to create file ' filename ])
  end
  
  if flag_writers_only
    filename = writers;
  end

end % ifitwrite

% ------------------------------------------------------------------------------
function [filename, writer] = ifitwrite_single(data, filename, writer, flag_writers_only, varargin)

  filename = char(filename);
  % check format. May return more than one.
  [this_writers, ~, conf] = get_formats(writer, filename, 'writers');
  
  if flag_writers_only
    writer = this_writers;
    return
  end

  % scan all possible writers until one works
  [p,f,e] = fileparts(filename); % will add format extension if not in filename
  for index=1:numel(this_writers)
    if   iscell(this_writers), 
         this_writer=this_writers{index};
    else this_writer=this_writers(index);
    end
  
    % prepare arguments for format method
    if isempty(e) && ~isempty(this_writer.ext), filename = [ filename '.' this_writer.ext{1} ]; end
    varg = get_format_args(this_writer, 'write', data, filename, varargin{:});
    
    % EXPORT HERE ==============================================================
    try
      if isfield(conf, 'verbosity') && conf.verbosity > 1
        disp([ mfilename ': trying ' this_writer.write '(''' filename ''') ' this_writer.description ]);
      end
      t0 = clock;
      this_filename = feval(this_writer.write, varg{:});
      % succeeds
      this_writer.duration   = etime(clock, t0);
      this_writer.importdate = datestr(now);
      this_writer.file       = dir(filename);
      this_writer.filename   = filename;
    catch ME
      if isfield(conf, 'verbosity') && conf.verbosity > 1
        disp(getReport(ME));
        disp([ mfilename ': FAILED ' this_writer.write '(''' filename ''') ' this_writer.description ]);
      end
      this_filename = []; % fails
      this_writer   = [];
    end
    
    if ~isempty(this_filename)
      filename= this_filename;
      writer  = this_writer;
      return % succeeds
    end
    
  end % index writers

end % ifitwrite_single

% ------------------------------------------------------------------------------
