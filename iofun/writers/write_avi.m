function filename = write_avi(a, filename, options)
% WRITE_AVI Save dataset into Audio Video Interleave (AVI) movie.
%
%   FILENAME = WRITE_AVI(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_AVI(DATA, FILENAME, OPTIONS)
%     OPTIONS specifies iData/plot options (a string), e.g.:
%       tight, hide_axes, interp, view2, view3, transparent, light, clabel,
%       colorbar, whole. Default is 'view2 axis tight'.
%
%   Open generated file with VLC or MPlayer.
%
% Example: y=write_avi(rand(100,100)); ~isempty(dir(y))
% See also: write_nii, write_nrrd

if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    id.description ='Audio Video Interleave (AVI) movie';
    id.write       = mfilename;
    id.ext         = {'avi'};
    id.isbinary    = true;
    filename       = id;
    return
end

if nargin < 2, filename = [ tempname '.avi' ]; end
if nargin < 3, options  = 'view2 axis tight';  end

if isnumeric(a), a = iData(a); end
if ~isa(a, 'iData') 
  error([ mfilename ': ERROR: can only write from numeric array and iData object, not ' class(a) ]); 
end

% first reduce the object to 2D/3D
if ndims(a) <= 1
  filename = '';
  return; 
elseif ndims(a) > 3
  % reduce dimensions: look for the axes which have the largest extension
  extent = 1:ndims(a);
  for index=1:ndims(a)
    this_axis     = axis(a, index);
    extent(index) = max(this_axis(:)) - min(this_axis(:));
  end
  % now get the largest extents
  [extent,extent_index] = sort(extent);
  extent_index = extent_index(end:-1:1);  % descending order
  extent       = extent(end:-1:1);
  % we use extend_index(1:3)
  % we keep axes [1:2 ndims(a)] (this allows e.g. to plot S(q,w))
  if isvector(a) > 1 % event data set
    to_remove = [];
    for index=2:ndims(a)
      if isempty([ strfind(options,'whole') strfind(options,'full') ])
        if index <= 4 && extent(index) > extent(1)*1e-2, continue; end
      elseif index <= 3, continue; end
      to_remove = [ to_remove extent_index(index) ];
    end
    a = rmaxis(a,to_remove);
  else
    sz = size(a); sz(extent_index(4:end)) = 1;
    warning([ mfilename ': Reducing ' num2str(ndims(a)) '-th dimensional data ' a.Tag ' "' a.Name '" to 3D with a=resize(a, ' mat2str(sz) ')' ]);
    a = squeeze(resize(a, sz));
  end
end

if exist('avifile') 
  fig=figure('Visible','off');
  plot(a, options);
  warning('off','MATLAB:audiovideo:avifile:FunctionToBeRemoved');
  aviobj = avifile(filename);
  for ang=0:5:360
    view([ ang 30])
    aviobj = addframe(aviobj,getframe(fig));
  end
  close(fig)
  aviobj = close(aviobj);
elseif exist('VideoWriter')
  fig=figure('Visible','off');
  plot(a, options);
  aviobj = VideoWriter(filename);
  open(aviobj);
  for ang=0:5:360
    view([ ang 30])
    writeVideo(aviobj,getframe(fig))
  end
  close(aviobj)
  close(fig)
else filename = '';
end

