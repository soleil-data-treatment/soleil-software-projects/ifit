Processed data writers
======================

This directory contains functions that write data into various file formats.
All writers work best with iData object. Other data types will first be 
converted to iData objects.

The writer specifications ('identify') refer to the corresponding reader when it
exists (which itself then contains a 'write' member).

Writers definition
-------------------------

When given a unique 'identify' argument, the writer should return a structure or 
a cell of structures:

 field      | content
------------|----------------------------------------------------------------------
 description| description of the data format
       write|function name/handle to write the file; file=method(data,file,...)
         ext| a single or a cellstr of extensions associated with the format
      istext| optional priority (int), e.g. for text files
    isbinary| optional indicate that file is 'binary' 
     options| optional list of arguments to pass to the method. If given as a string, they are catenated with file name. If given as a cell, sent to the method as additional arguments

This specification is inspired from the `imformats` documentation.

