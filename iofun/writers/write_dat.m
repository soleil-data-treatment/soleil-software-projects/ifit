function filename = write_dat(a, filename)
% WRITE_DAT Save dataset into a flat text file with comments.
%
%   FILENAME = WRITE_DAT(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array. 
%     When ommitted, FILENAME is set in TMDIR.
%     Comment lines start with the '#' character.
%
%   Open generated file with any text file viewer.
%
% Example: y=write_dat(rand(100,100)); ~isempty(dir(y))
% See also: write_csv, write_xls, write_json, write_yaml, write_xml

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    dat.description  = 'Flat text file with comments';
    dat.write        = mfilename;
    dat.ext          = {'dat'};
    dat.istext       = true;
    filename         = dat;
    return
  end

  if nargin < 2, filename = '';  end
  if isempty(filename), filename = [ tempname '.dat' ]; end

  NL = sprintf('\n');
  % write header
  str = [ '# Format:     data with text headers' NL ...
          '# URL:        https://gitlab.com/soleil-data-treatment/soleil-software-projects/ifit' NL ...
          '# Data_class: '      class(a) NL ];
  
  % write file
  [fid, message]=fopen(filename,'w+');
  if fid == -1
    error([ mfilename ': Error opening file ' filename ' to save data.' ]);
  end
  fprintf(fid, '%s', [ str class2str(inputname(1), a, 'flat') ]);
  fclose(fid);

