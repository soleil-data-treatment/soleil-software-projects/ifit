function filename = write_fig(a, filename, varargin)
% WRITE_FIG Save dataset into Matlab figure (FIG).
%
%   FILENAME = WRITE_FIG(DATA, FILENAME)
%     DATA must be a iData object or a single numeric array.
%     The file format is infered from the FILENAME extension.
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_FIG(DATA, FILENAME, 'OPTIONS')
%     OPTIONS indicates plot options a string with words:
%       tight, hide_axes, interp, view2, view3, transparent, light, clabel,
%       colorbar, whole, hide_errorbars (1D). Default is 'view2 axis tight'.
%
%   Open generated file with Matlab or Octave.
%
% Example: y=write_fig(rand(100,100)); ~isempty(dir(y))
% See also: write_m, write_mat

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      s1.description = 'Matlab figure (FIG)';
      s1.write       = mfilename;
      s1.ext         = 'FIG';
      filename       = s1;
      return
  end
  
  % handle input arguments
  if nargin < 2, filename = '';    end
  if nargin < 3 && isa(a,'iData'), varargin  = {'view2 axis tight'}; end
  if isempty(filename), filename = [ tempname '.fig' ]; end

  filename = write_pdf(a, filename, 'fig', varargin{:});
