function filename = write_npy(a, filename)
% WRITE_NPY Save dataset into a Numpy NPY array.
%
%   FILENAME = WRITE_NPY(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   Open with Silx, Python.
%
% Example: y=write_npy(rand(100,100)); ~isempty(dir(y))
% See also: write_nrrd, write_mat

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    dat.description  = 'Numpy NPY array';
    dat.write        = mfilename;
    dat.ext          = {'npy'};
    dat.isbinary     = true;
    filename         = dat;
    return
  end

  if nargin < 2, filename = '';  end
  if isempty(filename), filename = [ tempname '.npy' ]; end

  if ~isnumeric(a), a = double(a); end
  
  writeNPY(a, filename);
  if isempty(dir(filename)), filename = ''; end

end

% ------------------------------------------------------------------------------

function writeNPY(var, filename)
%
% Only writes little endian, fortran (column-major) ordering; only writes
% with NPY version number 1.0.
%
% Always outputs a shape according to matlab's convention, e.g. (10, 1)
% rather than (10,).


shape = size(var);
dataType = class(var);

header = constructNPYheader(dataType, shape);

fid = fopen(filename, 'w');
fwrite(fid, header, 'uint8');
fwrite(fid, var, dataType);
fclose(fid);


end

% ------------------------------------------------------------------------------



function header = constructNPYheader(dataType, shape, varargin)

	if ~isempty(varargin)
		fortranOrder = varargin{1}; % must be true/false
		littleEndian = varargin{2}; % must be true/false
	else
		fortranOrder = true;
		littleEndian = true;
	end

    dtypesMatlab = {'uint8','uint16','uint32','uint64','int8','int16','int32','int64','single','double', 'logical'};
    dtypesNPY = {'u1', 'u2', 'u4', 'u8', 'i1', 'i2', 'i4', 'i8', 'f4', 'f8', 'b1'};

    magicString = uint8([147 78 85 77 80 89]); %x93NUMPY
    
    majorVersion = uint8(1);
    minorVersion = uint8(0);

    % build the dict specifying data type, array order, endianness, and
    % shape
    dictString = '{''descr'': ''';
    
    if littleEndian
        dictString = [dictString '<'];
    else
        dictString = [dictString '>'];
    end
    
    dictString = [dictString dtypesNPY{strcmp(dtypesMatlab,dataType)} ''', '];
    
    dictString = [dictString '''fortran_order'': '];
    
    if fortranOrder
        dictString = [dictString 'True, '];
    else
        dictString = [dictString 'False, '];
    end
    
    dictString = [dictString '''shape'': ('];
    
%     if length(shape)==1 && shape==1
%         
%     else
%         for s = 1:length(shape)
%             if s==length(shape) && shape(s)==1
%                 
%             else
%                 dictString = [dictString num2str(shape(s))];
%                 if length(shape)>1 && s+1==length(shape) && shape(s+1)==1
%                     dictString = [dictString ','];
%                 elseif length(shape)>1 && s<length(shape)
%                     dictString = [dictString ', '];
%                 end            
%             end
%         end
%         if length(shape)==1
%             dictString = [dictString ','];
%         end
%     end

    for s = 1:length(shape)
        dictString = [dictString num2str(shape(s))];
        if s<length(shape)
            dictString = [dictString ', '];
        end
    end
    
    dictString = [dictString '), '];
    
    dictString = [dictString '}'];
    
    totalHeaderLength = length(dictString)+10; % 10 is length of magicString, version, and headerLength
    
    headerLengthPadded = ceil(double(totalHeaderLength+1)/16)*16; % the whole thing should be a multiple of 16
                                                                  % I add 1 to the length in order to allow for the newline character

	% format specification is that headerlen is little endian. I believe it comes out so using this command...
    headerLength = typecast(int16(headerLengthPadded-10), 'uint8');
	
    zeroPad = zeros(1,headerLengthPadded-totalHeaderLength, 'uint8')+uint8(32); % +32 so they are spaces
    zeroPad(end) = uint8(10); % newline character
    
    header = uint8([magicString majorVersion minorVersion headerLength dictString zeroPad]);

end
