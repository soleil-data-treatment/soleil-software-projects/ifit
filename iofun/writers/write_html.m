function filename = write_html(a, filename)
% WRITE_HTML Save dataset into Extensible Hypertext Markup Language (XHTML).
%
%   FILENAME = WRITE_HTML(DATA, FILENAME)
%     DATA must be an iData object or a struct. 
%     When ommitted, FILENAME is set in TMDIR.
%
%   This file text-based format is rather slow to write, with large file.
%   Open the HTML with any browser. A 'x3dom' directory is created.
%
% Example: y=write_html(rand(100,100)); ~isempty(dir(y))
% See also: write_svg

if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      s1.description = 'Extensible Hypertext Markup Language (XHTML/HTML)';
      s1.ext         = {'xhtml'};
      s1.write       = mfilename;
      s1.istext      = true;
      filename       = s1;
      return
  end

if nargin < 2, filename = ''; end
filename = write_x3d(a, filename, 'xhtml');
