function filename = write_image(a, filename, format, options)
% WRITE_IMAGE Save dataset into image formats (PNG JPG TIF ...).
%
%   FILENAME = WRITE_IMAGE(DATA, FILENAME)
%     DATA must be a iData object or a single numeric array.
%     The file format is infered from the FILENAME extension.
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_IMAGE(DATA, FILENAME, FORMAT)
%     FORMAT specifies the output data format. 
%     Supported formats are:
%      'gif','bmp','pbm','pcx','pgm','pnm','ppm','ras','xwd','hdf'
%      'tiff','png'
%
%   FILENAME = WRITE_IMAGE(DATA, FILENAME, 'FORMAT', 'OPTIONS')
%     OPTIONS indicates plot options a string with words:
%       tight, hide_axes, interp, view2, view3, transparent, light, clabel,
%       colorbar, whole, hide_errorbars (1D). Default is 'view2 axis tight'.
%
%   Open generated file with with e.g. ImageJ or GIMP.
%
% Example: y=write_image(rand(100,100)); ~isempty(dir(y))
% See also: write_avi, write_eps, write_pdf, write_fits, write_svg

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      % we get the imformats extensions
      imf    = imformats;
      imfext = {};
      for index=1:numel(imf)
        imfext = [ imfext imf(index).ext{:} ];
      end
      
      s1.description = 'Image/Picture (PNG JPG TIF ...)';
      s1.ext         = imfext;
      s1.write       = mfilename;
      filename       = s1;
      return
  end

  % handle input arguments
  if nargin < 2, filename = '';    end
  if nargin < 3, format   = 'png'; end
  if nargin < 4, options  = 'view2 axis tight'; end

  if isempty(filename), filename = tempname; end
  if strncmp(format,'.',1), format(1)=[]; end
  format = lower(format);
  [p,f,e] = fileparts(filename);
  if isempty(e)
    if ~isempty(format) filename = fullfile(p, [ f '.' format ]); end
  else format = lower(e(2:end));
  end
  
  % get name/source/date
  if isa(a, 'iData')
    name = char(a);
    src  = char(a.Source);
    dat  = datestr(a.Date);
  else
    dat  = datestr(now);
    name = class(a); src = pwd;
  end
  
  % get image matrix when possible
  b = [];
  if isa(a, 'iData') && ndims(a) ~= 2
    f = getframe(a,[], options);
    if isfield(f, 'cdata')
      b = f.cdata;
      if  strcmp(format(1:3),'jpe')
          b=sum(b,3);
      end
    end
  elseif isfield(a,'cdata')
    b = a.cdata;
  end
  if isempty(b) % 'data' option or getframe failed
    b = real(double(a));
    b = round((b-min(b(:)))/(max(b(:))-min(b(:)))*256);
    b = flipud(b);
  end
  if strcmp(format,'hdf4'), format='hdf'; end

  if isempty(b)
    error([ mfilename ': ERROR: could not save ' class(a) ' to ' format ]);
  end
  
  % write using imwrite
  switch format
  case 'png'
    try
      imwrite(b, jet(256), filename, format, 'Comment',name,'Mode','lossless', ...
        'Source',src, 'CreationTime', dat, 'Software', ['ifit/' mfilename ]);
    catch
      imwrite(b, jet(256), filename, format, 'Comment',name,'Mode','lossless', ...
        'Source',src);
    end
  case 'gif'
    imwrite(b, filename, format, 'Comment',name);
  case 'tiff'
    imwrite(b, jet(256), filename, format, 'Description',name);
  otherwise
    warning('off','MATLAB:imagesci:writehdf:colormapIgnoredForRgb');
    try
      imwrite(b, jet(256), filename, format);
    catch
      imwrite(b, filename, format);
    end
  end


