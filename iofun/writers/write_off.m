function filename = write_off(a, filename)
% WRITE_OFF Save dataset into OFF geomview.
%
%   FILENAME = WRITE_OFF(DATA, FILENAME)
%     DATA must be an iData object or a single numeric array
%     or any structure with fields any of:
%       x,y,z
%       faces,vertices
%     When ommitted, FILENAME is set in TMDIR.
%
%   This file text-based format is rather slow to write, with large file.
%   Open generated file with Meshlab, Geomview, F3d.
%
% Example: y=write_off(rand(100,100)); ~isempty(dir(y))
% See also: write_vtk, write_dae, write_kml, write_ply, write_stl, write_x3d, write_vrml

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
    off.description  = 'OFF 3D stereo-lithography CAD ascii';
    off.write        = mfilename;
    off.ext          = {'off'};
    off.istext       = true;
    filename         = off;
    return
  end

  if nargin < 2, filename = ''; end
  if isempty(filename), filename = tempname; end

  filename = write_stl(a, filename, 'off');
