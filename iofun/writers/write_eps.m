function filename = write_eps(a, filename, varargin)
% WRITE_EPS Save dataset into Encapsulated PostScript (EPS).
%
%   FILENAME = WRITE_EPS(DATA, FILENAME)
%     DATA must be a iData object or a single numeric array.
%     The file format is infered from the FILENAME extension.
%     When ommitted, FILENAME is set in TMDIR.
%
%   FILENAME = WRITE_EPS(DATA, FILENAME, 'OPTIONS')
%     OPTIONS indicates plot options a string with words:
%       tight, hide_axes, interp, view2, view3, transparent, light, clabel,
%       colorbar, whole, hide_errorbars (1D). Default is 'view2 axis tight'.
%
%   Open generated file with e.g. Word, LibreOffice, Evince.
%
% Example: y=write_eps(rand(100,100)); ~isempty(dir(y))
% See also: write_pdf, write_image

  if nargin == 0 || any(strcmp(a, {'identify','query','defaults'}))
      s1.description = 'Encapsulated PostScript (EPS)';
      s1.write       = mfilename;
      s1.ext         = 'EPS';
      filename       = s1;
      return
  end
  
  % handle input arguments
  if nargin < 2, filename = '';    end
  if nargin < 3 && isa(a,'iData'), varargin  = {'view2 axis tight'}; end
  if isempty(filename), filename = [ tempname '.fig' ]; end

  filename = write_pdf(a, filename, 'epsc2', varargin{:});
