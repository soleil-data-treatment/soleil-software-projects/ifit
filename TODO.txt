iData/squeeze -> Axes/Labels !!
subclasses .Label -> .Title or title()
handle Error = sqrt(this.Signal) -> matlab: ?
openhdf -> very slow !
cwt wavelets

iData: config option in loaders: use managed above file size threshold, default=Inf
iData: add interp, intersect, union, then meshgrid
iData: add save

m23=unique([ m2 ; m3 ]);
for index=1:numel(m23)
  this = m23{index};
  i2   = any(strcmp(m23{index}, m2));
  i3   = any(strcmp(m23{index}, m3));
  if (~i2 && i3) || (i2 && ~i3)
    fprintf(1, '%15s %i %i\n', this, i2, i3 );
  end
end
--------------------------------------------------------------------------------
         method v2 v3
       --------|-|------
        avifile 1 0
        camproj 1 0
       cart2sph 1 0
            cat 1 0
          caxis 1 0
       cdfwrite 1 0
    cell2struct 1 0
        cellstr 1 0
      circshift 1 0
           clim 1 0
          colon 1 0
       colormap 1 0
 commandhistory 1 0
       corrcoef 1 0
       csvwrite 1 0
       cumtrapz 1 0
            cwt 1 0
         dialog 1 0
           diff 1 0
            doc 1 0
            dog 1 0
           edit 1 0
          error 1 0
          event 1 0
            fft 1 0
           fill 1 0
      findpeaks 1 0
           fits 1 0
      fitswrite 1 0
            fix 1 0
       getframe 1 0
        h5write 1 0
      hdf5write 1 0
           hist 1 0
           ifft 1 0
          image 1 0
          imroi 1 0
        imwrite 1 0
        ind2sub 1 0
       inputdlg 1 0
       jacobian 1 0
         kmeans 1 0
       logspace 1 0
           mesh 1 0
        ncwrite 1 0
       nonzeros 1 0
           pack 1 0
            pca 1 0
           prod 1 0
        publish 1 0
         resize 1 0
         rotate 1 0
           save 1 0
         saveas 1 0
         setand 1 0
          setor 1 0
         setxor 1 0
       shiftdim 1 0
         smooth 1 0
            sqr 1 0
    struct2cell 1 0
      subsindex 1 0
          trapz 1 0
        uitable 1 0
         unique 1 0
        version 1 0
           xlim 1 0
       xlswrite 1 0
       xmlwrite 1 0
           ylim 1 0
           zlim 1 0
           
