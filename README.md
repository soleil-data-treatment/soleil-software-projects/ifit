            
Welcome to the iFit/iData package
=================================

<p align="center">

                          http://ifit.mccode.org

                          E. Farhi, EXP/GRADES [emmanuel.farhi@synchrotron-soleil.fr]

                          Version @IFIT_VERSION@ - @IFIT_DATE@

<br>
<img src="doc/images/iFit-logo.png">
<br>
<img src="doc/images/logo_soleil.png" style="width: 50%; height: 50%">
</p>

**:warning: WARNING:** this is a revamp of the initial iFit project. Not all functionalities have been re-integrated. This is a work-in-progress project.
====

Purpose
-------

  This library aims at providing basic functionality to achieve some of the 
  general tasks needed for scientific data analysis:
    Load, Plot, Save, Fit, Math operations, define and use Models.

Requirements
------------


**Requirements for Source package**
  Matlab (any version from 7.x, R2007a), possibly a C compiler for the `looktxt`  
  and the `cbf_uncompress` MeX.

Installation
------------

**Installation from Source package**
  Copy the library directories where-ever you want or in MALTAB/toolbox/local:
    /home/joe/Matlab/ifit
  or
    /opt/MATLAB/Rxxxxn/toolbox/local/ifit
  
  Then start Matlab and type in, e.g.:
```matlab
>> addpath(genpath('/home/joe/Matlab/ifit'))
```
  or
```matlab
>> addpath(genpath('/opt/MATLAB/Rxxxxn/toolbox/local/ifit'))
```

Contacts
--------
  You can register to the iFit mailing list at <http://mail.mccode.org/cgi-bin/mailman/listinfo/ifit-users>
  Send messages to the <ifit-users@mail.mccode.org>.
  Help pages are available at <http://ifit.mccode.org>
  
License: EUPL
-------------

  Basically this is open-source. Use it if you find it useful, and enrich it.
  If you do produce new methods, please send them back to me so that they are added in the software and thus benefit to the community.

  In short, you can use, copy, distribute and modify the Code. However, a number of restrictions apply, especially when producing derived work (that is modify and redistribute the code in other products). In particular, the derived work must be licensed under the EUPL or a Compatible License, label all modifications explicitly, distribute the Source Code, and cite the Original work.
  The Source code of iFit is freely available at <http://ifit.mccode.org>

  A number of additions, included in the software, where obtained from the Matlab Central contributions, and are BSD licensed.
  
  Matlab is a registered trademark of The Mathworks Inc.

  Contributions are listed in the Credits page at <http://ifit.mccode.org/Credits.html>

Disclaimer
----------
  This is not a professional tool, and there is no Dev team to actively take care
  of it. Expect occasional failures and bugs. However, I try my best to make the 
  software efficient and reliable. 

--------------------------------------------------------------------------------
$Revision$ - $Date$
